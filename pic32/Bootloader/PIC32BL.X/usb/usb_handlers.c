/*******************************************************************************
Copyright 2016 Microchip Technology Inc. (www.microchip.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

To request to license the code under the MLA license (www.microchip.com/mla_license), 
please contact mla_licensing@microchip.com
 *******************************************************************************/

/** INCLUDES *******************************************************/

#include "usb.h"
#include <GenericTypeDefs.h>
#include <string.h>
#include "usb_handlers.h"
#include <stdio.h>
#include <stdint.h>
#include "usb_device_hid.h"
#include "../Framework.h"
#include "../BootLoader.h"
/** DECLARATIONS ***************************************************/

/** TYPE DEFINITIONS ************************************************/

/** VARIABLES ******************************************************/
/* Some processors have a limited range of RAM addresses where the USB module
 * is able to access.  The following section is for those devices.  This section
 * assigns the buffers that need to be used by the USB module into those
 * specific areas.
 */
/** VARIABLES ******************************************************/
/* Some processors have a limited range of RAM addresses where the USB module
 * is able to access.  The following section is for those devices.  This section
 * assigns the buffers that need to be used by the USB module into those
 * specific areas.
 */
#if defined(FIXED_ADDRESS_MEMORY)
#if defined(COMPILER_MPLAB_C18)
#pragma udata HID_CUSTOM_OUT_DATA_BUFFER = HID_CUSTOM_OUT_DATA_BUFFER_ADDRESS
unsigned char ReceivedDataBuffer[64];
#pragma udata HID_CUSTOM_IN_DATA_BUFFER = HID_CUSTOM_IN_DATA_BUFFER_ADDRESS
unsigned char ToSendDataBuffer[64];
#pragma udata

#else defined(__XC8)
unsigned char ReceivedDataBuffer[64] @HID_CUSTOM_OUT_DATA_BUFFER_ADDRESS;
unsigned char ToSendDataBuffer[64] @HID_CUSTOM_IN_DATA_BUFFER_ADDRESS;
#endif
#else
unsigned char ReceivedDataBuffer[64];
unsigned char ToSendDataBuffer[64];
#endif

volatile USB_HANDLE USBOutHandle;
volatile USB_HANDLE USBInHandle;

uint32_t TxLen;
uint8_t *TxPtr;

static UINT8 UsbTxData[FRAMEWORK_BUFF_SIZE];

/*********************************************************************
 * Function: void APP_DeviceVendorBasicDemoInitialize(void);
 *
 * Overview: Initializes the demo code
 *
 * PreCondition: None
 *
 * Input: None
 *
 * Output: None
 *
 ********************************************************************/
void APP_DeviceCustomHIDInitialize(void)
{
    //initialize the variable holding the handle for the last
    // transmission
    USBInHandle = 0;
    USBOutHandle = 0;
    //enable the HID endpoint
    USBEnableEndpoint(CUSTOM_DEVICE_HID_EP, USB_IN_ENABLED | USB_OUT_ENABLED | USB_HANDSHAKE_ENABLED | USB_DISALLOW_SETUP);
    //Re-arm the OUT endpoint for the next packet
    USBOutHandle = (volatile USB_HANDLE)HIDRxPacket(CUSTOM_DEVICE_HID_EP, (uint8_t *)&ReceivedDataBuffer[0], 64);
}


void BootLoaderHIDTasks(void)
{
    /* If the USB device isn't configured yet, we can't really do anything
     * else since we don't have a host to talk to.  So jump back to the
     * top of the while loop. */
    if (USBGetDeviceState() < CONFIGURED_STATE)
    {
        return;
    }

    /* If we are currently suspended, then we need to see if we need to
     * issue a remote wakeup.  In either case, we shouldn't process any
     * keyboard commands since we aren't currently communicating to the host
     * thus just continue back to the start of the while loop. */
    if (USBIsDeviceSuspended() == true)
    {
        return;
    }

    //Check if we have received an OUT data packet from the host
    if (HIDRxHandleBusy(USBOutHandle) == false)
    {
        //We just received a packet of data from the USB host.
        //Check the first uint8_t of the packet to see what command the host
        //application software wants us to fulfill.

        FRAMEWORK_BuildRxFrame(ReceivedDataBuffer, 64);
        FRAMEWORK_FrameWorkTask();

        //Re-arm the OUT endpoint, so we can receive the next OUT data packet
        //that the host may try to send us.
        USBOutHandle = HIDRxPacket(CUSTOM_DEVICE_HID_EP, (uint8_t *)&ReceivedDataBuffer[0], 64);
    }

    // Check if bootloader has something to send out to PC.
    TxLen = FRAMEWORK_GetTransmitFrame(UsbTxData);

    // Initialize the transmit pointer.
    TxPtr = &UsbTxData[0];

    while (TxLen)
    {
        if (!HIDTxHandleBusy(USBInHandle))
        {
            USBInHandle = HIDTxPacket(CUSTOM_DEVICE_HID_EP, (uint8_t *)&TxPtr[0], 64);

            if (TxLen > 64)
            {
                // Send pending bytes in next loop.
                TxLen -= 64;
                // Point to next 64bytes.
                TxPtr += 64;
                // Probably a wait is needed here, otherwise PC app may miss frames.
                //Wait();
            }
            else
            {
                // No more bytes.
                TxLen = 0;
            }
        }
    }
}
