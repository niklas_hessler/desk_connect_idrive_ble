// Copyright (c) 2002-2010,  Microchip Technology Inc.
//
// Microchip licenses this software to you solely for use with Microchip
// products.  The software is owned by Microchip and its licensors, and
// is protected under applicable copyright laws.  All rights reserved.
//
// SOFTWARE IS PROVIDED "AS IS."  MICROCHIP EXPRESSLY DISCLAIMS ANY
// WARRANTY OF ANY KIND, WHETHER EXPRESS OR IMPLIED, INCLUDING BUT
// NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS
// FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.  IN NO EVENT SHALL
// MICROCHIP BE LIABLE FOR ANY INCIDENTAL, SPECIAL, INDIRECT OR
// CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, HARM TO YOUR
// EQUIPMENT, COST OF PROCUREMENT OF SUBSTITUTE GOODS, TECHNOLOGY
// OR SERVICES, ANY CLAIMS BY THIRD PARTIES (INCLUDING BUT NOT LIMITED
// TO ANY DEFENSE THEREOF), ANY CLAIMS FOR INDEMNITY OR CONTRIBUTION,
// OR OTHER SIMILAR COSTS.
//
// To the fullest extent allowed by law, Microchip and its licensors
// liability shall not exceed the amount of fees, if any, that you
// have paid directly to Microchip to use this software.
//
// MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE
// OF THESE TERMS.
#ifndef __BOOTLOADER_H__
#define __BOOTLOADER_H__


#include <xc.h>				// BMXPFMSZ

//#define PROGRAM_FLASH_END_ADRESS (0x9D000000+BMXPFMSZ-1)



//-------User configurable macros begin---------
#define BOOTLOADER_MAJOR_VERSION 2
#define BOOTLOADER_MINOR_VERSION 0
#define MACHINE_REASON_BOOTLOADER_MAGIC		0xFA2B31E7

/* APP_FLASH_BASE_ADDRESS and APP_FLASH_END_ADDRESS reserves program Flash for the application*/ 
/* Rule: 
 		1)The memory regions kseg0_program_mem, kseg0_boot_mem, exception_mem and 
 		kseg1_boot_mem of the application linker script must fall with in APP_FLASH_BASE_ADDRESS 
 		and APP_FLASH_END_ADDRESS
 		 
 		2)The base address and end address must align on  4K address boundary */
 
// ROL: We don't abide the rules set by Microchip in this file.
// Application flash area:
//  kseg0_program_mem     (rx)  : ORIGIN = 0x9D004000, LENGTH = 0x1B000
//  exception_mem               : ORIGIN = 0x9D01F000, LENGTH = 0x1000

#define APP_FLASH_BASE_ADDRESS 0x9D000000
#define APP_FLASH_END_ADDRESS 0x9D03B000

#define USER_APP_RESET_ADDRESS 0x9D00053D

/* Address of  the Flash from where the application starts executing */
/* Rule: Set APP_FLASH_BASE_ADDRESS to _RESET_ADDR value of application linker script*/


// Reset vector is in boothook to enable fork between application and bootloader
//#define RESET_VECTOR	0xBFC00000

//void bootloader_main(void);

inline static void SYSTEM_RegUnlock(void)
{
    SYSKEY = 0x0; //write invalid key to force lock
    SYSKEY = 0xAA996655; //write Key1 to SYSKEY
    SYSKEY = 0x556699AA; //write Key2 to SYSKEY
}

inline static void SYSTEM_RegLock(void)
{
   SYSKEY = 0x00000000; 
}

#endif		// __BOOTLOADER_H__
