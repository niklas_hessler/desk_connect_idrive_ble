#include <GenericTypeDefs.h>
#include "usb.h"
#include <xc.h>
#include "Bootloader.h"
#include <stdlib.h>
#include "Framework.h"
#include "usb_handlers.h"
#include <stdio.h>

#if 0
/* Code identifying the cause of the exception (CP0 Cause register). */
static unsigned int _excep_code;

/* Address of instruction that caused the exception. */
static unsigned int _excep_addr;

/* Pointer to the string describing the cause of the exception. */
static char *_cause_str;

/* Array identifying the cause (indexed by _exception_code). */
static char *cause[] = 
{
    "Interrupt",
    "Undefined",
    "Undefined",
    "Undefined",
    "Load/fetch address error",
    "Store address error",
    "Instruction bus error",
    "Data bus error",
    "Syscall",
    "Breakpoint",
    "Reserved instruction",
    "Coprocessor unusable",
    "Arithmetic overflow",
    "Trap",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved",
    "Reserved"
};

/**
 Section: Exception Handling
*/

/**
  Function:
    void _general_exception_handler ( void )

  Summary:
    Overrides the XC32 _weak_ _generic_exception_handler.
    
  Description:
    This function overrides the XC32 default _weak_ _generic_exception_handler.

  Remarks:
    Refer to the XC32 User's Guide for additional information.
 */

void _general_exception_handler ( void )
{
    /* Mask off Mask of the ExcCode Field from the Cause Register
    Refer to the MIPs Software User's manual */
    _excep_code = (_CP0_GET_CAUSE() & 0x0000007C) >> 2;
    _excep_addr = _CP0_GET_EPC();
    _cause_str  = cause[_excep_code];
    char arr[50];
    sprintf(arr,"%s",_cause_str);

    #ifdef __DEBUG    
      __builtin_software_breakpoint();
      /* If we are in debug mode, cause a software breakpoint in the debugger */
    #endif

    while (1)
    {
      /* TODO:  Insert exception handling code. */
    }
}

#endif
// Configuration bits: selected in the GUI
// FDEVOPT
#pragma config SOSCHP = OFF             // Secondary Oscillator High Power Enable bit (SOSC oprerates in normal power mode.)
#pragma config ALTI2C = ON              // Alternate I2C1 Pins Location Enable bit (Alternate I2C1 pins are used)
#pragma config FUSBIDIO = OFF            // USBID pin control (USBID pin is controlled by the port function)
#pragma config FVBUSIO = OFF             // VBUS Pin Control (VBUS pin is controlled by port function)
#pragma config USERID = 0xFFFF          // User ID bits (User ID bits)

// FICD
#pragma config JTAGEN = OFF             // JTAG Enable bit (JTAG is disabled)
#pragma config ICS = PGx2               // ICE/ICD Communication Channel Selection bits (Communicate on PGEC2/PGED2)

// FPOR
#pragma config BOREN = BOR3             // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware; SBOREN bit disabled)
#pragma config RETVR = OFF              // Retention Voltage Regulator Enable bit (Retention regulator is disabled)
#pragma config LPBOREN = ON             // Downside Voltage Protection Enable bit (Low power BOR is enabled, when main BOR is disabled)

// FWDT
#pragma config SWDTPS = PS1048576       // Sleep Mode Watchdog Timer Postscale Selection bits (1:1048576)
#pragma config FWDTWINSZ = PS25_0       // Watchdog Timer Window Size bits (Watchdog timer window size is 25%)
#pragma config WINDIS = OFF             // Windowed Watchdog Timer Disable bit (Watchdog timer is in non-window mode)
#pragma config RWDTPS = PS1048576       // Run Mode Watchdog Timer Postscale Selection bits (1:1048576)
#pragma config RCLKSEL = LPRC           // Run Mode Watchdog Timer Clock Source Selection bits (Clock source is LPRC (same as for sleep mode))
#pragma config FWDTEN = OFF             // Watchdog Timer Enable bit (WDT is disabled)
// FOSCSEL
#pragma config FNOSC = FRCDIV    // Oscillator Selection bits->Fast RC oscillator (FRC) with divide-by-N
#pragma config PLLSRC = FRC    // System PLL Input Clock Selection bit->FRC oscillator is selected as PLL reference input on device reset
#pragma config SOSCEN = OFF    // Secondary Oscillator Enable bit->Secondary oscillator (SOSC) is disabled
#pragma config IESO = ON    // Two Speed Startup Enable bit->Two speed startup is enabled
#pragma config POSCMOD = OFF    // Primary Oscillator Selection bit->Primary oscillator is disabled
#pragma config OSCIOFNC = OFF    // System Clock on CLKO Pin Enable bit->OSCO pin operates as a normal I/O
#pragma config SOSCSEL = ON    // Secondary Oscillator External Clock Enable bit->External clock is connected to SOSCO pin (RA4 and RB4 are controlled by I/O port registers)
#pragma config FCKSM = CSECMD    // Clock Switching and Fail-Safe Clock Monitor Enable bits->Clock switching is enabled; Fail-safe clock monitor is disabled

// FSEC
#pragma config CP = OFF                 // Code Protection Enable bit (Code protection is disabled)

void Bootloader_pin_manager(void)
{
    /****************************************************************************
     * Setting the Output Latch SFR(s)
     ***************************************************************************/
    LATA = 0x0000;
    LATB = 0x0000;
    LATC = 0x0000;
    /****************************************************************************
     * Setting the GPIO Direction SFR(s)
     ***************************************************************************/
    TRISA = 0xFFEF;
    TRISB = 0xEFFF;
    TRISC = 0xFFFF;
    /****************************************************************************
     * Setting the Analog/Digital Configuration SFR(s)
     ***************************************************************************/
    ANSELA = 0x384C;
    ANSELB = 0xE00F;
    ANSELC = 0x0123;
}

void Bootloader_Oscillator_Init(void)
{
    SYSKEY = 0x0; //write invalid key to force lock
    SYSKEY = 0xAA996655; //write Key1 to SYSKEY
    SYSKEY = 0x556699AA; //write Key2 to SYSKEY
    // ORPOL disabled; SIDL disabled; SRC USB; TUN Center frequency; POL disabled; ON enabled; 
    OSCTUN = 0x9000;
    // PLLODIV 1:4; PLLMULT 12x; PLLICLK FRC; 
    SPLLCON = 0x2050080;
    // SBOREN disabled; VREGS disabled; RETEN disabled; 
    PWRCON = PWRCON | 0x0;
    //Clear NOSC,CLKLOCK and OSWEN bits
    OSCCONCLR = _OSCCON_NOSC_MASK | _OSCCON_CLKLOCK_MASK | _OSCCON_OSWEN_MASK;
    // CF No Clock Failure; FRCDIV FRC/1; SLPEN Device will enter Idle mode when a WAIT instruction is issued; NOSC SPLL; SOSCEN Disable Seconday Oscillator (configure SOSCSEL=ON for GPIO operation); CLKLOCK Clock and PLL selections are not locked and may be modified; OSWEN Switch is Complete; 
    OSCCON = (0x100 | _OSCCON_OSWEN_MASK);
    SYSTEM_RegLock();
    // wait for switch   
    while (OSCCONbits.OSWEN == 1);
    // ON disabled; DIVSWEN disabled; RSLP disabled; ROSEL SYSCLK; OE disabled; SIDL disabled; RODIV 0; 
    REFO1CON = REFO1CON | 0x0;
    while ((!REFO1CONbits.ACTIVE) & REFO1CONbits.ON);
    // ROTRIM 0; 
    REFO1TRIM = REFO1TRIM | 0x0;
}

uint8_t ValidAppPresent(void)
{
    DWORD *AppPtr;
    AppPtr = (DWORD *) ((USER_APP_RESET_ADDRESS-1));
    if (*AppPtr == 0xFFFFFFFF)
    {
        return 0;
    }
    else
    {
        return 1;
    }
}

int main(void)
{
    Bootloader_Oscillator_Init();
    Bootloader_pin_manager();
    FRAMEWORK_Init();
    USBDeviceInit();
    uint32_t i = 0;
    TRISBbits.TRISB9 = 0;
    int nResetReason = _CP0_GET_USERLOCAL();
    bool nForceBootloader = (nResetReason == MACHINE_REASON_BOOTLOADER_MAGIC) ? true : false;

    if (nForceBootloader == false && ValidAppPresent())
    {
        //JUMPT TO APP
        _CP0_SET_HWRENA(1);
        _CP0_SET_USERLOCAL(0);
        void (*fptr)(void);
        fptr = (void (*)(void))USER_APP_RESET_ADDRESS;
        fptr();
    }

    while (!FRAMEWORK_ExitFirmwareUpgradeMode())
    {
        USBDeviceTasks();
        BootLoaderHIDTasks();

        if (i == 100000)
        {
            i = 0;

            LATBbits.LATB9 = ~LATBbits.LATB9;
        }
        else
            i++;

    }//end while
    _CP0_SET_HWRENA(1);
    _CP0_SET_USERLOCAL(0);
    void (*fptr)(void);
    fptr = (void (*)(void))USER_APP_RESET_ADDRESS;
    fptr();
    return 0;
}//end main

