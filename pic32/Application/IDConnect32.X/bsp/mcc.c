/**
@Generated PIC24 / dsPIC33 / PIC32MM MCUs Source File

@Company:
Microchip Technology Inc.

@File Name:
mcc.c

@Summary:
This is the mcc.c file generated using PIC24 / dsPIC33 / PIC32MM MCUs

@Description:
This header file provides implementations for driver APIs for all modules selected in the GUI.
Generation Information :
Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - pic24-dspic-pic32mm : 1.53.0.1
Device            :  PIC32MM0256GPM028
The generated drivers are tested against the following:
Compiler          :  XC32 v1.44
MPLAB             :  MPLAB X v4.05
 */

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
 */


// Configuration bits: selected in the GUI
// FDEVOPT
#pragma config SOSCHP = OFF             // Secondary Oscillator High Power Enable bit (SOSC oprerates in normal power mode.)
#pragma config ALTI2C = ON              // Alternate I2C1 Pins Location Enable bit (Alternate I2C1 pins are used)
#pragma config FUSBIDIO = OFF            // USBID pin control (USBID pin is controlled by the port function)
#pragma config FVBUSIO = OFF             // VBUS Pin Control (VBUS pin is controlled by port function)
#pragma config USERID = 0xFFFF          // User ID bits (User ID bits)

// FICD
#pragma config JTAGEN = OFF             // JTAG Enable bit (JTAG is disabled)
#pragma config ICS = PGx2               // ICE/ICD Communication Channel Selection bits (Communicate on PGEC2/PGED2)

// FPOR
#pragma config BOREN = BOR3             // Brown-out Reset Enable bits (Brown-out Reset enabled in hardware; SBOREN bit disabled)
#pragma config RETVR = OFF              // Retention Voltage Regulator Enable bit (Retention regulator is disabled)
#pragma config LPBOREN = ON             // Downside Voltage Protection Enable bit (Low power BOR is enabled, when main BOR is disabled)

// FWDT /*Watchdog Time-out period 524.0 ms*/
#pragma config SWDTPS = PS1048576    //Sleep Mode Watchdog Timer Postscale Selection bits->1:1048576
#pragma config FWDTWINSZ = PS25_0    //Watchdog Timer Window Size bits->Watchdog timer window size is 25%
#pragma config WINDIS = OFF    //Windowed Watchdog Timer Disable bit->Watchdog timer is in non-window mode
#pragma config RWDTPS = PS131072    //Run Mode Watchdog Timer Postscale Selection bits->1:131072
#pragma config RCLKSEL = FRC    //Run Mode Watchdog Timer Clock Source Selection bits->Clock source is FRC oscillator
#pragma config FWDTEN = ON    //Watchdog Timer Enable bit->WDT is enabled


// FOSCSEL
#pragma config FNOSC = FRCDIV    // Oscillator Selection bits->Fast RC oscillator (FRC) with divide-by-N
#pragma config PLLSRC = FRC    // System PLL Input Clock Selection bit->FRC oscillator is selected as PLL reference input on device reset
#pragma config SOSCEN = OFF    // Secondary Oscillator Enable bit->Secondary oscillator (SOSC) is disabled
#pragma config IESO = ON    // Two Speed Startup Enable bit->Two speed startup is enabled
#pragma config POSCMOD = OFF    // Primary Oscillator Selection bit->Primary oscillator is disabled
#pragma config OSCIOFNC = OFF    // System Clock on CLKO Pin Enable bit->OSCO pin operates as a normal I/O
#pragma config SOSCSEL = ON    // Secondary Oscillator External Clock Enable bit->External clock is connected to SOSCO pin (RA4 and RB4 are controlled by I/O port registers)
#pragma config FCKSM = CSECMD    // Clock Switching and Fail-Safe Clock Monitor Enable bits->Clock switching is enabled; Fail-safe clock monitor is disabled

// FSEC
#pragma config CP = OFF                 // Code Protection Enable bit (Code protection is disabled)

#include <xc.h> 
#include "mcc.h"
#include "usb.h"
#include "softset.h"
#include <sys/attribs.h>

void ledpwm(void)
{
    CCP1CON1 = 0;
    CCP1CON2 = 0;
    CCP1CON3 = 0;
    CCP1CON1bits.MOD = 0b0100; //dual edge compare mode
    CCP1CON2bits.OCBEN = 0; //enable OCM1B/RB9 (green)
    CCP1TMR = 0;
    CCP1RA = 0;
    CCP1RB = 70;
    CCP1PR = 0x3FF;
    CCP1CON1bits.ON = 1;
    CCP1CON2bits.OCAEN = 0; // OCM1A RB8 yellow
    CCP3CON1 = 0;
    CCP3CON2 = 0;
    CCP3CON3 = 0;
    CCP3CON1bits.MOD = 0b0100; //dual edge compare mode
    CCP3CON2bits.OCFEN = 0; // RB7 OCM3F red
    CCP3TMR = 0;
    CCP3RA = 0;
    CCP3RB = 70;
    CCP3PR = 0x3FF;
    CCP3CON1bits.ON = 1;
}

void SYSTEM_Initialize(void)
{
    PIN_MANAGER_Initialize();
    INTERRUPT_Initialize();
    CORETIMER_Initialize();
    OSCILLATOR_Initialize();
    LIN_Master_Initialize(2);
    UART1_Initialize();
    UART2_Initialize();
    TMR1_Initialize();
    TMR2_Initialize();
    TMR3_Initialize();
    INTERRUPT_GlobalEnable();
    ledpwm();
    SoftSet_Init();
}

void OSCILLATOR_Initialize(void)
{
    SYSTEM_RegUnlock();
    // ORPOL disabled; SIDL disabled; SRC USB; TUN Center frequency; POL disabled; ON enabled; 
    OSCTUN = 0x9000;
    // PLLODIV 1:4; PLLMULT 12x; PLLICLK FRC; 
    SPLLCON = 0x2050080;
    // SBOREN disabled; VREGS disabled; RETEN disabled; 
    PWRCON = PWRCON | 0x0;
    //Clear NOSC,CLKLOCK and OSWEN bits
    OSCCONCLR = _OSCCON_NOSC_MASK | _OSCCON_CLKLOCK_MASK | _OSCCON_OSWEN_MASK;
    // CF No Clock Failure; FRCDIV FRC/1; SLPEN Device will enter Idle mode when a WAIT instruction is issued; NOSC SPLL; SOSCEN disabled; CLKLOCK Clock and PLL selections are not locked and may be modified; OSWEN Switch is Complete; 
    OSCCON = (0x100 | _OSCCON_OSWEN_MASK);
    SYSTEM_RegLock();
    // wait for switch   
    while (OSCCONbits.OSWEN == 1);
    // ON disabled; DIVSWEN disabled; RSLP disabled; ROSEL SYSCLK; OE disabled; SIDL disabled; RODIV 0; 
    REFO1CON = REFO1CON | 0x0;
    while (!REFO1CONbits.ACTIVE & REFO1CONbits.ON);
    // ROTRIM 0; 
    REFO1TRIM = REFO1TRIM | 0x0;

    WDTCONbits.ON = 1;
}

/**
 End of File
 */
//

#if defined(USB_INTERRUPT)

void __ISR(_USB_VECTOR) _USB1Interrupt()
{
    USBDeviceTasks();

}
#endif

/*******************************************************************************************************************
NAME 
   read_eeprom

DESCRIPTION 
   This function returns one byte of data from EEPROM memory at the selected address.

INPUT
   One unsigned byte of EEPROM address

OUTPUT
   No outputs.

RETURNS
   One unsigned byte of EEPROM data.
 *******************************************************************************************************************/
INT8U read_eeprom(INT8U addr)
{
#ifdef __PIC32MM
    return SoftSet.UserSettings[addr];
#else
    EEADR = addr; /* Data Memory Address to read */
    CFGS = 0U; /* Deselect Configuration space */
    EEPGD = 0U; /* Point to DATA memory */
    RD = 1U; /* Set RD bit to begin read */
    return EEDATA;
#endif

} /* End read_eeprom */

/*******************************************************************************************************************
NAME 
   read_eeprom_16

DESCRIPTION 
   This fuction returns two bytes of data from EEPROM memory at the selected address +1.

INPUT
   One unsigned byte of EEPROM address

OUTPUT
   No outputs.

RETURNS
   One unsigned int of EEPROM data.
 *******************************************************************************************************************/
INT16U read_eeprom_16(INT8U addr)
{
#ifdef __PIC32MM
    INT16U temp;
    temp = (INT16U) read_eeprom(addr);
    temp = temp << 8U;
    temp += (INT16U) read_eeprom(addr + 1U);
    return temp;
#else
    INT16U temp;
    temp = (INT16U) read_eeprom(addr);
    temp = temp << 8U;
    temp += (INT16U) read_eeprom(addr + 1U);
    return temp;
#endif

} /* End read_eeprom_16 */

/*******************************************************************************************************************
NAME 
   write_eeprom

DESCRIPTION 
   This fuction waits until any previous EEPROM program/erase cycle has been completed before
   writing the data at the selected EEPROM address. This function does not wait for the write
   to complete before returning.

INPUT
   One unsigned byte of EEPROM address followed by one unsigned byte of data value

OUTPUT
   No outputs.

RETURNS
   No return
 *******************************************************************************************************************/
void write_eeprom(INT8U addr, INT8U value)
{
#ifdef __PIC32MM
    SoftSet.UserSettings[addr] = value;

#else
    EEADR = addr; /* Data Memory Address to write */
    EEDATA = value; /* Data Memory Value to write */
    CFGS = 0U; /* Deselect Configuration space */
    EEPGD = 0U; /* Point to DATA memory */
    WREN = 1U; /* Enable writes */
    EECON2 = 0x55;
    EECON2 = 0xAA;
    WR = 1U; /* Set WR bit to begin write */
    while (WR);
    WREN = 0U; /* Disable writes */
#endif

} /* End write_eeprom */