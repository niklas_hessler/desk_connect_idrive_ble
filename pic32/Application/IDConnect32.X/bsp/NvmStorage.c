/* ************************************************************************** */
/** Non volatile memory storage
  @Company
	ROL
  @File Name
	NvmStorage.c
  @Summary
	Non volatile memory storage.
  @Description
	Non volatile memory storage.
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include <stdbool.h> // bool
#include <string.h>	// memcpy

// ROL
#include "NvmStorage.h" // SoftParamPage
#include "softset.h"

// *****************************************************************************
// *****************************************************************************
// Section: Data Types
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/** Flash task event enum
  @Summary
	Flashtask event enum.    
  @Description
	Event id used for scheduling of the flash task.
	I.e. state machine.
  @Remarks
	The value 0 is reserved by AppTask. Event enum must be non-zero.
 */
typedef enum
{
	NVMS_EV_FLASH_INIT = 1, // Must start with non-zero
	NVMS_EV_FLASH_ERASE_PAGE,
	NVMS_EV_FLASH_BLANK_CHECK,
	NVMS_EV_FLASH_WRITE_PAGE,
	NVMS_EV_FLASH_ERROR,
	NVMS_EV_FLASH_DONE,
} NvmStorage_Event_t;

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/** Code block name
  @Summary
	Code block name.
  @Description
	Code block name. Used for log.
 @Remarks
 */
const char szNvmStorage[] = "NVMSTORAGE";

/* ************************************************************************** */
/** Flash write in progress
  @Summary
	Flash write in progress.
  @Description
	Flash write in progress.
	Set while flash erase and write is in progress.
 @Remarks
 */
bool nFlashWriteInProgress = false;

/* ************************************************************************** */
/** Page for softparam and RFID
  @Summary
	Page for softparam and RFID.
  @Description
	Page for softparam and RFID.
	The value shall persist over a firmware update.
  @Remarks
	This page/section must be exempted by boot loader!
	    int firstname;
    int lastname;
    int age;
    int length;
 */

const NvmStoragePage_t NvmStoragePage = {

		.Header.nMagic = NVMSTORAGE_MAGIC,
		.Header.nEraseCount = 0,
		.Header.nReserved[0 ... 1] = 0,
		.Header.Rec_SwParam.nVersion = 1,
		.Header.Rec_SwParam.nOffset = offsetof(NvmStoragePage_t, SoftSet),
		.Header.Rec_SwParam.nSize = sizeof(SoftSet),
		.Header.Rec_SwParam.nReserved = 0,
		.SoftSet.SaveToFlash_flag = 1,
		.SoftSet.checksum = 0,
		.SoftSet.UserSettings[0 ... (SoftSetAdressSize-1)]=0,
		.SoftSet.UserSettings[CALIBRATION_HIGH_ADD]=CALIBRATION_HIGH,
		.SoftSet.UserSettings[CALIBRATION_LOW_ADD]=CALIBRATION_LOW,
		.SoftSet.UserSettings[UNITS_ADD]=UNITS,
		.SoftSet.UserSettings[AUTODRIVE_ENABLED_ADD]=DEFAULT_AUTODRIVE_ENABLED,
		.SoftSet.UserSettings[CDS_END_MARGIN_HIGH_ADD]=CDS_END_MARGIN_HIGH,
		.SoftSet.UserSettings[CDS_END_MARGIN_LOW_ADD]=CDS_END_MARGIN_LOW,
		.SoftSet.UserSettings[MASTER_DELAY_NUMBER_ADDRESS]=DEFAULT_MASTER_DELAY_NUMBER,
		.SoftSet.UserSettings[MASTER_DELAY_OFFSET_ADDRESS]=DEFAULT_MASTER_DELAY_OFFSET,
		.SoftSet.UserSettings[HardwareSerialNumberMB_ADD] = HardwareSerialNumberMB,
		.SoftSet.UserSettings[HardwareSerialNumberUB_ADD] = HardwareSerialNumberUB,
		.SoftSet.UserSettings[HardwareSerialNumberHB_ADD] = HardwareSerialNumberHB,
		.SoftSet.UserSettings[HardwareSerialNumberLB_ADD] = HardwareSerialNumberLB,  
		.SoftSet.UserSettings[HardwareVersionMajor_ADD] = HardwareVersionMajor,
		.SoftSet.UserSettings[HardwareVersionMinor_ADD] = HardwareVersionMinor,
		.SoftSet.linSniffMode = 0,
		//.SoftSet.deviceID = 0xFFFFFFFFFFFFFFFF
};

/* ************************************************************************** */
/** Page header
  @Summary
	Page header
  @Description
	Page header
  @Remarks
 */
static NvmStorageHeader_t NvmStorageHeader;

/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

// *****************************************************************************
/** 
  @Function
	int NvmStorage_Flash_Task( int nEvent)
  @Summary
	Flash task for use with AppTask scheduler.
	Writes NVM storage page data to flash.
  @Remarks
	This is a scheduled function and never called directly.
 */
static int NvmStorage_Flash_Task(int nEvent)
{
	int nNewEvent = nEvent; // Repeat last event if not moved to new state
	switch (nEvent)
	{
	// Init
	case NVMS_EV_FLASH_INIT:
		// Hold a wakelock to prevent sleep

		// Keep statistics
		memcpy(&NvmStorageHeader, &NvmStoragePage.Header, sizeof(NvmStorageHeader));
		NvmStorageHeader.nEraseCount++;

		nNewEvent = NVMS_EV_FLASH_ERASE_PAGE;
		break;

	// Erase a page
	case NVMS_EV_FLASH_ERASE_PAGE:
	{

		if (Flash_ErasePage(&NvmStoragePage))
		{
			nNewEvent = NVMS_EV_FLASH_BLANK_CHECK;
		}
		else
		{

			nNewEvent = NVMS_EV_FLASH_ERROR;
		}
	}
	break;

	// Blank check
	case NVMS_EV_FLASH_BLANK_CHECK:
	{

		bool nBlankCheckFail = false;
		int n;
		for (n = 0; n < FLASH_PAGE_SIZE && !nBlankCheckFail; n++)
		{
			// 2017-12-08 PIC32MM issue, not read correctly back after erase
			if (NvmStoragePage.i32[n] != 0xFFFFFFFF && 0)
			{
				nBlankCheckFail = true;
			}
		}
		if (nBlankCheckFail)
		{
			nNewEvent = NVMS_EV_FLASH_ERROR;
		}
		else
		{
			nNewEvent = NVMS_EV_FLASH_WRITE_PAGE;
		}
	}
	break;

	// Write RAM data to flash
	case NVMS_EV_FLASH_WRITE_PAGE:
	{

		bool nWriteOk = Flash_Write(&NvmStoragePage.Header, &NvmStorageHeader, sizeof(NvmStoragePage.Header));
		if (nWriteOk)
		{
			nWriteOk = Flash_Write(&NvmStoragePage.SoftSet, &SoftSet, sizeof(NvmStoragePage.SoftSet));
		}
		nNewEvent = NVMS_EV_FLASH_DONE;
	}
	break;
	// An erase or write error occurred
	case NVMS_EV_FLASH_ERROR:
	{

		nNewEvent = NVMS_EV_FLASH_DONE;
	}
	break;

	// Done, clean up
	case NVMS_EV_FLASH_DONE:

		nFlashWriteInProgress--;
		// Done
		nNewEvent = 55; // Done
		break;
	}
	return nNewEvent;
}

/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

// *****************************************************************************
/** 
  @Function
	void NvmStorage_SaveToFlash( void)
  @Summary
	Save NvmStorage page to flash.
  @Remarks
	Refer to the SoftParam.h interface header for function usage details.
 */
uint8_t NvmStorage_SaveToFlash(uint8_t i)
{
	uint8_t y;
	// Indicate that we don't want any reboot now
	nFlashWriteInProgress++;
	y = NvmStorage_Flash_Task(i);
	// Schedule flash write as high priority to minimize failure window,
	// since we are keeping data in RAM while erasing flash
	//AppTask_ScheduleHiPrioTask( NVMS_EV_FLASH_INIT, NvmStorage_Flash_Task);
}

/* *****************************************************************************
 End of File
 */
