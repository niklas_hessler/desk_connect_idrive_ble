/**
  UART2 Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    uart2.c

  @Summary
    This is the generated driver implementation file for the UART2 driver using PIC24 / dsPIC33 / PIC32MM MCUs

  @Description
    This header file provides implementations for driver APIs for UART2.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - 1.155.0-a
        Device            :  PIC32MM0256GPM028
    The generated drivers are tested against the following:
        Compiler          :  XC32 v2.20
        MPLAB             :  MPLAB X v5.25
*/

/*
    (c) 2019 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
*/

/**
  Section: Included Files
*/
#include <xc.h>
#include "uart2.h"

#define UART2_RX_BUFFER_SIZE 100
#define UART2_TX_BUFFER_SIZE 100
/**
  Section: UART1 APIs
 */
/***********Global Variables***********/
volatile uint16_t UART2TxHead = 0;
volatile uint16_t UART2TxTail = 0;
volatile uint16_t UART2TxCount;
volatile uint8_t UART2TxBuffer[UART2_TX_BUFFER_SIZE];

volatile uint16_t UART2RxHead = 0;
volatile uint16_t UART2RxTail = 0;
volatile uint16_t UART2RxCount;
volatile uint8_t UART2RxBuffer[UART2_RX_BUFFER_SIZE];

void clearUart2Buffer(void)
{
    UART2RxHead = 0;
    UART2RxTail = 0;
    UART2RxCount = 0;
    UART2TxHead = 0;
    UART2TxTail = 0;
    UART2TxCount = 0;
}

void Uart2RxTask(void)
{
    while (UART2_IsRxReady())
    {
        UART2RxBuffer[UART2RxHead++] = UART2_Read();
        UART2RxCount++;
        if (sizeof (UART2RxBuffer) <= UART2RxHead)
            UART2RxHead = 0;
    }
}

uint8_t Uart2GetRxCount(void)
{
    return UART2RxCount;
}

uint8_t Uart2ReadRxData(uint8_t *data,uint8_t len)
{
    uint8_t cnt=0;
    while(cnt<len && UART2RxCount)
    {
        data[cnt++]=UART2RxBuffer[UART2RxTail++]; 
        if (sizeof (UART2RxBuffer) <= UART2RxTail)
            UART2RxTail = 0;
        UART2RxCount--;
    }
    return cnt;
}

uint8_t Uart2PeekRx(uint8_t index)
{
    if (index>=UART2RxCount)
        return 0;
    uint8_t i=UART2RxTail+index;
    if (sizeof (UART2RxBuffer) <= i)
        i=i-sizeof(UART2RxBuffer);
    return UART2RxBuffer[i];
}

void Uart2TxTask(void)
{
    if (UART2TxCount && UART2_IsTxReady())
    {//Send next byte
        UART2_Write(UART2TxBuffer[UART2TxTail++]);
        if (sizeof (UART2TxBuffer) <= UART2TxTail)
            UART2TxTail = 0;
        UART2TxCount--;
    }
}

uint8_t Uart2GetTxCount(void)
{
    return UART2TxCount;
}

uint8_t Uart2WriteTxData(uint8_t *data,uint8_t len)
{
    uint8_t cnt=0;
    while(cnt<len && UART2TxCount<sizeof (UART2TxBuffer))
    {
        UART2TxBuffer[UART2TxHead++] = data[cnt++];
        UART2TxCount++;
        if (sizeof (UART2TxBuffer) <= UART2TxHead)
            UART2TxHead = 0;
    }
    return cnt;
}
/**
  Section: UART2 APIs
*/

void UART2_Initialize(void)
{
    // Set the UART2 module to the options selected in the user interface.

    // STSEL 1; PDSEL 8N; RTSMD disabled; OVFDIS disabled; ACTIVE disabled; RXINV disabled; WAKE disabled; BRGH enabled; IREN disabled; ON enabled; SLPEN disabled; SIDL disabled; ABAUD disabled; LPBACK disabled; UEN TX_RX; CLKSEL PBCLK; 
    // Data Bits = 8; Parity = None; Stop Bits = 1;
    U2MODE = (0x8008 & ~(1<<15));  // disabling UARTEN bit
    // UTXISEL TX_ONE_CHAR; UTXINV disabled; ADDR 0; MASK 0; URXEN disabled; OERR disabled; URXISEL RX_ONE_CHAR; UTXBRK disabled; UTXEN disabled; ADDEN disabled; 
    U2STA = 0x00;
    // BaudRate = 115200; Frequency = 24000000 Hz; BRG 51; 
    U2BRG = 0x33;
    
    //Make sure to set LAT bit corresponding to TxPin as high before UART initialization
    U2STASET = _U2STA_UTXEN_MASK;
    U2MODESET = _U2MODE_ON_MASK;   // enabling UART ON bit
    U2STASET = _U2STA_URXEN_MASK;
    
    clearUart2Buffer();
}

uint8_t UART2_Read(void)
{
    while(!(U2STAbits.URXDA == 1))
    {
        
    }

    if ((U2STAbits.OERR == 1))
    {
        U2STACLR = _U2STA_OERR_MASK;
    }
    
    if ((U2STAbits.FERR == 1))
    {
        U2STACLR = _U2STA_FERR_MASK;
    }
    
    if ((U2STAbits.PERR == 1))
    {
        U2STACLR = _U2STA_PERR_MASK;
    }
    
    return U2RXREG;
}

void UART2_Write(uint8_t txData)
{
    while(U2STAbits.UTXBF == 1)
    {
        
    }

    U2TXREG = txData;    // Write the data byte to the USART.
}

bool UART2_IsRxReady(void)
{
    return U2STAbits.URXDA;
}

bool UART2_IsTxReady(void)
{
    return ((!U2STAbits.UTXBF) && U2STAbits.UTXEN );
}

bool UART2_IsTxDone(void)
{
    return U2STAbits.TRMT;
}


/*******************************************************************************

  !!! Deprecated API !!!
  !!! These functions will not be supported in future releases !!!

*******************************************************************************/

uint32_t __attribute__((deprecated)) UART2_StatusGet (void)
{
    return U2STA;
}

void __attribute__((deprecated)) UART2_Enable(void)
{
    U2STASET = _U2STA_UTXEN_MASK;
    U2STASET = _U2STA_URXEN_MASK;
    U2MODESET = _U2MODE_ON_MASK;
}

void __attribute__((deprecated)) UART2_Disable(void)
{
    U2STACLR = _U2STA_UTXEN_MASK;
    U2STACLR = _U2STA_URXEN_MASK;
    U2MODECLR = _U2MODE_ON_MASK;
}


