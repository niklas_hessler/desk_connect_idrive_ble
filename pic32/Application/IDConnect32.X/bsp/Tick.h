#ifndef __TICK_H
#define __TICK_H

#include "HardwareProfile.h"
// All TICKS are stored as 32-bit unsigned integers.
extern volatile unsigned long dwInternalTicks;

#define TICKS_PER_SECOND        1000 
#define TICKS_PER_MS            ((TICKS_PER_SECOND+500)/1000)     // Internal core clock drives timer


// Represents one second in Ticks
#define TICK_SECOND             ((QWORD)TICKS_PER_SECOND)
// Represents one minute in Ticks
#define TICK_MINUTE             ((QWORD)TICKS_PER_SECOND)*60ull)
// Represents one hour in Ticks
#define TICK_HOUR               ((QWORD)TICKS_PER_SECOND*3600ull)

void TickInit(void);
unsigned long TickGet(void);

#define getCurrentTimeStamp() TickGet()

#endif
