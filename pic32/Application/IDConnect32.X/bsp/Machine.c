/* ************************************************************************** */
/** Machine
  @Company
	ROL
  @File Name
	Machine.c
  @Summary
	Machine specific functions.
  @Description
	Machine specific functions.
 */
/* ************************************************************************** */

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include <xc.h>
#include <cp0defs.h>


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */



/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */


/* ************************************************************************** */
/** 
  @Function
	int Machine_GetResetReason( void)
  @Summary
	Get reset reason.
  @Description
	Get reset reason.
  @Remarks
 */
int Machine_GetResetReason( void)
	{
	return _CP0_GET_USERLOCAL();
	}

/* ************************************************************************** */
/** 
  @Function
	void Machine_SetResetReason( int nReason)
  @Summary
	Set reset reason.
  @Description
	Set reset reason.
  @Remarks
 */
void Machine_SetResetReason( int nReason)
	{
	_CP0_SET_HWRENA( 1);
	_CP0_SET_USERLOCAL( nReason);
	}


// *****************************************************************************
/** 
  @Function
	void Machine_SoftwareReset( int nReason)
  @Summary
	Trigger a software reset.
  @Remarks
	Refer to the Machine.h interface header for function usage details.
 */
void Machine_SoftwareReset( void)
	{
	// Disable interrupts
	asm("di");
	
	// The following code illustrates a software Reset
	// assume interrupts are disabled
	// assume the DMA controller is suspended
	// assume the device is locked
	// perform a system unlock sequence
	// starting critical sequence
	SYSKEY = 0x00000000; 	//write invalid key to force lock
	SYSKEY = 0xAA996655; 	//write key1 to SYSKEY
	SYSKEY = 0x556699AA;	//write key2 to SYSKEY
	// OSCCON is now unlocked
	// set SWRST bit to arm reset
	RSWRSTSET = 1;			// read RSWRST register to trigger reset
	unsigned int dummy;
	dummy = RSWRST;
	// prevent any unwanted code execution until reset occurs
	while(1);	
	}

/* ************************************************************************** */
/** 
  @Function
    void Machine_WFI( int nMode)
  @Summary
    Halt and wait for interrupt
  @Description
    Halt and wait for interrupt
  @Parameters
    @param	nMode		Wfi mode: 0=Idle or 1=Sleep
  @Remarks
 */
void Machine_WFI( int nMode)
	{	
	// Power-Saving Modes 61130F.pdf
	// Example 10-2: Placing the Device in an Idle Mode
	SYSKEY = 0x0;				// Write invalid key to force lock
	SYSKEY = 0xAA996655;		// Write Key1 to SYSKEY
	SYSKEY = 0x556699AA;		// Write Key2 to SYSKEY
	if(	nMode)
		{
		OSCCONSET = 0x10;		// set Power-Saving mode to Sleep
		}
	else {
		OSCCONCLR = 0x10;		// Set the power-saving mode to an idle mode
		}
	SYSKEY = 0x0;				// Write invalid key to force lock
	asm volatile ( "wait" );	// Put device in selected power-saving mode
	// Code execution will resume here after wake and the ISR is complete		
	}



/* *****************************************************************************
 End of File
 */
