/* ************************************************************************** */
/** Flash PIC32
  @Company
	ROL
  @File Name
	Flash32.h
  @Summary
	Flash functions for PIC32.
  @Description
	Flash functions for PIC32.
 */
/* ************************************************************************** */

#ifndef _FLASH32_H    /* Guard against multiple inclusion */
#define _FLASH32_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include <stdbool.h>			// bool
#include <xc.h>

/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C"
	{
#endif


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Constants                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/** Flash row and page constants.
  @Summary
	Flash row and page constants.
  @Description
	PIC32 Flash Programming Specification 61145L.pdf
	TABLE 5-1: CODE MEMORY SIZE
	PIC32 Device			Row Size(Words)	Page Size(Words)
	PIC32MX230FXX			32				256
	PIC32MM0256GPMXX		64				512
  @Remarks
	PIC32 Flash Programming Specification 61145L.pdf
	There are always 8 rows within a page.
 */
#ifdef __PIC32MX
	#define FLASH_ROW_SIZE				32
	#define FLASH_PAGE_SIZE				256
#elif __PIC32MM
	#define FLASH_ROW_SIZE				64
	#define FLASH_PAGE_SIZE				512
#endif

#define FLASH_ROW_SIZE_IN_BYTES		(sizeof(int) * FLASH_ROW_SIZE)
#define FLASH_PAGE_SIZE_IN_BYTES	(sizeof(int) * FLASH_PAGE_SIZE)


/* ************************************************************************** */
/** Flash erase cycles endurance
  @Summary
	Flash erase cycles endurance.
  @Description
	Flash erase cycles endurance.
  @Remarks
 */
#define FLASH_MAX_ERASE_CYCLES		20000


// *****************************************************************************
// *****************************************************************************
// Section: Data Types
// *****************************************************************************
// *****************************************************************************

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/** Flash operation address
  @Summary
	Flash operation address
  @Description
	Flash operation address.
 	Set by flash erase and flash write.
 	Used for logging failure address.
  @Remarks
 */
extern unsigned int nFlashOpAdr;	


// *****************************************************************************
// *****************************************************************************
// Section: Interface Functions
// *****************************************************************************
// *****************************************************************************

	// *****************************************************************************
	/**
	  @Function
		bool Flash_ErasePage( const void* pVirtualAdr);
	  @Summary
		Erase a page.
	  @Description
		Erase the page at param address.
	  @Parameters
        @param pFlashVirtualAdr		Virtual address in flash of page to erase
      @Returns
		True if page erase was successful, else false.
	  @Remarks
		The page must not be write protected.
	 */
	bool Flash_ErasePage( const void* pVirtualAdr);

	// *****************************************************************************
	/**
	  @Function
		bool Flash_WriteWord( const void* pFlashVirtualAdr, unsigned int nWordData)
	 * bool Flash_WriteDoubleWord( const void* pFlashVirtualAdr, unsigned int nWordData_l, unsigned int nWordData_h)
	  @Summary
		Write a word/double-word to flash.
	  @Description
		Write a word/double-word to flash.
	  @Parameters
        @param pFlashVirtualAdr		Virtual address in flash
        @param nWordData			Word data to write
      @Returns
		True if write was successful, else false.
	  @Remarks
		See Flash_programming: 60001121g.pdf
		5.5 WORD PROGRAMMING SEQUENCE
		Example 5-2: Word Program Example: NVMWriteWord
	 */
//#ifdef __PIC32MX	
	bool Flash_WriteWord( const void* pFlashVirtualAdr, unsigned int nWordData);
//#elif __PIC32MM
	bool Flash_WriteDoubleWord( const void* pFlashVirtualAdr, unsigned int nWordData_l, unsigned int nWordData_h);
//#endif

	// *****************************************************************************
	/**
	  @Function
		int Flash_Fill( const void* pFlashVirtualAdr, unsigned int nWordData, int nSizeInByte)
	  @Summary
		Fill data in flash.
	  @Description
		Fill data in flash.
	  @Parameters
        @param pFlashVirtualAdr		Virtual address in flash
        @param nWordData			Data to write
        @param nSizeInByte			Number of bytes to write (i.e. sizeof). Shall be a multiple of 4.
      @Returns
		Number of bytes written.
	  @Remarks
		The return value may differ from param nSizeInByte, due to flash write limitations.
	 */
	int Flash_Fill( const void* pFlashVirtualAdr, unsigned int nWordData, int nSizeInByte);
	
	// *****************************************************************************
	/**
	  @Function
		int Flash_Write( const void* pFlashVirtualAdr, const void* pRamData, int nSizeInByte)
	  @Summary
		Write data to flash.
	  @Description
		Write data to flash.
	  @Parameters
        @param pFlashVirtualAdr		Virtual address in flash
        @param pRamData				Data to write (RAM)
        @param nSizeInByte			Number of bytes to write (i.e. sizeof). Shall be a multiple of 4.
      @Returns
		Number of bytes written.
	  @Remarks
		The return value may differ from param nSizeInByte, due to flash write limitations.
	 */
	int Flash_Write( const void* pFlashVirtualAdr, const void* pRamData, int nSizeInByte);

/* Provide C++ Compatibility */
#ifdef __cplusplus
	}
#endif

#endif /* _FLASH32_H */

/* *****************************************************************************
 End of File
 */
