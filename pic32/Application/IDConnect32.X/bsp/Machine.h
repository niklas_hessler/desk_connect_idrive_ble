/* ************************************************************************** */
/** Machine
  @Company
	ROL
  @File Name
	Machine.h
  @Summary
	Machine specific functions.
  @Description
	Machine specific functions.
 */
/* ************************************************************************** */

#ifndef _MACHINE_H    /* Guard against multiple inclusion */
#define _MACHINE_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C"
	{
#endif


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Constants                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/** Magic for reset reasons.
  @Summary
	Magic for reset reasons.
  @Description
	Magic for reset reasons to invoke bootloader.
   @Remarks
*/
#define MACHINE_REASON_BOOTLOADER_MAGIC		0xFA2B31E7


// *****************************************************************************
// *****************************************************************************
// Section: Data Types
// *****************************************************************************
// *****************************************************************************



// *****************************************************************************
// *****************************************************************************
// Section: Interface Functions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/**
  @Function
	int Machine_GetResetReason( void)
  @Summary
	Get reset reason.
  @Description
	Get reset reason.
  @Returns
	Reset reason, set by Machine_SoftwareReset.
  @Remarks
*/
int Machine_GetResetReason( void);

// *****************************************************************************
/**
  @Function
	void Machine_SetResetReason( int nReason)
  @Summary
	Set reset reason.
  @Description
	Set reset reason.
  @Parameters
	@param nReason	Reason for reset.
  @Remarks
*/
void Machine_SetResetReason( int nReason);


// *****************************************************************************
/**
  @Function
	void Machine_SoftwareReset( int nReason)
  @Summary
	Trigger a software reset.
  @Description
	Trigger a software reset.
	In current implementation this will invoke bootloader.
  @Returns
	Function does not return.
  @Remarks
 	Code from 60001118G.pdf (Resets)
	Example 7-1:Software Reset Command Sequence
*/
void Machine_SoftwareReset( void);


// *****************************************************************************
/**
  @Function
	void Machine_WFI( int nMode)
  @Summary
	Halt and wait for interrupt.
  @Description
	Halt and wait for interrupt.
  @Parameters
    @param	nMode		Wfi mode: 0=Idle or 1=Sleep
  @Remarks
*/
void Machine_WFI( int nMode);


/* Provide C++ Compatibility */
#ifdef __cplusplus
	}
#endif

#endif /* _MACHINE_H */

/* *****************************************************************************
 End of File
 */
