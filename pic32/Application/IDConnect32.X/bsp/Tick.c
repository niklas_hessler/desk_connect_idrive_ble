/*********************************************************************
 *
 *                  Tick Manager for Timekeeping
 *
 ********************************************************************/
#define __TICK_C
#include <GenericTypeDefs.h>
#include "Tick.h"


// Internal counter to store Ticks.  This variable is incremented in an ISR and
// therefore must be marked volatile to prevent the compiler optimizer from
// reordering code to use this value in the main context while interrupts are
// disabled.
volatile unsigned long dwInternalTicks = 0; //Timer in ms

/*****************************************************************************
  Function:
    void TickInit(void)

  Summary:
    Initializes the Tick manager module.

  Description:
    Configures the Tick module and any necessary hardware resources.

  Precondition:
    None

  Parameters:
    None

  Returns:
    None

  Remarks:
    This function is called only one during lifetime of the application.
 ***************************************************************************/
void TickInit(void)
{
    dwInternalTicks = 0;
}

/*****************************************************************************
  Function:
    DWORD TickGet(void)

  Summary:
    Obtains the current Tick value.

  Description:
    This function retrieves the current Tick value, allowing timing and
    measurement code to be written in a non-blocking fashion.  This function
    retrieves the least significant 32 bits of the internal tick counter,
    and is useful for measuring time increments ranging from a few
    microseconds to a few hours.  Use TickGetDiv256 or TickGetDiv64K for
    longer periods of time.

  Precondition:
    None

  Parameters:
    None

  Returns:
    Lower 32 bits of the current Tick value.
 ***************************************************************************/
unsigned long TickGet(void)
{
    return dwInternalTicks;
}
