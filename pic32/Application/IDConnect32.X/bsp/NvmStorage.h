/* ************************************************************************** */
/** Non volatile memory storage
  @Company
	ROL
  @File Name
	NvmStorage.h
  @Summary
	Non volatile memory storage.
  @Description
	Non volatile memory storage.
 	Persist over firmware update.
 */
/* ************************************************************************** */

#ifndef _NVMSTORAGE_H    /* Guard against multiple inclusion */
#define _NVMSTORAGE_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include "Flash32.h"			// FLASH_PAGE_SIZE
#include "softset.h"
#include "manne.h"
/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C"
	{
#endif


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Constants                                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/** Magic for NVM storage page
  @Summary
	Magic for NVM storage page
  @Description
  @Remarks
*/
#define NVMSTORAGE_MAGIC		0xFA3EA51D
	

// *****************************************************************************
// *****************************************************************************
// Section: Data Types
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/** NVM storage record
  @Summary
	NVM storage record
  @Description
	NVM storage record
  @Remarks
 */
typedef struct {
	int nVersion;
	int nOffset;
	int nSize;
	int nReserved;
	} NvmRecord_t;

	
// *****************************************************************************
/** NVM storage page header
  @Summary
	NVM storage page header
  @Description
	NVM storage page header
  @Remarks
 */
typedef union {
	char v[ FLASH_ROW_SIZE_IN_BYTES];
	struct {
		int nMagic;					// Magic, can be use for version handling
		int nEraseCount;			// Number of times this page has been erased
		int nReserved[2];			// Reserved
		// For upgrade compatibility check
		NvmRecord_t Rec_SwParam;
		};
	} NvmStorageHeader_t;

// *****************************************************************************
/** Page for softparam and RFID
  @Summary
	Page for softparam and RFID
  @Description
	Page for softparam and RFID
  @Remarks
 	Due to PIC32MM flash row issue, each member must be aligned on FLASH_ROW_SIZE_IN_BYTES
 */
typedef union {
	/*unsigned char i8[ FLASH_PAGE_SIZE_IN_BYTES * 2]; * 2 so it takes 2 pay */
	unsigned char i8[ FLASH_PAGE_SIZE_IN_BYTES * 2];		// Size in bytes
	unsigned int i32[ FLASH_PAGE_SIZE * 2];					// Size in words
	struct {

// Page header
		NvmStorageHeader_t Header;

				// Soft param 16 words
		SoftSet_t SoftSet __attribute__((aligned(FLASH_ROW_SIZE_IN_BYTES)));
		

		};
	} NvmStoragePage_t;

/*
#if( sizeof( NvmStoragePage_t) > FLASH_PAGE_SIZE)
	#error Content of NvmStoragePage exceeds 1 page, code will be broken
#endif
*/
	

/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/** Flash write in progress
  @Summary
	Flash write in progress.
  @Description
	Flash write in progress:
	Set while flash erase and write is in progress.
  @Remarks
	Does not require atomic access.
 */
extern bool nFlashWriteInProgress;
	
/* ************************************************************************** */
/** Nvm storage for soft parameter and RFID keys
  @Summary
	Nvm storage for soft parameter and RFID keys
  @Description
	Nvm storage for soft parameter and RFID keys
	The value shall persist over a firmware update.
  @Remarks
	This page/section must be exempted by boot loader!
 */
__attribute__(( aligned( sizeof(int) * FLASH_PAGE_SIZE), section(".NvmStorage")))\
        extern const NvmStoragePage_t NvmStoragePage;


// *****************************************************************************
// *****************************************************************************
// Section: Interface Functions
// *****************************************************************************
// *****************************************************************************

    // *****************************************************************************
    /**
      @Function
        void NvmStorage_SaveToFlash( void)
      @Summary
		Save NvmStorage page to flash.
      @Description
        Save NvmStorage page to flash.
      @Remarks
     */
	uint8_t NvmStorage_SaveToFlash( uint8_t i);

/* Provide C++ Compatibility */
#ifdef __cplusplus
	}
#endif

#endif /* _NVMSTORAGE_H */

/* *****************************************************************************
 End of File
 */

