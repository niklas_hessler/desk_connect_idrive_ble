/**
  UART1 Generated Driver File

  @Company
    Microchip Technology Inc.

  @File Name
    uart1.c

  @Summary
    This is the generated driver implementation file for the UART1 driver using PIC24 / dsPIC33 / PIC32MM MCUs

  @Description
    This header file provides implementations for driver APIs for UART1.
    Generation Information :
        Product Revision  :  PIC24 / dsPIC33 / PIC32MM MCUs - pic24-dspic-pic32mm : v1.45
        Device            :  PIC32MM0256GPM064
    The generated drivers are tested against the following:
        Compiler          :  XC32 1.43
        MPLAB             :  MPLAB X 3.61
 */

/*
    (c) 2016 Microchip Technology Inc. and its subsidiaries. You may use this
    software and any derivatives exclusively with Microchip products.

    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY IMPLIED
    WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS FOR A
    PARTICULAR PURPOSE, OR ITS INTERACTION WITH MICROCHIP PRODUCTS, COMBINATION
    WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION.

    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE,
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP HAS
    BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO THE
    FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN
    ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT OF FEES, IF ANY,
    THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS SOFTWARE.

    MICROCHIP PROVIDES THIS SOFTWARE CONDITIONALLY UPON YOUR ACCEPTANCE OF THESE
    TERMS.
 */

/**
  Section: Included Files
 */
#include "uart1.h"
#include "linmasterprocess.h"
#define UART1_RX_BUFFER_SIZE 1000
#define UART1_TX_BUFFER_SIZE 1000
/**
  Section: UART1 APIs
 */
/***********Global Variables***********/
volatile uint16_t UART1TxHead = 0;
volatile uint16_t UART1TxTail = 0;
volatile uint16_t UART1TxCount;
volatile uint8_t UART1TxBuffer[UART1_TX_BUFFER_SIZE];

volatile uint16_t UART1RxHead = 0;
volatile uint16_t UART1RxTail = 0;
volatile uint16_t UART1RxCount;
volatile uint8_t UART1RxBuffer[UART1_RX_BUFFER_SIZE];
extern enum abs_state_enum ui8AbsByteState;
void clearuartbuffer(void)
{
    UART1RxHead = 0;
    UART1RxTail = 0;
    UART1RxCount = 0;
}

#if 0

void __attribute__((vector(_UART1_TX_VECTOR), interrupt(IPL1SOFT))) _UART1_TX(void)
{

    if (sizeof (UART1TxBuffer) > UART1TxCount)
    {
        U1TXREG = UART1TxBuffer[UART1TxTail++];
        if (sizeof (UART1TxBuffer) <= UART1TxTail)
        {
            UART1TxTail = 0;
        }

        UART1TxCount++;

    } else
    {
        IEC1bits.U1TXIE = 0;
    }


}
#endif

void __attribute__((vector(_UART1_RX_VECTOR), interrupt(IPL1SOFT))) _UART1_RX(void)
{
    while ((U1STAbits.URXDA == 1))
    {
        UART1RxBuffer[UART1RxHead++] = U1RXREG;
        UART1RxCount++;
        if (sizeof (UART1RxBuffer) <= UART1RxHead)
        {
            UART1RxHead = 0;
        }
    }
    //LIN
    if (ui8AbsByteState == ABS_WAIT_FOR_INTERRUPT)
    {
        /* Flag start of reception from slaves in arbitration */
        /* Used to detect collisions on the LIN bus */
        ui8AbsByteState = ABS_BYTE_RECEIVED;
    }
    IFS1CLR = 1 << _IFS1_U1RXIF_POSITION;
}

uint8_t getCount(void)
{
    return UART1RxCount;
}

void UART1_Initialize(void)
{
    // Set the UART1 module to the options selected in the user interface.
    //IEC1bits.U1RXIE = 0;
    // STSEL 1; PDSEL 8N; RTSMD disabled; OVFDIS disabled; ACTIVE disabled; RXINV disabled; WAKE disabled; 
    //BRGH disabled; IREN disabled; ON enabled; SLPEN disabled; SIDL disabled; ABAUD disabled; LPBACK disabled; UEN TX_RX; CLKSEL FRC; 
    // Data Bits = 8; Parity = None; Stop Bits = 1;
    // Data Bits = 8; Parity = None; Stop Bits = 1;
    U1MODE = (0x48008 & ~(1 << 15)); // disabling UART ON bit
    // UTXISEL TX_ONE_CHAR; UTXINV disabled; ADDR 0; MASK 0; URXEN disabled; OERR disabled; URXISEL RX_ONE_CHAR; UTXBRK disabled; UTXEN disabled; ADDEN disabled; 
    U1STA = 0x00;
    // U1TXREG 0; 
    U1TXREG = 0x00;
    // BaudRate = 19200; Frequency = 8000000 Hz; BRG 103; 
    U1BRG = 0x67;

    U1STAbits.UTXINV = 1;

    //Make sure to set LAT bit corresponding to TxPin as high before UART initialization
    U1STASET = _U1STA_UTXEN_MASK;
    U1MODESET = _U1MODE_ON_MASK; // enabling UART ON bit
    U1STASET = _U1STA_URXEN_MASK;

    UART1RxHead = 0;
    UART1RxTail = 0;
    UART1RxCount = 0;

    UART1TxHead = 0;
    UART1TxTail = 0;
    UART1TxCount = sizeof (UART1TxBuffer);

    // IEC1bits.U1RXIE = 1;
}

uint8_t UART1_Read(void)
{
    uint8_t readValue = 0;
//    while (0 == UART1RxCount)
//    {
//    }

    readValue = UART1RxBuffer[UART1RxTail++];

    if (sizeof (UART1RxBuffer) <= UART1RxTail)
    {
        UART1RxTail = 0;
    }

    IEC1bits.U1RXIE = 0;
    UART1RxCount--;
    IEC1bits.U1RXIE = 1;

    return readValue;
}
#if 0

void UART1_Write(uint8_t txData)
{
    while (0 == UART1TxCount)
    {
    }

    if (IEC1bits.U1TXIE == 0)
    {
        U1TXREG = txData; // Write the data byte to the USART.
    } else
    {
        IEC1bits.U1TXIE = 0;
        UART1TxBuffer[UART1TxHead++] = txData;
        if (sizeof (UART1TxBuffer) <= UART1TxHead)
        {
            UART1TxHead = 0;
        }
        UART1TxCount--;
    }
    IEC1bits.U1TXIE = 1;
}
#endif
#if 1

void UART1_Write(uint8_t txData)
{
    while (U1STAbits.UTXBF == 1)
    {
    }

    U1TXREG = txData; // Write the data byte to the USART.
}
#endif

UART1_STATUS UART1_StatusGet(void)
{
    return U1STA;
}

/**
  End of File
 */
