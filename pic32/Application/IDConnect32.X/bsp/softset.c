#include <stdbool.h> // bool
#include <string.h>  // memcpy
#include <xc.h>

// ROL
#include "NvmStorage.h" // SoftParamPage_t
#include "softset.h"
SoftSet_t SoftSet;

void SoftSet_Init(void)
{
    // Version handling if record needs conversion
    int nVersion = NvmStoragePage.Header.Rec_SwParam.nVersion;
    // Ensure position has not been moved by an upgrade
    int nOffset = NvmStoragePage.Header.Rec_SwParam.nOffset;
    int nSize = min(sizeof(SoftSet), NvmStoragePage.Header.Rec_SwParam.nSize);
    // Copy from flash to RAM
    memcpy(&SoftSet, &NvmStoragePage.i8[nOffset], nSize);
}


void SoftSet_SaveToFlash(void)
{
    uint32_t checksum = 0;
    uint8_t x = 0;
    uint8_t y = 1;
    uint8_t event = 1;
    for (x = 0; x < (SoftSetAdressSize - 1); x++)
    {
        checksum = SoftSet.UserSettings[x] + checksum;
    }
    checksum = checksum + SoftSet.linSniffMode;
    //checksum = checksum + SoftSet.deviceID;

    if (checksum != SoftSet.checksum)
    {
        SoftSet.checksum = checksum;
        //Call to save to flash
        while (y)
        {
            event = NvmStorage_SaveToFlash(event);
            if (event == 55)
            {
                y = 0;
            }
        }
    }
}