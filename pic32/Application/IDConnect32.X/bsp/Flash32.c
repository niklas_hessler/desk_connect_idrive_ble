/* *************************************************************************** */
/** Flash PIC32
  @Company
	ROL
  @File Name
	Flash32.c
  @Summary
	Flash functions for PIC32.
  @Description
	Flash functions for PIC32.
 */
/* ************************************************************************** */


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

#include <stdbool.h>			// bool
#include <string.h>				// memset
#define KVA_TO_PA(kva) ((uint32_t)(kva) & 0x1fffffff)
#include <xc.h>

// ROL
#include "Delay.h"					// delay_us
#include "Flash32.h"				// FLASH_ROW_SIZE


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: File Scope or Global Data                                         */
/* ************************************************************************** */
/* ************************************************************************** */

/* ************************************************************************** */
/** Flash operation address
  @Summary
	Flash operation address
  @Description
	Flash operation address.
 	Set by flash erase and flash write.
 	Used for logging failure address.
  @Remarks
 */
unsigned int nFlashOpAdr = 0;	


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Local Functions                                                   */
/* ************************************************************************** */
/* ************************************************************************** */

// *****************************************************************************
/** 
  @Function
	unsigned int NVMUnlock (unsigned int nvmop)
  @Summary
	Erase a page in nNvmStorage (section .NvmStorage)
  @Remarks
	Flash_programming: 60001121g.pdf
	5.4 LOCK-OUT FEATURE
	Example 5-1: Unlock Example
 */
static unsigned int NVMUnlock (unsigned int nvmop)
	{
	unsigned int status;
	// Suspend or Disable all Interrupts
	asm volatile ("di %0" : "=r" (status));

	// Pending error flags from last NVM operation	
	if( NVMCON & 0x3000)
		{
		NVMCON = 0x4000;
		delay_us( 7);
		}

	// Enable Flash Write/Erase Operations and Select
	// Flash operation to perform
	NVMCON = nvmop;
	
	// Wait for LVD to start-up
	delay_us( 7);
	
	// Write Keys
	NVMKEY = 0xAA996655;
	NVMKEY = 0x556699AA;
	// Start the operation using the Set Register
	NVMCONSET = 0x8000;
	// Wait for operation to complete
	while (NVMCON & 0x8000);
	// Restore Interrupts
	if (status & 0x00000001)
		asm volatile ("ei");
	else
		asm volatile ("di");
	// Disable NVM write enable
	NVMCONCLR = 0x0004000;
	// Return WRERR and LVDERR Error Status Bits
	return (NVMCON & 0x3000);
	}


/* ************************************************************************** */
/* ************************************************************************** */
// Section: Interface Functions                                               */
/* ************************************************************************** */
/* ************************************************************************** */

// *****************************************************************************
/** 
  @Function
	bool Flash_ErasePage( const void* pVirtualAdr)
  @Summary
	Erase a page.
  @Remarks
 */
bool Flash_ErasePage( const void* pVirtualAdr)
	{
	// Save operation address for fail logging
	nFlashOpAdr = (unsigned int)pVirtualAdr;
	
	NVMADDR = KVA_TO_PA( (unsigned int)pVirtualAdr);
	
//	DEVCFG0bits.PWP = ~0b00000001;
		
	NVMUnlock( 0x4004);		// WREN and NVMOP=erase page operation
		
//	DEVCFG0bits.PWP = ~0b00000000;
		
	return ( NVMCONbits.WRERR == 0 && NVMCONbits.LVDERR == 0)? true: false;
	}

// *****************************************************************************
/** 
  @Function
	unsigned int NVMWriteRow( const void* address, void* data)
  @Summary
	Write a word to flash.
  @Remarks
	See Flash_programming: 60001121g.pdf
	5.7 ROW PROGRAMMING SEQUENCE
	Example 5-4: Row Program Example
 */
bool NVMWriteRow( const void* address, void* data)
	{
	unsigned int res;
	
	// Blank check, else will cause exeption for double write when reading
	const int* pRow = address;
	int n;
	for( n = 0; n < FLASH_ROW_SIZE; n++, pRow++)
		{
		if( *pRow != 0xFFFFFFFF)
			{
			//Trace("NVMWriteRow: Blank check failure @%x\n", pRow);
			return false;
			}
		}
	
	// Set NVMADDR to Start Address of row to program
	NVMADDR = KVA_TO_PA( (unsigned int)address);
	// Set NVMSRCADDR to the SRAM data buffer Address
	NVMSRCADDR = KVA_TO_PA( (unsigned int)data);
	// Unlock and Write Row
	res = NVMUnlock(0x4003);

	// Return Result
	return ( res == 0)? true: false;
	}

// *****************************************************************************
/** 
  @Function
	bool Flash_WriteWord( const void* address, unsigned int data)
  @Summary
	Write a word to flash.
  @Remarks
	See Flash_programming: 60001121g.pdf
	5.5 WORD PROGRAMMING SEQUENCE
	Example 5-2: Word Program NVMWriteWord
 */
//#ifdef __PIC32MX
bool Flash_WriteWord( const void* pFlashVirtualAdr, unsigned int nWordData)
	{
	unsigned int res;
	// Load data into NVMDATA register
	NVMDATA0 = nWordData;
	// Save operation address for fail logging
	nFlashOpAdr = (unsigned int)pFlashVirtualAdr;
	// Load address to program into NVMADDR register
	NVMADDR = KVA_TO_PA( (unsigned int)pFlashVirtualAdr);
	// Unlock and Write Word
	res = NVMUnlock (0x4001);
	// Return Result
	return ( res == 0)? true: false;
	}
//#elif __PIC32MM
bool Flash_WriteDoubleWord( const void* pFlashVirtualAdr, unsigned int nWordData_l, unsigned int nWordData_h)
	{
	unsigned int res;
	
	// Load data into NVMDATA register
	NVMDATA0 = nWordData_l;
	NVMDATA1 = nWordData_h;

	// Save operation address for fail logging
	nFlashOpAdr = (unsigned int)pFlashVirtualAdr;
	// Load address to program into NVMADDR register
	NVMADDR = KVA_TO_PA( (unsigned int)pFlashVirtualAdr);
	// Unlock and Write Word
	res = NVMUnlock (0x4002);
	// Return Result
	return ( res == 0)? true: false;
	}
//#endifFlash32

// *****************************************************************************
/** 
  @Function
	int Flash_Fill( const void* pFlashVirtualAdr, unsigned int nWordData, int nSizeInByte)
  @Summary
	Fill data in flash.
  @Remarks
 */
int Flash_Fill( const void* pFlashVirtualAdr, unsigned int nWordData, int nSizeInByte)
	{
	int n;
	const int* pToData = (const int*)pFlashVirtualAdr;
	for( n = 0; n < nSizeInByte / sizeof( int); n += 2)
		{
		if( !Flash_WriteDoubleWord( &pToData[ n], nWordData, nWordData))
			{
			return n * sizeof( int);
			}
		}
	return n * sizeof( int);
	}

// *****************************************************************************
/** 
  @Function
	int Flash_Write( const void* pFlashVirtualAdr, const void* pRamData, int nCount)
  @Summary
	Write data to flash.
  @Remarks
	PIC32MM can only handle row write.
 */
int Flash_Write( const void* pFlashVirtualAdr, const void* pRamData, int nSizeInByte)
	{
	int n;
	const int* pToData = (const int*)pFlashVirtualAdr;
	const int* pFromData = (const int*)pRamData;
	for( n = 0; n < nSizeInByte / sizeof( int); n += 2)
		{
		if( !Flash_WriteDoubleWord( &pToData[ n], pFromData[ n], pFromData[ 1 + n]))
			{
			return n * sizeof( int);
			}
		}
	return n * sizeof( int);
	}



/* *****************************************************************************
 End of File
 */
