#ifndef _SOFTSET_H /* Guard against multiple inclusion */
#define _SOFTSET_H
#include <xc.h>
#include <stdint.h>
#define SoftSetAdressSize 200
typedef struct
{
    uint8_t UserSettings[SoftSetAdressSize];
    uint8_t SaveToFlash_flag;
    uint8_t linSniffMode;
    uint32_t checksum;
    //uint64_t deviceID;
} SoftSet_t;

extern SoftSet_t SoftSet;

void SoftSet_SaveToFlash(void);
void SoftSet_Init(void);

#endif