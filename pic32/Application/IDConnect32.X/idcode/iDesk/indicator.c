#define INDICATOR_C

#include "HardwareProfile.h"
#include "indicator.h"
#include "accessory_process.h"
/* Error logger */
ERROR_CODE ErrorLog[ERROR_LOG_SIZE];

void ErrorLogInit(void)
{
    memset(ErrorLog, 0, ERROR_LOG_SIZE);
}

void AddError(ERROR_CODE error)
{
    unsigned char i;
    for (i = 0; i < ERROR_LOG_SIZE && ErrorLog[i] != NO_ERROR_CODE; i++);
    if (i < ERROR_LOG_SIZE && (i == 0 || ErrorLog[i - 1] != error))
        ErrorLog[i] = error;
}

unsigned char errorShowTimer = 0;

void LINSnifferIndicator()
{
    static uint8_t state=0;
    switch(state)
    {
        case 0:
            LED0_IO = LED0_IO ? 0 : 1;
            state = 1;
            break;
        case 1:
            LED1_IO = LED1_IO ? 0 : 1;
            state = 2;
            break;
        case 2:
            LED2_IO = LED2_IO ? 0 : 1;
            state = 0;
            break;
            
    }
}


void IndicatorTask()
{
#if 0 // Led 1 RB8 and Led 2 RB9 is located at the same pins as UART2 Rx and Tx, fix when new hw arrives ***NH***    
    static unsigned char timer = 10;

    if (!errorShowTimer)
    {//No error showing - show normal information
        //Every 100ms
        if (processData.disconnectDelay)
            LED1_IO = LED1_IO ? 0 : 1; //Blink yellow fast to indicate pending disconnect
        else if (processData.connection_state == CONNECTION_OPEN)
            LED1_On();
        else
            LED1_Off();

        //Every second
        timer--;
        if (!timer)
        {
            timer = 10;

            //Hearbeat
            LED0_IO = LED0_IO ? 0 : 1; //Indicate green with heartbeat

            //Indicate error or warning
            if (ErrorLog[0] == NO_ERROR_CODE)
                LED2_Off(); //Show no errors
            else if (ErrorLog[0] > NO_WARNING)
                LED2_IO = LED2_IO ? 0 : 1; //Blink red to indicate warning
            else
                LED2_On(); //Constand red to indicate fatal error
        }
    }
    else
    {//Error showing for 3 seconds
        errorShowTimer--;
    }
#endif // Led 1 RB8 and Led 2 RB9 is located at the same pins as UART2 Rx and Tx, fix when new hw arrives ***NH*** 
}

void ShowError()
{
    unsigned char i;
    //Show next error
    errorShowTimer = 30; //show for 3 seconds
    SetLEDs(ErrorLog[0]);
    for (i = 1; i < ERROR_LOG_SIZE; i++)
        ErrorLog[i - 1] = ErrorLog[i];
}
