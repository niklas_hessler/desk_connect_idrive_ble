/* 
 * File:   flashSettings.h
 * Author: patros
 *
 * Created on den 27 september 2013, 14:54
 */

#ifndef FLASHSETTINGS_H
#define	FLASHSETTINGS_H

#include <stdint.h>

#define HardwareSerialNumberMB      0x00   //Most significant byte
#define HardwareSerialNumberUB      0x00  //Upper byte
#define HardwareSerialNumberHB      0x00    //high byte
#define HardwareSerialNumberLB      0x10    //low byte

#define HardwareSerialNumberMB_ADD  88U
#define HardwareSerialNumberUB_ADD  89U
#define HardwareSerialNumberHB_ADD  90U
#define HardwareSerialNumberLB_ADD  91U
 
#define HardwareVersionMajor_ADD    92U
#define HardwareVersionMinor_ADD    93U
#define HardwareVersionMajor        0x03 //high byte
#define HardwareVersionMinor        0x00 //low byte

#define SETTING_PAGE_LOCATION   0x00F00000UL     //Start of EEprom in Hex file...

typedef struct DeskProfileStruct {
    //Device profile
    union {
        uint32_t value;
        uint8_t bytes[4];
    } deskID;
    uint16_t deskOffset; //pulses - enbart initialt - använder EEPROM istället
    uint8_t units; //True Imperiad, False Metric cm - endast initialt - använder EEPROM istället
    uint8_t autodrive;
	uint8_t deviceName[17];   // BLE device name - null terminated, max 16 characters
} DeskProfile;
extern DeskProfile deskProfile;



void initDeskProfile(void);
void updateUSBProfile(void);
#endif	/* FLASHSETTINGS_H */

