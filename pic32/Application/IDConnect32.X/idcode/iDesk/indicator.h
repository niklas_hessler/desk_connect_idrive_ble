#ifndef INDICATOR_H
#define INDICATOR_H

////////////////////////////////////////////////////////////////////////////////
// Common
#include <GenericTypeDefs.h>
#include "HardwareProfile.h"
// *****************************************************************************
// Section: Typedefs and global declarations
// *****************************************************************************
/* Fatal errors - set by interrupt routines:
    Low voltage                 //Green only
    Unhandled interrupt         //Yellow only
    Oscillator failed           //Green and yellow
    Addressing error            //Red only
    Stack error                 //Green and red
    Math error                  //Yellow and red
    Bootloader ? insert USB memory stick    //All LEDs on
*/

typedef enum
{
    NO_ERROR_CODE=0,
    ERROR_USB_BUFFER,           //Green led only - USB buffer overrun
    ERROR_WIFI_FAILED,          //Yellow led only - Failed to connect wifi
    ERROR_3,                    //Green and yellow
    ERROR_4,                    //Red only
    ERROR_5,                    //Green and red
    ERROR_6,                    //Yellow and res
    ERROR_IAP_FAILED,           //All LEDs on - IAP init failed

    NO_WARNING=8,
    WARNING_NFC_FAILED,         //Green led only - NFC init failed
    WARNING_BLUETOOTH_FAILED,   //Orange led only - Bluetooth init failed
    WARNING_IDRIVE_FAILED,      //Both green and orange - iDrive init failed
    WARNING_TCP_FAILED,         //Red led only
    WARNING_USB_DEVICE,         //Green and Red - USB Device driver failed
    WARNING_USB_HOST,           //Oragne and Red - USB Host driver failed
    WARNING_BUFFER_FULL         //All LEDs on - A buffer was full, data discarded
}ERROR_CODE;
#define ERROR_LOG_SIZE  5
extern ERROR_CODE ErrorLog[ERROR_LOG_SIZE];

typedef enum
{
    BLINK_PATTERN_ON_OFF,
    BLINK_PATTERN_CYLON,
    BLINK_PATTERN_SWEEP_LEFT,
    BLINK_PATTERN_SWEEP_RIGHT,
    BLINK_PATTERN_IN_OUT,
    BLINK_PATTERN_BROKEN_THIRDS
} BLINK_PATTERNS;


void ErrorLogInit(void);
void AddError(ERROR_CODE error);
void ShowError(void); //Show next error for five seconds
void IndicatorTask(); //should be called every 100ms second to update indicator
void LINSnifferIndicator();

#endif

