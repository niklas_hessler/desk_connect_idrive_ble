#ifndef USER_PROFILES
#define USER_PROFILES
#include "userProfiles.h"
#include "GenericTypeDefs.h"
#include <string.h>


UserProfile userProfiles[MAX_NUMBER_OF_USER_PROFILES];
UINT8 lastProfileUsed = 0xff;

void AddUserProfile(UserProfile *user)
{
    INT8 i, found = -1, oldest = 0; //assume first one to be oldest...
    for (i = 0; i < MAX_NUMBER_OF_USER_PROFILES; i++)
    {
        //Try to find if user already exist
        if (userProfiles[i].ID == user->ID)
            found = i; //User exist
        if (userProfiles[i].timestamp < userProfiles[oldest].timestamp) //olderst so far?
            oldest = i;
    }

    if (found == -1) found = oldest;

    //store profile
    user->timestamp = getCurrentTimeStamp();
    memcpy((void *) (&(userProfiles[found])), (void *) user, sizeof (UserProfile));
    lastProfileUsed = found;
}

UserProfile *getUserProfile(UINT32 ID)
{
    UINT8 i;
    for (i = 0; i < MAX_NUMBER_OF_USER_PROFILES; i++)
        if (userProfiles[i].ID == ID)
        {
            lastProfileUsed = i;
            return &(userProfiles[i]);
        }
    return NULL;
}
#endif

