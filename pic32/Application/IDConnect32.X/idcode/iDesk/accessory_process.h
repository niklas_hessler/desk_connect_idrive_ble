/*
 * File:   accessory_process.h
 * Author: patros
 *
 * Created on den 26 oktober 2012, 13:23
 */

#ifndef ACCESSORY_PROCESS_H
#define	ACCESSORY_PROCESS_H

#include <GenericTypeDefs.h>
#include "interface.h"
#include "softset.h"
typedef enum
{
    CONNECTION_INIT=0,
    CONNECTION_READY,
    CONNECTION_OPEN
}CONNECTION_STATE1; //fixprobs



typedef struct
{
    UINT8   newCommand,lastCommand; //SPI_DUMMY if no command is valid/active/last/new
    UINT8   commandData; //Data attached to the new command
    UINT8   group;  //Active groups to be handled
    BOOL homing; //indicate that a homing is requested/ongoing
    UINT8   iDriveVersionMajor,iDriveVersionMinor;
    UINT16  destination;    //0 - no motion...
    UINT16  currPosition[4];
    UINT16  highLimit[4], lowLimit[4];
    //UINT16  highLimitMotors[4], lowLimitMotors[4];
    UINT8   unreadErrorCode;        //errorcode reported from iDrive
    UINT8   unreadErrorGroup; //error Group reported from iDrive
    UINT8   PWMChannel[3], PWMMax[3];  //PWM 0-100, Max 1-100 - using three channels due to Xybix customization
    union
    {
        UINT32 presentCardCode32;
        UINT8  presentCardCode[4]; //NFC card code, 0 no card
    };
    BYTE    inputs[2]; //Latest input state
    
    UINT8   spiReset;      //>0 perform SPI commands in sequence to reset values
    union
    {
        UINT32   value;
        struct
        {
            UINT8   overcurrentStateUSB     : 1;
            UINT8   highLimitUpdated        : 4;    //Binary coded for each group
            UINT8   lowLimitUpdated         : 4;    //Binary coded for each group
            UINT8   highLimitMotorsUpdated  : 4;    //Binary coded for each group
            UINT8   lowLimitMotorsUpdated   : 4;    //Binary coded for each group
            UINT8   inputChange             : 2;    //Binary coded for each channel
            UINT8   unreadError             : 1;
            UINT8   activeCommand           : 1;    //Active command in lastCommand
            UINT8   positionChange          : 4;    //Binary coded for each group
            UINT8   NFCChange               : 1;
            //Profile state
            UINT8   unitUpdated             : 1;    //Push unit to SPI
            UINT8   offsetUpdated           : 2;    //Push offset to SPI
            UINT8   updateProfile           : 1;     //Request update of profile from server
            UINT8   flashProfile            : 1;     //Set to write profile to flash
            UINT8   updateUserProfile       : 1;     //Request update of user profile from server
            UINT8   userProfileUpdated      : 1;     //Changes to user profile has occured
        };
    } flags;

    union
    {
        UINT32   value;
        struct
        {
            UINT8   setDeviceName           : 1;    //Send device name to BLE module
        };
    } bleFlags;
    UINT32 BLETimeout;  //timeout for incoming messages over BLE - if timeout stop any ongoing commands
  
    //Config read//write
    UINT8 configAddress,configData,configLegSlaveAddress; //Data to be written/read
    UINT8 LegSlaveAddress[8];
    enum recipe_state_enum configState;

    //State information
    UINT16 disconnectDelay; //Number of seconds remaining until disconnect delay
    CONNECTION_STATE1 connection_state;

    UINT16 memory[4][5];
    UINT8 updatedMemory[4]; //indicate, per group, what memory has been updated
                            //bit 0..4 indicate which memories has been updated for this group
                            //bit 7 indicate updated by iDrive -> triggers update to USB
                            //bit 6 indicate updated by USB -> triggers update to iDrive
}ProcessData;
extern ProcessData processData;

unsigned char groupIndex(unsigned char group); //Translates group to index
void ProcessIO(void);

#endif	/* ACCESSORY_PROCESS_H */

