/* 
 * File:   userProfiles.h
 * Author: patros
 *
 * Created on den 10 oktober 2013, 14:48
 */

#ifndef USERPROFILES_H
#define	USERPROFILES_H
#include <GenericTypeDefs.h>

#include "Tick.h"
#define getCurrentTimeStamp() TickGet()

typedef struct userprofile_struct
{
    UINT32 ID;    //(RF)ID - 0 indicates no user
    UINT8 outputs;  //bit 7-4 Offstate, bit 3-0 Onstate
    UINT8 PWM;      //bit 7 Offstate (1 leave on, 0 switch off), bit 6-0 power (0-100)
    UINT16 sitPosition,standPosition;
    UINT8 disconnectDelay;   //Minutes after disconnect until automatic disconnect of PWM and Outputs, 0 disabels
    UINT32 timestamp;   //time of added, used or updated
}UserProfile;

#define MAX_NUMBER_OF_USER_PROFILES 10
extern UserProfile userProfiles[MAX_NUMBER_OF_USER_PROFILES];
extern UINT8 lastProfileUsed;

void AddUserProfile(UserProfile *user);
UserProfile *getUserProfile(UINT32 ID);

#endif	/* USERPROFILES_H */


