#include "flashSettings.h"
#include "mcc.h"
#include "HardwareProfile.h"
#include "manne.h"
#include "softset.h"
DeskProfile deskProfile;
int64_t getUDID(void)
{
    uint32_t UDID1 = *(uint32_t *)0xBFC41840;
    UDID1=UDID1 & 0x00FFFFFF;   //LOT High
    uint32_t UDID2 = *(uint32_t *)0xBFC41844;
    UDID2=UDID2 & 0x00FFFFFF;   //LOT low
    uint32_t UDID3 = *(uint32_t *)0xBFC41848;
    UDID3=UDID3 & 0x000000FF;   //Scribe
    uint32_t UDID4 = *(uint32_t *)0xBFC4184C;
    UDID4=UDID4 & 0xFFFF;   //Die X coord
    uint32_t UDID5 = *(uint32_t *)0xBFC41850;
    UDID5=UDID5 & 0xFFFF;   //Die Y coord
    uint8_t udid[8];
    udid[0]=UDID5 & 0xFF;
    udid[1]=(UDID5 >> 8) & 0xff;
    udid[2]=UDID4 & 0xff;
    udid[3]=(UDID4 >> 8) & 0xff;
    udid[4]=UDID2 & 0xff;
    udid[5]=(UDID2 >> 8) & 0xff;
    udid[6]=0; //(UDID2 >> 16) & 0xff;
    udid[7]=0; //UDID1 & 0xff;
    return (((uint64_t)udid[7]<<56) | ((uint64_t)udid[6]<<48) | ((uint64_t)udid[5]<<40)| ((uint64_t)udid[4]<<32)| ((uint64_t)udid[3]<<24)| ((uint64_t)udid[2]<<16)| ((uint64_t)udid[1]<<8)| udid[0]);
}

void initDeskProfile(void)
{
    uint8_t i;

    uint32_t uw = read_eeprom_16(HardwareSerialNumberMB_ADD), lw = read_eeprom_16(HardwareSerialNumberHB_ADD);
    deskProfile.deskID.value = (uw << 16) + lw;
    deskProfile.units = read_eeprom(UNITS_ADD);
    deskProfile.deskOffset = read_eeprom_16(CALIBRATION_HIGH_ADD);
    if (read_eeprom(DEVICENAME_ADD)==0)
        memcpy(deskProfile.deviceName,DEVICENAME,strlen(DEVICENAME));
    else
        for (i=0;i<17;i++)
            deskProfile.deviceName[i]=read_eeprom(DEVICENAME_ADD+i);
    deskProfile.autodrive = read_eeprom(AUTODRIVE_ENABLED_ADD);
}

extern struct {
    uint8_t bLength;
    uint8_t bDscType;
    uint16_t string[24];
} sd002;

extern struct {
    uint8_t bLength;
    uint8_t bDscType;
    uint16_t string[9];
} sd003;

void updateUSBProfile(void)
{
    uint32_t uw = read_eeprom_16(HardwareSerialNumberMB_ADD), lw = read_eeprom_16(HardwareSerialNumberHB_ADD);
    deskProfile.deskID.value = (uw << 16) + lw;
    uint32_t id = deskProfile.deskID.value;
    USBDeviceDetach();
    uint8_t serialLen = 13, ch = 0, i=0;
    uint8_t str[13] = {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'};
    while (id != 0)
    {
        str[--serialLen] = (id % 10) + '0';
        id /= 10;
    }
    
    for (i = serialLen; i <  13; i++)
    {
        sd002.string[13 + ch] = sd003.string[ch]= str[i];
        ch++;
    }
    for (; ch <  13; ch++)
    {
        sd002.string[13 + ch] = sd003.string[ch] = '\0';
    }

    USBDeviceInit();
}