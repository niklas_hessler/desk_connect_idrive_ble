#include "spi_process.h"
#include "spi_commands.h"
#include "accessory_process.h"
#include "HardwareProfile.h"
#include "spi_slave.h"
#include <string.h>
#include "button.h"
#include "lcd_menu.h"
#include "manne.h"

extern enum AUTO_STATE_ENUM auto_state;
extern bool ui1all_keeping_still_flag;
extern volatile INT16U ui16LIN_position;
extern volatile INT16U ui16StartPosition;
extern volatile INT16U ui16group_latest_pos[4];
extern INT8U ui8error_groups;
extern uint16_t ui16temp;
enum AUTO_STATE_ENUM spi_auto_state = AUTO_IDLE;
unsigned char unread_error = 0; //Latest error message - sticks until read

unsigned char actionCommanded = 0;
unsigned char groupCommanded = 15;


void ProcessSPI(void)
{
    UINT8 i;

    if (processData.newCommand != SPI_DUMMY)
    {

        switch (processData.newCommand)
        {
            case SPI_HOMING:
                groupCommanded = processData.group;//does not matter in homing
                spi_auto_state = AUTO_IDLE;
                actionCommanded = ACTION_HOME;
                processData.homing = TRUE;
                break;
            case SPI_STOP:
                actionCommanded = ACTION_STOP;
                groupCommanded = 15;
                auto_state = AUTO_IDLE;
                control_action = DONT_MOVE;
                spi_auto_state = AUTO_IDLE;
                break;
            case SPI_MOVE_UP:
                reset_active_limits();
                groupCommanded = processData.group;
                actionCommanded = ACTION_MOVE_UP;
                spi_auto_state = AUTO_IDLE;
                break;
            case SPI_MOVE_DOWN:
                reset_active_limits();
                groupCommanded = processData.group;
                actionCommanded = ACTION_MOVE_DOWN;
                spi_auto_state = AUTO_IDLE;
                break;
            case SPI_STOP_AT:
            case SPI_GOTO_POS:
            {
                groupCommanded = processData.group;
                unsigned int pos = processData.destination;
                reset_active_limits();
                for (ui8temp_index = 0U;ui8temp_index < 4U;ui8temp_index++)
                {
                    if (groupCommanded & (1U << ui8temp_index))
                        ui16temp_saved_pos[ui8temp_index] = processData.destination;
                    else
                        ui16temp_saved_pos[ui8temp_index] = 0;
                }

                spi_auto_state = GRP_SELECT;
                actionCommanded = ACTION_GOTO_MEM;
                ui16StartPosition = ui16LIN_position;
            }
                break;
            case SPI_GOTO_MEM:
            {
                reset_active_limits();
                groupCommanded = processData.group;
                unsigned char memory = processData.commandData << 3;//memory number * 8 for addressing right group>>4;
                ui8related_address = M1_SAVEDPOS_G1H_ADD + memory;
                for (ui8temp_index = 0U;ui8temp_index < 4U;ui8temp_index++)
                { //Read memory address for all groups
                    ui16temp_saved_pos[ui8temp_index] = read_eeprom_16(ui8related_address);
                    ui8related_address += 2;
                }
                ui8temp_index = 0U;
                spi_auto_state = GRP_SELECT;
                actionCommanded = ACTION_GOTO_MEM;
                ui16StartPosition = ui16LIN_position;
            }
                break;

            default:
                break;
        }
        processData.lastCommand = processData.newCommand;
        processData.newCommand = SPI_DUMMY;
    }

    //Update position if changed
    if (processData.homing)
    { //Homing initiated - set position to 0xFEFF to indicate homing mode
        processData.currPosition[0] = processData.currPosition[1] = processData.currPosition[2] = processData.currPosition[3] = 0xFEFF;
        if (actionCommanded == ACTION_STOP)
            processData.homing = FALSE;
    }
    else
    {
        for (i = 0;i < 4;i++)
        {
            if (processData.currPosition[i] != ui16group_latest_pos[i])
            {

                processData.currPosition[i] = ui16group_latest_pos[i];
                if(processData.currPosition[i] > 0)//filter out if position '0'
                    processData.flags.positionChange |= 1 << i;
            }
        }
    }

    //Any new errors?
    if (unread_error)
    {
        processData.unreadErrorCode = unread_error;
        processData.unreadErrorGroup = ui8error_groups;
        processData.flags.unreadError = 1;
        unread_error = 0;
    }

    UINT8 groupCounter;
    for (groupCounter = 0;groupCounter < 4;groupCounter++)
    {
        //Check if limits are updated
        UINT16 gotPos = read_eeprom_16(USER_HIGH_LIMIT_G1H_ADD + (groupCounter << 1));
        if (processData.highLimit[groupCounter] != gotPos)
        { //Memory not the same - update
            processData.highLimit[groupCounter] = gotPos;
            processData.flags.highLimitUpdated |= 1 << groupCounter;
        }
        gotPos = read_eeprom_16(USER_LOW_LIMIT_G1H_ADD + (groupCounter << 1));
        if (processData.lowLimit[groupCounter] != gotPos)
        { //Memory not the same - update
            processData.lowLimit[groupCounter] = gotPos;
            processData.flags.lowLimitUpdated |= 1 << groupCounter;
        }

        //Check if mems been updated
        UINT8 memCounter;
        for (memCounter = 0;memCounter < 5;memCounter++)
        {
            gotPos = read_eeprom_16(M1_SAVEDPOS_G1H_ADD + (memCounter << 3) + (groupCounter << 1));
            if (processData.memory[groupCounter][memCounter] != gotPos)
            { //Memory not the same - update
                processData.memory[groupCounter][memCounter] = gotPos;
                processData.updatedMemory[groupCounter] |= 0x80 | (1 << memCounter);
            }
        }
    }
}

void spiActionGotoMem()
{

    static unsigned char timer = 0;
    switch (spi_auto_state)
    {
        case GRP_SELECT:
            ui8temp_group = groupCommanded;
            spi_auto_state += groupSelect();

            break;

        case PRE_MOVE_UP:
            spi_auto_state = GRP_MOVE_UP;
            control_action = UP_ONLY;
            timer = 0;
            break;

        case PRE_MOVE_DOWN:
            spi_auto_state = GRP_MOVE_DOWN;
            control_action = DOWN_ONLY;

            timer = 0;
            break;

        case GRP_MOVE_UP:
        case GRP_MOVE_DOWN:

            if (ui1all_keeping_still_flag && timer++ > 25)
            {
                spi_auto_state = GRP_SELECT;
                control_action = DONT_MOVE;
            }
            else
            {
                if (!ui1all_keeping_still_flag)
                    timer = 0;
                if (spi_auto_state == GRP_MOVE_DOWN)
                    control_action = DOWN_ONLY;
                if (spi_auto_state == GRP_MOVE_UP)
                    control_action = UP_ONLY;
            }
            break;

        case AUTO_IDLE:

            actionCommanded = ACTION_STOP;
            control_action = DONT_MOVE;
            ui8group.byte = groupCommanded;
            break;

        default:
            break;
    }
}