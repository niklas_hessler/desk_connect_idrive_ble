#include "accessory_process.h"
#include "accessory_commands.h"
#include "hal.h"
#include "spi_commands.h"
#include "manne.h"
#include "linmasterprocess.h"
#include "mcc.h"
#include "flashSettings.h"
#include "userProfiles.h"
#include "indicator.h"
#include "lcd_menu.h"
#include "linmaster.h"
#include "Machine.h"
#include <xc.h>
/************Extern******************/
extern INT16U ui16group_low_limit[4];
extern INT16U ui16group_high_limit[4];
extern INT16U ui16group_high_limit_motors[4];
extern INT16U ui16group_low_limit_motors[4]; 
extern INT16U lowestVersion;
extern INT16U highestVersion;
extern bool ui1complete_arbitration;
extern INT8U ui8ArbMotorNumber;
extern DeskProfile deskProfile;
void resetProcessFlags(void)
{
    //Reset all flags too avoid flood on connect
    processData.flags.value = 0;
    processData.configState = CONFIG_READY;
    processData.updatedMemory[0] = 0;
    processData.updatedMemory[1] = 0;
    processData.updatedMemory[2] = 0;
    processData.updatedMemory[3] = 0;
}

unsigned char groupIndex(unsigned char group) //Translates group to index
{
    if (group & 0x01)
        return 0;
    if (group & 0x02)
        return 1;
    if (group & 0x04)
        return 2;
    if (group & 0x08)
        return 3;
    return 0; //All multigroup reqeusts defaults to group 1/index 0
}

void adjustDestination()
{
    unsigned char i;
    for (i = 0; i < 4; i++)
    { //Adjust destination according to valid limits in adressed groups
        if (processData.group & (1 << i))
        { //Group addressed
            if (processData.highLimit[i] != 0 && processData.lowLimit[i] != 0)
            { //Motor exist in iDrive
                if (processData.destination < processData.lowLimit[i])
                    processData.destination = processData.lowLimit[i];
                if (processData.destination > processData.highLimit[i])
                    processData.destination = processData.highLimit[i];
            }
        }
    }
    if (processData.destination < 112)
        processData.destination = 112; //QuickFix due to bug in 3.8 END_STOP_DISTANCE
    processData.newCommand = SPI_GOTO_POS;
}

/********************************************************************
 * Function:        void ProcessIO(void)
 *
 * Processes every incoming command package/dataFrame
 *******************************************************************/

void ProcessIO(void)
{
    BYTE *packet = NULL;

    while ((packet = getNextFrame(&incomingDataFrames)) != NULL)
    {

        processData.group = packet[2];
        switch (packet[1])
        {
        case ACC_COMMAND_MOVE: //Data: Group, Direction (-1 down, 0 stop, 1 up)
            switch (packet[3])
            {
            case 0xff:
                processData.newCommand = SPI_MOVE_DOWN;
                break;
            case 0:
                processData.newCommand = SPI_STOP;
                break;
            case 1:
                processData.newCommand = SPI_MOVE_UP;
                break;
            }
            break;

        case ACC_COMMAND_HOMING: //Data: Group - Perform homing/reset
            processData.newCommand = SPI_HOMING;
            break;

        case ACC_COMMAND_STOP_AT:       //Data: Group, Position (high byte, low byte)
        case ACC_COMMAND_GOTO_POSITION: //Data: Group, Position (high byte, low byte)
            processData.destination = (packet[3] << 8) | packet[4];
            adjustDestination();
            processData.newCommand = SPI_GOTO_POS;
            break;

            //iDrive features
        case ACC_COMMAND_GOTO_MEMORY: //Data: Group, Memory (0-4)
            processData.commandData = packet[3];
            processData.newCommand = SPI_GOTO_MEM;
            break;

        case ACC_SET_MEMORY: //Data: Group,Memory (0-4) - Sets memory to current position
        {
            UINT8 i;
            for (i = 0; i < 4; i++)
            { //Set all position for groups addressed
                if ((1 << i) & processData.group)
                { //Group addressed - set position
                    processData.memory[i][packet[3]] = processData.currPosition[groupIndex(processData.group)];
                    write_eeprom(M1_SAVEDPOS_G1H_ADD + (i << 1) + (packet[3] << 3), HIGH_BYTE(processData.memory[i][packet[3]]));
                    write_eeprom(M1_SAVEDPOS_G1L_ADD + (i << 1) + (packet[3] << 3), LOW_BYTE(processData.memory[i][packet[3]]));
                }
            }
        }
        break;
        case ACC_SET_MEMORY_TO: //Data: Group,Memory (0-4), Position (high byte, low byte) - Sets memory to given position
        {
            UINT8 i;
            for (i = 0; i < 4; i++)
            { //Set all position for groups addressed
                if ((1 << i) & processData.group)
                { //Group addressed - set position
                    processData.memory[i][packet[3]] = (packet[4] << 8) | packet[5];
                    write_eeprom(M1_SAVEDPOS_G1H_ADD + (i << 1) + (packet[3] << 3), packet[4]);
                    write_eeprom(M1_SAVEDPOS_G1L_ADD + (i << 1) + (packet[3] << 3), packet[5]);
                }
            }
        }
        break;
        case ACC_REPORT_MEMORY: //Data: Group,Memory (0-4) - Requests board to report memory position
        {
            BYTE respons[] = {6,                 //length
                              ACC_UPDATE_MEMORY, //command
                              0, 0, 0, 0,
                              0x00}; //Checksum
            respons[2] = packet[2];  //group
            respons[3] = packet[3];  //memory
            respons[4] = HIGH_BYTE(processData.memory[groupIndex(packet[2])][packet[3]]);
            respons[5] = LOW_BYTE(processData.memory[groupIndex(packet[2])][packet[3]]);
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
        break;

        case ACC_SET_HIGH_LIMIT: //Data: Group - Sets upper limit to current position
        {
            unsigned char i;
            for (i = 0; i < 4; i++)
            {
                if (processData.group & (1 << i))
                {
                    processData.highLimit[i] = processData.currPosition[i];
                    write_eeprom(USER_HIGH_LIMIT_G1H_ADD + (i << 1), HIGH_BYTE(processData.highLimit[i]));
                    write_eeprom(USER_HIGH_LIMIT_G1L_ADD + (i << 1), LOW_BYTE(processData.highLimit[i]));
                }
            }
        }
        break;
        case ACC_SET_HIGH_LIMIT_TO: //Data: Group, Position (high byte, low byte) - Sets upper limit to given position
        {
            unsigned char i;
            for (i = 0; i < 4; i++)
            {
                if (processData.group & (1 << i))
                {
                    unsigned int pos = (packet[3] << 8) | packet[4];
                    if (processData.lowLimit[i] < pos)
                    { //high limit is acceptable
                        processData.highLimit[i] = pos;
                        write_eeprom(USER_HIGH_LIMIT_G1H_ADD + (i << 1), packet[3]);
                        write_eeprom(USER_HIGH_LIMIT_G1L_ADD + (i << 1), packet[4]);
                    }
                    else
                        processData.flags.highLimitUpdated = processData.group;
                }
            }
        }
        break;
        case ACC_REPORT_HIGH_LIMIT: //Data: Group, - Requests board to report upper limit position
            processData.flags.highLimitUpdated = processData.group;
            break;
        case ACC_SET_LOW_LIMIT: //Data: Group - Sets upper limit to current position
        {
            unsigned char i;
            for (i = 0; i < 4; i++)
            {
                if (processData.group & (1 << i))
                {
                    processData.lowLimit[i] = processData.currPosition[i];
                    write_eeprom(USER_LOW_LIMIT_G1H_ADD + (i << 1), HIGH_BYTE(processData.lowLimit[i]));
                    write_eeprom(USER_LOW_LIMIT_G1L_ADD + (i << 1), LOW_BYTE(processData.lowLimit[i]));
                }
            }
        }
        break;
        case ACC_SET_LOW_LIMIT_TO: //Data: Group, Position (high byte, low byte) - Sets upper limit to given position
        {
            unsigned char i;
            for (i = 0; i < 4; i++)
            {
                if (processData.group & (1 << i))
                {
                    unsigned int pos = (packet[3] << 8) | packet[4];
                    if (processData.highLimit[i] == 0 || processData.highLimit[i] > pos)
                    { //low limit is acceptable
                        processData.lowLimit[i] = pos;
                        write_eeprom(USER_LOW_LIMIT_G1H_ADD + (i << 1), packet[3]);
                        write_eeprom(USER_LOW_LIMIT_G1L_ADD + (i << 1), packet[4]);
                    }
                    else
                        processData.flags.lowLimitUpdated = processData.group;
                }
            }
        }
        break;
        case ACC_REPORT_LOW_LIMIT: //Data: Group, - Requests board to report upper limit position
            processData.flags.lowLimitUpdated = processData.group;
            break;
        case ACC_REPORT_HIGH_MOTOR_LIMIT: //Data: Group - Requests board to report upper motor limit
            processData.flags.highLimitMotorsUpdated = processData.group;
            break;
        case ACC_REPORT_LOW_MOTOR_LIMIT: //Data: Group - Requests board to report lower motor limit
            processData.flags.lowLimitMotorsUpdated = processData.group;
            break;

            //iDrive features
        case ACC_REPORT_POSITION:                                 //Data: Group - Requests board to report position
            processData.flags.positionChange = processData.group; //Will trigger an update
            break;

        case ACC_REPORT_ACTIVE_FUNCTION: //No data - Requests board to report any ongoing function in iDrive
            processData.flags.activeCommand = 1;
            break;

        case ACC_REPORT_ERROR: //Data: Group - Requests board to report any known error in iDrive group
            processData.flags.unreadError = 1;
            break;

            //Other features
        case ACC_REPORT_INFORMATION:
                    {
            BYTE respons[] = {12,                     //length
                              ACC_UPDATE_INFORMATION, //command
                              HARDWARE_VERSION_MAJOR, HARDWARE_VERSION_MINOR,
                              FIRMWARE_VERSION_MAJOR, FIRMWARE_VERSION_MINOR,
                              MAJOR_VERSION, MINOR_VERSION,
                              0, 0, 0, 0, //deskid
                              0x00};      //Checksum
            respons[2] = read_eeprom(HardwareVersionMajor_ADD);
            respons[3] = read_eeprom(HardwareVersionMinor_ADD);
            respons[8] = deskProfile.deskID.bytes[3];
            respons[9] = deskProfile.deskID.bytes[2];
            respons[10] = deskProfile.deskID.bytes[1];
            respons[11] = deskProfile.deskID.bytes[0];
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
#if 0
        {
            BYTE respons[] = {16,                     //length
                              ACC_UPDATE_INFORMATION, //command
                              HARDWARE_VERSION_MAJOR, HARDWARE_VERSION_MINOR,
                              FIRMWARE_VERSION_MAJOR, FIRMWARE_VERSION_MINOR,
                              MAJOR_VERSION, MINOR_VERSION,
                              0, 0, 0, 0,0, 0, 0, 0, //deskid
                              0x00};      //Checksum
            respons[2] = read_eeprom(HardwareVersionMajor_ADD);
            respons[3] = read_eeprom(HardwareVersionMinor_ADD);
            respons[8] = deskProfile.deskID.bytes[7];
            respons[9] = deskProfile.deskID.bytes[6];
            respons[10] = deskProfile.deskID.bytes[5];
            respons[11] = deskProfile.deskID.bytes[4];
            respons[12] = deskProfile.deskID.bytes[3];
            respons[13] = deskProfile.deskID.bytes[2];
            respons[14] = deskProfile.deskID.bytes[1];
            respons[15] = deskProfile.deskID.bytes[0];
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
#endif
        break;

#ifdef IDESK
            //iDesk board features
        case ACC_SET_OUTPUT: //Data: Channel, Value  - Sets output - Channel 0 = Relays, 1 = GPIO1, 2 = GPIO2
            if (packet[2] == 1)
                GPIO0_O = packet[3] & 0x01 ? 1 : 0;
            break;

        case ACC_SET_LED: //Data: Value  - Sets indicator LEDs (0=green, 1=yellow, 2=red)
            LED0_IO = packet[2] & 1 ? 0 : 1;
            LED1_IO = packet[2] & 2 ? 0 : 1;
            LED2_IO = packet[2] & 4 ? 0 : 1;
            break;
        case ACC_REPORT_LED: //No data - Requests board to indicator LEDs
        {
            BYTE respons[] = {3,              //length
                              ACC_UPDATE_LED, //command
                              0,
                              0x00}; //Checksum
            respons[2] = LED0_IO ? 1 : 0 | LED1_IO ? 2 : 0 | LED2_IO ? 4 : 0;
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
        break;

        case ACC_REPORT_INPUT:   //Data: Channel - Requests board to report output
        case ACC_REPORT_OUTPUT:  //Data: Channel - Requests board to report output
        case ACC_REPORT_PWM:     //Data: Channel - Requests board to report lighting PWM
        case ACC_REPORT_PWM_MAX: //Data: Channel
        {
            BYTE respons[] = {4, //length
                              0, //command
                              0,
                              0,
                              0x00}; //Checksum
            respons[1] = packet[1] + 1;
            respons[2] = packet[2]; //Channel
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
        break;

        case ACC_REPORT_NFC_CARD:
        case ACC_REPORT_UNIX_TIME: //No data
        {
            BYTE respons[] = {6,          //length
                              0,          //command
                              0, 0, 0, 0, //Card code
                              0x00};      //Checksum
            respons[1] = packet[1] + 1;
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
        break;

            //Time/Date functions
        case ACC_REPORT_TIME: //No data
        case ACC_REPORT_DATE: //No data
        {
            BYTE respons[] = {5, //length
                              0, //command
                              0, 0, 0,
                              0x00}; //Checksum
            respons[1] = packet[1] + 1;
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
        break;
#endif

        case ACC_SET_MODE: //Data: Main state modes
            switch (packet[2])
            {
            case NORMAL:
                if (main_state == CONFIG)
                { //Set LIN arbitration, will goto Normal
                    ui1complete_arbitration = 1U;
                    main_state = NORMAL;
                    //LIN_init(TABLE_SIZE, NORMALSCHED); // start schedule
                    LIN_Master_Initialize(1);
                }
                if (SoftSet.linSniffMode == 1)
                {
                    SoftSet.linSniffMode = 0;
                    SoftSet_SaveToFlash();
                    Machine_SoftwareReset();
                }
                break;
            case CONFIG:
                main_state = CONFIG;
                // LIN_init(TABLE_SIZE2, CONFIGSCHED);
                LIN_Master_Initialize(2);
                ui8ArbMotorNumber = 0U;
                if (SoftSet.linSniffMode == 1)
                {
                    SoftSet.linSniffMode = 0;
                    SoftSet_SaveToFlash();
                    Machine_SoftwareReset();
                }
                break;
            case LIN_SNIFFER:
                SoftSet.linSniffMode = 1;
                SoftSet_SaveToFlash();
                Machine_SoftwareReset();
                break;
            }
            //No break - send a direct report
        case ACC_REPORT_MODE: //No data
        {
            BYTE respons[] = {3,               //length
                              ACC_UPDATE_MODE, //command
                              0,
                              0x00}; //Checksum
            respons[2] = main_state;
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
        break;

        case ACC_SET_CONFIG: //Data: Slave, Address, Value
            processData.configAddress = packet[3];
            processData.configData = packet[4];
            // if (packet[5] == 1)
            // {
            //     packet[5] = 0;
            //     processData.configLegSlaveAddress = packet[6];
            // }
            processData.configState = CONFIG_WRITE;

            break;
        case ACC_REPORT_CONFIG_SLAVES:
        {
            BYTE respons[] = {5,                        //length
                              ACC_REPORT_CONFIG_SLAVES, //command
                              0,                        //High address
                              0,                        //low address
                              0,                        //data
                              0x00};                    //Checksum
            respons[3] = processData.LegSlaveAddress[0];
            respons[4] = processData.LegSlaveAddress[1];
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
        break;
        case ACC_REPORT_CONFIG: //Data: Slave, Address
            processData.configAddress = packet[3];
            processData.configState = CONFIG_READ;
            break;
        case ACC_SET_DESK_PROFILE:
            write_eeprom(packet[3], packet[4]);
            initDeskProfile();
            if (menustate == 11)
                menustate = 10;
            break;
        case ACC_REPORT_DESK_PROFILE:
        {
            BYTE respons[] = {5,                       //length
                              ACC_UPDATE_DESK_PROFILE, //command
                              0,                       //High address
                              0,                       //low address
                              0,                       //data
                              0x00};                   //Checksum
            respons[3] = packet[3];
            respons[4] = read_eeprom(packet[3]);
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
        break;

            case ACC_SET_DEVICENAME:
            {//LEN CMD ch0 ... chN CS
                memcpy(deskProfile.deviceName,&packet[2],packet[0]-2);
                deskProfile.deviceName[packet[0]-2]='\0';
                processData.bleFlags.setDeviceName=1;
            }
            break;
            case ACC_REPORT_DEVICENAME:
            {
                BYTE respons[20];
                respons[0]=strlen(deskProfile.deviceName)+2;
                respons[1]=ACC_UPDATE_DEVICENAME;
                memcpy(&respons[2],deskProfile.deviceName,strlen(deskProfile.deviceName));
                updateChecksum(respons);
                if (!addDataFrame(&outgoingDataFrames, respons))
                    AddError(WARNING_BUFFER_FULL);
            }
        case ACC_REPORT_DESK_VERSIONS:
        {
            BYTE respons[] = {6,                        //length
                              ACC_UPDATE_DESK_VERSIONS, //command
                              0, 0,                     //Maximum version
                              0, 0,                     //Minimum version
                              0x00};                    //Checksum
            respons[2] = highestVersion >> 8;
            respons[3] = highestVersion;
            respons[4] = lowestVersion >> 8;
            respons[5] = lowestVersion;
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
        break;
        case ACC_REPORT_UDID:
        {
            uint8_t respons[] = {22,              //length
                                 ACC_UPDATE_UDID, //command
                                 0, 0, 0, 0,      //UDID1
                                 0, 0, 0, 0,      //UDID2
                                 0, 0, 0, 0,      //UDID3
                                 0, 0, 0, 0,      //UDID4
                                 0, 0, 0, 0,      //UDID5
                                 0x00};
            respons[2] = UDID1 >> 24;
            respons[3] = (UDID1 >> 16) & 0x00FF;
            respons[4] = (UDID1 >> 8) & 0x0000FF;
            respons[5] = UDID1 & 0x000000FF;

            respons[6] = UDID2 >> 24;
            respons[7] = (UDID2 >> 16) & 0x00FF;
            respons[8] = (UDID2 >> 8) & 0x0000FF;
            respons[9] = UDID2 & 0x000000FF;

            respons[10] = UDID3 >> 24;
            respons[11] = (UDID3 >> 16) & 0x00FF;
            respons[12] = (UDID3 >> 8) & 0x0000FF;
            respons[13] = UDID3 & 0x000000FF;

            respons[14] = UDID4 >> 24;
            respons[15] = (UDID4 >> 16) & 0x00FF;
            respons[16] = (UDID4 >> 8) & 0x0000FF;
            respons[17] = UDID4 & 0x000000FF;

            respons[18] = UDID5 >> 24;
            respons[19] = (UDID5 >> 16) & 0x00FF;
            respons[20] = (UDID5 >> 8) & 0x0000FF;
            respons[21] = UDID5 & 0x000000FF;

            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
        break;

        case ACC_SET_NEW_LOCAL_ID:
                    {
            SoftSet.UserSettings[HardwareSerialNumberMB_ADD] = packet[2];
            SoftSet.UserSettings[HardwareSerialNumberUB_ADD] = packet[3];
            SoftSet.UserSettings[HardwareSerialNumberHB_ADD] = packet[4];
            SoftSet.UserSettings[HardwareSerialNumberLB_ADD] = packet[5];
            updateUSBProfile();

            BYTE respons[] = {6,                       //length
                              ACC_UPDATE_NEW_LOCAL_ID, //command
                              0, 0,                    //Maximum version
                              0, 0,                    //Minimum version
                              0x00};
            respons[2] = SoftSet.UserSettings[HardwareSerialNumberMB_ADD];
            respons[3] = SoftSet.UserSettings[HardwareSerialNumberUB_ADD];
            respons[4] = SoftSet.UserSettings[HardwareSerialNumberHB_ADD];
            respons[5] = SoftSet.UserSettings[HardwareSerialNumberLB_ADD];

            updateChecksum(respons);

            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
#if 0
        {
            memcpy(&deskProfile.deskID.bytes[0],&packet[2],8);
            updateUSBProfile();

            BYTE respons[] = {10,                       //length
                              ACC_UPDATE_NEW_LOCAL_ID, //command
                              0, 0,                    
                              0, 0,                    
                              0, 0,                    
                              0, 0,                   
                              0x00};
            memcpy(&respons[2],&deskProfile.deskID.bytes[0],8);
            updateChecksum(respons);

            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
        }
#endif
        break;
        case ACC_BOOTLOADER:
            Machine_SetResetReason(MACHINE_REASON_BOOTLOADER_MAGIC);
            Machine_SoftwareReset();
            break;

        case ACC_REBOOT:
            Machine_SoftwareReset();
            break;
        }

        removeNextFrame(&incomingDataFrames);
    }

    /* Check and reports any updates */
    if (processData.flags.positionChange || processData.flags.highLimitUpdated || processData.flags.lowLimitUpdated || processData.flags.highLimitMotorsUpdated || processData.flags.lowLimitMotorsUpdated)
    {                                          //data change on groups
        unsigned char group = 0;               //index for groups
        BYTE respons[] = {5,                   //length
                          ACC_UPDATE_POSITION, //command
                          0, 0, 0,
                          0x00}; //Checksum
        if (processData.flags.positionChange)
        { //Update changed position
            group = groupIndex(processData.flags.positionChange);
            respons[1] = ACC_UPDATE_POSITION; //command
            respons[3] = HIGH_BYTE(processData.currPosition[group]);
            respons[4] = LOW_BYTE(processData.currPosition[group]);
            processData.flags.positionChange = processData.flags.positionChange & (0x0F - (1 << group));
        }
        else if (processData.flags.highLimitUpdated)
        { //Update changed limit
            group = groupIndex(processData.flags.highLimitUpdated);
            respons[1] = ACC_UPDATE_HIGH_LIMIT;
            respons[3] = HIGH_BYTE(processData.highLimit[group]);
            respons[4] = LOW_BYTE(processData.highLimit[group]);
            processData.flags.highLimitUpdated = processData.flags.highLimitUpdated & (0x0F - (1 << group));
        }
        else if (processData.flags.lowLimitUpdated)
        { //Update changed limit
            group = groupIndex(processData.flags.lowLimitUpdated);
            respons[1] = ACC_UPDATE_LOW_LIMIT;
            respons[3] = HIGH_BYTE(processData.lowLimit[group]);
            respons[4] = LOW_BYTE(processData.lowLimit[group]);
            processData.flags.lowLimitUpdated = processData.flags.lowLimitUpdated & (0x0F - (1 << group));
        }
        else if (processData.flags.highLimitMotorsUpdated)
        { //Update changed limit
            group = groupIndex(processData.flags.highLimitMotorsUpdated);
            respons[1] = ACC_UPDATE_HIGH_MOTOR_LIMIT;
            respons[3] = HIGH_BYTE(ui16group_high_limit_motors[group]);
            respons[4] = LOW_BYTE(ui16group_high_limit_motors[group]);
            processData.flags.highLimitMotorsUpdated = processData.flags.highLimitMotorsUpdated & (0x0F - (1 << group));
        }
        else if (processData.flags.lowLimitMotorsUpdated)
        { //Update changed limit
            group = groupIndex(processData.flags.lowLimitMotorsUpdated);
            respons[1] = ACC_UPDATE_LOW_MOTOR_LIMIT;
            respons[3] = HIGH_BYTE(ui16group_low_limit_motors[group]);
            respons[4] = LOW_BYTE(ui16group_low_limit_motors[group]);
            processData.flags.lowLimitMotorsUpdated = processData.flags.lowLimitMotorsUpdated & (0x0F - (1 << group));
        }
        respons[2] = 1 << group;
        updateChecksum(respons);
        if (!addDataFrame(&outgoingDataFrames, respons))
            AddError(WARNING_BUFFER_FULL);
    }

    if (processData.flags.unitUpdated || processData.flags.offsetUpdated)
    {                                              //Change in profile
        BYTE respons[] = {5,                       //length
                          ACC_UPDATE_DESK_PROFILE, //command
                          0, 0, 0,
                          0x00}; //Checksum
        if (processData.flags.unitUpdated)
        { //Update changed unit
            respons[3] = UNITS_ADD;
            respons[4] = deskProfile.units;
            processData.flags.unitUpdated = 0;
        }
        else if (processData.flags.offsetUpdated == 2)
        { //Update changed unit
            respons[3] = CALIBRATION_HIGH_ADD;
            respons[4] = HIGH_BYTE(deskProfile.deskOffset);
            processData.flags.offsetUpdated = 1;
        }
        else if (processData.flags.offsetUpdated == 1)
        {
            respons[3] = CALIBRATION_LOW_ADD;
            respons[4] = LOW_BYTE(deskProfile.deskOffset);
            processData.flags.offsetUpdated = 0;
        }
        updateChecksum(respons);
        if (!addDataFrame(&outgoingDataFrames, respons))
            AddError(WARNING_BUFFER_FULL);
    }

    static UINT8 lastReportedError = 0;
    static UINT16 lastReportTimer = 0;
    if (processData.flags.unreadError)
    { //Update unread errors
        if (lastReportedError != processData.unreadErrorCode)
        {                                       //new error - report
            BYTE respons[] = {4,                //length
                              ACC_UPDATE_ERROR, //command
                              0, 0,
                              0x00};                   //Checksum
            respons[2] = processData.unreadErrorGroup; //group
            respons[3] = processData.unreadErrorCode;
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
            lastReportedError = processData.unreadErrorCode;
            lastReportTimer = 20000;
        }
        processData.flags.unreadError = 0;
        processData.unreadErrorCode = 0;
    }
    else
    { //no error
        if (lastReportedError)
        {
            if (lastReportTimer)
                lastReportTimer--;
            else
                lastReportedError = 0; //Restore
        }
    }

    if (processData.flags.activeCommand)
    {                                                 //Update active command
        BYTE respons[] = {3,                          //length
                          ACC_UPDATE_ACTIVE_FUNCTION, //command
                          0,
                          0x00}; //Checksum
        respons[2] = processData.lastCommand;
        updateChecksum(respons);
        if (!addDataFrame(&outgoingDataFrames, respons))
            AddError(WARNING_BUFFER_FULL);
        processData.flags.activeCommand = 0;
    }
    if (processData.configState == CONFIG_SUCCESS || processData.configState == CONFIG_FAILURE)
    {                                        //Update result of config read/write command
        BYTE respons[] = {5,                 //length
                          ACC_UPDATE_CONFIG, //command
                          0, 0, 0,
                          0x00}; //Checksum
        respons[2] = processData.configState == CONFIG_SUCCESS ? 0x81 : 0x01;
        respons[3] = processData.configAddress;
        respons[4] = processData.configData;
        updateChecksum(respons);
        if (!addDataFrame(&outgoingDataFrames, respons))
            AddError(WARNING_BUFFER_FULL);
        processData.configState = CONFIG_READY;
    }
    //static enum main_state_enum 
    static uint8_t oldMainState = NORMAL;
    if (main_state != oldMainState)
    {
        oldMainState = main_state;
        BYTE respons[] = {3,               //length
                          ACC_UPDATE_MODE, //command
                          0,
                          0x00}; //Checksum
        respons[2] = main_state;
        updateChecksum(respons);
        if (!addDataFrame(&outgoingDataFrames, respons))
            AddError(WARNING_BUFFER_FULL);
    }

    static UINT8 groupCounter = 0, memoryCounter = 0;
    if (processData.updatedMemory[groupCounter] & 0x80)
    { //Memory updated - send update to device
        if (processData.updatedMemory[groupCounter] & (1 << memoryCounter))
        {
            BYTE respons[] = {6,                 //length
                              ACC_UPDATE_MEMORY, //command
                              0, 0, 0, 0,
                              0x00};        //Checksum
            respons[2] = 1 << groupCounter; //group
            respons[3] = memoryCounter;     //memory
            respons[4] = HIGH_BYTE(processData.memory[groupCounter][memoryCounter]);
            respons[5] = LOW_BYTE(processData.memory[groupCounter][memoryCounter]);
            updateChecksum(respons);
            if (!addDataFrame(&outgoingDataFrames, respons))
                AddError(WARNING_BUFFER_FULL);
            else
                processData.updatedMemory[groupCounter] ^= 1 << memoryCounter; //Update done
        }
        
        if (!(processData.updatedMemory[groupCounter] & 0x1F))
            processData.updatedMemory[groupCounter] = 0; //All updates done
        
        memoryCounter++;
        if (memoryCounter == 5)
        {
            memoryCounter = 0;
            groupCounter++;
            if (groupCounter == 4)
                groupCounter = 0;
        }
    }

} //end ProcessIO
