/* 
 * File:   spi_process.h
 * Author: patros
 *
 * Created on den 26 oktober 2012, 13:23
 */

#ifndef SPI_PROCESS_H
#define	SPI_PROCESS_H

#define STOPDELAYTIME 100	//time to slow down and stop (i.e. 1 seks)
#define IDRIVEPOLLINTERVALL 50 //Poll iDrive for changes

#ifdef	__cplusplus
extern "C" {
#endif

//Call every 10ms to not overload the LIN processor
void ProcessSPI(void);

#ifdef	__cplusplus
}
#endif

#endif	/* SPI_PROCESS_H */

