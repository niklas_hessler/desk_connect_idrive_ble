#include "accessory_commands.h"
#include "HardwareProfile.h"

DataFrameBuffer incomingDataFrames, outgoingDataFrames;

void initDataFrameBuffers()
{
    incomingDataFrames.head = incomingDataFrames.tail = 0;
    outgoingDataFrames.head = outgoingDataFrames.tail = 0;
}

/* Retrieves next dataframe from buffer, if any. Otherwise reurn Null
 * Does NOT remove packet from buffer */
BYTE *getNextFrame(DataFrameBuffer *buffer)
{
    if (buffer->head < buffer->tail)
        return &(buffer->dataFrameBuffer[buffer->head]);
    return NULL;
}

/* Removes the next frame from the buffer */
void removeNextFrame(DataFrameBuffer *buffer)
{
    if (buffer->head < buffer->tail)
        buffer->head += buffer->dataFrameBuffer[buffer->head] + 1;
    if (buffer->head >= buffer->tail)
        buffer->head = buffer->tail = 0; //empty buffer - restart
}

/* Copies new dataframe to buffer */
//ToDo: Make buffer cirkular, but keep frames intact, i.e. skip end if space is less than one frame

BOOL addDataFrame(DataFrameBuffer *buffer, BYTE *dataFrame)
{
    if (buffer->tail + dataFrame[0] + 1 >= DATAFRAME_BUFFER_SIZE)
        return FALSE;
    memcpy(&(buffer->dataFrameBuffer[buffer->tail]), dataFrame, dataFrame[0] + 1);
    buffer->tail += dataFrame[0] + 1;
    return TRUE;
}

//Calculate checksum of dataframe, i.e. from lengthbyte

BYTE calculateChecksum(BYTE *dataFrame)
{
    UINT8 cs = 0;
    UINT8 i;
    for (i = 0; i < dataFrame[0]; i++)
        cs += dataFrame[i];
    return cs;
}

void updateChecksum(BYTE *dataFrame)
{
    dataFrame[dataFrame[0]] = calculateChecksum(dataFrame);
}

BOOL validateChecksum(BYTE *dataFrame)
{
    return dataFrame[dataFrame[0]] == calculateChecksum(dataFrame);
}

BOOL validateDataFrame(BYTE *dataFrame)
{
    if (dataFrame[0] < 2 || dataFrame[0] > MAX_DATA_LENGTH + 2)
        return FALSE;
    if (!validateChecksum(dataFrame))
        return FALSE;
    return TRUE;
}