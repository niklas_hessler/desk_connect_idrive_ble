#ifndef ACCESSORY_COMMANDS_H
#define ACCESSORY_COMMANDS_H

#include "HardwareProfile.h"

/* Host/Device COMMUNICATION */
/* Frame definition:
 * Byte 1: Sync byte 1
 * Byte 2: Sync byte 2
 * Byte 3: Length for dataframe (including checksum)
 * Dataframe:
 * Byte 4: Command
 * Byte 5..n-1: Data
 * Byte n: Checksum (sum of lengthbyte and dataframe)
 * */

#define SYNC_BYTE_1 0x5A
#define SYNC_BYTE_2 0xA5

//Definitions of the various application commnands that can be sent

typedef enum _ACCESSORY_COMMANDS {
    //Actions
    ACC_COMMAND_MOVE = 0x01, //Data: Group, Direction (-1 down, 0 stop, 1 up)
    ACC_COMMAND_GOTO_MEMORY = 0x02, //Data: Group, Memory (0-4)
    ACC_COMMAND_HOMING = 0x03, //Data: Group - Perform homing/reset
    ACC_COMMAND_STOP_AT = 0x04, //Data: Group, Position (high byte, low byte)
    ACC_COMMAND_GOTO_POSITION = 0x05, //Data: Group, Position (high byte, low byte)

    //iDrive features
    ACC_SET_MEMORY = 0x10, //Data: Group, Memory (0-4) - Sets memory to current position
    ACC_SET_MEMORY_TO = 0x11, //Data: Group, Memory (0-4), Position (high byte, low byte) - Sets memory to given position
    ACC_REPORT_MEMORY = 0x12, //Data: Group, Memory (0-4) - Requests board to report memory position
    ACC_UPDATE_MEMORY = 0x13, //Data: Group, Memory (0-4), Position (high byte, low byte) - Board reports set memory position
    ACC_SET_HIGH_LIMIT = 0x14, //Data: Group - Sets upper limit to current position
    ACC_SET_HIGH_LIMIT_TO = 0x15, //Data: Group, Position (high byte, low byte) - Sets upper limit to given position
    ACC_REPORT_HIGH_LIMIT = 0x16, //Data: Group, - Requests board to report upper limit position
    ACC_UPDATE_HIGH_LIMIT = 0x17, //Data: Group, Position (high byte, low byte) - Board reports upper limit position
    ACC_SET_LOW_LIMIT = 0x18, //Data: Group - Sets lower limit to current position
    ACC_SET_LOW_LIMIT_TO = 0x19, //Data: Group, Position (high byte, low byte) - Sets lower limit to given position
    ACC_REPORT_LOW_LIMIT = 0x1A, //Data: Group, - Requests board to report lower limit position
    ACC_UPDATE_LOW_LIMIT = 0x1B, //Data: Group, Position (high byte, low byte) - Board reports lower limit position

    //iDesk board features
    ACC_SET_OUTPUT = 0x20, //Data: Channel, Value  - Sets output - Channel 0 = Relays, 1 = GPIO1, 2 = GPIO2
    ACC_REPORT_OUTPUT = 0x21, //Data: Channel - Requests board to report output
    ACC_UPDATE_OUTPUT = 0x22, //Data: Channel, Value - Board reports output
    ACC_SET_PWM = 0x23, //Data: Channel, Percentage  - Sets lighting PWM
    ACC_REPORT_PWM = 0x24, //Data: Channel - Requests board to report lighting PWM
    ACC_UPDATE_PWM = 0x25, //Data: Channel, Percentage - Board reports lighting PWM
    ACC_SET_LED = 0x26, //Data: Value  - Sets indicator LEDs (0=green, 1=yellow, 2=red)
    ACC_REPORT_LED = 0x27, //No data - Requests board to indicator LEDs
    ACC_UPDATE_LED = 0x28, //Data: Value - Board indicator LEDs
    ACC_SET_PWM_MAX = 0x29, //Data: Channel, Max value (1-100)
    ACC_REPORT_PWM_MAX = 0x2A, //Data: Channel
    ACC_UPDATE_PWM_MAX = 0x2B, //Data: Channel, Max value (1-100)

    //iDrive features
    ACC_REPORT_POSITION = 0x30, //Data: Group - Requests board to report position
    ACC_UPDATE_POSITION = 0x31, //Data: Group, Position (high byte, low byte) - Board reports position
    ACC_REPORT_ACTIVE_FUNCTION = 0x32, //No data - Requests board to report any ongoing function in iDrive
    ACC_UPDATE_ACTIVE_FUNCTION = 0x33, //Data: Function - Board reports active function in iDrive
    ACC_REPORT_ERROR = 0x34, //Data: Group - Requests board to report any known error in iDrive group
    ACC_UPDATE_ERROR = 0x35, //Data: Group, Error - Board reports any known error in iDrive groups
    ACC_REPORT_HIGH_MOTOR_LIMIT = 0x36, //Data: Group - Requests board to report upper motor limit
    ACC_UPDATE_HIGH_MOTOR_LIMIT = 0x37, //Data: Group, Position (high byte, low byte) - Board reports upper motor limit
    ACC_REPORT_LOW_MOTOR_LIMIT = 0x38, //Data: Group - Requests board to report lower motor limit
    ACC_UPDATE_LOW_MOTOR_LIMIT = 0x39, //Data: Group, Position (high byte, low byte) - Board reports lower motor limit

    //iDesk board features
    ACC_REPORT_INPUT = 0x40, //Data: Channel - Requests board to report input - Channel 1 = GPIO1, 2 = GPIO 2
    ACC_UPDATE_INPUT = 0x41, //Data: Channel, Value - Board reports input

    //NFC functions
    ACC_REPORT_NFC_CARD = 0x50, //No data
    ACC_UPDATE_NFC_CARD = 0x51, //Data: NFC card code (4 bytes, MSB first)

    //Date and time functions
    ACC_SET_TIME = 0x60, //Data: hours (0-23), minutes (0-59), seconds (0-59)
    ACC_REPORT_TIME = 0x61, //No data
    ACC_UPDATE_TIME = 0x62, //Data: hours (0-23), minutes (0-59), seconds (0-59)
    ACC_SET_DATE = 0x63, //Data: years since 2000 (0-99), month (1-12), day of month (1-31)
    ACC_REPORT_DATE = 0x64, //No data
    ACC_UPDATE_DATE = 0x65, //Data: years since 2000 (0-99), month (1-12), day of month (1-31)
    ACC_SET_UNIX_TIME = 0x66, //Data: unix timestamp (4 bytes, MSB first)
    ACC_REPORT_UNIX_TIME = 0x67, //No data
    ACC_UPDATE_UNIX_TIME = 0x68, //Data: unix timestamp: (4 bytes, MSB first)

    //User profiles
    ACC_ADD_PROFILE = 0x70, //Data: (RF)ID (4 bytes),
    //      Output (1 byte: bit 7-4 disconnect-state, bit 3-0 connect-state),
    //      PWM (1 byte: bit 7 disconnect-state (0 off, 1 on), bit 6-0 power in connect-state),
    //      Sit position (high byte, low byte),
    //      Stand position (high byte, low byte),
    //      DisconnectDelay (1 byte, 0-255 minutes)
    ACC_DELETE_PROFILE = 0x71, //Data: (RF)ID (4 bytes)

    //Statistics
    ACC_REPORT_STATISTICS = 0x80, //Data: number of reports (1 byte: 0 all, 1-255) - request board to send stored statistics
    ACC_UPDATE_STATISTICS = 0x81, //Data: timestamp (4 bytes), Event (1 byte), Data (2 bytes)
    //Event: Sit                0x01 - Data: position (high byte, low byte)
    //       Stand              0x03 - Data: position (high byte, low byte)
    //       Change position    0x02 - Data: position (high byte, low byte)
    //       Outlet change      0x04 - Data: Outlets  (1st byte:4 bits, 2nd byte not used
    //       PWM change         0x05 - Data: PWM (channel 0, channel 1)
    //       Error              0xFF - Data: Error information  (bytes tbd)

    //iDrive config functions
    ACC_SET_MODE = 0x90, //Data: Main state modes - 0 normal, 12 config
    ACC_REPORT_MODE = 0x91, //No data
    ACC_UPDATE_MODE = 0x92, //Data: Main state modes
    ACC_SET_CONFIG = 0x93, //Data: Slave (bit 6..4 handsets, bit 2..0 motors, 0x00 master), Address, Value
    ACC_REPORT_CONFIG = 0x94, //Data: Slave (bit 6..4 handsets, bit 2..0 motors, 0x00 master), Address
    ACC_UPDATE_CONFIG = 0x95, //Data: Slave (bit 7 0 failed; 1 success, bit 6..4 handsets, bit 2..0 motors), 0x7F failed), Address, Value
    ACC_REPORT_CONFIG_SLAVES = 0x96, // used to read active motor slaves id, 

    //Profile handling
    ACC_SET_DESK_PROFILE = 0xA0, //Data: Address (UINT16), Data (uint8) - referenced from profile start address, i.e. 0 = SETTING_PAGE_LOCATION
    ACC_REPORT_DESK_PROFILE = 0xA1, //Data: Address (UINT16)
    ACC_UPDATE_DESK_PROFILE = 0xA2, //Data: Address (UINT16), Data (uint8)
    ACC_REPORT_DESK_VERSIONS = 0xA3, //No data - request actuator firmweare versions
    ACC_UPDATE_DESK_VERSIONS = 0xA4, //Data: Maximum major version, Maximum minor version, Minimum major version, Minimum minor version

    //BLE functions
    ACC_SET_DEVICENAME = 0xC0,  //Data: devicename - maximum 16 bytes - null termination added by Firmware
    ACC_REPORT_DEVICENAME = 0xC1, //Data: No data
    ACC_UPDATE_DEVICENAME = 0xC2, //Data: devicename - maximum 16 bytes
            
    //Logging
    ACC_UPDATE_LOG = 0xB0, //Data: stringdata (uint8[length-5])
    ACC_UPDATE_LIN_RAW = 0xB1, // Up top 64 bytes of raw data
    //Other features
    ACC_REPORT_INFORMATION = 0xF0, //No data - Requests board to report board information
    ACC_UPDATE_INFORMATION = 0xF1, //Data: see below - Board reports information about the board itself
    // HW version major/minor
    // FW major/minor version
    // iDrive major/minor version
    // iDesk id (4 bytes)
    ACC_DISCONNECT = 0xF2, //Data: Channel to disconnect: USB Device 0, USB Host 1, BT 2

    // Reports the device Unique Device Identifier (UDID)
    ACC_REPORT_UDID = 0xF3, // Request report the device Unique Device Identifier (UDID)
    ACC_UPDATE_UDID = 0xF4, // Data: Unique Device Identifier (UDID) 20 bytes

    ACC_SET_NEW_LOCAL_ID = 0xF5, // Sets the local ID.
    ACC_UPDATE_NEW_LOCAL_ID = 0xF6, // Reports the local ID.

    //USB Dongle features
    ACC_SET_BUTTONS = 0xF8, //Data: group, buttons matching LCD_KYB
    ACC_REPORT_DISPLAY = 0xF9, //No data
    ACC_UPDATE_DISPLAY = 0xFA, //Data:4 byte screen data
    ACC_BOOTLOADER = 0xFE,
    ACC_REBOOT = 0xFF
} ACCESSORY_COMMANDS;

#define MAX_DATA_LENGTH 17
/* Example of dataframe
typedef struct __attribute__((packed))
{
    unsigned char length;
    unsigned char command;
    BYTE data[MAX_DATA_LENGTH];
    BYTE checksum;
}ACCESSORY_APP_PACKET;

typedef struct __attribute__((packed))
{
    BYTE sessionId_hi, sessionId_lo;
    BYTE syncByte_1, syncByte_2;
    ACCESSORY_APP_PACKET dataFrame;
} ACCESSORY_USB_PACKET;
 */

#define DATAFRAME_BUFFER_SIZE 64

typedef struct {
    UINT16 head; //first active dataframe
    UINT16 tail; //end of last active dataframe
    BYTE dataFrameBuffer[DATAFRAME_BUFFER_SIZE];
} DataFrameBuffer;
extern DataFrameBuffer incomingDataFrames, outgoingDataFrames;
void initDataFrameBuffers();
BYTE *getNextFrame(DataFrameBuffer *buffer);
void removeNextFrame(DataFrameBuffer *buffer); /* Removes the next frame from the buffer */
BOOL addDataFrame(DataFrameBuffer *buffer, BYTE *dataFrame); /* Copies new dataframe to buffer */

BYTE calculateChecksum(BYTE *dataFrame); //Calculates checksum on dataframe, i.e. frame[length data[0]..[length-1]]
void updateChecksum(BYTE *dataFrame); //Updates checksum byte in packet
BOOL validateChecksum(BYTE *dataFrame); //Validates checksum within dataframe, i.e. frame[length data[0]..[length-1] checksum]
BOOL validateDataFrame(BYTE *dataFrame); //Validates length and checksum within packet

#endif
