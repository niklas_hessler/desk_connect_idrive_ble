#include "spi_master_async.h"
#include <xc.h>

#include "HardwareProfile.h"
//#include "Tick.h"

/* Globally defined communication buffers */
volatile spiReport lastReport[3];
spiFrame nextFrame[3];

void spiInit()
{
	memset(nextFrame,0,sizeof(spiFrame)*3);
        memset((void *)lastReport,0,sizeof(spiReport)*3);
}

BOOL SPIWordCommand(BYTE command, BYTE data)
{//Send word size command
    //Find free frame
    UINT8 i=3;
    if (nextFrame[0].bytesToSend==0) i=0;
    else if (nextFrame[1].bytesToSend==0) i=1;
    else if (nextFrame[2].bytesToSend==0) i=2;
    if (i<3)
    {//Free frame found - fill information
        nextFrame[i].sendCommand=command;
        nextFrame[i].sendFrame[0]=data;
        nextFrame[i].bytesToSend=2;
        nextFrame[i].bytesToRecieve=0;
        return TRUE;
    }
   // DebugUART_PrintString("SPI send buffer full word cmd.\r\n");
    return FALSE;
}

BOOL SPIFrameCommand(BYTE command,BYTE length,BYTE *data)
{
    //Find free frame
    UINT8 i=3;
    if (nextFrame[0].bytesToSend==0) i=0;
    else if (nextFrame[1].bytesToSend==0) i=1;
    else if (nextFrame[2].bytesToSend==0) i=2;
    if (i<3)
    {//Frame free to use
        nextFrame[i].sendCommand=command;
        nextFrame[i].sendFrame[0]=length+1;   //Length of frame exluding command-byte
        memcpy(&(nextFrame[i].sendFrame[1]),data, length);
        nextFrame[i].bytesToSend=length+2; //Command+length+data
        return TRUE;
    }
//    DebugUART_PrintString("SPI send buffer full frame cmd.\r\n");
    return FALSE;
}

BOOL SPIRequestReport(BYTE what)
{//Send request for report
     //Find free frame
    UINT8 i=3;
    if (nextFrame[0].bytesToSend==0) i=0;
    else if (nextFrame[1].bytesToSend==0) i=1;
    else if (nextFrame[2].bytesToSend==0) i=2;
    if (i<3)
    {
        nextFrame[i].sendCommand=SPI_REPORT;
        nextFrame[i].sendFrame[0]=what;
        nextFrame[i].bytesToSend=2;
        return TRUE;
    }
    //DebugUART_PrintString("SPI send buffer full req. rep.\r\n");
    return FALSE;
}

BOOL SPIRequestReportFrame(BYTE what,BYTE data)
{
    //Find free frame
    UINT8 i=3;
    if (nextFrame[0].bytesToSend==0) i=0;
    else if (nextFrame[1].bytesToSend==0) i=1;
    else if (nextFrame[2].bytesToSend==0) i=2;
    if (i<3)
    {//Frame free to use
        nextFrame[i].sendCommand=SPI_REPORT_FRAME;
        nextFrame[i].sendFrame[0]=3;    //length of frame excluding command byte
        nextFrame[i].sendFrame[1]=what;
        nextFrame[i].sendFrame[2]=data;
        nextFrame[i].bytesToSend=4; //Command+length+what+data
        return TRUE;
    }
    //DebugUART_PrintString("SPI send buffer full req. rep. frame.\r\n");
    return FALSE;
}

BOOL SPIReports(void)
{//Check if any new reports arrived
    if (lastReport[0].report!=0)
        return TRUE;
    return FALSE;
}

void removeSPIReport(void)
{//Remove first SPI report from FIFO queue
    memcpy((void *)&(lastReport[0]),(void *)&(lastReport[1]),sizeof(spiReport));
    memcpy((void *)&(lastReport[1]),(void *)&(lastReport[2]),sizeof(spiReport));
    lastReport[2].report=0;
}
