#ifndef SPI_MASTER_H
#define SPI_MASTER_H

#include <xc.h>
#include <GenericTypeDefs.h>
#include "spi_commands.h"

#define MIN_IDRIVE_VERSION_MAJOR 4
#define MIN_IDRIVE_VERSION_MINOR 6

typedef struct
{
        BYTE report;    //If SPI_DUMMY - invalid report (e.g. wrong checksum)
        BYTE data[3];
}spiReport;
extern volatile spiReport lastReport[3]; //FiFo buffer for incoming reports.

typedef struct /*__attribute__((packed))*/
{
	unsigned char bytesToSend; //number of bytes INCLUDING command
	unsigned char sendCommand;
	unsigned char sendFrame[4];
        unsigned char bytesToRecieve;
	unsigned char responsFrame[3];
}spiFrame;
extern spiFrame nextFrame[3];

/* Initialize */
void spiInit();

/* Functions */
BOOL SPIWordCommand(BYTE command, BYTE data);               //Send Word-size command to SPI
BOOL SPIFrameCommand(BYTE command,BYTE length,BYTE *data);  //Send Frame-size command to SPI
BOOL SPIRequestReport(BYTE what);                           //Send Request report to SPI
BOOL SPIRequestReportFrame(BYTE what,BYTE data);            //Send reqeust report frame to SPI
BOOL SPIReports(void);                                      //Check if report is available in lastReport
void removeSPIReport(void);                                 //Removes first report from buffer

#endif
