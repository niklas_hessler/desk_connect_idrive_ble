#ifndef SPICOMMANDS_H
#define SPICOMMANDS_H

/* SPI Commands
 * Command byte bit 7 indicated Word (0) or Frame (1)
 * Word command includes 1 data byte
 * Frame command includes dynamic length of data*/

/* Byte command - dummy command */
#define SPI_DUMMY 0xff //Byte command, used for dummy, reset, invalid command etc

/* Word commands - followed by one byte (as described) */
#define SPI_STOP 0x00           //Type (Soft, Hard)
#define SPI_MOVE_UP 0x01        //Group
#define SPI_MOVE_DOWN 0x02      //Group
#define SPI_GOTO_MEM 0x03       //Memory & group (Group in bit 0..3, memory in bit 4..6) - Goto iDrive memory
#define SPI_SET_MEM 0x04        //Memory & group (Group in bit 0..3, memory in bit 4..6) - Set iDrive memory to current position
#define SPI_SET_HIGH_LIMIT 0x05 //Group - set limit to current position
#define SPI_SET_LOW_LIMIT 0x06  //Group - set limit to current position
#define SPI_HOMING 0x07         //Group

#define SPI_REPORT 0x7f //What - descibed below

/* Frame commands - first byte in frame always length of frame excluding command, including length byte*/
#define SPI_STOP_AT 0x80u           //group, position hi, position lo
#define SPI_GOTO_POS 0x81u          //group, position hi, position lo
#define SPI_SET_HIGH_LIMIT_TO 0x82u //group, position hi, position lo
#define SPI_SET_LOW_LIMIT_TO 0x83u  //group, position hi, position lo
#define SPI_SET_MEM_TO 0x84u        //memory & group, position hi, position lo (Group in bit 0..3, memory in bit 4..6)
#define SPI_SET_MASTER_CONFIG 0x85u //address, value

#define SPI_REPORT_FRAME 0x8fu //What and data - descibed below in three bytes reply

/* Report - what - following number of bytes to reply */
//One byte replies - use with SPI_REPORT
#define SPI_REPORT_LATEST_COMMAND 0x01  //1b command
#define SPI_REPORT_ACTIVE_FUNCTION 0x02 //1b function
#define SPI_REPORT_ACTIVE_MOTORS 0x03   //1b number of active motors
#define SPI_REPORT_ERROR_GROUPS 0x04    //1b [b7-4 groups, b3-0 error]
#define SPI_REPORT_MOTION_DETECTS 0x05  //1b Number of motions detected
//Two bytes replies - use with SPI_REPORT
#define SPI_REPORT_VERSION 0x80u           //2b Major, minor
#define SPI_REPORT_HIGHEST_MOTOR_POS 0x81u //2b position hi, position lo
#define SPI_REPORT_LOWEST_MOTOR_POS 0x82u  //2b position hi, position lo
#define SPI_REPORT_HIGH_LIMIT 0x83u        //2b position hi, position lo
#define SPI_REPORT_LOW_LIMIT 0x84u         //2b position hi, position lo
#define SPI_REPORT_LIN_POS 0x85u           //2b position hi, position lo
#define SPI_REPORT_NEAR_HIGH_LIMIT 0x86u   //2b position hi, position lo
#define SPI_REPORT_NEAR_LOW_LIMIT 0x87u    //2b position hi, position lo
#define SPI_REPORT_HIGH_LIMIT_MOTORS 0x88u //2b position hi, position lo
#define SPI_REPORT_LOW_LIMIT_MOTORS 0x89u  //2b position hi, position lo

//Three bytes reply with request data - use with SPI_REPORT_FRAME
#define SPI_REPORT_MASTER_CONFIG 0xC0u           //Data: Address, Reply: Address, Value
#define SPI_REPORT_GROUP_HIGH_LIMIT 0xC3u        //Data: group, Reply: 3b position hi, position lo, group
#define SPI_REPORT_GROUP_LOW_LIMIT 0xC4u         //Data: group, Reply: 3b position hi, position lo, group
#define SPI_REPORT_GROUP_POS 0xC5u               //Data: group, Reply: 3b position hi, position lo, group
#define SPI_REPORT_GROUP_HIGH_LIMIT_MOTORS 0xC8u //Data: group, Reply: 3b position hi, position lo, group
#define SPI_REPORT_GROUP_LOW_LIMIT_MOTORS 0xC9u  //Data: group, Reply: 3b position hi, position lo, group
#define SPI_REPORT_MEMORY 0xCAu                  //Data: memory & group Reply: 3b position hi, position lo, memory & group (Group in bit 0..3, memory in bit 4..6)

#endif
