/* 
 * File:   lcd.h
 * Author: patros
 *
 * Created on den 11 juli 2013, 19:05
 */

#ifndef LCD_HEADER
#define	LCD_HEADER




#define BL_ON { BL_0_ON BL_1_ON }
#define BL_OFF { BL_0_OFF BL_1_OFF }

void lcd_init(void);
void lcd_clear(void);
void lcd_display_int(unsigned int num,unsigned char decimals);
void lcd_display_value(unsigned int num);
void lcd_update(void);

#endif	/* LCD_HEADER */

