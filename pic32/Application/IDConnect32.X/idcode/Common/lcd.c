#include "lcd.h"
#include "manne.h"

unsigned char lcd_digit[4]={0,0,0,0};
const unsigned char decimal_lcd_digit[10]={0b01110111,0b00100100,0b01011101,0b01101101,0b00101110,
                             0b01101011,0b01111011,0b00100101,0b01111111,0b01101111};
void lcd_clear(void)
{
    lcd_digit[2]=lcd_digit[1]=lcd_digit[0]=0;
    lcd_digit[3]&=0xFB;
    lcd_update();
}

//Converts integer 0-1999 to LCD
void lcd_display_int(unsigned int num, unsigned char decimals)
{
    lcd_clear();
    if (num>999) lcd_digit[3]|=4; 
    if (decimals==2 || num>99) lcd_digit[2]=decimal_lcd_digit[(num/100)%10];
    if (decimals==2) lcd_digit[2]|=128;
    if (num>9 || decimals>=1) lcd_digit[1]=decimal_lcd_digit[(num/10)%10];
    if (decimals==1) lcd_digit[1]|=128;
    lcd_digit[0]=decimal_lcd_digit[num%10];
    lcd_update();
}

//Converts integer 0-1999 to LCD with leading 0
void lcd_display_value(unsigned int num)
{
    lcd_clear();
    if (num>999) lcd_digit[3]|=4;
    lcd_digit[2]=decimal_lcd_digit[(num/100)%10];
    lcd_digit[1]=decimal_lcd_digit[(num/10)%10];
    lcd_digit[0]=decimal_lcd_digit[num%10];
    lcd_update();
}

void lcd_update()
{
//#ifdef IDRIVE_MASTER
    l_bytes_wr_DISPLAY_DATA(0,4,lcd_digit); //Send to any slaves...

// #endif
}
