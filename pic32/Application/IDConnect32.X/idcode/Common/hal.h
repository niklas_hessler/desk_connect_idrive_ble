#ifndef	HAL_H
#define	HAL_H

#include <GenericTypeDefs.h>
#include <stdbool.h>
/*******************************************************************************************************************
** Copyright (C) ROL Ergo AB
** ----------------------------------------------------------------------------------------
** IDENTIFICATION
** Unit Name	  CONTROLBOX_HAL
** File Name	  CONTROLBOX_HAL.h
** 
** ----------------------------------------------------------------------------------------
** DESCRIPTION
** This file contains configurations that are target CPU specific
**
** ----------------------------------------------------------------------------------------
** REVISION SUMMARY
** Rev	Date			Name						Changes from previous issue
** 1.0	11-08-11		Anders Rambrant					Initial version
 * 1.1  13-10-01                Patrik Rostedt                                  Added Backlight support, adapted to microchip standard codes, added configurable firmware
*******************************************************************************************************************/

#define MAJOR_VERSION 5             //iDrive version
#define MINOR_VERSION 0            //iDrive verion
#define HARDWARE_VERSION_MAJOR 3    //USB Dongle HW version
#define HARDWARE_VERSION_MINOR 0
#define FIRMWARE_VERSION_MAJOR 2    //iDesk FW version
#define FIRMWARE_VERSION_MINOR 0

/* datatype */
#define     INT8S       char
#define     INT8U       unsigned char
#define     INT16S      short
#define     INT16U      unsigned short
#define     INT32S      int
#define     INT32U      unsigned int
#define     BOOL1U      bool


//#if sizeof	(INT8S)	!=	1
//#error "Wrong size of INT8S"
//#endif
//
//#if sizeof	(INT8U)	!=	1
//#error "Wrong size of INT8U"
//#endif
//
//#if sizeof	(INT16S)  !=  2
//#error "Wrong size of INT16S"
//#endif
//
//#if sizeof	(INT16U)  !=  2
//#error "Wrong size of INT16U"
//#endif
//
//#if sizeof	(INT32U)  !=  4
//#error "Wrong size of INT32U"
//#endif

#define HI      1
#define LO      0

/*----------------------------------------------------------------------
** PORTA
**----------------------------------------------------------------------*/
//#define TRISA_INIT_VALUE	0b00011111U;
//#define ANSELA_INIT_VALUE	0b00000000U;
/*----------------------------------------------------------------------*/
//#define NC_PA0          PORTAbits.RA0
//#define NC_PA1          PORTAbits.RA1
//#define NC_PA2          PORTAbits.RA2
//#define NC_PA3          PORTAbits.RA3
//#define NC_PA4          PORTAbits.RA4
#define LED0_TRIS	TRISBbits.TRISB7
#define LED0_IO		CCP3CON2bits.OCFEN // sser       // RB7 OCM3F red

#define LED0_On()       LED0_IO=1
#define LED0_Off()      LED0_IO=0
#define LED1_TRIS	TRISBbits.TRISB8
#define LED1_IO		CCP1CON2bits.OCAEN		// OCM1A RB8 yellow
#define LED1_On()       LED1_IO=1
#define LED1_Off()      LED1_IO=0

#define LED2_TRIS	TRISBbits.TRISB9
#define LED2_IO		CCP1CON2bits.OCBEN		// OCM1B/RB9 (green)
#define LED2_On()       LED2_IO=1
#define LED2_Off()      LED2_IO=0

#define GetLEDs()       ((LED2_IO?4:0) | (LED1_IO?2:0) | (LED0_IO?1:0))
#define SetLEDs(x)      {                       \
                            LED0_IO=(x&1)?1:0;  \
                            LED1_IO=(x&2)?1:0;  \
                            LED2_IO=(x&4)?1:0;  \
                        }


/*----------------------------------------------------------------------
** PORTB
**----------------------------------------------------------------------*/
//#define TRISB_INIT_VALUE	0b11110111U;
//#define ANSELB_INIT_VALUE	0b00000000U;
//#define WPUB_INIT_VALUE		0b00000000U;
/*----------------------------------------------------------------------*/
//#define NC_PB0          1//PORTBbits.RB0
//#define NC_PB1          PORTBbits.RB1
//#define NC_PB2          PORTBbits.RB2
#define bus_pullup_out  LATBbits.LATB0
#define BUS_PULLUP_ON	bus_pullup_out = 1;
#define BUS_PULLUP_OFF	bus_pullup_out = 0;
//#define GPIO0_TRIS      TRISBbits.TRISB4
//#define GPIO0_I         PORTBbits.RB4
//#define GPIO0_O         LATBbits.LATB4
//#define NC_PB5          PORTBbits.RB5
//#define NC_PB6          PORTBbits.RB6   //PGC
//#define NC_PB7          PORTBbits.RB7   //PGD


/*----------------------------------------------------------------------
** PORTC
**----------------------------------------------------------------------*/
//#define TRISC_INIT_VALUE	0b10111111U;
//#define ANSELC_INIT_VALUE       0b00000000U;
/*----------------------------------------------------------------------*/
//#define NC_PC0          PORTCbits.RC0
//#define NC_PC1          PORTCbits.RC1
//#define NC_PC2          PORTCbits.RC2
//#define NC_PC3          PORTCbits.RC3
//#define DNEG            PORTCbits.RC4
//#define DPOS            PORTCbits.RC5
/*----------------------------------------------------------------------*/
#define tx_out          LATBbits.LATB14
#define TX_OUT_HI       tx_out = HI;
#define TX_OUT_LO       tx_out = LO;
#define rx_in           PORTBbits.RB15
#define RX_IN_IS_LO     rx_in == LO
#define RX_IN_IS_HI     rx_in == HI


/*----------------------------------------------------------------------
** PORTE
**----------------------------------------------------------------------*/
//#define TRISE_INIT_VALUE                0b11111111U;
//#define WPUE_INIT_VALUE			0b00000000U;
/*----------------------------------------------------------------------*/
//#define MCLR            PORTEbits.RE3
/*----------------------------------------------------------------------*/

//Refered but nonexisting ports
#define BL_0_OFF  ;
#define BL_0_ON   ;
#define BL_1_OFF  ;
#define BL_1_ON   ;


/* General definitions */
#define timertick 	IFS0bits.T1IF	/* 125us tick */
//#define NOOP		asm("NOP");

/* Prototype definitions ****************************************************/

///************ Initiate PIC ********************* */
//void init_PIC( void );
//

/* general keys */
#define NO_INPUT	0b11111111U

/* Command data in button data -  used for sensor keyb.*/
#define KYB_STOP        0x01U
#define KYB_COLLISION   0x02U
#define KYB_PRESENCE    0x03U
#define KYB_NO_INPUT    NO_INPUT

/* keyboard types */
#define EASY_KYB	0U  //0 ohm to GND
#define SPI_KYB         1U  //2.4k Fictive - no keyboard on SPI
#define PADDLE_KYB      2U  //4.7k
#define VISUAL_KYB      3U  //7.5k - B6 and B8 used for backlight
#define unused_keyb4    4U  //13k
#define SENSOR_KYB      5U  //22k Non keyboard based sensor slave, e.g. Collision detector - button data used for commands
#define LCD_KYB         6U  //43kohm to GND - B6 and B8 used for backlight
#define	MEMORY_KYB	7U  //Open

typedef union KEYBOARD_INPUT
{
	struct {
		unsigned B0:1;
		unsigned B1:1;
		unsigned B2:1;
		unsigned B3:1;
		unsigned B4:1;
		unsigned B5:1;
		unsigned B6:1;
		unsigned B7:1;
	}bits;
	INT8U byte;
}KeyboardInputType;
KeyboardInputType ui8keyboard;
INT8U 	ui8keyboard_type; //Defines type of keypad connected
INT8U	ui8group_mask;	  //Defined group for this device

typedef struct keyboardStruct
{
    INT8U type;
    KeyboardInputType keys;
    INT8U groupmask;
}keyboardType;
keyboardType activeKeyboard;

#define BUTTON_LEFT     (!activeKeyboard.keys.bits.B2)
#define BUTTON_OK       (!activeKeyboard.keys.bits.B4)
#define BUTTON_OK_ONLY  (activeKeyboard.keys.byte==0xEF)
#define BUTTON_NONE     (activeKeyboard.keys.byte==NO_INPUT)
#define BUTTON_DOWN     (!activeKeyboard.keys.bits.B0)
#define BUTTON_UP       (!activeKeyboard.keys.bits.B1)
#define BUTTON_RIGHT    (!activeKeyboard.keys.bits.B3)


#define get_keyboard()  ui8keyboard.byte=NO_INPUT;

#endif /* ends ifndef HAL_H */