/*******************************************************************************************************************
 ** Copyright (C) ROL Ergo AB
 ** ----------------------------------------------------------------------------------------
 ** IDENTIFICATION
 ** Unit Name	  control box
 ** File Name	  main.h
 **
 ** ----------------------------------------------------------------------------------------
 ** DESCRIPTION
 ** 
 **
 ** ----------------------------------------------------------------------------------------
 ** REVISION SUMMARY
 ** Rev	Date		Name					Changes from previous issue
 ** 
 ** 
 *******************************************************************************************************************/

#ifndef MANNE_H
#define	MANNE_H
#define USE_SPI
/************************************************ Dependencie  ****************************************************/
#include <xc.h>
#include "hal.h"
#include "genLinConfig.h"
#include "linmasterprocess.h"
#include "motor.h"
#include "spi_slave.h"
#include "button.h"
#include "flashSettings.h"

// *****************************************************************************
// Section: Configuration
// *****************************************************************************

typedef enum {
    INIT = 0,
    READY,
    OPEN
} CONNECTION_STATE;
extern CONNECTION_STATE connection_state;


/************************************************ definitions  ****************************************************/

/* 	addresses can be changed but each block should be 
    kept packed and the order should be kept intact 	*/

/* M1 position address block */

#define M1_SAVEDPOS_G1H_ADD		8
#define M1_SAVEDPOS_G1L_ADD		9
#define M1_SAVEDPOS_G2H_ADD		10
#define M1_SAVEDPOS_G2L_ADD		11
#define M1_SAVEDPOS_G3H_ADD		12
#define M1_SAVEDPOS_G3L_ADD		13
#define M1_SAVEDPOS_G4H_ADD		14
#define M1_SAVEDPOS_G4L_ADD		15

/* M2 position address block */
#define M2_SAVEDPOS_G1H_ADD		16
#define M2_SAVEDPOS_G1L_ADD		17
#define M2_SAVEDPOS_G2H_ADD		18
#define M2_SAVEDPOS_G2L_ADD		19
#define M2_SAVEDPOS_G3H_ADD		20
#define M2_SAVEDPOS_G3L_ADD		21
#define M2_SAVEDPOS_G4H_ADD		22
#define M2_SAVEDPOS_G4L_ADD		23

/* M3 position address block */
#define M3_SAVEDPOS_G1H_ADD		24
#define M3_SAVEDPOS_G1L_ADD		25
#define M3_SAVEDPOS_G2H_ADD		26
#define M3_SAVEDPOS_G2L_ADD		27
#define M3_SAVEDPOS_G3H_ADD		28
#define M3_SAVEDPOS_G3L_ADD		29
#define M3_SAVEDPOS_G4H_ADD		30
#define M3_SAVEDPOS_G4L_ADD		31

/* M4 position address block */
#define M4_SAVEDPOS_G1H_ADD		32
#define M4_SAVEDPOS_G1L_ADD		33
#define M4_SAVEDPOS_G2H_ADD		34
#define M4_SAVEDPOS_G2L_ADD		35
#define M4_SAVEDPOS_G3H_ADD		36
#define M4_SAVEDPOS_G3L_ADD		37
#define M4_SAVEDPOS_G4H_ADD		38
#define M4_SAVEDPOS_G4L_ADD		39

/* M5 position address block */
#define M5_SAVEDPOS_G1H_ADD		40
#define M5_SAVEDPOS_G1L_ADD		41
#define M5_SAVEDPOS_G2H_ADD		42
#define M5_SAVEDPOS_G2L_ADD		43
#define M5_SAVEDPOS_G3H_ADD		44
#define M5_SAVEDPOS_G3L_ADD		45
#define M5_SAVEDPOS_G4H_ADD		46
#define M5_SAVEDPOS_G4L_ADD		47

/* HIGH and LOW limit addresses block */
#define USER_HIGH_LIMIT_G1H_ADD         48
#define USER_HIGH_LIMIT_G1L_ADD         49
#define USER_HIGH_LIMIT_G2H_ADD         50
#define USER_HIGH_LIMIT_G2L_ADD         51
#define USER_HIGH_LIMIT_G3H_ADD         52
#define USER_HIGH_LIMIT_G3L_ADD         53
#define USER_HIGH_LIMIT_G4H_ADD         54
#define USER_HIGH_LIMIT_G4L_ADD         55

#define USER_LOW_LIMIT_G1H_ADD          56
#define USER_LOW_LIMIT_G1L_ADD          57
#define USER_LOW_LIMIT_G2H_ADD          58
#define USER_LOW_LIMIT_G2L_ADD          59
#define USER_LOW_LIMIT_G3H_ADD          60
#define USER_LOW_LIMIT_G3L_ADD          61
#define USER_LOW_LIMIT_G4H_ADD          62
#define USER_LOW_LIMIT_G4L_ADD          63

/* this will affect the group mask for the memory keyboard 
changing the memory location of this will have to synchronized with the slave application  */
#define ACTIVE_GROUP_MASK_ADD           64U
#define ACTIVE_GROUP_MASK               1
/* default group mask is set to address group 1 */

#define CALIBRATION_HIGH_ADD            65U      //Base offset for LCD display
#define CALIBRATION_LOW_ADD             66U
#define CALIBRATION_HIGH                0x18    //default offset = 63cm
#define CALIBRATION_LOW                 0xB9
#define UNITS_ADD                       67U      //Used units for LCD
#define UNITS                           0xff
#define ERRORCOUNTERS_ADD               68U     //Starting address in EEprom for 15 bytes of error counters
//      ERRORCOUNTERS_END               88U - i.e. ERRORCOUNTERS_ADD + NUMBER_OF_ERROR_LOGS

#define AUTODRIVE_ENABLED_ADD         96U
#define DEFAULT_AUTODRIVE_ENABLED     0       //Enable/Disable doubleclick for Go-to-memory
#define USEPIN_ADD                      97U
#define PIN0_ADD                        98U     //100 digit
#define PIN1_ADD                        99U     //10 digit
#define PIN2_ADD                        100U     //1 digit

#define CDS_END_MARGIN_HIGH_ADD         101U
#define CDS_END_MARGIN_LOW_ADD          102U
#define CDS_END_MARGIN_HIGH             0x01
#define CDS_END_MARGIN_LOW              0xF4    //0x1F4 = 500 = 5cm marginal

#define DEVICENAME                      "RIO HANDSET" 
#define DEVICENAME_ADD                  103U    //17 bytes 103..120

#define MASTER_DELAY_NUMBER_ADDRESS     0
#define MASTER_DELAY_OFFSET_ADDRESS     1

#ifdef USE_SPI
#define 	DEFAULT_MASTER_DELAY_NUMBER		2 //12			/* 12 for first iDesk MASTER */
#else
#define 	DEFAULT_MASTER_DELAY_NUMBER		15			/* 15 for first simple MASTER */
#endif
#define DEFAULT_MASTER_DELAY_OFFSET	14 			/* X*10ms delay, 14 => 140 ms offset                                                              */
/* Offset to compensate for C-startup execution time and eventual oscillator startup time.        */
/* Trim this to the time that remains from VCC high to main() startup delay to be typically 200ms */
/* This aims to get all master candidates to start their arbitration at the same time typically   */
#define BOOTLOADER_SWITCH_ADD       0xFF    //Set to 0 to start in bootloader mode
#define BOOTLOADER_SWITCH_VALUE     0xFF


#ifdef IDRIVE_SLAVE
//Reserv this position for the master/slave interaction

enum main_state_enum {
    NORMAL,
    HOME,
    ERROR_HARD,
    ERROR_BACKOFF,
    ERROR_STOP,
    ERROR_WAIT_FOR_BUTTON, /* Wait for button press to preform movement in opposite direction to that during error  */
    ERROR_WAIT_FOR_NON, /* Wait untill no botton is being pressed */
    MASTER_INIT_DELAY,
    SLAVE_STARTUP,
    MASTER_STARTUP,
    LIN_ARBITRATION,
    NORMAL_SLAVE

};
static volatile enum main_state_enum main_state @0x20;
#endif

#endif/* end ifndef MAIN_H */
/************************************************ END OF FILE *****************************************************/ 