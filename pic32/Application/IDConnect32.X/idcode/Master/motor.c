//
/*******************************************************************************************************************
 ** Copyright (C) ROL Ergo AB
 ** ----------------------------------------------------------------------------------------
 ** IDENTIFICATION
 ** Unit Name	  CONTROL BOX MCU
 ** File Name	  motor.c
 **
 ** ----------------------------------------------------------------------------------------
 ** DESCRIPTION
 ** This file contains LIN functionality for control box to handle data from motors. 
 **
 ** ----------------------------------------------------------------------------------------
 ** REVISION SUMMARY
 ** Rev	 Date	    Name			        Changes from previous issue
 ** 1.0	 11-08-11 	Anders Rambrant			Initial version
 ** 3.0	 11-12-06 	Anders Rambrant			LIN arbitration of motors added
 ** 4.30	 12-03-12	Khalid Obaid			ISP action and new run action select trigger 
 ** 4.10  14-06-03       Patrik Rostedt                  Error handling, error reports, overheat and overcurrent added
 ** DEPENDENCY LIST 
 **
 ** Note: 
 **
 **
 **
 **
 **
 **
 **
 *******************************************************************************************************************/

/************************************************ Dependencie  ****************************************************/


#include "manne.h"
#include "motor.h"
#include "spi_slave.h"
#include "linmasterprocess.h"
#include "button.h"
#include "genLinConfig.h"


extern bool ui1complete_arbitration;
extern INT8U ui8ArbMotorNumber;
extern bool ui1clear_limits_flag;
extern unsigned char unread_error;
/******************************************** variable declarations ***********************************************/

volatile INT16U ui16LIN_position; /* position to be sent in LIN run command */
volatile INT16U ui16highest_motor_pos; /* highest motor position received from all motors*/
volatile INT16U ui16lowest_motor_pos; /* lowest motor position received from all motors */
volatile INT16U ui16stop_motor_position; /* stop position for motor with stop distance */
volatile INT16U ui16received_motor_position; /* received motor_position */
volatile INT16U ui16group_pos[4] = {0, 0, 0, 0}; /* for storing heighest position of different groups  */
volatile INT16U ui16group_latest_pos[4] = {0, 0, 0, 0}; /* for storing the latest highest reported position of different groups */
volatile INT16U ui16group_lowest_pos[4] = {0xFFFF, 0xFFFF, 0xFFFF, 0xFFFF}; /* For storing the lowest position in each group */
volatile INT8U ui8group_ISP[4]; /* for storing group ISP information */
volatile INT8U ui8group_partial_collision[4]; /* for storing group partial collision values */
volatile INT16U ui16StartPosition;

INT16U ui16high_limit; /* the active high limit */
INT16U ui16low_limit; /* the active low limit */
INT16U ui16group_high_limit[4]; /* the lowest high limit programmed in the motors for different groups */
INT16U ui16group_low_limit[4]; /* the highest low limit programmed in the motors for different groups */
INT16U ui16near_high_limit; /* at close proximity from the high limit */
INT16U ui16near_low_limit; /* at close proximity from the low limit */
INT16U ui16high_limit_motors; /* the lowest high limit programmed in the motors overall */
INT16U ui16low_limit_motors; /* the highest low limit programmed in the motors overall */
INT16U ui16group_high_limit_motors[4]; /* the lowest high limit programmed in the motors for different groups received from motors */
INT16U ui16group_low_limit_motors[4]; /* the highest low limit programmed in the motors for different groups received from motors*/
INT16U ui16CDS_end_margin; /* Distance to limit that the CDS is inactive */
INT16U ui16highest_motor_pos_old; /* highest motor position received from all motors, saved from previous run command */
INT16U ui16lowest_motor_pos_old; /* lowest motor position received from all motors, saved from previous run command */
INT8U ui8MotorResponses = 0U; /* Used to count number of motor responses during a schedule loop, to detect missing or added motors */
INT8U ui8ArbMismatchCnt = 0U; /* Used to count number of mismatches against expected number of motor responses during a schedule loop, to get some delay */
INT8U ui8rearb_window = 0U; /* Used as a window time in which a restart arbitration will occure on missing motors detection */
INT8U ui8motion_delay_counter; /* counter for checking the homing condition */
INT8U ui8old_lin_ID;
INT8U ui8temp_motor_group; /* used to store temporary motor group number index information */
INT8U ui8temp_motor_number; /* used to store temporary motor number information, the number is calculated from the lin ID */
INT8U ui8group_index; /* motor group number index, used to access group specific information */
INT8U ui8group_list[8]; /* for storing motor group information */
INT8U ui8error_groups; /* groups with errors requiring error action */
INT8U ui8high_partial_col; /* groups with partial collision above 3 */


INT16U ui16BackOffDistanceUp = DEFAULT_BACKOFF_DISTANCE_UP;
INT16U ui16BackOffDistanceDown = DEFAULT_BACKOFF_DISTANCE_DOWN;


INT8U ui8BackOffTime = BACKOFF_TIMEOUT;

INT16U lowestVersion = 0xffff; //lowest detected motor version
INT16U highestVersion = 0x0000; //highest detected motor version

uint8_t ActiveMotorPID;
uint8_t unarbitratedMotorReplyFlag = 0;
/******************Flags***************************/
bool ui1any_motor_dec_flag = 0U;
bool ui1all_moving_or_dec_flag = 0U;
bool ui1any_above_upper_limit_flag = 0U;
bool ui1any_near_upper_limit_flag = 0U;
bool ui1any_below_lower_limit_flag = 0U;
bool ui1any_near_lower_limit_flag = 0U;
bool ui1motors_are_far_apart_flag = 0U;
bool ui1first_motor_keeping_still = 0U;


/***********Flags with extern on other files***************/
bool ui1any_motor_has_responded_flag = 0U;
bool ui1all_keeping_still_flag = 0U;
bool ui1all_overheat_flag = 0U;
bool ui1all_overcurrent_flag = 0U;
bool ui1all_at_home_flag = 0U;
bool ui1restart_arbitration_flag = 0U;



/*******************************************************************************************************************
NAME 
   reset_active_limits

DESCRIPTION
    This function handles the received data from different motors.
INPUT
    No input 

RETURNS
    No return
 *******************************************************************************************************************/
void reset_active_limits(void)
{
    INT16U ui16temp_limit;
    ui8temp_index = 0U;

    /* reset high and low limit to default values */
    ui16low_limit = 0U;
    ui16high_limit = 0xFFFFU;

    /*first check if there is any user defined low limit */
    for (ui8related_address = USER_LOW_LIMIT_G1H_ADD;ui8related_address <= USER_LOW_LIMIT_G4L_ADD;ui8related_address += 2U)
    {
        ui16temp_limit = read_eeprom_16(ui8related_address);
        if (ui16temp_limit != 0U && ui16temp_limit > ui16group_low_limit_motors[ui8temp_index])
            ui16group_low_limit[ui8temp_index] = ui16temp_limit;
        else
            ui16group_low_limit[ui8temp_index] = ui16group_low_limit_motors[ui8temp_index];/*no user defined limit means set limit back to motor limit */

        /* Select the highest overall limit as the default low limit */
        if (ui16group_low_limit[ui8temp_index] > ui16low_limit)
            ui16low_limit = ui16group_low_limit[ui8temp_index];
        ui8temp_index++;/* next group */
    }
    /*now check if there is any user defined high limit */
    ui8temp_index = 0U;/*re-initialize group index */
    for (ui8related_address = USER_HIGH_LIMIT_G1H_ADD;ui8related_address <= USER_HIGH_LIMIT_G4L_ADD;ui8related_address += 2U)
    {
        ui16temp_limit = read_eeprom_16(ui8related_address);
        if (ui16temp_limit != 0U && ui16temp_limit < ui16group_high_limit_motors[ui8temp_index])
            ui16group_high_limit[ui8temp_index] = ui16temp_limit;
        else
            ui16group_high_limit[ui8temp_index] = ui16group_high_limit_motors[ui8temp_index];

        /* Select the LOWEST overall limit as the default high limit */
        if (ui16group_high_limit[ui8temp_index] < ui16high_limit)
            ui16high_limit = ui16group_high_limit[ui8temp_index];
        ui8temp_index++;/* next group */
    }
    /* recalculate new near limit distances based on new limits */
    ui16near_high_limit = ui16high_limit - END_STOP_DISTANCE;
    ui16near_low_limit = ui16low_limit + END_STOP_DISTANCE;
}/*end of void set_active_limit(enum active_limit_enum active_limit)*/

/*******************************************************************************************************************
NAME 
   process_received_data

DESCRIPTION
    This function handles the received data from different motors.
INPUT
    None

RETURNS
    No return
 *******************************************************************************************************************/
void process_received_data(void)
{
    ui1any_motor_has_responded_flag = 1U;

    /* compare value to higher limit */
    if (ui16received_motor_position > ui16highest_motor_pos)
    {
        ui16highest_motor_pos = ui16received_motor_position;
        if (ui16highest_motor_pos > ui16high_limit)
        {
            ui1any_above_upper_limit_flag = 1U;
        }
        if (ui16highest_motor_pos >= ui16near_high_limit)
        {
            ui1any_near_upper_limit_flag = 1U;
        }
    }

    /* compare value to lower limit */
    if (ui16received_motor_position < ui16lowest_motor_pos)
    {
        ui16lowest_motor_pos = ui16received_motor_position;
        if (ui16lowest_motor_pos < ui16low_limit)
        {
            ui1any_below_lower_limit_flag = 1U;
        }
        if (ui16lowest_motor_pos <= ui16near_low_limit)
        {
            ui1any_near_lower_limit_flag = 1U;
        }
    }

    /* check state and update special flags and group ISP if applicable  */
    ui1all_overheat_flag = 0;//Assume not overheated...
    ui1all_overcurrent_flag = 0;
    switch (received_motor_run_state)
    {
        case KEEPING_STILL:
            /*if (ui8group_ISP [ui8group_index] < received_priority)
            {
                ui8group_ISP [ui8group_index] =  received_priority;
            }*/
            ui1all_moving_or_dec_flag = 0U;
            ui1all_at_home_flag = 0U;
            break;

        case AT_HOME:
            ui1all_moving_or_dec_flag = 0U;
            ui1all_keeping_still_flag = 0U;
            break;

        case DECELERATING:
            ui1all_keeping_still_flag = 0U;
            ui1all_at_home_flag = 0U;
            ui1any_motor_dec_flag = 1U;
            break;

        case KEEPING_STILL_DUE_TO_OVERHEAT:
            ui1all_overheat_flag = 1;
            ui1all_keeping_still_flag = 0U;
            ui1all_at_home_flag = 0U;
            break;
        case KEEPING_STILL_DUE_TO_OVERCURRENT:
            ui1all_overcurrent_flag = 1U;
            ui1all_keeping_still_flag = 0U;
            ui1all_at_home_flag = 0U;
            break;
        case MOVING:
        case KEEPING_STILL_WITH_ERROR:
            ui1all_keeping_still_flag = 0U;
            ui1all_at_home_flag = 0U;
            break;

        default:
            /* case HOMING: */
            ui1all_moving_or_dec_flag = 0U;
            ui1all_keeping_still_flag = 0U;
            ui1all_at_home_flag = 0U;
    }

    /* update error priority if not keeping still */
    if (received_motor_run_state != KEEPING_STILL)
    {
        if (received_partial_collision > BELOW_50)
        {
            if (received_partial_collision == ABOVE_75)
                ui8high_partial_col |= (1U << ui8group_index);/* if one 3 is received add the group to high partial col list */
            ui8group_partial_collision [ui8group_index] += received_partial_collision;
            if ((ui8group_partial_collision [ui8group_index] >= 5U) /* if partial col sum is > 5*/
                    && (ui8temp_motor_group & ui8high_partial_col)) /* and if at least one 3 is received from the groups motors */
            {
                /* update received priority to group ISP action */
                if (received_priority < ui8group_ISP [ui8group_index])
                    received_priority = ui8group_ISP [ui8group_index];
            }
        }
        if (group_error_priority [ui8group_index] < received_priority)
        {
            group_error_priority [ui8group_index] = received_priority;
        }
        // check error status 
        error_priority_set(received_priority);
    }
}

/*******************************************************************************************************************
NAME 
   get_lin_motor_data

DESCRIPTION
    This function handles the reception of the data from different slaves.  
	
INPUT
    None

RETURNS
    No return.
 *******************************************************************************************************************/


void get_lin_motor_data(void)
{
    /* check response from Motor and handle */
    /* clear flags */
    l_flg_clr_MOTOR_POS_STATUS();
    l_flg_clr_MOTOR_POS_RUN_STATE();
    l_flg_clr_MOTOR_POS_POSITION();
    l_flg_clr_MOTOR_POS_PARTIAL_COLLISION();
    if (unarbitratedMotorReplyFlag == 1)
    {
        /* Reply from an unarbirated motor received */
        /* Perform incremental arbitration */
        unarbitratedMotorReplyFlag = 0;
        ui1restart_arbitration_flag = 1U;
    }
    else
    {
        //ui8temp_motor_number = (ui8old_lin_ID & 0x0F) - (LIN_ID_8 & 0x0F);
        ui8temp_motor_number = ActiveMotorPID - 8;
        ui8temp_motor_group = ui8group_list[ui8temp_motor_number];
        switch (ui8temp_motor_group)
        {
            case 1U:
                ui8group_index = 0U;
                break;
            case 2U:
                ui8group_index = 1U;
                break;
            case 4U:
                ui8group_index = 2U;
                break;
            case 8U:
                ui8group_index = 3U;
                break;

            default:;
        }

        if (ui8temp_motor_group & ui8group.byte)/* get data from LIN frame */
        {

            ui16received_motor_position = l_u16_rd_MOTOR_POS_POSITION();
            received_priority = l_u8_rd_MOTOR_POS_STATUS();
            received_motor_run_state = l_u8_rd_MOTOR_POS_RUN_STATE();
            received_partial_collision = l_u8_rd_MOTOR_POS_PARTIAL_COLLISION();
            //			/* process received data */
            process_received_data();
        }
        //		
        //		/* get highest motor position within the group */	

        if (ui16group_pos [ui8group_index] < l_u16_rd_MOTOR_POS_POSITION())
            ui16group_pos [ui8group_index] = l_u16_rd_MOTOR_POS_POSITION();
        if (ui16group_lowest_pos[ui8group_index] > l_u16_rd_MOTOR_POS_POSITION())
            ui16group_lowest_pos [ui8group_index] = l_u16_rd_MOTOR_POS_POSITION();


    }

} /* end of get_lin_motor_data function */

/*******************************************************************************************************************
NAME 
   run_action_select

DESCRIPTION
    This function handles selecting the run action command and lin distance value to be sent on the LIN bus, 
    the function checks the button state and fills in the data according to the described condition. 
	
INPUT
    None

RETURNS
    No return
 *******************************************************************************************************************/
void run_action_select(void)
{
    INT16U ui16motor_pos_diff;/* Used to store motor position difference since last sample position */
    INT8U i;
    static INT16U ui16collisionPosition = 0;

    if (ui8rearb_window < 11U)
    {
        ui8rearb_window++;
    }

    /* Check for LIN configuration errors */
    if (ui8MotorResponses < ui8ArbMotorNumber)
    {
        /* Motor/s are missing in the communication since last arbitration */
        //TODO:ROLP
        ui8ArbMismatchCnt++;

        if (ui8ArbMismatchCnt >= ARB_MismatchNumberOf100ms)
        {

            if (ui8rearb_window <= 10U) /* redo arbitration if within reabitration window */
            {
                ui1complete_arbitration = 1U;
                ui8rearb_window = 0U;
            }
            else
            {
                error_priority_set(SOFT_STOP);
                error_type = LOST_MOTOR;
#ifdef USE_SPI
                unread_error = REPORT_ERROR_LOST_MOTOR;
#endif
            }
        }
    }
    else if (ui8MotorResponses > ui8ArbMotorNumber)
    {
        /* Motors has been added to the communication since last arbitration */
        /* Perform rearbitration to add the newcomer to the schedule */
        error_priority_set(SOFT_STOP);
        ui1restart_arbitration_flag = 1U;
    }
    else
    {
        /* Clear fail counter if no mismatches are active */
        ui8ArbMismatchCnt = 0U;
    }

    switch (main_state)
    {
        case NORMAL:
        {

            /* Calculate a fresh stop position to be used at the first STOP_ALL command */
            if (ui8motion_delay_counter == 0)
            {

                if (direction == UP)
                {

                    if (ui16highest_motor_pos <= ui16highest_motor_pos_old)
                    {
                        ui1first_motor_keeping_still = 1U;
                    }
                    if (ui16lowest_motor_pos >= ui16lowest_motor_pos_old)
                    {
                        /* true if last motor has been still or moved in ordered direction since last run_motor command */
                        ui16motor_pos_diff = ui16lowest_motor_pos - ui16lowest_motor_pos_old;/* The positive difference is a measure of the last motors's speed */

                        ui16motor_pos_diff -= LAST_MOTOR_SPEED_THRESHOLD;
                        ui16motor_pos_diff += (ui16motor_pos_diff << 1);/* Multiply by 3 */
                        ui16motor_pos_diff += THRESHOLD_STOP_DISTANCE;
                        /*if statement added by khalid obaid - compensation for a motor that is very much behind */

                        if (ui16highest_motor_pos > ui16lowest_motor_pos + FIRST_MOTOR_STOP_DIST)
                        {
                            ui16stop_motor_position = ui16lowest_motor_pos + ui16motor_pos_diff;
                            ui1motors_are_far_apart_flag = 1U;
                        }
                        else
                        {
                            ui16stop_motor_position = ui16highest_motor_pos + ui16motor_pos_diff + FIRST_TO_LAST_CORR;
                        }

                    }

                }
                else /* (direction == DOWN) */
                {
                    if (ui16lowest_motor_pos >= ui16lowest_motor_pos_old)
                    {
                        ui1first_motor_keeping_still = 1U;
                    }
                    if (ui16highest_motor_pos_old >= ui16highest_motor_pos)
                    {
                        /* true if last motor has been still or moved in ordered direction since last run_motor command */
                        ui16motor_pos_diff = ui16highest_motor_pos_old - ui16highest_motor_pos;/* The difference is a measure of the last motors's speed */
                        ui16motor_pos_diff -= LAST_MOTOR_SPEED_THRESHOLD;
                        ui16motor_pos_diff += (ui16motor_pos_diff << 1);
                        ui16motor_pos_diff += THRESHOLD_STOP_DISTANCE;/* Multiply by 3 */
                        /*if statement added by khalid obaid - compensation for a motor that is very much behind */

                        if ((ui16highest_motor_pos - ui16lowest_motor_pos) > FIRST_MOTOR_STOP_DIST)
                        {
                            ui16stop_motor_position = ui16highest_motor_pos - ui16motor_pos_diff;
                            ui1motors_are_far_apart_flag = 1U;
                        }
                        else
                        {
                            ui16stop_motor_position = ui16lowest_motor_pos - ui16motor_pos_diff - FIRST_TO_LAST_CORR;
                        }
                    }


                }

                //check each group for sync
                if (error_type == NO_ERROR || error_type == OUT_OF_SYNC)
                {
                    error_type = NO_ERROR;
                    for (i = 0;i < 4;i++)
                        if (ui16group_lowest_pos[i] != DEFAULT_LOWEST_POS && ui16group_pos[i] != DEFAULT_HIGHEST_POS
                                && ((ui16group_pos[i] - ui16group_lowest_pos[i]) > WARNING_OUT_OF_SYNC))
                            error_type = OUT_OF_SYNC;
                }

            }
            /* Any button combination behavior */
            /* Prio 1a */
            if (error_priority > SOFT_STOP)
            {
                ui8error_groups = 0U;
                /* collect the info about groups with higher errors */
                for (ui8group_index = 0U;ui8group_index < 4U;ui8group_index++)
                {
                    if (group_error_priority[ui8group_index] > SOFT_STOP)
                    {
                        ui8error_groups |= (1U << ui8group_index);
                    }

                }
                error_direction = direction;
                run_action = KEEP_STILL;
                ui16LIN_position = ui16highest_motor_pos;
                ui8motion_delay_counter = 0U;
                main_state = ERROR_HARD;
                break;
            }

            /* Prio 2a */

            if ((ui1all_moving_or_dec_flag) && (error_priority == SOFT_STOP) && (ui8motion_delay_counter < 3U))
            {
                run_action = STOPP;
                ui16LIN_position = ui16stop_motor_position;
                ui8motion_delay_counter++;
                break;
            }

            /* Prio 2b */

            if ((ui1any_motor_dec_flag) && (ui8motion_delay_counter < 3U))
            {
                run_action = STOPP;
                ui16LIN_position = ui16stop_motor_position;
                ui8motion_delay_counter++;
                break;
            }

            /* Prio 3 */
            /* if ui8stop_timer >= 3U issue KEEP_STILL command*/
            if (ui8motion_delay_counter >= 3U)
            {
                run_action = KEEP_STILL;
                ui16LIN_position = ui16highest_motor_pos;
                ui8motion_delay_counter = 0U;
                break;
            }

            /* end of Any combination behavior if statement */

            /* Prio > 3  --> button combination specific behaviour */

            switch (control_action)
            {
                case UP_ONLY:
                {

                    /* Prio 4a */
                    if ((ui1any_above_upper_limit_flag) && (!ui1motors_are_far_apart_flag))
                    {
                        /* KEEP_STILL if at the limit */
                        run_action = KEEP_STILL;
                        ui16LIN_position = ui16highest_motor_pos;
                        ui8motion_delay_counter = 0U;

                        break;
                    }

                    /* Prio 4b */
                    if ((ui1all_keeping_still_flag) && (ui1any_near_upper_limit_flag) && (!ui1motors_are_far_apart_flag))
                    {
                        /* KEEP_STILL if at the limit */
                        run_action = KEEP_STILL;
                        ui16LIN_position = ui16highest_motor_pos;
                        ui8motion_delay_counter = 0U;

                        break;
                    }

                    /* Prio 5 */
                    if (error_priority > NO_ACTION)
                    {
                        /* KEEP_STILL if any error */
                        run_action = KEEP_STILL;
                        ui16LIN_position = ui16highest_motor_pos;
                        ui8motion_delay_counter = 0U;
                        break;
                    }

                    /* Prio 6 */
                    if ((ui1all_moving_or_dec_flag) && (ui1any_near_upper_limit_flag) && (!ui1motors_are_far_apart_flag))
                    {
                        /* STOP if near the limit */
                        run_action = STOPP;
                        if (ui8motion_delay_counter == 0U)
                        {
                            if (ui1first_motor_keeping_still)
                            {
                                if (ui16highest_motor_pos > ui16high_limit)
                                {
                                    ui16stop_motor_position = ui16high_limit;
                                }
                                else
                                {
                                    ui16stop_motor_position = ui16highest_motor_pos;
                                }
                            }
                            else
                            {
                                ui16stop_motor_position = ui16high_limit;
                            }
                        }
                        //ui16stop_motor_position = HIGH_LIMIT_H * 256 + HIGH_LIMIT_L;
                        ui16LIN_position = ui16stop_motor_position;
                        ui8motion_delay_counter++;
                        break;
                    }

                    /* Prio 7 */
                    /* Normal move in up direction */

                    run_action = MOVE_UP;

                    direction = UP;
                    ui16LIN_position = ui16lowest_motor_pos;
                    ui8motion_delay_counter = 0U;
                    break;


                }/* end of case UP_ONLY for main_state NORMAL */
                case DOWN_ONLY:
                {
                    /* Prio 4a */
                    if ((ui1any_below_lower_limit_flag) && (!ui1motors_are_far_apart_flag))
                    {
                        /* KEEP_STILL if at the limit */
                        run_action = KEEP_STILL;
                        ui16LIN_position = ui16highest_motor_pos;
                        ui8motion_delay_counter = 0U;
                        break;
                    }

                    /* Prio 4b */
                    if ((ui1all_keeping_still_flag) && (ui1any_near_lower_limit_flag) && (!ui1motors_are_far_apart_flag))
                    {
                        /* KEEP_STILL if at the limit */
                        run_action = KEEP_STILL;
                        ui16LIN_position = ui16highest_motor_pos;
                        ui8motion_delay_counter = 0U;
                        break;
                    }

                    /* Prio 5 */
                    if (error_priority > NO_ACTION)
                    {
                        /* KEEP_STILL if any error */
                        run_action = KEEP_STILL;
                        ui16LIN_position = ui16highest_motor_pos;
                        ui8motion_delay_counter = 0U;
                        break;
                    }


                    /* Prio 6 */
                    if ((ui1all_moving_or_dec_flag) && (ui1any_near_lower_limit_flag)&& (!ui1motors_are_far_apart_flag))
                    {
                        /* STOP if near the limit */
                        run_action = STOPP;
                        if (ui8motion_delay_counter == 0U)
                        {
                            if (ui1first_motor_keeping_still)
                            {
                                if (ui16lowest_motor_pos < ui16low_limit)
                                {
                                    ui16stop_motor_position = ui16low_limit;
                                }
                                else
                                {
                                    ui16stop_motor_position = ui16lowest_motor_pos;
                                }
                            }
                            else
                            {
                                ui16stop_motor_position = ui16low_limit;
                            }
                        }
                        //ui16stop_motor_position = LOW_LIMIT_H * 256 + LOW_LIMIT_L;	
                        ui16LIN_position = ui16stop_motor_position;
                        ui8motion_delay_counter++;
                        break;
                    }

                    /* Prio 7 */

                    run_action = MOVE_DOWN;
                    direction = DOWN;
                    ui16LIN_position = ui16highest_motor_pos;
                    ui8motion_delay_counter = 0U;
                    break;
                }/* end of case DOWN_ONLY for main_state NORMAL */

                default:
                    /* covers case DONT_MOVE for main_state NORMAL */
                {
                    /* Prio 4 */
                    if (ui1all_moving_or_dec_flag)
                    {
                        run_action = STOPP;
                        ui16LIN_position = ui16stop_motor_position;
                        ui8motion_delay_counter++;
                        break;
                    }

                    /* Prio 5 */
                    if (ui1all_keeping_still_flag)
                    {
                        run_action = KEEP_STILL;
                        ui16LIN_position = ui16highest_motor_pos;
                        ui8motion_delay_counter = 0U;
                        /* clear all errors */
                        l_bool_wr_MOTOR_RUN_RESET_ALL_ERRORS(1U);
                        break;
                    }

                    /* Prio 6 */
                    run_action = KEEP_STILL;
                    ui16LIN_position = ui16highest_motor_pos;
                    ui8motion_delay_counter = 0U;

                } /* end of case DONT_MOVE for main_state NORMAL */
            }/* end of button combination specific behaviour swtich statement for main_state NORMAL */

            if (ui16LIN_position == 0)
                ui16LIN_position = ui16received_motor_position;//Do not trigger homing
            //TODO:Check homing


            break;
        } /* End of NORMAL case*/

        case HOME:
        {

            ui8group.byte = 15U;/* address all groups regardless of requested group */
            switch (control_action)
            {
                case DONT_MOVE:
                {
                    /* Prio 1 */
                    if ((ui1all_at_home_flag)&& (!ui1clear_limits_flag))
                    {
                        run_action = KEEP_STILL;
                        ui8motion_delay_counter = 0U;
                        ui16LIN_position = ui16highest_motor_pos;
                        main_state = NORMAL;
                        break;
                    }

                    /* Prio 2 */
                    run_action = KEEP_STILL;
                    ui8motion_delay_counter = 0U;
                    ui16LIN_position = ui16highest_motor_pos;
                    /* clear all errors */
                    l_bool_wr_MOTOR_RUN_RESET_ALL_ERRORS(1U);
                    break;
                } /* End case DONT_MOVE for main_state HOME */

                case DOWN_ONLY:
                {
                    /* Prio 1 */
                    if (error_priority > 0)
                    {
                        run_action = KEEP_STILL;
                        ui8motion_delay_counter = 0U;
                        ui16LIN_position = ui16highest_motor_pos;
                        break;
                    }

                    /* Prio 2 */
                    run_action = MOVE_DOWN;
                    ui8motion_delay_counter = 0U;
                    ui16LIN_position = 0U;
                    break;
                } /* End case DOWN_ONLY for main_state HOME */

                default:
                {
                    /* covers cases UP_ONLY and BOTH_BUTTONS */
                    run_action = KEEP_STILL;
                    ui8motion_delay_counter = 0U;
                    ui16LIN_position = ui16highest_motor_pos;
                } /* End of UP_ONLY and BOTH_BUTTONS for main_state HOME */
            }
            break;
        }/* End of HOME case*/

        case ERROR_HARD:
        {

            /* address only groups with backoff error irrespective of the requested groups */
            ui8group.byte = ui8error_groups;
            ui16LIN_position = ui16highest_motor_pos;
            run_action = KEEP_STILL;

            /* Prio 1*/
            if ((error_priority == AUTO_BACK_OFF || error_type == COLLISION_DETECTED))
            {

                if (error_direction == UP)
                {
                    ui16collisionPosition = ui16highest_motor_pos - ui16BackOffDistanceUp;
                    if (ui16collisionPosition < ui16StartPosition)
                        ui16collisionPosition = ui16StartPosition + REBOUND_CALIBRATION_UP;
                }
                else
                {
                    ui16collisionPosition = ui16lowest_motor_pos + ui16BackOffDistanceDown;
                    if (ui16collisionPosition > ui16StartPosition)
                        ui16collisionPosition = ui16StartPosition - REBOUND_CALIBRATION_DOWN;
                }
                ui8motion_delay_counter = 0U;
                main_state = ERROR_BACKOFF;
                break;
            }/*end of Prio 1*/

            /*Prio 2 */
            if (activeKeyboard.keys.byte == NO_INPUT) //control_action == DONT_MOVE)
            {//Wait for button to be released
                ui8motion_delay_counter = 0U;
                main_state = ERROR_WAIT_FOR_BUTTON;
                break;
            }

            /* prio 3 */
            ui8motion_delay_counter++;

            break;
        }/* end of ERROR_HARD state*/

        case ERROR_BACKOFF:
        {
            /* address only groups with backoff error irrespective of the requested groups */
            ui8group.byte = ui8error_groups;

            /* Prio 1 */
            if (error_direction == UP)
            {
                run_action = MOVE_DOWN;
                direction = DOWN;
                ui16LIN_position = ui16highest_motor_pos;
                if (ui16LIN_position < ui16collisionPosition)
                {
                    main_state = ERROR_STOP;
                    ui8motion_delay_counter = 0U;
                    break;
                }
            }
            else
            {
                run_action = MOVE_UP;
                direction = UP;
                ui16LIN_position = ui16lowest_motor_pos;
                if (ui16LIN_position > ui16collisionPosition)
                {
                    main_state = ERROR_STOP;
                    ui8motion_delay_counter = 0U;
                    break;
                }
            }

            /* Prio 2*/
            if (ui8motion_delay_counter > ui8BackOffTime) //7U) //timeout
            {
                ui8motion_delay_counter = 0U;
                main_state = ERROR_STOP;
                break;
            }/*end of Prio 1*/

            /*Prio 2 */
            ui8motion_delay_counter++;
            break;
        }/* end of ERROR_BACKOFF state*/

        case ERROR_WAIT_FOR_BUTTON:
        {
            /* address only groups with backoff error irrespective of the requested groups */
            ui8group.byte = ui8error_groups;
            if (activeKeyboard.keys.byte != NO_INPUT) //control_action != DONT_MOVE)
            {
                if (error_direction == UP)
                {
                    run_action = MOVE_DOWN;
                    direction = DOWN;
                    ui16LIN_position = ui16highest_motor_pos;

                }
                else
                {
                    run_action = MOVE_UP;
                    direction = UP;
                    ui16LIN_position = ui16lowest_motor_pos;
                }

                /* Prio 1*/
                if (ui8motion_delay_counter >= 7U)
                {
                    ui8motion_delay_counter = 0U;
                    main_state = ERROR_STOP;
                    break;
                }/*end of Prio 1*/

                /*Prio 2 */
                ui8motion_delay_counter++;

            }
            else
            {
                if (ui8motion_delay_counter > 0U)
                {
                    if (error_direction == UP)
                    {
                        run_action = MOVE_DOWN;
                        direction = DOWN;
                        ui16LIN_position = ui16highest_motor_pos;

                    }
                    else
                    {
                        run_action = MOVE_UP;
                        direction = UP;
                        ui16LIN_position = ui16lowest_motor_pos;
                    }
                    ui8motion_delay_counter = 0U;
                    main_state = ERROR_STOP;
                }
                else
                {
                    run_action = KEEP_STILL;
                    ui16LIN_position = ui16highest_motor_pos;
                    ui8motion_delay_counter = 0U;
                }
            }
            break;
        }/* end of ERROR_WAIT_FOR_OPPOSITE state*/


        case ERROR_STOP:
        {
            /* address only groups with backoff error irrespective of the requested groups */
            ui8group.byte = ui8error_groups;
            run_action = STOPP;
            ui16LIN_position = ui16stop_motor_position;

            /* Prio 1 */
            if (ui8motion_delay_counter >= 3U)
            {
                ui8motion_delay_counter = 0U;
                main_state = ERROR_WAIT_FOR_NON;
                break;
            }/*end of Prio 1*/

            /*Prio 2 */
            ui8motion_delay_counter++;
            break;
        }/* end of ERROR_STOP state*/


        case ERROR_WAIT_FOR_NON:
        {
            /* address only groups with backoff error irrespective of the requested groups */
            ui8group.byte = ui8error_groups;
            run_action = KEEP_STILL;
            ui16LIN_position = ui16highest_motor_pos;
            /* Prio 1 */
            getSlaveInputs();
            if (activeKeyboard.keys.byte == NO_INPUT) //control_action == DONT_MOVE)
            {
                ui8motion_delay_counter = 0U;
                main_state = NORMAL;
                error_type = NO_ERROR;
                l_bool_wr_MOTOR_RUN_RESET_ALL_ERRORS(1U);
                break;
            }/*end of Prio 1*/

            ui8motion_delay_counter = 0U;
            break;
        }/* end of ERROR_WAIT_FOR_NON state*/
        


        default:;

    } /* END of main_state switch statement*/

    /* run all motor groups */
    l_u8_wr_MOTOR_RUN_GROUP_MASK(ui8group.byte);
    /* transfer the calculated position and run command to the LIN stack */
    l_u16_wr_MOTOR_RUN_POSITION(ui16LIN_position);
    l_u8_wr_MOTOR_RUN_ACTION(run_action);
} /* End of run action select function */

/*******************************************************************************************************************
NAME 
   sync_lin_motor_data

DESCRIPTION
    This function resets all motor positions and flags to neutral values. 
    The get_lin_motor_data() and process_received_data() functions will adjust the data as it becomes
    available after reception from each motor. 
	
INPUT
    None

RETURNS
    No return
 *******************************************************************************************************************/
void sync_lin_motor_data(void)
{
    /* Reset motor response counter */
    ui8MotorResponses = 0U;

    /* Save positions for speed factor in stop_motor_pos calculation */
    ui16highest_motor_pos_old = ui16highest_motor_pos;
    ui16lowest_motor_pos_old = ui16lowest_motor_pos;

    /* initalize highest and lowest positions */
    ui16highest_motor_pos = DEFAULT_HIGHEST_POS;
    ui16lowest_motor_pos = DEFAULT_LOWEST_POS;

    /* intialize flags */
    ui1all_moving_or_dec_flag = 1U;
    ui1all_keeping_still_flag = 1U;
    ui1all_at_home_flag = 1U;
    ui1any_motor_dec_flag = 0U;
    ui1any_motor_has_responded_flag = 0U;


     for (ui8group_index = 0U;ui8group_index < 4U;ui8group_index++)
     {
         ui16group_pos [ui8group_index] = DEFAULT_HIGHEST_POS;
         ui16group_lowest_pos [ui8group_index] = DEFAULT_LOWEST_POS;
         //ui8group_ISP [ui8group_index] = 0U;
         ui8group_partial_collision [ui8group_index] = 0U;
         group_error_priority [ui8group_index] = NO_ACTION;
     }


    /* clear all errors */
    error_priority = NO_ACTION;

    /*clear limit flags */
    ui1any_above_upper_limit_flag = 0U;
    ui1any_near_upper_limit_flag = 0U;
    ui1any_below_lower_limit_flag = 0U;
    ui1any_near_lower_limit_flag = 0U;

    ui1motors_are_far_apart_flag = 0U;
    ui1first_motor_keeping_still = 0U;

} /* End of sync_lin_motor_data function */

/*******************************************************************************************************************
NAME 
   error_priority_set

DESCRIPTION
    set the error priority to be transmitted on the lin bus 
	
	
INPUT
    None

RETURNS
    No return
 *******************************************************************************************************************/
void error_priority_set(enum error_priority_enum priority)
{
    /* see if new priority is higher than old priority */
    if (priority > error_priority)
    {
        error_priority = priority;
    }
}/*end of error_priority_set function */

/****************************************** END OF FILE ***********************************************************/