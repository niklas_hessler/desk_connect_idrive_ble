/*******************************************************************************************************************
 ** Copyright (C) ROL Ergo AB
 ** ----------------------------------------------------------------------------------------
 ** IDENTIFICATION
 ** Unit Name	  CONTROL BOX MCU
 ** File Name	  interface.c
 **
 ** ----------------------------------------------------------------------------------------
 ** DESCRIPTION
 ** This file contains main function for interface card functionality
 **
 ** ----------------------------------------------------------------------------------------
 **
 **
 *******************************************************************************************************************/

/************************************************ Dependencie  ****************************************************/

#include "manne.h"
#include "interface.h"
#include "accessory_process.h"
#include "linmasterprocess.h"

extern uint8_t MasterCMD[8];
//Process for reading or writing values to iDrive with retries and reports

void ReadWriteConfig(void)
{
    UINT8 dataState = 0;
    static UINT8 retries = 0;

    switch (processData.configState)
    {
        case CONFIG_READY:break;

        case CONFIG_READ:
            processData.configState = CONFIG_READ_AND_WAIT;
            ui8_wr_MASTER_REQ_KEY(ARB_GenericKey);
            retries = 0;
            break;
        case CONFIG_WRITE:
            processData.configState = CONFIG_WRITE_AND_WAIT;
            retries = 0;
            break;

        case CONFIG_READ_AND_WAIT:
            dataState = readWriteByteFromSlave(1, processData.configAddress, &processData.configData);
        case CONFIG_WRITE_AND_WAIT:
            if (processData.configState == CONFIG_WRITE_AND_WAIT)
                dataState = readWriteByteFromSlave(0, processData.configAddress, &processData.configData);
            if (dataState == READ_NO_REPLY || dataState == READ_CORRUPT)
            {
                if (retries < 3)
                    retries++;
                else
                {
                    processData.configState = CONFIG_FAILURE;
                    memset(MasterCMD,0xFF,8);
                }
                    

            }
            else if (dataState == READ_BYTE_RECIEVED)
            {//Successfully written
                processData.configState = CONFIG_SUCCESS;
                memset(MasterCMD,0xFF,8);
            }
            break;

        case CONFIG_FAILURE:
        case CONFIG_SUCCESS:
        
            break;
    }
}/* end of program_recipe function */


