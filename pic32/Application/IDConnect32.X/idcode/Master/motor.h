/*******************************************************************************************************************
 ** Copyright (C) ROL Ergo AB
 ** ----------------------------------------------------------------------------------------
 ** IDENTIFICATION
 ** Unit Name	  CONTROL BOX MCU
 ** File Name	  motor.h
 **
 ** ----------------------------------------------------------------------------------------
 ** DESCRIPTION
 ** This file contains LIN functionality for control box
 **
 ** ----------------------------------------------------------------------------------------
 ** REVISION SUMMARY
 ** Rev	 Date	    Name			        Changes from previous issue
 ** 1.0	 11-08-11 	Anders Rambrant			Initial version
 ** 1.1	 29-11-11	Khalid Obaid			adapted to new lin stack with new specifications dated ---------
 **											* new error structure
 **											* new motor group structure
 **
 **
 ** DEPENDENCY LIST
 **
 ** Note:
 **
 **
 **
 **
 **
 **
 **
 *******************************************************************************************************************/

#ifndef MOTOR_H
#define MOTOR_H

/************************************************ Dependencie  ****************************************************/
#include "hal.h"
#include "genLinConfig.h"
/************************************************ definitions  ****************************************************/

#define LIN_PWM 82U
#define TOP_POSITION 8190U             /* Highest position number divisible with 3 */
#define NEAR_LIMIT_STOP_DISTANCE 72U   /* Deceleration distance to near limit stop = 7.2mm */
#define END_STOP_DISTANCE 111U         /* Deceleration distance to stop at end = 11.1mm - changed by khalid to requested value */
#define LAST_MOTOR_SPEED_THRESHOLD 15U /* Minimum movement 2.0mm per 100ms on last motor to increase the stop distance */
#define THRESHOLD_STOP_DISTANCE 24U    /* Stop distance 3.6mm at speeds below threshold */
#define FIRST_TO_LAST_CORR 30U
#define LOW_LIMIT_H 0U
#define LOW_LIMIT_L 135U
#define HIGH_LIMIT_H 19U
#define HIGH_LIMIT_L 86U
#define DEFAULT_HIGHEST_POS 0x00U
#define DEFAULT_LOWEST_POS 0xFFFFU
#define FIRST_MOTOR_STOP_DIST 123U
#define WARNING_OUT_OF_SYNC 500U //Triggers warning if distance between lowest and highest are larger than this

#define LOW_VOLTAGE_THRESHOLD 228  //16vdc
#define HIGH_VOLTAGE_THRESHOLD 592 //40vdc


#define REBOUND_CALIBRATION_UP 120        //difference in stop distance on rebound after collsion going up
#define REBOUND_CALIBRATION_DOWN 145      //difference in stop distance on rebound after collsion going down
#define DEFAULT_BACKOFF_DISTANCE_UP 396   //2 tum...
#define DEFAULT_BACKOFF_DISTANCE_DOWN 371 //2 tum...


#define BACKOFF_TIMEOUT 28 //Timeout before giving up on reaching position (7 original before BackOffDistance, 28 tested during 2" backoff)
/************************************************** Macros ********************************************************/

/********************************************* enum declarations **************************************************/

enum direction_enum {
    DOWN,
    UP
};

enum run_action_enum {
    KEEP_STILL,
    MOVE_DOWN,
    MOVE_UP,
    STOPP
};

enum motor_run_state_enum {
    KEEPING_STILL,
    AT_HOME,
    MOVING,
    DECELERATING,
    HOMING,
    KEEPING_STILL_WITH_ERROR,
    KEEPING_STILL_DUE_TO_OVERHEAT,
    KEEPING_STILL_DUE_TO_OVERCURRENT
};

enum main_state_enum {
    NORMAL,
    HOME,
    ERROR_HARD,
    ERROR_BACKOFF,
    ERROR_STOP,
    ERROR_WAIT_FOR_BUTTON, /* Wait for button press to preform movement in opposite direction to that during error  */
    ERROR_WAIT_FOR_NON, /* Wait untill no botton is being pressed */
    MASTER_INIT_DELAY,
    SLAVE_STARTUP,
    MASTER_STARTUP,
    LIN_ARBITRATION,
    NORMAL_SLAVE,
    CONFIG,
    LIN_SNIFFER
};

enum error_priority_enum {
    NO_ACTION,
    SOFT_STOP,
    MANUAL_BACK_OFF,
    AUTO_BACK_OFF
};

enum error_type_enum {
    NO_ERROR = 0,
    //ERROR_HARD=1,
    //ERROR_BACKOFF=2,
    //ERROR_STOP=3,
    //ERROR_WAIT_BUTTON=4,
    //ERROR_WAIT_NO_BUTTON=5,
    LOST_MOTOR = 6,
    //ERROR_OVERCURRENT=7,
    //ERROR_OVERHEAT=8,
    LOW_VOLTAGE = 9,
    HIGH_VOLTAGE = 10,
    OUT_OF_SYNC = 11,
    ABOVE_UPPER_LIMIT = 12,
    BELOW_LOWER_LIMIT = 13,
    INTERNAL_ERROR = 14,
    COLLISION_DETECTED = 15,
    WRONG_NUMBER_OF_MOTORS = 16,
    ABOVE_UPPER_MOTOR_LIMIT = 17,
    BELOW_LOWER_MOTOR_LIMIT = 18,
    SLAVE_LOST = 19,
    MOTOR_NOT_MOVING = 20
};

enum active_limit_enum {
    MOTOR_LIMITS,
    MEMORY_POSITION,
    USER_LIMITS
};

enum partial_collision_enum {
    BELOW_25,
    BELOW_50,
    BELOW_75,
    ABOVE_75
};

enum motor_run_state_enum received_motor_run_state;
enum error_priority_enum received_priority;
enum partial_collision_enum received_partial_collision;
enum error_type_enum error_type;
enum run_action_enum run_action;
//#ifdef __DEBUG
enum main_state_enum main_state;
//#else
//	static volatile enum main_state_enum 		main_state	@0x20;
//#endif
enum direction_enum direction;
enum direction_enum error_direction;
enum error_priority_enum error_priority;
enum error_priority_enum group_error_priority[4];

/************************************************** structures ****************************************************/

/* motor groups modified according to new specifications - khalid Obaid */

union MOTOR_GROUPS {

    struct {
        unsigned group_1 : 1; /* Group 1 flag */
        unsigned group_2 : 1; /* Group 2 flag */
        unsigned group_3 : 1; /* Group 3 flag */
        unsigned group_4 : 1; /* Group 4 flag */
    } bits;
    INT8U byte;
} ui8group;



/********************************************* function prototypes ************************************************/

void run_action_select(void);
void get_lin_motor_data(void);
void process_received_data(void);
void sync_lin_motor_data(void);
void error_priority_set(enum error_priority_enum priority);
void reset_active_limits(void);

#endif /*end def MOTOR_H*/
/************************************************ END OF FILE *****************************************************/