/*******************************************************************************************************************
** Copyright (C) ROL Ergo AB
** ----------------------------------------------------------------------------------------
** IDENTIFICATION
** Unit Name	  CONTROL BOX MCU
** File Name	  interface.h
**
** ----------------------------------------------------------------------------------------
** DESCRIPTION
** 150127   Patrik Rostedt      Rebuilt for simpler boxes without PLC etc
**
*******************************************************************************************************************/
#ifndef INTERFACE_H
#define INTERFACE_H

/************************************************ Dependencie  ****************************************************/

/* memorey addresses */
#define 	TOOL_ID_ADDR 		255		// Tool ID , must be unique to different tools!
#define 	SEQUENCE_NBR_ADDR	252

#define 	ADD_SEQ_NO_1 		0x12		// address to store sequence number HIGH
#define 	ADD_SEQ_NO_2 		0x13		// address to store sequence number MEDIUM
#define 	ADD_SEQ_NO_3 		0x14		// address to store sequence number LOW

#define 	LL_ADD 			10		// Low limit address	- High byte 
#define 	UL_ADD 			12		// Upper limit address  - High byte 
#define 	MS_ADD			17U		// Max PWM address
#define		SS_ADD			28U		// Start PWM address
#define		HS_ADD		 	29U		// Homing PWM address
#define 	MG_ADD 			9U		// Motor Group address
#define 	IA_ADD 			24U		// Error action at collision address
#define 	IC_ADD 			25U		// ISP current address	- High byte 
#define 	IT_ADD 			27U		// ISP time address
#define 	IF_ADD			36U		// ISP fast time constant factor address
#define 	IS_ADD			37U		// ISP slow time constant factor address 
#define 	IP_ADD			38U		// ISP partial collision detection address 
#define 	MC_ADD			30U		// Max current address
#define 	HC_ADD			31U		// Homing current address
#define		OL_ADD			32U		// Temperature Limit address - High byte 
#define		OH_ADD			34U		// Temperature hysteresis address
#define 	OT_ADD			35U		// Temerature time constant
#define         RA_ADD                  40              //ramp up factor
#define         RD_ADD                  41              //ramp down factor
#define         NR_ADD                  42              //Number of motor required
#define         UN_ADD                  43              //Units
#define         BO_ADD                  44              //Back off time
#define         SU_ADD                  45              //Speed up
#define         SD_ADD                  46              //Speed down
#define         ES_ADD                  47              //error storage

#define         LV_ADD                  48              //Low voltage limit, high address
#define         HV_ADD                  50              //High voltage limit, high address
#define         VL_ADD                  52              //(linear) voltage limit, high address
#define         SV_ADD                  54              //Storage voltage level, high address

#define         RT_ADD                  56              //Rampup time, High address

#define 	LIMIT_OFFSET		99U

enum recipe_state_enum
{
    CONFIG_READY,
    CONFIG_WRITE,
    CONFIG_WRITE_AND_WAIT,
    CONFIG_READ,
    CONFIG_READ_AND_WAIT,
    CONFIG_SUCCESS,
    CONFIG_FAILURE
};

void ReadWriteConfig(void);

#endif 
/************************************************ END OF FILE *****************************************************/



