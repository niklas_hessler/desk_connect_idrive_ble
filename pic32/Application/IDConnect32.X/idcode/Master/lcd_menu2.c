#include "lcd_menu2.h"
#include "hal.h"
#include "manne.h"
#include "lcd.h"
#include "lcd_menu.h"
#include "mcc.h"
#include "motor.h"
#include "button.h"
#include "spi_slave.h"
#include "linmasterprocess.h"
extern const unsigned char decimal_lcd_digit[10];
unsigned char activeMemory=0;
extern volatile INT16U ui16LIN_position;
extern INT16U highestVersion;
extern uint16_t ui16temp;
extern unsigned char lcd_digit[4];
#ifndef BOOTLOADER
unsigned char usePin=0, pin[3]={0,0,0};
#endif
extern DeskProfile deskProfile;
void NextMenuState()
{
    if (BUTTON_UP) menustate-=11;
    if (BUTTON_DOWN) menustate+=9;
    if (BUTTON_OK) menustate++;
}

void lcd_menu2(void)
{
#ifndef TESTCYCLES
    static unsigned char menuEnterType;
#ifndef BOOTLOADER
    static unsigned char tempPin[3]={10,10,10},ucTemp;
#endif
    
    getSlaveInputs();

    backlightAndErrors();

    switch(menustate)
    {
        /********************************************************************************************************/
        /* Main state                                                                                           */
        /********************************************************************************************************/
        case 11:
            if (ui16LIN_position>25 && ui16LIN_position<TOP_POSITION)
            {
                display=toUnit(ui16LIN_position+deskProfile.deskOffset);
                if (display!=oldDisplay)
                {//Only update display of changes been made
                    ShowSymbols(SHOW_UNITS);
                    for (selected_mem=0;selected_mem<5;selected_mem++)
                    {
                        getMemoryPosition(selected_mem);
                        for (ui8temp_index=0;ui8temp_index<4;ui8temp_index++)
                        {
                            if ((activeKeyboard.groupmask & 1<<ui8temp_index) &&
                                (ui16temp_saved_pos[ui8temp_index]!=0) &&
                                (ui16LIN_position>ui16temp_saved_pos[ui8temp_index]-35) &&
                                (ui16LIN_position<ui16temp_saved_pos[ui8temp_index]+35))
                            {
                                ShowSymbols(SHOW_MEMORY | SHOW_UNITS);
                                selected_mem=5;
                            }
                        }
                    }

                    if (display>1999)
                        lcd_display_int(display/10,0);
                    else
                        lcd_display_int(display,1); //units);

                    oldDisplay=display;
                }
            }
            
            if (BUTTON_OK_ONLY)
            {
                menutimer++;
                menuEnterType=activeKeyboard.type;
                    if (menutimer==3000)
                    { //indicate mem menu
                        ShowSymbols(SHOW_MEMORY);
                        lcd_clear();
                    }
                else if (menutimer==8000)
                    {
                        ShowSymbols(0);
#ifdef BOOTLOADER
                        lcd_display_string(LCD_STRING_PC);
#else
                        lcd_display_string(LCD_STRING_INVALID);
#endif
                    }
                }
            else if (menutimer>100)
            {//Button released
                if (menutimer<3000){ menutimer=0; menustate=20; selected_mem=0; uiTemp=0; } //short press - quick set memory
                else if (menuEnterType==VISUAL_KYB)
                {
                    if (menutimer<8000)
                    menustate=50;//>8 sek press - memory menu
                else menustate=90; //long press - settings menu
            }
            }
            else menutimer=0;   //no button pressed
            if (main_state==HOME) menustate=0;
            break;


        /********************************************************************************************************/
        /* Quick set memory                                                                                     */
        /********************************************************************************************************/
        case 20:
            if (menutimer<500) ShowSymbols(SHOW_MEMORY | SHOW_SPANNER); else ShowSymbols(SHOW_SPANNER);
            lcd_update();
            if (BUTTON_NONE) menustate++;
            uiTemp++;
            if (uiTemp>10) menustate=10; //Timeout
            break;
        case 21:
            if (BUTTON_DOWN){ selected_mem=3; menutimer=0; menustate=13;}
            else if (BUTTON_UP){ selected_mem=4; menutimer=0; menustate=13;}
            else if (BUTTON_OK) menustate=10;
            else
            {
                menutimer++;
                if (menutimer==500) menustate=20;
                else if (menutimer==1000){ menutimer=0; menustate=20; }
            }
            break;


        /********************************************************************************************************/
        /* Memory menu                                                                                         */
        /********************************************************************************************************/
        case 50://mem menu init
            uiTemp=0;
            activeMemory=0;
            //read memory to see if set
            for (selected_mem=0;selected_mem<3;selected_mem++)
            {
                getMemoryPosition(selected_mem);
                for (ui8temp_index = 0U; ui8temp_index < 4U; ui8temp_index++)               
                    if (ui16temp_saved_pos[ui8temp_index]!=0) activeMemory=activeMemory | (1<<selected_mem); //Memory active - mark
            }
            menustate=51;

        case 51://View menu-option
            ShowSymbols(SHOW_MEMORY);
            if (uiTemp==0)
                if ((activeMemory & 1)==0) uiTemp++; else lcd_display_int(1,0);
            if (uiTemp==1)
                if ((activeMemory & 2)==0) uiTemp++; else lcd_display_int(2,0);
            if (uiTemp==2)
                if((activeMemory & 4)==0) uiTemp++; else lcd_display_int(3,0);
            if (uiTemp==3)
            {
                ShowSymbols(SHOW_MEMORY | SHOW_SPANNER);
                lcd_display_string(LCD_STRING_SET);
            }
            if (uiTemp==4)
            {
                ShowSymbols(0);
                lcd_display_string(LCD_STRING_ESC);
            }
            if (BUTTON_NONE) menustate=52;
            break;
        case 52:
            if (BUTTON_UP)
            {
                if (uiTemp==0) uiTemp=4;
                else
                {
                    uiTemp--;
                    if (uiTemp==2 && ((activeMemory & 4)==0)) uiTemp--;
                    if (uiTemp==1 && ((activeMemory & 2)==0)) uiTemp--;
                    if (uiTemp==0 && ((activeMemory & 1)==0)) uiTemp=4;
                }
                menustate=51;
            }
            else if (BUTTON_DOWN)
            {
                uiTemp++;
                if (uiTemp==5) uiTemp=0;
                menustate=51;
            }
            else if (BUTTON_OK || BUTTON_RIGHT)
            {
                if (uiTemp<3)
                {//Goto mem
                    selected_mem=uiTemp;
                    menustate=60;
                }
                else if (uiTemp==3)
                {
                    selected_mem=0;
                    menustate=55;
                }
                else menustate=10;
            }
            break;


        /********************************************************************************************************/
        /* Set memory                                                                                          */
        /********************************************************************************************************/
        case 55://Set memory init
            if (selected_mem<3){ ShowSymbols(SHOW_SPANNER | SHOW_MEMORY); lcd_display_int(selected_mem+1,0); }
            else { ShowSymbols(0); lcd_display_string(LCD_STRING_ESC); }
            if (BUTTON_NONE) menustate++;
            break;
        case 56://Set memory
            if (BUTTON_DOWN){ selected_mem++; if (selected_mem>3) selected_mem=0; menustate=55;}
            if (BUTTON_UP){ if (selected_mem>0) selected_mem--; else selected_mem=3; menustate=55;}
            if (BUTTON_OK || BUTTON_RIGHT){ menustate=57;}
            break;
        case 57: //Set memory - wait for key release
            if (BUTTON_NONE)
            {
                if (selected_mem==3)
                    menustate=50;
                else
                {
                    menutimer=0;
                    menustate=13;
                }
            }
            break;
 

        /********************************************************************************************************/
        /* Goto memory                                                                                          */
        /********************************************************************************************************/
        case 60://Go to memory
            getMemoryPosition(selected_mem);
            menustate=63;
            break;

        case 69: //key released
            control_action = DONT_MOVE;
            if (BUTTON_NONE) menustate = 10;
            break;

        /********************************************************************************************************/
        /* Settings menu                                                                                          */
        /********************************************************************************************************/
#ifdef BOOTLOADER
        /********************************************************************************************************/
        /* PC - indicate that the menu is in the PC instead                                                     */
        /********************************************************************************************************/
        case 90:           
            menutimer=0;
            if (BUTTON_NONE) menustate=91;
            break;
        case 91: 
            if (BUTTON_OK)
                menustate=10;
            menutimer++;
            if (menutimer==1000)
            {
                menutimer=0;                
                menustate=10;
            }
            break;
#else //BOOTLOADER
        /********************************************************************************************************/
        /* PIN protected?                                                                                         */
        /********************************************************************************************************/
        case 90:
            usePin=read_eeprom(USEPIN_ADD);
            pin[0]=read_eeprom(PIN0_ADD);
            pin[1]=read_eeprom(PIN1_ADD);
            pin[2]=read_eeprom(PIN2_ADD);
            if (usePin==0)
                menustate=100;
            else
            {
                menustate=91;
                uiTemp=0;
                menutimer=0;
                ucTemp=0;
                tempPin[0]=tempPin[1]=tempPin[2]=10;
            }
            break;
        case 91://view pin
            if (uiTemp==0 && menutimer<500)
                lcd_digit[2]=0;
            else
                if (tempPin[0]>9) lcd_digit[2]=LCD_minus; else lcd_digit[2]=decimal_lcd_digit[tempPin[0]];
            if (uiTemp==1 && menutimer<500)
                    lcd_digit[1]=0;
                else
                    if (tempPin[1]>9) lcd_digit[1]=LCD_minus; else lcd_digit[1]=decimal_lcd_digit[tempPin[1]];

            if (uiTemp==2 && menutimer<500)
                    lcd_digit[0]=0;
                else
                    if (tempPin[2]>9) lcd_digit[0]=LCD_minus; else lcd_digit[0]=decimal_lcd_digit[tempPin[2]];
            lcd_update();
            if (BUTTON_NONE) menustate=92;
            break;
        case 92: //Set pin code
            if (BUTTON_UP){ tempPin[uiTemp]++; if (tempPin[uiTemp]>9) tempPin[uiTemp]=0; menustate=91; ucTemp=0;}
            if (BUTTON_DOWN){ tempPin[uiTemp]--; if (tempPin[uiTemp]>9) tempPin[uiTemp]=9; menustate=91; ucTemp=0;}
            if (BUTTON_OK)
            {
                uiTemp++;
                menustate=91;
                ucTemp=0;
                if (uiTemp==3)
                {//Compare pins
                    if (tempPin[0]==pin[0] && tempPin[1]==pin[1] && tempPin[2]==pin[2])
                        menustate=100;
                    else
                        menustate=90;
                    break;
                }
            }

            menutimer++;
            if (menutimer==500)
                menustate=91;
            else if (menutimer==1000)
            {
                menutimer=0;
                ucTemp++;
                if (ucTemp>10) 
                    menustate=10;
                else
                    menustate=91;
            }
            break;
            
        /********************************************************************************************************/
        /* Upper limit                                                                                          */
        /********************************************************************************************************/
        case 100://Menu UL init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_UL);
            menutimer=0;
            if (BUTTON_NONE) menustate++;
            break;
        case 101:
            NextMenuState();
            if (BUTTON_UP) menustate=200;
            break;

        /********************************************************************************************************/
        /* Lower limit                                                                                          */
        /********************************************************************************************************/
        case 110://Menu LL init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_LL);
            menutimer=0;
            if (BUTTON_NONE) menustate++;
            break;
        case 111:
            NextMenuState();
            break;


        /********************************************************************************************************/
        /* Calibration                                                                                          */
        /********************************************************************************************************/
        case 120://Menu CAL init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_CAL);
            menutimer=0;
            if (BUTTON_NONE) menustate++;
            break;
        case 121:
            NextMenuState();
            break;
 

        /********************************************************************************************************/
        /* Select unit                                                                                          */
        /********************************************************************************************************/
        case 130://Menu Unit init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_UN);
            if (BUTTON_NONE) menustate++;
            break;
        case 131:
            NextMenuState();
            break;

        /********************************************************************************************************/
        /* Homing                                                                                               */
        /********************************************************************************************************/
        case 141:
            NextMenuState();
            break;

        /********************************************************************************************************/
        /* Autodrive                                                                                  */
        /********************************************************************************************************/
        case 151:
            NextMenuState();
            break;

        /********************************************************************************************************/
        /* PIN                                                                                                  */
        /********************************************************************************************************/
        case 160://Menu pin menu init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_PIN);
            if (BUTTON_NONE) menustate++;
            break;
        case 161:
            if (BUTTON_OK) uiTemp=0; //select
            NextMenuState();
            break;
        case 162: //pin menu
            ShowSymbols(SHOW_SPANNER);
            if (uiTemp==0) lcd_display_string(LCD_STRING_SET);
            else if (uiTemp==1) lcd_display_string(LCD_STRING_OFF);
            else lcd_display_string(LCD_STRING_ESC);
            if (BUTTON_NONE) menustate=163;
            break;
        case 163:
            if (BUTTON_OK)
            {
                if (uiTemp==0) menustate=164; //set
                else if (uiTemp==1)
                {
                    usePin=0;
                    write_eeprom(USEPIN_ADD,usePin);
                    menustate=160;
                }
                else menustate=160;
            }
            if (BUTTON_UP){if (uiTemp==0) uiTemp=2; else uiTemp--; menustate=162; }
            if (BUTTON_DOWN) { if (uiTemp==2) uiTemp=0; else uiTemp++; menustate=162;}
            break;
        case 164://view pin
            if (uiTemp==0 && menutimer<500)
                lcd_digit[2]=0;
            else
                if (pin[0]>9) lcd_digit[2]=LCD_minus; else lcd_digit[2]=decimal_lcd_digit[pin[0]];
            if (uiTemp==1 && menutimer<500)
                    lcd_digit[1]=0;
                else
                    if (pin[1]>9) lcd_digit[1]=LCD_minus; else lcd_digit[1]=decimal_lcd_digit[pin[1]];

            if (uiTemp==2 && menutimer<500)
                    lcd_digit[0]=0;
                else
                    if (pin[2]>9) lcd_digit[0]=LCD_minus; else lcd_digit[0]=decimal_lcd_digit[pin[2]];
            lcd_update();
            if (BUTTON_NONE) menustate=165;
            break;
        case 165: //Set pin code
            if (BUTTON_UP){ pin[uiTemp]++; if (pin[uiTemp]>9) pin[uiTemp]=0; menustate=164;}
            if (BUTTON_DOWN){ pin[uiTemp]--; if (pin[uiTemp]>9) pin[uiTemp]=9; menustate=164;}
            if (BUTTON_OK)
            {
                uiTemp++;
                menustate=164;
                if (uiTemp==3)
                {
                    usePin=1;
                    write_eeprom(USEPIN_ADD,usePin);
                    write_eeprom(PIN0_ADD,pin[0]);
                    write_eeprom(PIN1_ADD,pin[1]);
                    write_eeprom(PIN2_ADD,pin[2]);
                    menustate=160;
                    break;
                }
            }

            menutimer++;
            if (menutimer==500)
                menustate=164;
            else if (menutimer==1000)
            {
                menutimer=0;
                menustate=164;
            }
            break;

        /********************************************************************************************************/
        /* Error logs                                                                                               */
        /********************************************************************************************************/
        case 170:
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_ERR);
            if (BUTTON_NONE) menustate++;
            break;
        case 171:
            if (BUTTON_OK) uiTemp=0;
            NextMenuState();
            break;

        /********************************************************************************************************/
        /* Motor firmware versions                                                                              */
        /********************************************************************************************************/
        case 180:
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_ACT);
            if (BUTTON_NONE) menustate++;
            break;
        case 181:
            if (BUTTON_OK) uiTemp=((highestVersion>>8) * 100) + (highestVersion & 0xff);
            NextMenuState();
            break;

        /********************************************************************************************************/
        /* Version                                                                                               */
        /********************************************************************************************************/
        case 190://Menu Version init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_VER);
            if (BUTTON_NONE) menustate++;
            break;
        case 191:
            NextMenuState();
            break;

        /********************************************************************************************************/
        /* Esc menu                                                                                            */
        /********************************************************************************************************/
        case 200://Menu ESC init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_ESC);
            if (BUTTON_NONE) menustate++;
            break;
        case 201:
            if (BUTTON_UP) menustate=190;
            if (BUTTON_DOWN) menustate=100;
            if (BUTTON_OK) menustate=10; //select
            break;
#endif //BOOTLOADER
        default:commonMenu(); break;
      }
#endif
}
