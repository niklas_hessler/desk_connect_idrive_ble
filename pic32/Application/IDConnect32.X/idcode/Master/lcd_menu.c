#include "lcd_menu.h"
#include "lcd.h"
#include "hal.h"
#include "manne.h"
#include "motor.h"
#include "button.h"
#include "spi_slave.h"
#include "accessory_process.h"
#include "flashSettings.h"
#include "mcc.h"

extern DeskProfile deskProfile;
/****************Extern*****************/
extern INT16U ui16high_limit;
extern bool ui1all_keeping_still_flag;
extern bool ui1all_overheat_flag;
extern bool ui1all_overcurrent_flag;
extern INT16U ui16low_limit;
extern volatile INT16U ui16LIN_position;
extern volatile INT16U ui16group_latest_pos[4];
extern INT16U ui16group_high_limit[4];
extern INT16U ui16group_low_limit[4];
extern INT16U ui16near_high_limit;
extern INT16U ui16near_low_limit;
extern INT16U ui16group_high_limit_motors[4];
extern INT16U ui16group_low_limit_motors[4];
extern INT16U lowestVersion;
extern INT16U highestVersion;
extern uint16_t ui16temp;
extern unsigned char lcd_digit[4];
extern unsigned char unread_error;

unsigned char menustate = 0;
unsigned int backlight_timer = 0;
unsigned int menutimer = 0;
unsigned char selected_mem = 0;
unsigned int display = 0, oldDisplay = 0;
unsigned int uiTemp = 0;
unsigned int value, minValue, maxValue;
unsigned char errorCounter[NUMBER_OF_ERROR_LOGS];

//Last digit, Middle digit, First digit, 1 with symbols
const char strings[16][4]={{LCD_L, LCD_A,LCD_C,0},    //CAL
                        {0,LCD_L | LCD_DOT,LCD_U | LCD_DOT,0},    //U.L.
                        {0,LCD_L | LCD_DOT,LCD_L | LCD_DOT,0},    //L.L.
                        {0,LCD_n | LCD_DOT,LCD_U,0},    //Un.
                        {LCD_S, LCD_E,LCD_R,0},    //RES
                        {LCD_F, LCD_O,LCD_S,0},    //SOF
                        {LCD_r, LCD_r,LCD_E,0},    //Err
                        {LCD_t, LCD_c,LCD_A,0},    //Act
                        {LCD_t, LCD_u,LCD_A,0},    //Aut
                        {0, LCD_n,LCD_O,0},        //On
                        {LCD_F, LCD_F,LCD_O,0},    //OFF
                        {LCD_t, LCD_E,LCD_S,0},    //SEt
                        {LCD_C, LCD_S, LCD_E,0},   //ESC
                        {LCD_n, LCD_i, LCD_P,0},   //Pin
                        {0,LCD_C, LCD_P,0},         //PC
                        {8,8,8,0}}; //--- INVALID

void lcd_menu_init(void)
{
#ifndef BOOTLOADER
    //Read Eeprom
    char i;
    for (i = 0; i < NUMBER_OF_ERROR_LOGS; i++)
        errorCounter[i] = read_eeprom(ERRORCOUNTERS_ADD + i);
#endif
}

unsigned int toUnit(unsigned long value)
{//Convert from internal representation to value in seleted units
    //unsigned long result=value;
    if (deskProfile.units == 0) //CM
    {
        value = (value << 6)+(value << 5)+(value << 2); //*100
        return value >> 10; // /1024
        //return (unsigned int)(value/PULSES_PER_CM);
    }
    else //INCHES
    {
        value = (value << 15)+(value << 13)-(value << 9)+(value << 3) + value; //x40457
        return value >> 20; // / 1048576
        //return (unsigned int)(value/PULSES_PER_INCH);
    }
}

#ifndef BOOTLOADER

unsigned int fromUnit(unsigned int value)
{//Convert from value in selected units to internal representation
    if (deskProfile.units == 0) //CM
        return (unsigned int) (value * PULSES_PER_CM);
    else
        return (unsigned int) (value * PULSES_PER_INCH);
}
#endif

//Update ui16low/high_limit with the highest of the low motor limits and the lowest of the high motor limits

void getMotorLimits()
{
    ui16low_limit = 0;
    ui16high_limit = 0xFFFFU;

    for (ui8temp_index = 0; ui8temp_index < 4; ui8temp_index++)
    {//Traversing groups
        if (((1U << ui8temp_index) & activeKeyboard.groupmask) && ui16low_limit < ui16group_low_limit_motors[ui8temp_index])
            ui16low_limit = ui16group_low_limit_motors[ui8temp_index];
        if (((1U << ui8temp_index) & activeKeyboard.groupmask) && ui16high_limit > ui16group_high_limit_motors[ui8temp_index])
            ui16high_limit = ui16group_high_limit_motors[ui8temp_index];
    }
}

void ShowSymbols(unsigned char symbols) //0x01: Spanner, 0x02: M, 0x08: Error, 0x10: Home, 0x20: Units
{
    lcd_digit[3] = (lcd_digit[3] & 0x44) | symbols; //assume inches, do not affect overheat or 1 (1000)
    if ((!deskProfile.units) && (symbols & SHOW_UNITS))
        lcd_digit[3] = (lcd_digit[3] & 0x5F) | 0x80; //Cm
}

void lcd_display_string(unsigned char string)
{
    lcd_digit[0] = strings[string][0];
    lcd_digit[1] = strings[string][1];
    lcd_digit[2] = strings[string][2];
    //if (strings[string][3]) lcd_digit[3]|=4; else 
    lcd_digit[3] &= 0xFB;
    lcd_update();
}

unsigned char errorNumber = 0;

void translateError()
{
    if (BUTTON_NONE)
    {//Button released - reset errors
        if (error_type == ABOVE_UPPER_LIMIT || error_type == BELOW_LOWER_LIMIT || error_type == ABOVE_UPPER_MOTOR_LIMIT || error_type == BELOW_LOWER_MOTOR_LIMIT)
            error_type = NO_ERROR;
    }

    if (error_type == COLLISION_DETECTED)
        errorNumber = 15;
    else if (main_state >= ERROR_HARD && main_state <= ERROR_WAIT_FOR_NON)
        errorNumber = main_state - 1;
        //1: Hard stop
        //2: Back off
        //3: Stop
        //4: Wait for button to be pressed
        //5: Wait for button to be released
    else if (ui1all_overcurrent_flag) errorNumber = 7;
    else
        errorNumber = error_type;



#ifdef USE_SPI
    if (ui1all_overheat_flag)
        unread_error = REPORT_ERROR_OVERHEAT;
    if (errorNumber)
        unread_error = errorNumber;
#endif
}

void lcd_keyboard(void)
{
    static unsigned char oldKeys = NO_INPUT, newKeys = NO_INPUT; //, oldSlaveKeys=NO_INPUT;
    static unsigned char cnt = 0;

    //Get keyboard debounced
    get_keyboard();
    if (ui8keyboard.byte == newKeys)
    {
        cnt++;
        if (cnt > 25)
        {//Debounced
            oldKeys = newKeys;
            cnt = 0;
        }
    }
    else
    {//not same - restore old values
        cnt = 0;
        newKeys = ui8keyboard.byte;
    }
    activeKeyboard.keys.byte = oldKeys;
    activeKeyboard.type = ui8keyboard_type;
    activeKeyboard.groupmask = ui8group_mask;
}

void backlightAndErrors(void)
{
    static unsigned int oldLCDCS = 0;
    static unsigned char overheat_timer = 0, OHTimer = 0;
    unsigned int lcdcs = (lcd_digit[0] + lcd_digit[1] + lcd_digit[2]+(lcd_digit[3]&(255 - 0x02))); //summ all segments, ignore M

    if (backlight_timer > BACKLIGHT_TIMEOUT) BL_OFF
    else if (backlight_timer++ == MENU_TIMEOUT)
        menustate = 10;
    if (!BUTTON_NONE || oldLCDCS != lcdcs || !ui1all_keeping_still_flag)
    { //Turn on backlight
        oldLCDCS = lcdcs;
        BL_ON
        backlight_timer = 0;
    }

    translateError();
    if (menustate < 200 && errorNumber) menustate = 250;

    if (ui1all_overheat_flag) overheat_timer = 10;
    if (overheat_timer)
    {
        if (OHTimer-- == 0)
        {
            if (lcd_digit[3] & 0x40)
                lcd_digit[3] &= 0xBF;
            else
                lcd_digit[3] |= 0x40;
            lcd_update();
            overheat_timer--;
            if (overheat_timer == 1)
            {
                errorCounter[7]++;
                write_eeprom(ERRORCOUNTERS_ADD + 7, errorCounter[7]);
            }
        }
    }
    else
        lcd_digit[3] &= 0xBF;
}

void writeGroupsToEEprom(INT16U value)
{
    for (ui8temp_index = 0U; ui8temp_index < 4U; ui8temp_index++)
    {
        if (activeKeyboard.groupmask & (1U << ui8temp_index))
        {
            write_eeprom(ui8related_address, (value >> 8U));
            ui8related_address++;
            write_eeprom(ui8related_address, value);
            ui8related_address++;
        }
        else
        {
            ui8related_address += 2U;
        }
    }
}

INT8U navigate(INT8U left, INT8U right, INT8U ok)
{
    if (BUTTON_LEFT) return left;
    if (BUTTON_RIGHT) return right;
    if (BUTTON_OK) return ok;
    return menustate;
}

#include "linmasterprocess.h"

void commonMenu(void)
{
    static unsigned char oldErrorNumber = 0;

    switch (menustate)
    {
            /********************************************************************************************************/
            /* Initialize menu                                                                                            */
            /********************************************************************************************************/
        case 0://Initialize menu
            BL_ON
            ShowSymbols(main_state == HOME ? SHOW_HOME : 0);
            lcd_display_string(LCD_STRING_INVALID);
            menustate = 1;
            break;
        case 1://Awating timeout, arbitration, valid position
            if (main_state == NORMAL) menustate = 10;
            break;

        case 9://debounce key release
            if (BUTTON_NONE) //Released
            {
                menutimer++;
                if (menutimer > 300)
                {
                    menutimer = 0;
                    menustate = 11;
                    resetButtonState();
                }
            }
            else
                menutimer = 0; //Pressed - restart wait
            break;

        case 10://Normal state - init
            oldDisplay = 0;
            menutimer = 0;
            ShowSymbols(SHOW_UNITS); //Units
            menustate = 9;
            break;

        case 13: //Store memory
            ShowSymbols(SHOW_SPANNER | SHOW_MEMORY);
            menutimer++;
            switch (menutimer)
            {
                case 1:
                    ui8related_address = M1_SAVEDPOS_G1H_ADD + (selected_mem << 3);
                    saveToEEprom();
                    break;
                case 500:
                case 1500: if (activeKeyboard.type == PADDLE_KYB) menustate = 10;
                case 2500: lcd_clear();
                    break;
                case 1000:
                case 2000:
                case 3000:
                    if (selected_mem > 2 && activeKeyboard.type == VISUAL_KYB)
                    {//Blink position isf mem 4/5
                        if (display > 1999)
                            lcd_display_int(display / 10, 0);
                        else
                            lcd_display_int(display, 1); //units);
                    }
                    else
                        lcd_display_int(selected_mem + 1, 0);
                    break;
                case 3500: menustate = 10;
                    break;
            }
            break;

        case 14: //Flash to indicate set Upper limit
        case 15: //Flash to indicate set Lower limit
        case 16: //Flash to indicate error setting stuff
            ShowSymbols(SHOW_SPANNER);
            menutimer++;
            switch (menutimer)
            {
                case 500:
                case 1500:
                case 2500: lcd_clear();
                    break;
                case 1:
                case 1000:
                case 2000:
                case 3000:
                {
                    switch (menustate)
                    {
                        case 14: lcd_display_string(LCD_STRING_UL);
                            break;
                        case 15: lcd_display_string(LCD_STRING_LL);
                            break;
                        case 16: lcd_display_string(LCD_STRING_ERR);
                            break;
                    }
                }
                    break;
                case 3500: menustate = 10;
                    break;
            }
            break;

            /********************************************************************************************************/
            /* Goto memory                                                                                          */
            /********************************************************************************************************/

        case 63://Group select
            menustate += groupSelect(); //AUTO_IDLE;
            break;
        case 64://PRE_MOVE_UP
            menustate = 65; //GRP_MOVE_UP;
            menutimer = 0;
            control_action = UP_ONLY;
            break;
        case 66://PRE_MOVE_DOWN
            menustate = 67; //GRP_MOVE_DOWN;
            menutimer = 0;
            control_action = DOWN_ONLY;
            break;
        case 65://GRP_MOVE_UP
        case 67://GRP_MOVE_DOWN
            if ((deskProfile.autodrive == 0 && BUTTON_NONE) //Ej autodrive och knappen släppt
                    || (deskProfile.autodrive == 1 && (!BUTTON_NONE) && (!BUTTON_OK)) //Autodrive och inte ok-knappen
                    || (error_type != NO_ERROR))
                menustate = 68; //Key released
            if (ui1all_keeping_still_flag)
            {
                if (menutimer++ > 25)
                {
                    menustate = 63; //GRP_SELECT; //Redo if more to go
                    control_action = DONT_MOVE;
                }
            }
            else
            {
                menutimer = 0;
                control_action = menustate == 65 ? UP_ONLY : DOWN_ONLY;
            }
            break;

        case 68://AUTO_IDLE
            ui8group.byte = 15U;
            control_action = DONT_MOVE;
            if (BUTTON_NONE) menustate = 69;
            break;

#ifndef BOOTLOADER
            /********************************************************************************************************/
            /* Adjust calibrate                                                                                           */
            /********************************************************************************************************/
        case 122://adjust cal
            value = toUnit(deskProfile.deskOffset + ui16LIN_position);
            menustate++;
        case 123:
            ShowSymbols(SHOW_UNITS | SHOW_SPANNER);
            lcd_display_int(value, 1); //units);
            if (BUTTON_NONE)
            {
                menustate++;
                menutimer = AUTOSTEP_TIMER;
            }
            if (menutimer > 0)
            {//Autoincrease/decrease
                menutimer--;
                if (menutimer == 0)
                {
                    menutimer = AUTOSTEP_INTERVAL;
                    menustate++;
                }
            }
            break;
        case 124:
            if (BUTTON_UP && value < 1999)
            {
                value++;
                menustate--;
            }
            if (BUTTON_DOWN && value > 0)
            {
                value--;
                menustate--;
            }
            if (BUTTON_OK)
            {
                deskProfile.deskOffset = fromUnit(value) - ui16LIN_position;
                processData.flags.offsetUpdated = 2;
                write_eeprom(CALIBRATION_HIGH_ADD, (deskProfile.deskOffset >> 8U));
                write_eeprom(CALIBRATION_LOW_ADD, deskProfile.deskOffset);
                menustate -= 4;
            } //store value
            break;

            /********************************************************************************************************/
            /* Adjust limits                                                                                          */
            /********************************************************************************************************/
        case 102: //UL
        case 112: //LL
            getMotorLimits();
            //maxValue=toUnit(ui16high_limit+cal);
            maxValue = ui16high_limit;
            //minValue=toUnit(ui16low_limit+cal);
            minValue = ui16low_limit;
            reset_active_limits();
            if (menustate == 102)
            {//UL
                if (minValue < ui16low_limit)
                    minValue = ui16low_limit;
                value = toUnit(ui16high_limit + deskProfile.deskOffset);
            }
            else
            {//LL
                if (maxValue > ui16high_limit)
                    maxValue = ui16high_limit;
                value = toUnit(ui16low_limit + deskProfile.deskOffset);
            }
            minValue = toUnit(minValue + deskProfile.deskOffset);
            maxValue = toUnit(maxValue + deskProfile.deskOffset);
            menustate++;

        case 103:
        case 113:
            ShowSymbols(SHOW_UNITS | SHOW_SPANNER);
            lcd_display_int(value, 1); //units);
            if (BUTTON_NONE)
            {
                menustate++;
                menutimer = AUTOSTEP_TIMER;
            }
            if (menutimer > 0)
            {//Autoincrease/decrease
                menutimer--;
                if (menutimer == 0)
                {
                    menutimer = AUTOSTEP_INTERVAL;
                    menustate++;
                }
            }
            break;
        case 104: //UL
        case 114: //LL
            if (BUTTON_UP && value < maxValue)
            {
                value++;
                menustate--;
            }
            if (BUTTON_DOWN && value > minValue)
            {
                value--;
                menustate--;
            }
            if (BUTTON_OK)
            {
                if (menustate == 104)
                {//high limit
                    ui16high_limit = fromUnit(value) - deskProfile.deskOffset;
                    ui8related_address = USER_HIGH_LIMIT_G1H_ADD;
                    writeGroupsToEEprom(ui16high_limit);
                }
                else
                {//Low limit
                    ui16low_limit = fromUnit(value) - deskProfile.deskOffset;
                    ui8related_address = USER_LOW_LIMIT_G1H_ADD;
                    writeGroupsToEEprom(ui16low_limit);
                }
                reset_active_limits();
                menustate -= 4;
            }//store value
            break;

            /********************************************************************************************************/
            /* Select unit                                                                                          */
            /********************************************************************************************************/
        case 132: //adjust unit
            ShowSymbols(SHOW_UNITS | SHOW_SPANNER);
            lcd_clear();
            if (BUTTON_NONE) menustate++;
            break;
        case 133:
            if (BUTTON_UP)
            {
                deskProfile.units = 1;
                menustate--;
            }
            if (BUTTON_DOWN)
            {
                deskProfile.units = 0;
                menustate--;
            }
            if (BUTTON_OK)
            {
                write_eeprom(UNITS_ADD, deskProfile.units);
                processData.flags.unitUpdated = 1;
                menustate = 130;
            }
            break;

            /********************************************************************************************************/
            /* Homing                                                                                               */
            /********************************************************************************************************/
        case 140://Menu Homing init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_RES);
            if (BUTTON_NONE) menustate = 141;
            break;

        case 142: //set homing mode
            ShowSymbols(SHOW_SPANNER | SHOW_HOME);
            lcd_clear();
            if (BUTTON_NONE) menustate = 143;
            break;
        case 143:
            if (BUTTON_OK) menustate = 140;
            if (BUTTON_DOWN)
            {
                control_action = DOWN_ONLY;
                keyboard_state = KYB_PRE_HOME; //KYB_MANUAL;
                //ui1clear_limits_flag = 1U;
                main_state = HOME;
                menustate = 0;
            }
            break;

            /********************************************************************************************************/
            /* Autodrive                                                                                  */
            /********************************************************************************************************/
        case 150://Menu dbl init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_AUT);
            if (BUTTON_NONE) menustate++;
            break;
        case 152: //adjust unit
            ShowSymbols(SHOW_SPANNER);
            if (deskProfile.autodrive) lcd_display_string(LCD_STRING_ON);
            else lcd_display_string(LCD_STRING_OFF);
            if (BUTTON_NONE) menustate = 153;
            break;
        case 153:
            if (BUTTON_UP)
            {
                deskProfile.autodrive = 1;
                menustate = 152;
            }
            if (BUTTON_DOWN)
            {
                deskProfile.autodrive = 0;
                menustate = 152;
            }
            if (BUTTON_OK)
            {
                write_eeprom(AUTODRIVE_ENABLED_ADD, deskProfile.autodrive);
                menustate = 150;
            }
            break;

            /********************************************************************************************************/
            /* Error logs                                                                                             */
            /********************************************************************************************************/
        case 172:
            ShowSymbols(SHOW_SPANNER | SHOW_ERROR);
            lcd_display_int(uiTemp + 1, 0);
            if (BUTTON_NONE)
            {
                menustate = 173;
                menutimer = 0;
            }
            break;
        case 173:
            if (BUTTON_OK) menustate = 170;
            if (BUTTON_DOWN)
            {
                if (uiTemp) uiTemp--;
                menustate = 172;
            }
            if (BUTTON_UP)
            {
                uiTemp++;
                if (uiTemp == NUMBER_OF_ERROR_LOGS) menustate = 174;
                else menustate = 172;
            }
            if (menutimer == 1000)
            {//Show counter
                lcd_display_value(errorCounter[uiTemp]);
                menutimer++;
            }
            else if (menutimer < 1000)
                menutimer++;
            break;
        case 174:
            lcd_display_string(LCD_STRING_RES);
            if (BUTTON_NONE) menustate = 175;
            break;
        case 175:
            if (BUTTON_OK)
            {
                uiTemp = 0;
                menutimer = 0;
                menustate = 176;
            }
            if (BUTTON_DOWN)
            {
                uiTemp = NUMBER_OF_ERROR_LOGS - 1;
                menustate = 172;
            }
            break;
        case 176://Reset counters
            if (menutimer++ == 0)
            {
                errorCounter[uiTemp] = 0;
                write_eeprom(ERRORCOUNTERS_ADD + uiTemp, 0);
                if (uiTemp & 0x01)
                    lcd_display_string(LCD_STRING_RES);
                else
                    lcd_clear();
                uiTemp++;
                if (uiTemp == NUMBER_OF_ERROR_LOGS) menustate = 170;
            }
            else if (menutimer == 500) menutimer = 0;
            break;

            /********************************************************************************************************/
            /* Motor firmware versions                                                                                              */
            /********************************************************************************************************/
        case 182:
            ShowSymbols(SHOW_SPANNER);
            lcd_display_int(uiTemp, 2);
            if (BUTTON_NONE) menustate++;
            break;
        case 183:
            if (BUTTON_OK) menustate -= 3;
            if (BUTTON_DOWN)
            {
                uiTemp = ((lowestVersion >> 8) * 100) + (lowestVersion & 0xff);
                menustate--; //select
            }
            if (BUTTON_UP)
            {
                uiTemp = ((highestVersion >> 8) * 100) + (highestVersion & 0xff);
                menustate--; //select
            }
            break;

            /********************************************************************************************************/
            /* Version                                                                                               */
            /********************************************************************************************************/
        case 192: //show version
            ShowSymbols(SHOW_SPANNER);
            lcd_display_int(MAJOR_VERSION * 100 + MINOR_VERSION, 2);
            if (BUTTON_NONE) menustate = 193;
            break;
        case 193:
            if (BUTTON_OK) menustate = 190;
            break;
#endif //BOOTLOADER

            /********************************************************************************************************/
            /* Errors                                                                                               */
            /********************************************************************************************************/
        case 250://Error - initiate
            ShowSymbols(SHOW_ERROR);
            lcd_update();
            menutimer = 0;
            menustate = 251;
            oldErrorNumber = 0;
            break;
        case 251://wait for error to stabilize...
            if (menutimer++ < 300)
            {
                if (errorNumber && oldErrorNumber != errorNumber)
                {
                    menutimer = 0;
                    oldErrorNumber = errorNumber;
                }
            }
            else
            {
                lcd_display_int(oldErrorNumber, 0);
                errorCounter[oldErrorNumber - 1]++;
                write_eeprom(ERRORCOUNTERS_ADD + oldErrorNumber - 1, errorCounter[oldErrorNumber - 1]);
                menutimer = 0;
                menustate = 252;
            }
            break;
        case 252://Show error for x seconds
            if (errorNumber != oldErrorNumber && menutimer++ > 3000)
                menustate = 10;
            break;
    }
}

//Select groups and direction to go for goto memory

unsigned char groupSelect()
{
    INT8U res = 5; //default AUTO_IDLE
    ui8group.byte = 0U;
    ui16high_limit = 0xFFFFU;
    ui16low_limit = 0U;
    for (ui8temp_index = 0U; ui8temp_index < 4U; ui8temp_index++)
    {//find lowest active motor, check if needs to go up to catch up

        if ((ui16temp_saved_pos[ui8temp_index] != 0U) && (ui8temp_group & (1U << ui8temp_index)))
        {
            if (ui16group_latest_pos[ui8temp_index] < (ui16temp_saved_pos[ui8temp_index] - END_STOP_DISTANCE))
            {

                res = 1; //PRE_MOVE_UP;
                if (ui16temp_saved_pos[ui8temp_index] > ui16group_high_limit[ui8temp_index])
                {//destinationen ovanför gruppens limit
                    ui16temp_saved_pos[ui8temp_index] = ui16group_high_limit[ui8temp_index];
                    if (ui16group_high_limit[ui8temp_index] != ui16group_high_limit_motors[ui8temp_index])
                        error_type = ABOVE_UPPER_LIMIT;
                    else
                        error_type = ABOVE_UPPER_MOTOR_LIMIT;
                }
                if (ui16temp_saved_pos[ui8temp_index] < ui16high_limit)
                {//önskad destination lägre är tidigare dest
                    ui16high_limit = ui16temp_saved_pos[ui8temp_index];
                    ui16near_high_limit = ui16high_limit - END_STOP_DISTANCE;
                }
            }
        }
    }

    if (res == 5) //AUTO_IDLE)
    { //No motors below - check if need to go down to catch up

        for (ui8temp_index = 0U; ui8temp_index < 4U; ui8temp_index++)
        {
            if ((ui16temp_saved_pos[ui8temp_index] != 0U) && (ui8temp_group & (1U << ui8temp_index)))
            {

                if (ui16group_latest_pos[ui8temp_index] > (ui16temp_saved_pos[ui8temp_index] + END_STOP_DISTANCE))
                {
                    res = 3; //PRE_MOVE_DOWN;
                    if (ui16temp_saved_pos[ui8temp_index] < ui16group_low_limit[ui8temp_index])
                    { //destinationen under gruppens limit
                        ui16temp_saved_pos[ui8temp_index] = ui16group_low_limit[ui8temp_index];
                        if (ui16group_low_limit[ui8temp_index] != ui16group_low_limit_motors[ui8temp_index])
                            error_type = BELOW_LOWER_LIMIT;
                        else
                            error_type = BELOW_LOWER_MOTOR_LIMIT;
                    }
                    if (ui16temp_saved_pos[ui8temp_index] > ui16low_limit)
                    {
                        ui16low_limit = ui16temp_saved_pos[ui8temp_index];
                        ui16near_low_limit = ui16low_limit + END_STOP_DISTANCE;
                    }
                }
            }
        }
    }
    for (ui8temp_index = 0U; ui8temp_index < 4U; ui8temp_index++)
    {//Select groups to move up/down
        if ((ui16temp_saved_pos[ui8temp_index] != 0U) && (ui8temp_group & (1U << ui8temp_index)))
        {
            if ((res == 1 /*PRE_MOVE_UP*/) && (ui16group_latest_pos[ui8temp_index] < ui16near_high_limit) && (ui16group_latest_pos[ui8temp_index] <= (ui16temp_saved_pos[ui8temp_index] - END_STOP_DISTANCE)) && (ui16temp_saved_pos[ui8temp_index] <= ui16group_high_limit[ui8temp_index]))
            {
                ui8group.byte |= (1U << ui8temp_index);
            }
            if ((res == 3 /*PRE_MOVE_DOWN*/) && (ui16group_latest_pos[ui8temp_index] > ui16near_low_limit)
                    && (ui16group_latest_pos[ui8temp_index] >= (ui16temp_saved_pos[ui8temp_index] + END_STOP_DISTANCE)) && (ui16temp_saved_pos[ui8temp_index] >= ui16group_low_limit[ui8temp_index]))
            {
                ui8group.byte |= (1U << ui8temp_index);
            }
        }
    }

    return res;
}

void lcd_menu(void)
{
    static INT8U menuEnterType;
#ifndef TESTCYCLES
    getSlaveInputs();

    backlightAndErrors();

    switch (menustate)
    {
            /********************************************************************************************************/
            /* Main state                                                                                           */
            /********************************************************************************************************/
        case 11:
            if (ui16LIN_position > 25 && ui16LIN_position < TOP_POSITION)
            {
                display = toUnit(ui16LIN_position + deskProfile.deskOffset);
                if (display != oldDisplay)
                {//Only update display of changes been made
                    if (display > 1999)
                        lcd_display_int(display / 10, 0);
                    else
                        lcd_display_int(display, 1); //units);
                    oldDisplay = display;
                }
            }
            if (BUTTON_OK_ONLY) //button_type==OK_BUTTON)
            { //Ok button
                menutimer++;
                menuEnterType = activeKeyboard.type;
            }
            else if (menutimer > 5 && menutimer < 1000 && (menuEnterType == LCD_KYB || menuEnterType == VISUAL_KYB))
                menustate = 20; //go to mem
            else menutimer = 0; //too long - ignore
            if (main_state == HOME) menustate = 0;
            break;


            /********************************************************************************************************/
            /* Goto memory                                                                                          */
            /********************************************************************************************************/
        case 20://Go to memory - init
            ShowSymbols(SHOW_MEMORY); //M
            lcd_display_int(1, 0);
            selected_mem = 0;
            if (BUTTON_NONE)
                menustate = 21;
            break;
        case 21://Go to memory
            if (BUTTON_UP && selected_mem < 4)
            {
                selected_mem++;
                lcd_display_int(selected_mem + 1, 0);
                menustate = 22;
            }
            else if (BUTTON_DOWN && selected_mem > 0)
            {
                selected_mem--;
                lcd_display_int(selected_mem + 1, 0);
                menustate = 22;
            }
            else if (BUTTON_LEFT) menustate = 23;
            else if (BUTTON_RIGHT) menustate = 30;
            else if (BUTTON_OK)
            {//Goto mem
                getMemoryPosition(selected_mem);
                menustate = 63;
            }
            else
                //                if (lcdDoubleClickTimer) lcdDoubleClickTimer--;
                break;
        case 22: //Set memory - wait for key release
            if (BUTTON_NONE)
            {
                menustate = 21;
            } //lcdDoubleClickTimer=0;}
            break;
        case 23: //Wait for key release
            if (BUTTON_NONE) menustate = 10;
            break;

        case 69: //key released
            control_action = DONT_MOVE;
            //            lcdDoubleClickTimer=1000;
            menustate = 21;
            break;


            /********************************************************************************************************/
            /* Set memory                                                                                          */
            /********************************************************************************************************/
        case 30://Set memory init
            ShowSymbols(SHOW_SPANNER | SHOW_MEMORY);
            lcd_display_int(1, 0);
            selected_mem = 0;
            if (BUTTON_NONE) menustate = 31;
            break;
        case 31://Set memory
            if (BUTTON_UP && selected_mem < 4)
            {
                selected_mem++;
                lcd_display_int(selected_mem + 1, 0);
                menustate = 32;
            }
            if (BUTTON_DOWN && selected_mem > 0)
            {
                selected_mem--;
                lcd_display_int(selected_mem + 1, 0);
                menustate = 32;
            }
            if (BUTTON_LEFT) menustate = 20;
            if (BUTTON_RIGHT) menustate = 120;
            if (BUTTON_OK)
            {
                menutimer = 0;
                menustate = 13;
            }
            break;
        case 32: //Set memory - wait for key release
            if (BUTTON_NONE) menustate = 31;
            break;

#ifdef BOOTLOADER
            /********************************************************************************************************/
            /* PC - indicate that the menu resides in the PC                                                        */
            /********************************************************************************************************/
        case 120://Menu PC init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_PC);
            menutimer = 0;
            if (BUTTON_NONE) menustate = 121;
            break;
        case 121:
            if (BUTTON_LEFT) menustate = 30;
            break;

#else //BOOTLOADER
            /********************************************************************************************************/
            /* Calibration                                                                                          */
            /********************************************************************************************************/
        case 120://Menu CAL init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_CAL);
            menutimer = 0;
            if (BUTTON_NONE) menustate = 121;
            break;
        case 121:
            /*if (BUTTON_LEFT) menustate=30;
            if (BUTTON_RIGHT) menustate=100;
            if (BUTTON_OK)menustate=122; //select*/
            menustate = navigate(30, 100, 122);
            break;

            /********************************************************************************************************/
            /* Upper limit                                                                                          */
            /********************************************************************************************************/
        case 100://Menu UL init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_UL);
            menutimer = 0;
            if (BUTTON_NONE) menustate = 101;
            break;
        case 101:
            /*if (BUTTON_LEFT) menustate=120;
            if (BUTTON_RIGHT) menustate=110;
            if (BUTTON_OK) menustate=102;*/
            menustate = navigate(120, 110, 102);
            break;

            /********************************************************************************************************/
            /* Lower limit                                                                                          */
            /********************************************************************************************************/
        case 110://Menu LL init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_LL);
            menutimer = 0;
            if (BUTTON_NONE) menustate = 111;
            break;
        case 111:
            /*if (BUTTON_LEFT) menustate=100;
            if (BUTTON_RIGHT) menustate=130;
            if (BUTTON_OK) menustate=112;*/
            menustate = navigate(100, 130, 112);
            break;


            /********************************************************************************************************/
            /* Select unit                                                                                          */
            /********************************************************************************************************/
        case 130://Menu Unit init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_UN);
            if (BUTTON_NONE) menustate = 131;
            break;
        case 131:
            /*if (BUTTON_LEFT) menustate=110;
            if (BUTTON_RIGHT) menustate=140;
            if (BUTTON_OK) menustate=132; //select*/
            menustate = navigate(110, 140, 132);
            break;

            /********************************************************************************************************/
            /* Homing                                                                                               */
            /********************************************************************************************************/
        case 141:
            /*if (BUTTON_LEFT) menustate=130;
            if (BUTTON_RIGHT) menustate=190;
            if (BUTTON_OK) menustate=142; //select*/
            menustate = navigate(130, 150, 142);
            break;

            /********************************************************************************************************/
            /* Autodrive                                                                                  */
            /********************************************************************************************************/
        case 151:
            menustate = navigate(140, 190, 152);
            break;

            /********************************************************************************************************/
            /* Version                                                                                               */
            /********************************************************************************************************/
        case 190://Menu Version init
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_VER);
            if (BUTTON_NONE) menustate = 191;
            break;
        case 191:
            /*if (BUTTON_LEFT) menustate=140;
            if (BUTTON_RIGHT) menustate=170;
            if (BUTTON_OK) menustate=192; //select*/
            menustate = navigate(140, 170, 192);
            break;

            /********************************************************************************************************/
            /* Error logs                                                                                               */
            /********************************************************************************************************/
        case 170:
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_ERR);
            if (BUTTON_NONE) menustate = 171;
            break;
        case 171:
            if (BUTTON_LEFT) menustate = 190;
            if (BUTTON_RIGHT) menustate = 180;
            if (BUTTON_OK)
            {
                uiTemp = 0;
                menustate = 172; //select
            }
            break;


            /********************************************************************************************************/
            /* Motor firmware versions                                                                              */
            /********************************************************************************************************/
        case 180:
            ShowSymbols(SHOW_SPANNER);
            lcd_display_string(LCD_STRING_ACT);
            if (BUTTON_NONE) menustate = 181;
            break;
        case 181:
            if (BUTTON_LEFT) menustate = 170;
            //if (BUTTON_RIGHT) menustate=120;
            if (BUTTON_OK)
            {
                uiTemp = ((highestVersion >> 8) * 100) + (highestVersion & 0xff);
                menustate = 182; //select
            }
            break;
#endif //BOOTLOADER


        default:commonMenu();
            break;
    }
#endif
}