/*
 * File:   lcd_menu.h
 * Author: patros
 *
 * Created on den 12 juli 2013, 10:06
 */

#ifndef LCD_MENU_H
#define	LCD_MENU_H

#define BACKLIGHT_TIMEOUT 15000 //15 secs
#define MENU_TIMEOUT 10000 //10 secs
#define AUTOSTEP_TIMER 1000 //1 secs
#define AUTOSTEP_INTERVAL 60 //0.05sec

#define PULSES_PER_INCH 25.925  //25.918 pulses per inches
#define PULSES_PER_CM 10.245  //102.041 pulses per cm -> OPTIMIZED: check code 10.24

extern unsigned char menustate;
extern unsigned int backlight_timer;
extern unsigned int menutimer;
extern unsigned char selected_mem;

extern unsigned char units;
extern unsigned char usePin, pin[3];
extern unsigned int cal;
extern unsigned int uiTemp;
//extern unsigned int lcdDoubleClickTimer;

extern unsigned int value, minValue, maxValue;
extern unsigned int display,oldDisplay;


#define NUMBER_OF_ERROR_LOGS 20
extern unsigned char errorCounter[NUMBER_OF_ERROR_LOGS];
extern unsigned char lcd_digit[4];
#define LCD_SPANNER_ON lcd_digit[3]|=1
#define LCD_SPANNER_OFF lcd_digit[3]&=254
#define LCD_MEMORY_ON lcd_digit[3]|=2
#define LCD_MEMORY_OFF lcd_digit[3]&=253
#define LCD_ERROR_ON lcd_digit[3]|=8
#define LCD_ERROR_OFF lcd_digit[3]&=247
#define LCD_HOME_ON lcd_digit[3]|=16
#define LCD_HOME_OFF lcd_digit[3]&=239
#define LCD_INCHES_ON lcd_digit[3]|=32
#define LCD_INCHES_OFF lcd_digit[3]&=223
#define LCD_TEMP_ON lcd_digit[3]|=64
#define LCD_TEMP_OFF lcd_digit[3]&=191
#define LCD_CM_ON lcd_digit[3]|=128
#define LCD_CM_OFF lcd_digit[3]&=127

#define SHOW_SPANNER 0x01
#define SHOW_MEMORY 0x02
#define SHOW_ERROR 0x08
#define SHOW_HOME 0x10
#define SHOW_UNITS 0x20

//Dot after, Lower,Lower right, lower left, middle, upper right, upper left, upper
#define LCD_A 0b00111111
#define LCD_b 0b01111010
#define LCD_C 0b01010011
#define LCD_c 0b01011000
#define LCD_d 0b01111100
#define LCD_E 0b01011011
#define LCD_F 0b00011011
#define LCD_G
#define LCD_H
#define LCD_I
#define LCD_i 0b00010000
#define LCD_J
#define LCD_L 0b01010010
#define LCD_l
#define LCD_n 0b00111000
#define LCD_O 0b01110111
#define LCD_o
#define LCD_P 0b00011111
#define LCD_r 0b00011000
#define LCD_R 0b00111111
#define LCD_S 0b01101011
#define LCD_t 0b01011010
#define LCD_U 0b01110110
#define LCD_u 0b01110000
#define LCD_y
#define LCD_DOT 0b10000000
#define LCD_minus 0b00001000



#define LCD_STRING_CAL 0
#define LCD_STRING_UL 1
#define LCD_STRING_LL 2
#define LCD_STRING_UN 3
#define LCD_STRING_RES 4
#define LCD_STRING_VER 5
#define LCD_STRING_ERR 6
#define LCD_STRING_ACT 7
#define LCD_STRING_AUT 8
#define LCD_STRING_ON 9
#define LCD_STRING_OFF 10
#define LCD_STRING_SET 11
#define LCD_STRING_ESC 12
#define LCD_STRING_PIN 13
#define LCD_STRING_PC  14
#define LCD_STRING_INVALID 15
void lcd_display_string(unsigned char string);

extern unsigned char errorNumber;
void translateError();

unsigned int toUnit(unsigned long value);
unsigned int fromUnit(unsigned int value);
void getMotorLimits();
void ShowSymbols(unsigned char symbols); //0x01: Spanner, 0x02: M, 0x08: Error, 0x10: Home, 0x20: Units

unsigned char groupSelect(void); //Selects group and direction for goto memory
unsigned char menuSetCalibrate(unsigned char state);
unsigned char menuSetLimit(unsigned char state);
unsigned char viewErrorLogs(unsigned char state);

void lcd_keyboard(void);
void lcd_menu_init(void);
void backlightAndErrors(void);
void commonMenu(void);

void lcd_menu(void);

#endif	/* LCD_MENU_H */

