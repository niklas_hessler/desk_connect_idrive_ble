/*******************************************************************************************************************
** Copyright (C) ROL Ergo AB
** ----------------------------------------------------------------------------------------
** IDENTIFICATION
** Unit Name	  CONTROL BOX MCU
** File Name	  button.h
**
** ----------------------------------------------------------------------------------------
** DESCRIPTION
** This file contains LIN functionality for control box
**
** ----------------------------------------------------------------------------------------
** REVISION SUMMARY
** Rev	 Date	    Name			        Changes from previous issue
** 1.0	 11-08-11 	Anders Rambrant			Initial version
**
**
** DEPENDENCY LIST
**
** Note: 
**
**
**
**
**
**
**
*******************************************************************************************************************/

#ifndef BUTTON_H
#define BUTTON_H

/************************************************ Dependencie  ****************************************************/

#include "hal.h"
#include "lcd_menu.h"

/************************************************ definitions  ****************************************************/

#define HIGH_LOW_LIMIT_MIN_DISTANCE 	800U		/* Minimum distance between upper and lower user limits. 0.1 mm / unit => 80mm */
#define INITIAL_STORED_POS		0
								
/************************************************** STRUCTURES ********************************************************/

/********************************************* enum declarations **************************************************/

/* This enum defines the button state - it is general to all buttons */
enum BUTTON_STATE_ENUM
{
	NOT_PRESSED,
	PRE_PRESSED,
	PRESSED
};

/* This enum defines the button type, 
   buttons are divided into the following categories */
enum BUTTON_TYPE_ENUM
{
	NO_BUTTON,
	DOWN_BUTTON,
	UP_BUTTON,
	BUTTON_COMB,
	NUM_BUTTON,
	OK_BUTTON
};

/* This enum defines the possible table actions */
enum CONTROL_ACTION_ENUM
{
	DONT_MOVE=0,
	DOWN_ONLY=1,
	UP_ONLY=2
};

/* This enum defines the automatic movement state machine */
enum AUTO_STATE_ENUM
{
        GRP_SELECT,
	PRE_MOVE_UP,
	GRP_MOVE_DOWN,
        PRE_MOVE_DOWN,
	GRP_MOVE_UP,
        AUTO_IDLE
};

/* this enum defines the keyboard states - it is general to all keyboards */
enum KEYBOARD_STATE_ENUM
{
	KYB_MANUAL,
	KYB_AUTO,
	KYB_SAVE_MEM,
	KYB_SAVE_LIMIT,
	KYB_PRE_HOME,
        KYB_3_PUSH
};

enum BUTTON_TYPE_ENUM		button_type;		/* button type declaration  */
enum BUTTON_TYPE_ENUM		old_button_type;	/* memory of the last button pressed  */
enum CONTROL_ACTION_ENUM	control_action;		/* control action declaration */
enum KEYBOARD_STATE_ENUM	keyboard_state;		/* kyeboard state declaration */

		
/******************************************** variable declarations ***********************************************/

INT8U 	ui8related_address;
INT8U 	ui8saved_port_value;		/* port value aquired before debounce period */
INT8U	ui8button_timer;
INT8U	ui8temp_group;				/* temp group selection */
INT8U	ui8temp_index;
INT16U 	ui16temp_saved_pos[4];	/* temp array to store memory positions fetched from EEPROM */
BOOL1U 	ui1new_write_flag;		/* protection flag to disable writing to the eeprom more than once every command */

/********************************************* function prototypes ************************************************/

void resetButtonState(void);
void goDown(void);
void goUp(void);
void get_user_input (void);
void group_keyboard (void);
void memory_keyboard (void);
void keyboard_state_set(void);
void getMemoryPosition(unsigned char mem);
void saveToEEprom(void); //Stores current position on ui8related_address for all active groups
unsigned char getSlaveInputs(void);   //Check info from slaves


#endif /*end def BUTTON_H*/
/************************************************ END OF FILE *****************************************************/