/*******************************************************************************************************************
 ** Copyright (C) ROL Ergo AB
 ** ----------------------------------------------------------------------------------------
 ** IDENTIFICATION
 ** Unit Name	  CONTROL BOX MCU
 ** File Name	  button.c
 **
 ** ----------------------------------------------------------------------------------------
 ** DESCRIPTION
 ** This file contains LIN functionality for control box
 **
 ** ----------------------------------------------------------------------------------------
 ** REVISION SUMMARY
 ** Rev	 Date	    Name			        Changes from previous issue
 **
 **
 ** DEPENDENCY LIST
 **
 ** Note: 
 **
 **
 **
 **
 **
 **
 **
 *******************************************************************************************************************/
#include "manne.h"
#include "button.h"
#ifdef USE_SPI
#include "spi_slave.h"
#endif
#include "lcd_menu.h"
#include "accessory_process.h"
extern DeskProfile deskProfile;

/********Extern******************/
extern unsigned char groupCommanded;
extern INT16U ui16group_low_limit[4];
extern INT16U ui16group_high_limit[4];
extern volatile INT16U ui16LIN_position;
extern volatile INT16U ui16group_latest_pos[4];
extern volatile INT8U ui8group_ISP[4];
extern volatile INT16U ui16StartPosition;
extern INT16U ui16low_limit;
extern INT16U ui16high_limit;
extern bool ui1all_keeping_still_flag;
extern INT16U ui16near_high_limit;
extern INT16U ui16near_low_limit;
extern INT16U ui16group_high_limit_motors[4];
extern INT16U ui16group_low_limit_motors[4]; 
extern INT16U ui16CDS_end_margin;
extern INT8U ui8error_groups;
extern uint16_t ui16temp;
extern INT8U ui8general_timer_1ms_tick;
extern INT8U ui8SchedulePhase;
extern INT8U ui8NumberOfSlaves;
extern INT8U ui8SlaveReported;
extern bool ui1clear_limits_flag;
extern unsigned char selectedMenu;
extern unsigned char actionCommanded;
unsigned char pushCounter = 0, movementStarted = 0;
unsigned char firstButton = 0xff;
INT8U motionDetected = 0; //Counter for detected motions when stadning still - resets when read by SPI
enum AUTO_STATE_ENUM auto_state=AUTO_IDLE;			/* automatic movement state machine declaration */

void getMemoryPosition(unsigned char mem) //Read memory to ui16temp_saved_position[0..3]
{
    INT8U adr = M1_SAVEDPOS_G1H_ADD + (mem << 3);
    for (ui8temp_index = 0U; ui8temp_index < 4U; ui8temp_index++)
    {//Get memory positions
        ui16temp_saved_pos[ui8temp_index] = read_eeprom_16(adr);
        adr += 2;
    }
}

void getNextPosition(char riktning) //1 up, 0 down
{
    unsigned char i;
    INT16U tmp = riktning ? 0xffff : 0;
    for (i = 3; i < 5; i++)
    {//Check both memories
        getMemoryPosition(i);
        for (ui8temp_index = 0; ui8temp_index < 4; ui8temp_index++)
        {//Check all groups
            if ((ui8temp_group & (1 << ui8temp_index)) && //group Active
                    (ui16temp_saved_pos[ui8temp_index] != 0)) //Memory set
            {
                if (riktning == 0)
                {
                    if ((ui16temp_saved_pos[ui8temp_index] > tmp) && //Above previous memory position
                            (ui16temp_saved_pos[ui8temp_index] < ui16group_latest_pos[ui8temp_index] - END_STOP_DISTANCE) && //ui16LIN_position-END_STOP_DISTANCE)) //Below current position
                            (ui16temp_saved_pos[ui8temp_index] > ui16group_low_limit[ui8temp_index])) //Above lower limit
                    {//Closer position found
                        tmp = ui16temp_saved_pos[ui8temp_index];
                        ui8related_address = M1_SAVEDPOS_G1H_ADD + (i << 3);
                        button_type = NUM_BUTTON;
                    }
                }
                else
                {
                    if ((ui16temp_saved_pos[ui8temp_index] < tmp) && //Below previous memory position
                            (ui16temp_saved_pos[ui8temp_index] > ui16group_latest_pos[ui8temp_index] + END_STOP_DISTANCE) && //ui16LIN_position+END_STOP_DISTANCE)) //Above current position
                            (ui16temp_saved_pos[ui8temp_index] < ui16group_high_limit[ui8temp_index])) //Below upper limit
                    {//Closer position found
                        tmp = ui16temp_saved_pos[ui8temp_index];
                        ui8related_address = M1_SAVEDPOS_G1H_ADD + (i << 3);
                        button_type = NUM_BUTTON;
                    }
                }
            }
        }
    }
}

void saveToEEprom(void)
{
    for (ui8temp_index = 0U; ui8temp_index < 4U; ui8temp_index++)
    {//All groups
        //check limits
        if (ui8related_address == USER_HIGH_LIMIT_G1H_ADD + 2 * ui8temp_index)
        {//High limit 
            if (ui16group_latest_pos[ui8temp_index] != 0 && ui16group_latest_pos[ui8temp_index]<(ui16group_low_limit[ui8temp_index] + END_STOP_DISTANCE))
                menustate = 16; //to close to lower limit - ignore 
        }
        else if (ui8related_address == USER_LOW_LIMIT_G1H_ADD + 2 * ui8temp_index)
        {//Low limit 
            if (ui16group_latest_pos[ui8temp_index] != 0 && ui16group_latest_pos[ui8temp_index]>(ui16group_high_limit[ui8temp_index] - END_STOP_DISTANCE))
                menustate = 16; //to close to upper limit - ignore 
        }
        if ((menustate != 16) && (ui16group_latest_pos[ui8temp_index] != DEFAULT_HIGHEST_POS) && (activeKeyboard.groupmask & (1U << ui8temp_index)))
        {//Group active and not masked
            write_eeprom(ui8related_address, (ui16group_latest_pos[ui8temp_index] >> 8U));
            ui8related_address++;
            write_eeprom(ui8related_address, ui16group_latest_pos[ui8temp_index]);
            ui8related_address++;
        }
        else
        {
            ui8related_address += 2U;
        }
    }
}

void resetButtonState(void)
{
    //ui8saved_port_value=0;
    ui8button_timer = 0U;
    keyboard_state = KYB_MANUAL;
    old_button_type = button_type = NO_BUTTON;
    auto_state = AUTO_IDLE;
}

#define DOUBLECLICKTIME 3  //Maximum time between button clicks

INT8U doubleClickState = 0, pushTimer, oldFirstButton = 0;
INT8U doubleClick = 0;

void doubleClickCheck(void)
{
    switch (doubleClickState)
    {
        case 0: //Wait for push
            if (firstButton)
            {
                doubleClickState++;
                pushTimer = 0;
                oldFirstButton = firstButton;
                doubleClick = 0;
            }
            break;
        case 1: //wait for release
            pushTimer++;
            if (pushTimer > DOUBLECLICKTIME)
                doubleClickState = 4; //too long press - cancel double click             
            else if (!firstButton) // button released
            {
                doubleClickState++;
                pushTimer = 0;
            }
            break;
        case 2: //Wait for next button
            pushTimer++;
            if (pushTimer > DOUBLECLICKTIME)
                doubleClickState = 0; //Too long release - cancel double click
            else if (firstButton)
            {//Button pressed
                if (firstButton != oldFirstButton)
                    doubleClickState = 0; //Not same key - cancel double click
                else
                {//Same key
                    doubleClickState++;
                    pushTimer = 0;
                }
            }
            break;
        case 3: //Wait for release
            pushTimer++;
            if (pushTimer > DOUBLECLICKTIME)
                doubleClickState = 4; //Too long press - cancel double click
            else if (!firstButton)
            {
                doubleClick = 1;
                doubleClickState = 0;
            }
            break;
        case 4: //Wait for release - not doubleclick
            if (!firstButton)
                doubleClickState = 0;
            break;
    }
}

/*******************************************************************************************************************
NAME 
   memory_keyboard

DESCRIPTION
    This function translates input from memory keyboard to the general formate which is defined by the 
    button_type, its related address (if any) and related group. 
INPUT
    None

RETURNS
    No return
 *******************************************************************************************************************/
void memory_keyboard(void)
{
    if (!firstButton)
    {//First button to be pressed
        if (BUTTON_DOWN) //Down
        {
            ui8related_address = USER_LOW_LIMIT_G1H_ADD;
            button_type = DOWN_BUTTON;
            ui8temp_group = activeKeyboard.groupmask;
            if ((keyboard_state != KYB_PRE_HOME) && (selectedMenu == VISUAL_KYB) && (doubleClickState == 2 || activeKeyboard.type != PADDLE_KYB))
                getNextPosition(0);
            firstButton = 0x01;
            ui16StartPosition = ui16group_latest_pos[0]; //ui16LIN_position;
        }
        else if (BUTTON_UP) //Up
        {
            ui8related_address = USER_HIGH_LIMIT_G1H_ADD;
            button_type = UP_BUTTON;
            ui8temp_group = activeKeyboard.groupmask;
            if (selectedMenu == VISUAL_KYB && (doubleClickState == 2 || activeKeyboard.type != PADDLE_KYB))
                getNextPosition(1);
            firstButton = 0x02;
            ui16StartPosition = ui16group_latest_pos[0]; //ui16LIN_position;
        }
        else if (BUTTON_LEFT) //mem 1 / Left
        {
            ui8temp_group = activeKeyboard.groupmask;
            ui8related_address = M1_SAVEDPOS_G1H_ADD;
            button_type = NUM_BUTTON;
            firstButton = 0x04;
        }
        else if (BUTTON_RIGHT) //mem 2 / Right
        {
            ui8temp_group = activeKeyboard.groupmask;
            ui8related_address = M2_SAVEDPOS_G1H_ADD;
            button_type = NUM_BUTTON;
            firstButton = 0x08;
        }
        else if (BUTTON_OK) //Set / Select
        {
            ui8temp_group = activeKeyboard.groupmask;
            ui8related_address = 0U; /* no related address */
            button_type = OK_BUTTON;
            firstButton = 0x10;
        }
    }
    else
    {//Not first button press
        //check if the first button pressed is still pressed
        if ((~activeKeyboard.keys.byte) & firstButton)
        {//First button still pressed
            if (BUTTON_DOWN && BUTTON_UP)
            {//Down/Up combo
                ui8related_address = 0U; /* no related address */
                button_type = BUTTON_COMB;
                firstButton = 0x03;
            }
        }
        else
        {//First button released
            button_type = NO_BUTTON;
            firstButton = 0;
            if (ui1all_keeping_still_flag)
            {
                reset_active_limits();
                ui8group.byte = 15U; /* address all groups */
                ui8temp_group = activeKeyboard.groupmask;
                ui8related_address = 0U; /* reset related address info */
            }
        }
    }
    doubleClickCheck();
}/* end of void memory_keyboard (void) */

/*******************************************************************************************************************
NAME 
   keyboard_state_set

DESCRIPTION
    this function handles the general keyboard state machine  
INPUT
    None

RETURNS
    No return
 *******************************************************************************************************************/
#define PassLimitTime 25

void goDown(void)
{
    ui8group.byte = 0U;
    ui16low_limit = 0U;
    /* select limit */
    for (ui8temp_index = 0U; ui8temp_index < 4U; ui8temp_index++)
    {
        if (ui8button_timer >= PassLimitTime)
            ui16group_low_limit[ui8temp_index] = ui16group_low_limit_motors[ui8temp_index]; //push pass user limit, use motor limnits
        if ((ui16group_latest_pos[ui8temp_index] != DEFAULT_HIGHEST_POS)
                && (ui8temp_group & (1U << ui8temp_index)))
        {//Group addressed and exist
            if (ui16group_latest_pos[ui8temp_index] > (ui16group_low_limit[ui8temp_index] + END_STOP_DISTANCE))
            {
                if (ui16group_low_limit[ui8temp_index] > ui16low_limit)
                {
                    ui16low_limit = ui16group_low_limit[ui8temp_index];
                    ui16near_low_limit = ui16low_limit + END_STOP_DISTANCE;
                }
                ui8group.byte |= (1U << ui8temp_index);
            }
        }
    }
}

void goUp(void)
{
    ui8group.byte = 0U;
    ui16high_limit = 0xFFFFU;
    /* select limit */
    for (ui8temp_index = 0U; ui8temp_index < 4U; ui8temp_index++)
    {
        if (ui8button_timer >= PassLimitTime)
            ui16group_high_limit[ui8temp_index] = ui16group_high_limit_motors[ui8temp_index]; //push pass user limit, use motor limnits
        if ((ui16group_latest_pos[ui8temp_index] != DEFAULT_HIGHEST_POS) //Motor present in group
                && (ui8temp_group & (1U << ui8temp_index))) //Group active
        {
            if (ui16group_latest_pos[ui8temp_index] < (ui16group_high_limit[ui8temp_index] - END_STOP_DISTANCE))
            {
                if (ui16group_high_limit[ui8temp_index] < ui16high_limit)
                {
                    ui16high_limit = ui16group_high_limit[ui8temp_index];
                    ui16near_high_limit = ui16high_limit - END_STOP_DISTANCE;
                }
                ui8group.byte |= (1U << ui8temp_index);
            }
        }
    }
}

void keyboard_state_set(void)
{
    static INT8U oldKeyboardType;

    switch (keyboard_state)
    {
        case KYB_MANUAL:
            switch (button_type)
            {
                case DOWN_BUTTON:
                {
                    INT8U tp = ui8group.byte;
                    old_button_type = DOWN_BUTTON;
                    goDown();
                    if (ui8group.byte == 0)
                    {
                        ui8group.byte = tp; //Workaround for motor v3.9 error
                        if (!movementStarted)
                        {//Press at limit...
                            if (ui8button_timer < PassLimitTime) //About 4 secs
                                ui8button_timer++;
                        }
                    }
                    else
                    {
                        control_action = DOWN_ONLY;
                        movementStarted = 1;
                    }
                }
                    break;

                case UP_BUTTON:
                {
                    INT8U tp = ui8group.byte;
                    old_button_type = UP_BUTTON;
                    goUp();
                    if (ui8group.byte == 0)
                    {
                        ui8group.byte = tp; //Workaround for motor v3.9 error
                        if (!movementStarted)
                        {//Press at limit...
                            if (ui8button_timer < PassLimitTime) //About 4 secs
                                ui8button_timer++;
                        }
                    }
                    else
                    {
                        control_action = UP_ONLY;
                        movementStarted = 1;
                    }
                }
                    break;

                case BUTTON_COMB:
                    control_action = DONT_MOVE;
                    ui8button_timer++;
                    old_button_type = BUTTON_COMB;
                    break;

                case OK_BUTTON:
                    if ((ui1all_keeping_still_flag) && (error_priority == NO_ACTION) && (main_state != HOME))
                    {
                        control_action = DONT_MOVE;
                        ui8button_timer++;
                        old_button_type = OK_BUTTON;
                        oldKeyboardType = activeKeyboard.type;
                    }
                    break;

                case NUM_BUTTON:
                    if ((ui1all_keeping_still_flag) && (error_priority == NO_ACTION))
                    {
                        control_action = DONT_MOVE;
                        keyboard_state = KYB_AUTO;
                        getMemoryPosition((ui8related_address - M1_SAVEDPOS_G1H_ADD) >> 3);
                        auto_state = GRP_SELECT;
                    }
                    break;

                default: //release
                    control_action = DONT_MOVE;
                    if (old_button_type == BUTTON_COMB)
                    {
                        if (ui8button_timer >= 51U)//About 7 secs
                            keyboard_state = KYB_PRE_HOME;
                        else
                        {
                            keyboard_state = KYB_3_PUSH;
                            pushCounter = 2;
                        }
                    }
                    else if (old_button_type == OK_BUTTON)
                    {//Ok released 
                        if (oldKeyboardType == PADDLE_KYB && (ui8button_timer > 51))
                            keyboard_state = KYB_PRE_HOME; //7 sec long press - home
                        else if (ui8button_timer >= 11U)
                        {//Not a short keypressed
                            keyboard_state = KYB_SAVE_MEM;
                            ui1new_write_flag = 1U;
                        }
                    }
                    old_button_type = NO_BUTTON;
                    if (ui8button_timer >= PassLimitTime)
                        reset_active_limits(); //restore limits after pass of limits
                    ui8button_timer = 0U;
                    movementStarted = 0;
                    break;

            }/* end of switch (button_type) */
            break; /* end of KYB_MANUAL case */

        case KYB_PRE_HOME:
            switch (button_type)
            {
                case DOWN_BUTTON:
                    control_action = DOWN_ONLY;
                    keyboard_state = KYB_MANUAL;
                    main_state = HOME;
                    ui1clear_limits_flag = 1U;
                    /* Update the related address - important for clearing user defined limits */
                    ui8related_address = USER_HIGH_LIMIT_G1H_ADD;
                    break;

                case BUTTON_COMB:
                    control_action = DONT_MOVE;
                    break;

                case UP_BUTTON:
                case OK_BUTTON:
                case NUM_BUTTON:
                    /* on pressing anything else .. break the sequence */
                    control_action = DONT_MOVE;
                    keyboard_state = KYB_MANUAL;
                    break;

                default:
                    control_action = DONT_MOVE;
                    ui8button_timer++;
                    if (ui8button_timer >= 101U)
                        keyboard_state = KYB_MANUAL;
            }/* end of switch (button_type) */
            break; /* end of KYB_PRE_HOME case */

        case KYB_SAVE_MEM:
            control_action = DONT_MOVE;
            switch (button_type)
            {
                    /*                        case OK_BUTTON:
                                                    reset_active_limits();			// set new active limits 
                                                    keyboard_state = KYB_MANUAL;	// return to manual state 
                                                    ui8button_timer = 0U;
                                                    break;*/

                case NUM_BUTTON:
                    /* write addresses to selected memory location if groups are on the buss */
                    if (ui1new_write_flag)
                    {
                        menutimer = 2;
                        menustate = 13;
                        selected_mem = (ui8related_address - M1_SAVEDPOS_G1H_ADD) >> 3;
                        saveToEEprom();
                        for (ui8temp_index = 0U; ui8temp_index < 4U; ui8temp_index++)
                        {//All groups
                            if ((ui16group_latest_pos[ui8temp_index] != DEFAULT_HIGHEST_POS) && (ui8temp_group & (1U << ui8temp_index)))
                                processData.updatedMemory[ui8temp_index] |= 1 << selected_mem | 0x80;
                        }
                    }
                    ui1new_write_flag = 0U;
                    break;

                case NO_BUTTON:
                    if (ui1new_write_flag)
                    {
                        ui8button_timer++;
                        if (/*(old_button_type == OK_BUTTON)&&*/(ui8button_timer >= 21U))
                        {//Cancel after 2 seconds
                            keyboard_state = KYB_MANUAL;
                        }
                    }
                    else
                    {
                        keyboard_state = KYB_MANUAL;
                    }
                    break;

                default:
                    /* on pressing anything else .. break the sequence */
                    reset_active_limits(); // set new active limits 
                    keyboard_state = KYB_MANUAL;
                    ui8button_timer = 0U;
            }
            break;

        case KYB_3_PUSH:
            control_action = DONT_MOVE;
            switch (button_type)
            {
                case OK_BUTTON:
                case NUM_BUTTON:
                    ui8button_timer = 0;
                    keyboard_state = KYB_MANUAL;
                    break;
                case BUTTON_COMB:
                    if (old_button_type == NO_BUTTON)
                    {
                        pushCounter++;
                        ui8button_timer = 0;
                        old_button_type = BUTTON_COMB;
                    }
                    break;
                case NO_BUTTON:
                    if (old_button_type == BUTTON_COMB)
                    {
                        pushCounter++;
                        ui8button_timer = 0;
                        old_button_type = NO_BUTTON;
                    }
                    break;
            }
            ui8button_timer++;
            if (ui8button_timer > 21) //about 3 secs
            {//To long press or no press - abort
                ui8button_timer = 0;
                keyboard_state = KYB_MANUAL;
            }
            if (pushCounter == 6)
            {
                keyboard_state = KYB_SAVE_LIMIT;
                ui1new_write_flag = 1;
                ui8button_timer = 0;
            }
            break;

        case KYB_SAVE_LIMIT:
            control_action = DONT_MOVE;
            switch (button_type)
            {
                    /*                        case OK_BUTTON:
                                                    reset_active_limits();		// set new active limits 
                                                    keyboard_state = KYB_MANUAL;	// return to manual state 
                                                    ui8button_timer = 0U;
                                                    break;*/

                case UP_BUTTON:
                case DOWN_BUTTON:
                    /* write addresses to selected memory location if groups are on the buss */
                    if (ui1new_write_flag)
                    {
                        menutimer = 2;
                        menustate = 14; //UL
                        if (button_type == DOWN_BUTTON)
                            menustate = 15; //LL                               
                        saveToEEprom();
                        if (button_type == DOWN_BUTTON)
                            processData.flags.lowLimitUpdated |= ui8temp_group;
                        else
                            processData.flags.highLimitUpdated |= ui8temp_group;
                    }
                    ui1new_write_flag = 0U;
                    break;


                    //case NUM_BUTTON:
                case NO_BUTTON:
                    if (ui1new_write_flag)
                    {
                        ui8button_timer++;
                        if ((old_button_type == OK_BUTTON)&&(ui8button_timer >= 21U))
                        {
                            keyboard_state = KYB_MANUAL;
                            ui8button_timer = 0U;
                        }
                    }
                    else
                    {
                        reset_active_limits(); /* set new active limits */
                        keyboard_state = KYB_MANUAL; /* return to manual state */
                        ui8button_timer = 0U;
                    }
                    break;

                default:
                    /* on pressing anything else .. break the sequence */
                    keyboard_state = KYB_MANUAL;
                    ui8button_timer = 0U;
            }
            break;

            //        case KYB_AUTO:
            //        {
            //            static uint8_t timerr = 0;
            //            if (error_priority != NO_ACTION) //Error
            //                auto_state = AUTO_IDLE; //stop
            //            else if (deskProfile.autodrive)
            //            {
            //                if ((button_type != NO_BUTTON) && (button_type != NUM_BUTTON)) //another button
            //                    auto_state = AUTO_IDLE; //stop
            //                if (/*(selectedMenu == VISUAL_KYB) &&*/ (!doubleClick) && (button_type == NO_BUTTON))
            //                    auto_state = AUTO_IDLE; //Stop
            //            }
            //            else if (button_type != NUM_BUTTON)
            //                auto_state = AUTO_IDLE;
            //
            //
            //            switch (auto_state)
            //            {
            //                case GRP_SELECT:
            //                    auto_state = GRP_SELECT + groupSelect();
            //                    break;
            //
            //                case PRE_MOVE_UP:
            //                    auto_state = GRP_MOVE_UP;
            //                    control_action = UP_ONLY;
            //                    break;
            //
            //                case PRE_MOVE_DOWN:
            //                    auto_state = GRP_MOVE_DOWN;
            //                    control_action = DOWN_ONLY;
            //                    break;
            //
            //                case GRP_MOVE_UP:
            //                    control_action = UP_ONLY;
            //                case GRP_MOVE_DOWN:
            //                    if (auto_state == GRP_MOVE_DOWN)
            //                        control_action = DOWN_ONLY;
            //                    if (ui1all_keeping_still_flag)
            //                    {
            //                        // if(timerr++ > 25)
            //                        // {
            //                        auto_state = GRP_SELECT;
            //                        control_action = DONT_MOVE;
            //                        // }
            //
            //                    }
            //                    break;
            //
            //                case AUTO_IDLE:
            //                    keyboard_state = KYB_MANUAL;
            //                    reset_active_limits();
            //                    control_action = DONT_MOVE;
            //                    ui8group.byte = 15U; /* address all groups */
            //                    break;
            //            }
            //        }
            //            break;
    }/* end of switch (keyboard_state)*/
}/* end of void keyboard_state_set (void) */

/*******************************************************************************************************************
NAME 
   get_user_input

DESCRIPTION
    This function handles the input from the user from the keyboards. 
INPUT
    None

RETURNS
    No return
 *******************************************************************************************************************/
void get_user_input(void)
{
    static INT16U movementTimer = 0, oldLINpos = 0;
    static uint8_t keytimer = 0;
    /*button update switch statement*/
static uint8_t timerr = 0;
    if (keyboard_state == KYB_AUTO)
    {
        
        if (error_priority != NO_ACTION) //Error
            auto_state = AUTO_IDLE; //stop
        else if (deskProfile.autodrive)
        {
            if ((button_type != NO_BUTTON) && (button_type != NUM_BUTTON)) //another button
                auto_state = AUTO_IDLE; //stop
            if (/*(selectedMenu == VISUAL_KYB) &&*/ (!doubleClick) && (button_type == NO_BUTTON))
                auto_state = AUTO_IDLE; //Stop
        }
        else if (button_type != NUM_BUTTON)
            auto_state = AUTO_IDLE;


        switch (auto_state)
        {
            case GRP_SELECT:
                auto_state = GRP_SELECT + groupSelect();
                break;

            case PRE_MOVE_UP:
                auto_state = GRP_MOVE_UP;
                control_action = UP_ONLY;
                timerr = 0;
                break;

            case PRE_MOVE_DOWN:
                auto_state = GRP_MOVE_DOWN;
                control_action = DOWN_ONLY;
                timerr = 0;
                break;

            case GRP_MOVE_UP:
                control_action = UP_ONLY;
            case GRP_MOVE_DOWN:
                if (auto_state == GRP_MOVE_DOWN)
                    control_action = DOWN_ONLY;
                if (ui1all_keeping_still_flag)
                {
                    if (timerr++ > 25)
                    {
                        auto_state = GRP_SELECT;
                        control_action = DONT_MOVE;
                    }

                }
                else
                {
                    timerr = 0;
                }
                
                break;

            case AUTO_IDLE:
                keyboard_state = KYB_MANUAL;
                reset_active_limits();
                control_action = DONT_MOVE;
                ui8group.byte = 15U; /* address all groups */
                break;
        }
    }



    switch (ui8general_timer_1ms_tick)
    {

        case 49: /*10ms period - check port value again after debounce period */
            get_keyboard(); /* update keyboard bit structure */
            if (ui8keyboard.byte != ui8saved_port_value)
                ui8keyboard.byte = NO_INPUT; /* keyboard bounce - reset keyboard input byte */
            activeKeyboard.groupmask = ui8group_mask; /* set group mask to memory keyboard default set during production */
            activeKeyboard.type = ui8keyboard_type;
            activeKeyboard.keys = ui8keyboard;

            getSlaveInputs(); //Check info from slaves

#ifdef USE_SPI
            if (!BUTTON_NONE)
                actionCommanded = ACTION_STOP;
            else if (actionCommanded == ACTION_MOVE_UP)
            {
                activeKeyboard.groupmask = groupCommanded;
                activeKeyboard.keys.byte = 0xFD; //Button up
            }
            else if (actionCommanded == ACTION_MOVE_DOWN)
            {
                activeKeyboard.groupmask = groupCommanded;
                activeKeyboard.keys.byte = 0xFE; //button down
            }
#endif

            /* Translate the keyboard input */
            memory_keyboard();

            /* set the keyboard state */
            if (error_priority < SOFT_STOP)
                keyboard_state_set();
            else
            {//An error  - stop all actions
                auto_state = AUTO_IDLE;
#ifdef USE_SPI
                actionCommanded = NO_ACTION;
#endif
                if (button_type == NO_BUTTON)
                    control_action = DONT_MOVE; //Stop motion on error
            }

            //Check if motors are moving as they should
            if (error_type == MOTOR_NOT_MOVING)
            {//error occured
                if (button_type == NO_BUTTON)
                    error_type = NO_ERROR;
                else
                    control_action = DONT_MOVE;
            }
            else if (control_action == DOWN_ONLY || control_action == UP_ONLY)
            {//should move...
                movementTimer++;
                if (main_state == HOME || ui16LIN_position != oldLINpos)
                {//Movement...
                    oldLINpos = ui16LIN_position;
                    movementTimer = 0;
                }
                else
                {//Not moving
                    if (movementTimer > 30) //about 5 secs
                        error_type = MOTOR_NOT_MOVING;
                }
            }
            else
                movementTimer = 0;
            break;
    }/* end of button update switch statement */

}/* end of void get_user_input (void) */

unsigned char getSlaveInputs(void)
{
    unsigned char i, iBit, retval = 0;
    static INT8U lastCollisionState = 0, slaveCountState = 0, oldSlaveCount = 0, slaveLostCount = 0;

    switch (slaveCountState)
    {
        case 0: //Initiate a slave count
            if (ui8SchedulePhase < 10)
                ui8SlaveReported = 0;
            slaveCountState++;
            break;
        case 20:
            if (oldSlaveCount <= ui8SlaveReported)
            {
                oldSlaveCount = ui8SlaveReported; //Get first active slave-map
                if (error_type == SLAVE_LOST) error_type = NO_ERROR; //Restore error
                slaveLostCount = 0;
            }
            else
            {//Slave map has decreased...
                slaveLostCount++;
                if (slaveLostCount > 2)
                {
                    oldSlaveCount = 0;
                    error_type = SLAVE_LOST;
                }
            }
            slaveCountState = 0;
            break;
        default:
            if (slaveCountState & 0x01)
            {
                if (ui8SchedulePhase > 16) slaveCountState++;
            }
            else
                if (ui8SchedulePhase < 10) slaveCountState++;
            break;
    }

    for (i = 0; i <= ui8NumberOfSlaves; i++) //Always test for non-arbitrated slaves, i.e. 4.13
    {
        if (i < 7)
        {//Index 7 is the same as index 0
            switch (slaveKeyboards[i].type)
            {
                case 0xff: break; //Not updated

                case SENSOR_KYB:
                    if (main_state != HOME) //no collisions detect during homing
                    {
                        iBit = 1 << i;
                        if (slaveKeyboards[i].keys.byte == KYB_COLLISION) //Collision check
                        {
                            if (control_action == DONT_MOVE)
                            {//Stand still
                                if(error_type == COLLISION_DETECTED)
                                {
                                     
                                     error_priority_set(3);
                                }
                                   
#ifdef USE_SPI
                                if (!(lastCollisionState & iBit) && motionDetected < 255)
                                    motionDetected++;
                                lastCollisionState |= iBit;
#endif
                            }
                            else
                            {
                                if (slaveKeyboards[i].groupmask & ui8group.byte)
                                {//collision detected
                                    if (ui16LIN_position > ui16low_limit + ui16CDS_end_margin && ui16LIN_position < ui16high_limit - ui16CDS_end_margin)
                                    {//Collison within valid region
                                        ui8error_groups = slaveKeyboards[i].groupmask;
                                        for (ui8temp_index = 0; ui8temp_index < 4; ui8temp_index++)
                                            if (ui8error_groups & (1 << ui8temp_index))
                                            {
                                                INT8U gp = ui8group_ISP[ui8temp_index];
                                                if (gp == 0) gp = 4;
                                                group_error_priority [ui8temp_index] = gp;
                                                error_priority_set(gp);
                                                error_type = COLLISION_DETECTED;
                                            }
                                        
#ifdef USE_SPI
                                        actionCommanded = ACTION_STOP;
#endif
                                        auto_state = AUTO_IDLE;
                                        retval = 1;
                                    }
                                }
                            }
                        }
#ifdef USE_SPI
                        else if (slaveKeyboards[i].keys.byte == KYB_PRESENCE) //Presence check
                        {
                            if (control_action == DONT_MOVE)
                            {//Presence detected
                                if (!(lastCollisionState & iBit) && motionDetected < 255)
                                    motionDetected++;
                                lastCollisionState |= iBit;
                            }
                        }
#endif
                        else
                            lastCollisionState &= 0xffu - iBit;
                    }
                    break;

                case MEMORY_KYB:
                case LCD_KYB:
                    selectedMenu = LCD_KYB;
                default:
                    if ((activeKeyboard.keys.byte == NO_INPUT) && (slaveKeyboards[i].keys.byte != NO_INPUT))
                    {
                        activeKeyboard.keys = slaveKeyboards[i].keys;
                        activeKeyboard.type = slaveKeyboards[i].type;
                        activeKeyboard.groupmask = slaveKeyboards[i].groupmask;
                        retval = 1;
                    }
                    break;
            }
        }
    }

    return retval;
}
/****************************************** END OF FILE ***********************************************************/