/*******************************************************************************
Copyright 2016 Microchip Technology Inc. (www.microchip.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

To request to license the code under the MLA license (www.microchip.com/mla_license), 
please contact mla_licensing@microchip.com
 *******************************************************************************/

/** INCLUDES *******************************************************/

#include "usb.h"
#include "indicator.h"
#include <GenericTypeDefs.h>
#include "accessory_process.h"
#include "accessory_commands.h"
#include "HardwareProfile.h"
#include <string.h>
#include "usb_handlers.h"
#include <stdio.h>
#include <stdint.h>
#include "usb_device_hid.h"

/** DECLARATIONS ***************************************************/

/** TYPE DEFINITIONS ************************************************/

/** VARIABLES ******************************************************/
/* Some processors have a limited range of RAM addresses where the USB module
 * is able to access.  The following section is for those devices.  This section
 * assigns the buffers that need to be used by the USB module into those
 * specific areas.
 */
/** VARIABLES ******************************************************/
/* Some processors have a limited range of RAM addresses where the USB module
 * is able to access.  The following section is for those devices.  This section
 * assigns the buffers that need to be used by the USB module into those
 * specific areas.
 */
#if defined(FIXED_ADDRESS_MEMORY)
#if defined(COMPILER_MPLAB_C18)
#pragma udata HID_CUSTOM_OUT_DATA_BUFFER = HID_CUSTOM_OUT_DATA_BUFFER_ADDRESS
unsigned char ReceivedDataBuffer[64];
#pragma udata HID_CUSTOM_IN_DATA_BUFFER = HID_CUSTOM_IN_DATA_BUFFER_ADDRESS
unsigned char ToSendDataBuffer[64];
#pragma udata

#else defined(__XC8)
unsigned char ReceivedDataBuffer[64] @HID_CUSTOM_OUT_DATA_BUFFER_ADDRESS;
unsigned char ToSendDataBuffer[64] @HID_CUSTOM_IN_DATA_BUFFER_ADDRESS;
#endif
#else
unsigned char ReceivedDataBuffer[64];
unsigned char ToSendDataBuffer[64];
#endif

volatile USB_HANDLE USBOutHandle;
volatile USB_HANDLE USBInHandle;

/*********************************************************************
 * Function: void APP_DeviceVendorBasicDemoInitialize(void);
 *
 * Overview: Initializes the demo code
 *
 * PreCondition: None
 *
 * Input: None
 *
 * Output: None
 *
 ********************************************************************/
void APP_DeviceCustomHIDInitialize()
{
    //initialize the variable holding the handle for the last
    // transmission
    USBInHandle = 0;
    USBOutHandle = 0;
    //enable the HID endpoint
    USBEnableEndpoint(CUSTOM_DEVICE_HID_EP, USB_IN_ENABLED | USB_OUT_ENABLED | USB_HANDSHAKE_ENABLED | USB_DISALLOW_SETUP);
    //Re-arm the OUT endpoint for the next packet
    USBOutHandle = (volatile USB_HANDLE)HIDRxPacket(CUSTOM_DEVICE_HID_EP, (uint8_t *)&ReceivedDataBuffer[0], 64);
}

BOOL KeepReceiveBufferUnmodified()
{
    //something received
    //ignore session ID
    //Check for sync bytes
    unsigned char index = 0;
    BOOL keepPacket = FALSE;

    while (index < (USB_BUFFER_SIZE - 1))
    { //Use if multiple commands might appear in buffer
        while ((index < (USB_BUFFER_SIZE - 1)) &&
               (!((ReceivedDataBuffer[index] == SYNC_BYTE_1) && (ReceivedDataBuffer[index + 1] == SYNC_BYTE_2))))
        {
            index++;
        }

        if (index < (USB_BUFFER_SIZE - 1))
        { //Frame start found
            BYTE *packet = &(ReceivedDataBuffer[index + 2]);
// #if DEBUG_LOG_USB_HANDLER
//             prints("\n\rUSB: Got:");
//             printHex(packet, 1 + packet[0]);
// #endif

            if (validateDataFrame(packet))
            { //Packet valid - process
                if (!addDataFrame(&incomingDataFrames, packet))
                {
                    // Packet is valid, but rest of system is not in phase - keep it
                    AddError(WARNING_BUFFER_FULL);
                    keepPacket = TRUE;
                }
                index += packet[0]; //+1 below..
            }
            // else
            // { //Dump invalid buffer
            //     // prints("\n\rIndex:");
            //     // printHexByte(index);
            //     // prints(" Buffer:");
            //     // printHex(ReceivedDataBuffer,64);
            // }

            index++;
        }
    }

    return keepPacket;
}

void usbTransfer(void)
{
    //Check if we have received an OUT data packet from the host
    if (HIDRxHandleBusy(USBOutHandle) == false)
    {   //something received
        if (!KeepReceiveBufferUnmodified())
        {
            USBOutHandle = HIDRxPacket(CUSTOM_DEVICE_HID_EP, (uint8_t *)&ReceivedDataBuffer[0], 64);
        }
        #if 0 //Deprecated
        // ignore session ID
        // Check for sync bytes
        unsigned char index = 0;
        while (index < USB_BUFFER_SIZE - 1) //Use if multiple commands might appear in buffer
        {
            while ((index < USB_BUFFER_SIZE - 1) && (!((ReceivedDataBuffer[index] == SYNC_BYTE_1) && (ReceivedDataBuffer[index + 1] == SYNC_BYTE_2))))
                index++;
            if (index < USB_BUFFER_SIZE - 1)
            { //Frame start found
                BYTE *packet = &(ReceivedDataBuffer[index + 2]);

                if (validateDataFrame(packet))
                { //Packet valid - process
                    if (!addDataFrame(&incomingDataFrames, packet))
                        AddError(WARNING_BUFFER_FULL);
                    index += packet[0]; //+1 below..
                }
                index++;
            }
        }
        //Re-arm the OUT endpoint for the next packet
        USBOutHandle = HIDRxPacket(CUSTOM_DEVICE_HID_EP, (uint8_t *)&ReceivedDataBuffer[0], 64);
        #endif
    }

    if (!HIDTxHandleBusy(USBInHandle))
    { //Ready to send
        BYTE *packet = getNextFrame(&outgoingDataFrames);

        if (packet != NULL)
        { //Send outgoing frames
            ToSendDataBuffer[0] = SYNC_BYTE_1;
            ToSendDataBuffer[1] = SYNC_BYTE_2;
            memcpy(&ToSendDataBuffer[2], packet, packet[0] + 1);
            USBInHandle = HIDTxPacket(CUSTOM_DEVICE_HID_EP, (uint8_t *)&ToSendDataBuffer[0], 64);
            removeNextFrame(&outgoingDataFrames);
        }
    }
}