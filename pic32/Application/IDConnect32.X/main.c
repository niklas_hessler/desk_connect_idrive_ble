#include "mcc.h"
#include "linmasterprocess.h"
#include <GenericTypeDefs.h>
#include "usb_handlers.h"
#include "usb_device.h"
#include "HardwareProfile.h"
#include "BLE.h"
/****iDRIVE*****/
#include "manne.h"
#include "motor.h"
#include "lcd_menu.h"
#include "lcd.h"
#include "usb_config.h"
/*******IDESK********/
#include "spi_process.h"
#include "spi_commands.h"
#include "accessory_process.h"
#include "accessory_commands.h"
#include "userProfiles.h"
#include "indicator.h"
#include "flashSettings.h"
#include "Tick.h"
#include "tmr2.h"

#define TICKS_PER_250US TICKS_PER_SECOND / 4000
#define TICKS_PER_10MS TICKS_PER_SECOND / 100
#define TICKS_PER_100MS TICKS_PER_SECOND / 10
#define HIGH_BYTE(x) ((x >> 8) & 0xFF)
#define LOW_BYTE(x) (x & 0xFF)
uint16_t frametick = 0;
extern INT16U ui16CDS_end_margin;
extern DeskProfile deskProfile;
void linSniff()
{
    static uint8_t linFrame[20];
    static uint8_t linbuf[64];
    static uint8_t i = 0;
    static uint8_t k = 0;
    static uint8_t package_frame_counter = 0;
    if (package_frame_counter == 2 || ((package_frame_counter > 0) && frametick >= 100))
    {
        frametick = 0;
        package_frame_counter = 0;
        BYTE respons[k + 3];
        BYTE *bpekare;
        bpekare = respons;
        respons[0] = k + 2;
        respons[1] = ACC_UPDATE_LIN_RAW;
        respons[k + 2] = 0x00;
        memcpy(&respons[2], linbuf, k);
        updateChecksum(respons);

        if (!addDataFrame(&outgoingDataFrames, respons))
        {
            AddError(WARNING_BUFFER_FULL);
        }
        k = 0;
    }

    if (getCount() > 0)
    {
        linFrame[i++] = UART1_Read();

        if (i >= 2 && ((linFrame[i - 2] == 0 && linFrame[i - 1] == 0x55) || i == 20))
        {
            i = i - 2;
            memcpy(&linbuf[k], linFrame, i);
            k = k + i;
            i = 2;
            linFrame[0] = 0x00;
            linFrame[1] = 0x55;
            package_frame_counter++;
        }
    }
}

void Initialize(void);

ProcessData processData;
extern volatile USB_HANDLE USBInHandle;

void main(void)
{
    memset(processData.LegSlaveAddress, 0, 8);
    static DWORD tickCopy, timeLast10ms = 0, timeLast100ms = 0, timeLast1s = 0;
    CONNECTION_STATE oldConnection = CONNECTION_INIT;
    static uint8_t linTimerHandlerTimeout = 0;
    static bool DoOnce = true;
    SYSTEM_Initialize();
    //SoftSet.linSniffMode = 1;
    if (SoftSet.linSniffMode == 1)
    {
        BUS_PULLUP_OFF;
        main_state = LIN_SNIFFER;
        LIN_enableRx();
    }
    else
    {
        init_controller();
    }
    Initialize();
    while (1)
    {
#if defined(USB_POLLING)
        USBDeviceTasks();
#endif
        processData.connection_state = CONNECTION_INIT;
        if (USBGetDeviceState() == CONFIGURED_STATE)
        {
            processData.connection_state = CONNECTION_READY;
            if (USBIsDeviceSuspended() != true)
            {
                processData.connection_state = CONNECTION_OPEN;
                //Application specific tasks
                if (DoOnce)
                {
                    DoOnce = false;
                    clearuartbuffer();
                }
                if (SoftSet.linSniffMode == 1)
                {
                    linSniff();
                }
                usbTransfer();
            }
        }

        if (processData.connection_state == CONNECTION_OPEN)
        { //Connected
            oldConnection = processData.connection_state;
            processData.disconnectDelay = 0;
        } /*        else if (processData.connection_state==CONNECTION_READY)
        {//USB suspended
        }*/
        else
        { //Not connected
            if (oldConnection == CONNECTION_OPEN)
            { //Just disconnected
                clearuartbuffer();
                ErrorLogInit();
                DoOnce = true;
                processData.spiReset = 1; //Stop and position update
                initDataFrameBuffers();   //Drain any commands in the queues
                oldConnection = processData.connection_state;
            }
        }
    
        ProcessIO();

        BLE_Task();

        tickCopy = TickGet(); //Fetch timer
        if ((tickCopy - timeLast10ms) > TICKS_PER_10MS)
        {
            frametick++;
            timeLast10ms = tickCopy;
            ProcessSPI();
            if ((tickCopy - timeLast100ms) > TICKS_PER_100MS)
            { //Every 100ms
                timeLast100ms = tickCopy;
                if (processData.homing && processData.lastCommand == SPI_STOP)
                {
                    processData.spiReset = 1;
                    processData.homing = FALSE;
                }

                if (processData.spiReset)
                {
                    processData.newCommand = SPI_STOP;
                    processData.spiReset--;
                }
                if (SoftSet.linSniffMode)
                    LINSnifferIndicator();
                else
                    IndicatorTask();
            }
        }

        if (SoftSet.linSniffMode == 0)
        {
            LIN_Masterhandler();
            if (timertick)
            {
                timertick = 0;
                if (linTimerHandlerTimeout == 1)
                {
                    LIN_timerHandler();
                    linTimerHandlerTimeout = 0;
                }
                else
                    linTimerHandlerTimeout++;
                master_function();
            }
        }
    } //end while
} //end main

extern struct
{
    uint8_t bLength;
    uint8_t bDscType;
    uint16_t string[24];
} sd002;

extern struct
{
    uint8_t bLength;
    uint8_t bDscType;
    uint16_t string[9];
} sd003;

void Initialize(void)
{
    //init_controller();
    WDT_WatchdogTimerClear();
    timertick = 0U;

    ui8keyboard_type = SPI_KYB;
    lcd_menu_init();

    //Read flash settings
    initDeskProfile();
    uint8_t serialLen = 13, ch = 0, i=0;
    uint8_t str[13] = {'\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0','\0'};
    uint64_t id = deskProfile.deskID.value;
    const uint8_t baseTable[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    while (id != 0)
    {
//        str[--serialLen] = baseTable[id % 36];
//        id /= 36;
        str[--serialLen] = (id % 10) + '0';
        id /= 10;
    }
    
    for (i = serialLen; i <  13; i++)
    {
        sd002.string[13 + ch] = sd003.string[ch]= str[i];
        ch++;
    }
    for (; ch <  13; ch++)
    {
        sd002.string[13 + ch] = sd003.string[ch] = '\0';
    }
    ErrorLogInit();
    //initialise Global memory areas
    initDataFrameBuffers();
    memset((UINT8 *)&processData, 0, sizeof(ProcessData));
    processData.newCommand = processData.lastCommand = SPI_DUMMY;
    processData.group = 1;
    processData.bleFlags.setDeviceName=1;
    
    //Gather settingsdata from EEprom
    ui16CDS_end_margin = read_eeprom_16(CDS_END_MARGIN_HIGH_ADD);
    ui8group_mask = read_eeprom(ACTIVE_GROUP_MASK_ADD); /* set group mask to memory keyboard default set during production */
    USBDeviceInit();
#if defined(USB_INTERRUPT)
    USBDeviceAttach();
#endif
}

/**
 * 
 * 
 * 
 * 
 * 
 * 
 */