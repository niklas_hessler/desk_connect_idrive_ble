/*****************************************************************************\
*                                                                             *
*                _  _               ___             _             			  *
*               (_)| |__   _ __    / _ \ _ __ ___  | |__    /\  /\			  *
*               | || '_ \ | '__|  / /_\/| '_ ` _ \ | '_ \  / /_/ /            *
*               | || | | || |    / /_\\ | | | | | || |_) |/ __  / 			  *
*               |_||_| |_||_|    \____/ |_| |_| |_||_.__/ \/ /_/  			  *
*      																		  *
*  ihr GmbH																	  *
*  Airport Boulevard B210													  *
*  77836 Rheinmünster - Germany												  *
*  http://www.ihr.de													      *
*  Phone +49(0) 7229-18475-0        									      *
*  Fax   +49(0) 7229-18475-11       									      *
*                                                                             *
*******************************************************************************
*    					                                                      *
*    					                                                      *
* (c) Alle Rechte bei IHR GmbH, auch fuer den Fall von Schutzrechts-    	  *
* anmeldungen. Jede Verfuegungsbefugnis, wie Kopier- und Weitergaberecht  	  *
* bei uns.                                                                    *
*                                                                             *
* (c) All rights reserved by IHR GmbH including the right to file             *
* industrial property rights. IHR GmbH retains the sole power of              *
* disposition such as reproduction or distribution.                           *
*    					                                                      *
*    					                                                      *
********************   Workfile: 	  lin_type.h     	 **********************
*                                                                             *
*  PROJECT-DESCRIPTION:  LIN Driver Protocol Layer						      *
*                                                                             *
*  FILE-DESCRIPTION:  All defines special data types for the lin driver       *
*    					                                                      *
*                                                                             *
*******************************************************************************
*        Revision: 		1.5		                                              *
*        From Author:   B.Kroepfl                                             *
*		 Creation date: 05/02/2009			                                  *
*        Last Modtime:  30/09/2011				                              *
*                                                                             *
*  HISTORY:                                                                   *
*                                                                             *
*    Rev 1.0   05/02/2009 by B.Kroepfl 										  *
*					- Creation of the file                              	  *
*    Rev 1.1   26/01/2010 by B.Kroepfl 										  *
*					- word data type for autobaud bug fix added               *
*		 Rev 1.2   15/02/2010 by B.Kroepfl                                    *
*		      - Fileheader changed                                            *
*		 Rev 1.3   18/06/2010 by B.Kroepfl                                    *
*		      - Compiler Switches added                                       *
*		      - Datatype for timeout control added                            *
*		 Rev 1.4   26/10/2010 by B.Roegl                                      *
*		      - Datatype added for Eventtriggered Collision resolving         *
*		      - Fileheader changed                                            *
*        Rev 1.5   30/09/2011 by B.Roegl                                      *
*             - Added include for compiler switches                           *
*                                                                             *
\*****************************************************************************/
/**
@file lin_type.h
@brief Header file for all lin files

	Defines all datatypes needed in the lin driver
*/

#include "genlinconfig.h"

#ifndef LIN_TYPE_H //to interprete header file only once
#define LIN_TYPE_H
//========================================================================
//  Type Definitions
//========================================================================
typedef unsigned char l_bool;		///< Boolean Datatype
typedef unsigned char l_u8;	///< 8Bit Datatype 
typedef unsigned int l_u16; ///< 16Bit Datatype
typedef unsigned int l_irqmask, l_ioctl_op; ///< Datatypes for special LIN functions
typedef unsigned int l_signal_handle, l_flag_handle, l_ifc_handle, l_schedule_handle; ///< Datatypes for special LIN functions

//========================================================================
//  Structures
//========================================================================
/**
@struct t_Lin_Frame
@brief	Stucture for one line of the LIN Framebuffer
*/
#ifdef LIN_ENABLE_ASSIGN_FRAME_ID
typedef union{ 
  l_u8 reg[5];
  struct{
    struct
    {
      l_u8  lo;
      l_u8  hi;
    }msg_id;
    l_u8	pid;
    struct
  	{
      l_u8 publisher               :1;
      l_u8 update_flag             :1;
      l_u8 res                     :2;
      l_u8 transfer_type           :4;
    }frame_type;
    l_u8	length;
  }frame;	
}t_Lin_Frame_Ctrl;
#else
typedef union{ 
  l_u8 reg[3];
  struct
  {
    l_u8	pid;
		struct
  	{
      l_u8 publisher               :1;
      l_u8 update_flag             :1;
      l_u8 res                     :2;
      l_u8 transfer_type           :4;
    }frame_type;
    l_u8	length;
  }frame;	
}t_Lin_Frame_Ctrl;
#endif //end #ifdef LIN_ENABLE_ASSIGN_FRAME_ID
/**
@struct LIN_DIAG_MREQ_s
@brief	Stucture for diagnostic master request frame
*/
typedef union
{
  l_u8 byte[8];
  struct
  {
    l_u8              NAD;
    l_u8              PCI;
    union
    {
      struct
      {
        l_u8              SID;
        l_u8              data[5];
      }SF;
      struct
      {
        l_u8              LEN;
        l_u8              SID;
        l_u8              data[4];
      }FF;
      struct
      {
        l_u8              data[6];
      }CF;
    }type;
  }frame;
}t_lin_diag_frame;



/**
@struct LIN_PROD_ID_s
@brief	Stucture for LIN Production Identification 
*/
typedef struct LIN_PROD_ID_s
{
  l_u8    NAD;
  l_u8    Supplier_lo;
  l_u8    Supplier_hi;
  l_u8    Function_lo;
  l_u8    Function_hi;
  l_u8    Variant;
	l_u8		Initial_NAD;
}t_lin_prod_id;

/**
@struct LIN_PROD_ID_s
@brief	Stucture for LIN Production Identification 
*/
typedef union u_lin_status_word
{
  struct
  {
    l_u16               error_in_resp                 :1;
    l_u16               successful_transfer           :1;
    l_u16               overrun                       :1;
    l_u16               goto_sleep                    :1;
    l_u16               bus_activity                  :1;
    l_u16               event_triggerd_frame_coll     :1;
    l_u16               save_config                   :1;
    l_u16               reserved                      :1;
    l_u16               last_pid                      :8;  
  }flag;
  l_u16                 reg;
}t_lin_status_word;

/**
@struct LIN_TP_TX_RX_CTRL
@brief	Stucture for LIN Transport Protocol to control receive of consecutive frames,
        and transmission of frames. 
*/
typedef struct LIN_TP_RX_CF_CTRL
{
  l_u8    rx_cf_on    :1;
  l_u8    tx_on       :1;
  l_u8    rx_t_out_on :1;
  l_u8    tx_t_out_on :1;
  l_u8    rx_cf_num   :4;
}t_lin_tp_tx_rx_ctrl;

/**
@struct LIN_RX_CTRL
@brief	Stucture for LIN to control receive of frames. 
*/
typedef union
{
  struct
  {
    l_u16   rx_diag     :1;
    l_u16   res1        :1;
		l_u16   rx_func_nad :1;
	  l_u16   res2				:1;
    l_u16   evttrgfrm   :1;
    l_u16   res3        :3;
    
  }flag;
  l_u16      reg        :8;
}t_lin_rx_ctrl;

/**
@struct LIN_TP_STATUS
@brief	Stucture for status of the active Jobs in the LIN Transport Protocol. 
*/
typedef struct LIN_TP_STATUS
{
  l_u16    rx_stat        :4;
  l_u16    tx_stat        :4;
}t_lin_tp_status;

/**
@struct L_SCHEDULETABLE
@brief	Stucture for a standard frame scheduletable. 
*/

/*
typedef union L_SCHEDULETABLE
{
	struct
	{
		l_u8	PID;
		l_u8	Type;
		l_u16	Delay;
	};

}l_ScheduleTable;
*/
typedef l_u16 l_ScheduleTable[3];

/**
@struct L_SCHEDULETABLE_EXT
@brief	Stucture for special frames in scheduletable. 
*/

typedef l_u16 l_ScheduleTable_ext[4];

/**
@struct L_SCHEDULETABLELIST
@brief	Stucture for list of scheduletables. 
*/

/*
typedef union L_SCHEDULETABLELIST
{
	l_ScheduleTable* table;
	l_ScheduleTable_ext* table_ext;
}l_ScheduleTableList;
*/
typedef l_u16* l_ScheduleTableList;

/**
@struct _lin_sched_data
@brief	Stucture for Schedule Table Management
*/
typedef struct _lin_sched_data
{
	unsigned char lin_current_schedule;	///< store actual used schedule 
	unsigned char lin_next_frame;		///< store next frame to transfer
	unsigned int lin_current_delay;		///< store delay until next transfer
	unsigned char lin_next_PID;			///< store PID of next Frame
} t_lin_sched_data;

/**
@struct l_node_diag_info
@brief	Stucture for Node Diagnostics Information
*/
typedef union l_node_diag_info
{
	l_u16		reg[6];
	struct 
	{
		l_u16 supplier_id;
		l_u16 function_id;
		l_u8  variant;
		l_u8  number_of_frames;
		l_u8	init_NAD;
		l_u8	conf_NAD;
		l_u16 p2_min;
		l_u16 st_min;
	}data;
} t_l_node_diag_info;

/**
@struct _lin_sched_data
@brief	Stucture for configurable Frames
*/
typedef union l_conf_frame_info
{
	l_u8	reg[3];
	struct
	{
		l_u8	msg_id_lo;
		l_u8	msg_id_hi;
		l_u8	pid;
	}data;
} t_l_conf_frame_info;

/**
@struct _lin_sched_data
@brief	Stucture for configurable Frames
*/
typedef t_l_node_diag_info* t_l_node_diag_info_list; 

/**
@struct _lin_sched_data
@brief	Stucture for configurable Frames
*/
typedef t_l_conf_frame_info* t_l_conf_frm_info_list;

/**
@struct l_word
@brief	Stucture for handling 16bit types easier on 8 bit MCUs. 
*/
typedef union
{
	struct
	{
		l_u8	lo;
		l_u8	hi;
	}b;
	l_u16	word;
}l_word;

/**
@struct LIN_TIMEOUT_CTRL
@brief	Stucture for control of timeout counters. 
*/
typedef union
{
	struct LIN_TIMEOUT_CTRL
	{
	  l_u16   autobaud        :1;
	  l_u16   frame           :1;
		l_u16   res							:6;
	}flag;
	l_u8 byte;
}t_lin_timeout_ctrl;

typedef struct
{
	l_u8 ETF_id;
	l_u8 resolver_sched_id;
}t_lin_resolver_table;


#endif // end ifndef LIN_TYPE_H
