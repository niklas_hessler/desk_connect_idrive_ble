#include "linmaster.h"
#include "linmasterprocess.h"
#include "uart1.h"
#include "tmr1.h"
#include "manne.h"
#include <stdbool.h>


/***********Extern*********************/
extern LIN_SLAVE_STATE_t lin_slave_state_g;
extern volatile INT16U ui16group_pos[4];
extern volatile INT16U ui16group_latest_pos[4];
extern uint8_t g_lin_diag_config_active;
extern bool ConfigDone;
extern INT8U ui8general_timer_1ms_tick;
static uint8_t SlaveReq = 0;
static void (*LIN_processData)(void);
extern INT8U ui8SchedulePhase;
static lin_packet_t LIN_packet;
static lin_rxpacket_t LIN_rxPacket;
static bool LIN_txReady = false;
lin_cmd_packet_t *schedule;
static uint8_t scheduleLength;
static uint8_t LIN_timeout = 10;
static uint8_t LIN_period = 0;
static bool LIN_timerRunning = false;
static bool LIN_enablePeriodTx = false;
volatile uint8_t LIN_timerCallBack = 0;
volatile uint8_t LIN_periodCallBack = 0;
static uint8_t rxIndex = 0;
bool linTiMEOUT = false;
uint8_t g_lin_active_pid=0;
void LIN_init(uint8_t tableLength, lin_cmd_packet_t *table, void (*processData)(void))
{
    schedule = table;
    scheduleLength = tableLength;
    LIN_processData = processData;
    LIN_stopTimer();
    LIN_setTimerHandler();
    LIN_startPeriod();
    lin_slave_state_g = idle;
}

void LIN_queuePacket(uint8_t cmd, uint8_t *data)
{

    lin_cmd_packet_t *tempSchedule = schedule; //copy table pointer so we can modify it
    uint8_t i;
    for (i = 0; i < scheduleLength; i++)
    {
        if (cmd == tempSchedule->cmd)
        {
            break;
        }

        tempSchedule++; //go to next entry
    }

    if (tempSchedule->cmd == LIN_ID_61)
        ui8general_timer_1ms_tick = 0U;

    if (tempSchedule->cmd == LIN_ID_17)
    {
        /* Next frame to be sent is MOTOR_SAMPLE_REQUEST */
        /* sync timer to LIN schedule */
        ui8general_timer_1ms_tick = 0U;
        //Store position data before reseting value for new round
        ui16group_latest_pos[0] = ui16group_pos[0];
        ui16group_latest_pos[1] = ui16group_pos[1];
        ui16group_latest_pos[2] = ui16group_pos[2];
        ui16group_latest_pos[3] = ui16group_pos[3];
        /* ~13 us @ 16MHz */
        sync_lin_motor_data();
    }

    UpdateSendReqData();
    if (tempSchedule->cmd == LIN_ID_18)
    {
        l_bool_wr_MOTOR_RUN_RESET_ALL_ERRORS(0U);
    }

    //Add ID
    LIN_packet.PID = LIN_calcParity(tempSchedule->cmd);
    if (tempSchedule->type == TRANSMIT)
    {

        //Build Packet - User defined data
        //add data
        if (tempSchedule->length > 0)
        {
            LIN_packet.length = tempSchedule->length;
            memcpy(LIN_packet.data, data, tempSchedule->length);
        }
        else
        {
            LIN_packet.length = 1; //send dummy byte for checksum
            LIN_packet.data[0] = 0xAA;
        }

        //Add Checksum
        LIN_packet.checksum = LIN_getChecksum(LIN_packet.length, LIN_packet.PID, LIN_packet.data);
    }
    else
    {                                                 //Rx packet2
        LIN_rxPacket.rxLength = tempSchedule->length; //data length for rx data processing
        LIN_rxPacket.cmd = tempSchedule->cmd;         //command for rx data processing
        LIN_rxPacket.timeout = tempSchedule->timeout;
    }

    LIN_txReady = true;
}

lin_state_t LIN_Masterhandler(void)
{
    static lin_state_t LIN_state = LIN_IDLE;

    //State Machine
    switch (LIN_state)
    {
    case LIN_IDLE:
    {
        lin_slave_state_g = idle;
        if ((LIN_txReady == true))
        {
            lin_slave_state_g = tx_data;
            LIN_txReady = false;
            LIN_disableRx(); //disable EUSART rx
            clearuartbuffer();
            //LIN_startTimer(LIN_rxPacket.timeout);

            LIN_sendPacket();                                              //Send Transmission
            memset(LIN_packet.rawPacket, 0, sizeof(LIN_packet.rawPacket)); //clear send data
            LIN_state = LIN_TX_IP;
        }
        //            else
        //            {
        //                //No Transmission to send
        //            }
    }
    break;
    case LIN_TX_IP:
    {
        lin_slave_state_g = tx_data;
        //Transmission currently in progress.
        if (LIN_TXIE == false)
        {
            if (LIN_TRMT == 1)
            {
                //Packet transmitted
                if (LIN_rxPacket.rxLength > 0)
                {
                    //Need data returned?
                    LIN_startTimer(LIN_rxPacket.timeout);
                    LIN_enableRx(); //enable EUSART rx
                    LIN_state = LIN_RX_IP;
                }
                else
                {

                    LIN_state = LIN_IDLE;
                }
            }
        }
    }
    break;
    case LIN_RX_IP:
    {
        lin_slave_state_g = rx_data;
        //Receiving Packet within window
        if (LIN_timerRunning == false)
        {
            //  LIN_txReady = false;
            if (LIN_rxPacket.cmd == 0x8)
            {
                linTiMEOUT = true;
            }
            if (LIN_rxPacket.cmd == 0x49)
            {
                linTiMEOUT = true;
            }
            if (LIN_rxPacket.cmd == 0x42)
            {
                linTiMEOUT = true;
            }
            if ((TST_LINComFailState(ENABLED)))
            {
                SET_LINComFailState(FAILED); /* Set failure flag, will be cleared by lin_arbitration() */
            }
            LIN_disableRx();
            //Timeout
            LIN_state = LIN_IDLE;
            //lin_slave_state_g = idle;
            linTiMEOUT = true;
            rxIndex = 0;
            memset(LIN_rxPacket.rawPacket, 0, sizeof(LIN_rxPacket.rawPacket)); //clear receive data
        }
        else if (getCount() > 0)
        {
            if (LIN_receivePacket() == true)
            {
                //All data received and verified
                //correct data recived
                LIN_disableRx(); //disable EUSART rx
                LIN_state = LIN_RX_RDY;
            }
        }
    }
    break;
        //
    case LIN_RX_RDY:
    {
        //Received Transmission
        LIN_processData();

        LIN_state = LIN_IDLE;
    }
    break;
    }
    return LIN_state;
}

bool LIN_receivePacket(void)
{
    if (rxIndex < LIN_rxPacket.rxLength)
    {
        //save data
        LIN_rxPacket.data[rxIndex++] = UART1_Read();
        Nop();
    }
    else
    {
        rxIndex = 0;
        lin_slave_state_g = rx_checksum;
        if (UART1_Read() == LIN_getChecksum(LIN_rxPacket.rxLength, LIN_rxPacket.cmd, LIN_rxPacket.data))
        {
            return true;
        }
    }
    return false;
}

void LIN_sendPacket(void)
{
    //Build Packet - LIN required data
    //Add Break
    uint8_t i;
    U1STAbits.UTXBRK = 1; //SEND

    UART1_Write(0x00); //send dummy transmission
    //Add Preamble
    UART1_Write(0x55);
    //Add ID
    UART1_Write(LIN_packet.PID);

    if (LIN_rxPacket.rxLength == 0)
    { //not receiving data
        //Build Packet - User defined data
        //add data
        for (i = 0; i < LIN_packet.length; i++)
        {
            UART1_Write(LIN_packet.data[i]);
        }
        //Add Checksum
        UART1_Write(LIN_packet.checksum);

        g_lin_diag_config_active = 0;
    }
    else
    {
        SlaveReq = 0;
    }
}

uint8_t LIN_getPacket(uint8_t *data)
{
    uint8_t cmd = LIN_rxPacket.cmd & 0x3F;

    memcpy(data, LIN_rxPacket.data, sizeof(LIN_rxPacket.data));
    memset(LIN_rxPacket.rawPacket, 0, sizeof(LIN_rxPacket.rawPacket)); //clear receive data

    return cmd;
}

uint8_t LIN_calcParity(uint8_t CMD)
{
    lin_pid_t PID;
    PID.rawPID = CMD;

    //Workaround for compiler bug:
    //    PID.P0 = PID.ID0 ^ PID.ID1 ^ PID.ID2 ^ PID.ID4;
    //    PID.P1 = ~(PID.ID1 ^ PID.ID3 ^ PID.ID4 ^ PID.ID5);
    PID.P0 = PID.ID0 ^ PID.ID1;
    PID.P0 = PID.P0 ^ PID.ID2;
    PID.P0 = PID.P0 ^ PID.ID4;
    PID.P1 = PID.ID1 ^ PID.ID3;
    PID.P1 = PID.P1 ^ PID.ID4;
    PID.P1 = PID.P1 ^ PID.ID5;
    PID.P1 = ~PID.P1;

    return PID.rawPID;
}

uint8_t LIN_getChecksum(uint8_t length, uint8_t pid, uint8_t *data)
{
    uint16_t checksum = pid;
    uint8_t i = 0;
    if (pid == LIN_ID_60 || pid == LIN_ID_61)
    {
        checksum = 0;
    }

    for (i = 0; i < length; i++)
    {
        checksum = checksum + *data++;
        if (checksum > 0xFF)
            checksum -= 0xFF;
    }
    checksum = ~checksum;

    return (uint8_t)checksum;
}

void LIN_startTimer(uint8_t timeout)
{
    LIN_timeout = timeout;
    //TMR2_Stop();
    //TMR2_Counter16BitSet();
    //TMR2_Start();
    LIN_timerRunning = true;
}

void LIN_timerHandler(void)
{

    if (LIN_timerRunning == true)
    {
        if (++LIN_timerCallBack >= LIN_timeout)
        {
            // ticker function call
            LIN_stopTimer();
        }
    }
    if (LIN_enablePeriodTx == true)
    {
        if (++LIN_periodCallBack >= LIN_period)
        {
            if ((g_lin_diag_config_active == 1) || (SlaveReq == 1))
            {
                LIN_sendPeriodicTx();
            }
            else if (ConfigDone == true)
            {
                LIN_sendPeriodicTx();
            }
        }
    }
    if (LIN_timerRunning == true)
    {

        Nop();
    }
}

void LIN_setTimerHandler(void)
{
    TMR2_SetInterruptHandler(LIN_timerHandler);
}

void LIN_stopTimer(void)
{
    // reset ticker counter
    LIN_timerCallBack = 0;
    LIN_timerRunning = false;
}

void LIN_startPeriod(void)
{
    LIN_enablePeriodTx = true;
}

void LIN_stopPeriod(void)
{
    // reset ticker counter
    LIN_periodCallBack = 0;
    LIN_enablePeriodTx = false;
}

void LIN_disableRx(void)
{
    U1STAbits.URXEN = 0;
    IEC1bits.U1RXIE = 0;
}

void LIN_enableRx(void)
{
    U1STAbits.URXEN = 1;
    IEC1bits.U1RXIE = 1;
}

void LIN_sendPeriodicTx(void)
{
    static uint8_t scheduleIndex = 0;
    lin_cmd_packet_t *periodicTx; //copy table pointer so we can modify it

    LIN_periodCallBack = 0;
    periodicTx = schedule + scheduleIndex;
    ui8SchedulePhase = scheduleIndex;
    g_lin_active_pid = periodicTx->cmd;

    if (periodicTx->cmd == 60)
    {
        SlaveReq = 1;
    }

    if (periodicTx->period > 0)
    {
        LIN_queuePacket(periodicTx->cmd, periodicTx->data);
    }

    do
    { //Go to next valid periodic command
        if (++scheduleIndex >= scheduleLength)
        {
            scheduleIndex = 0;
        }
        periodicTx = schedule + scheduleIndex;
    } while (periodicTx->period == 0);

    LIN_period = periodicTx->period;
}
