/* 
 * File:   linMaster.h
 * Author: Ramsen Nissan
 *
 * Created on den 13 Feb 2018
 */

#ifndef LINMASTER_H
#define	LINMASTER_H



#include <xc.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "tmr1.h"
#include "uart1.h"

#define LIN_TRMT U1STAbits.TRMT
#define LIN_TXIE IEC1bits.U1TXIE
#define LIN_RCIE IEC1bits.U1RXIE
#define SendBreak U1STAbits.UTXBRK


typedef enum {
    LIN_IDLE,
    LIN_TX_IP,
    LIN_RX_IP,
    LIN_RX_RDY
} lin_state_t;

typedef enum {
    TRANSMIT,
    RECEIVE
} lin_packet_type_t;

typedef struct {
    uint8_t cmd;
    lin_packet_type_t type;
    uint8_t length;
    uint8_t timeout;
    uint8_t period;
    uint8_t* data;
} lin_cmd_packet_t;

typedef union {

    struct {
        uint8_t PID;
        uint8_t data[8];
        uint8_t checksum;
        uint8_t length;
    };
    uint8_t rawPacket[11];
} lin_packet_t;

typedef union {

    struct {
        uint8_t cmd;
        uint8_t rxLength;
        uint8_t data[8];
        uint8_t checksum;
        uint8_t timeout;
    };
    uint8_t rawPacket[12];
} lin_rxpacket_t;

typedef union {

    struct {
        unsigned ID0 : 1;
        unsigned ID1 : 1;
        unsigned ID2 : 1;
        unsigned ID3 : 1;
        unsigned ID4 : 1;
        unsigned ID5 : 1;
        unsigned P0 : 1;
        unsigned P1 : 1;
    };
    uint8_t rawPID;
} lin_pid_t;

//Set up schedule table timings

void LIN_init(uint8_t tableLength, lin_cmd_packet_t* table, void (*processData)(void));

void LIN_queuePacket(uint8_t cmd, uint8_t* data);

bool LIN_receivePacket(void);

void LIN_sendPacket(void);

uint8_t LIN_getPacket(uint8_t* data);

lin_state_t LIN_Masterhandler(void);

uint8_t LIN_getChecksum(uint8_t length, uint8_t pid, uint8_t* data);

uint8_t LIN_calcParity(uint8_t CMD);

//Timer Functions
void LIN_startTimer(uint8_t timeout);

void LIN_timerHandler(void);

void LIN_setTimerHandler(void);

void LIN_stopTimer(void);

void LIN_startPeriod(void);

void LIN_stopPeriod(void);

void LIN_enableRx(void);

void LIN_disableRx(void);

void LIN_sendBreak(void);

void LIN_sendPeriodicTx(void);
#endif