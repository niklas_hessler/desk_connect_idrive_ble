#include "linmasterprocess.h"
#include "manne.h"
#include "button.h"
#include "motor.h"
#include "mcc.h"
#include "lcd_menu.h"
#include "lcd_menu2.h"
#include "interface.h"
#include "tmr2.h"
#include "accessory_process.h"



/***********Flags********************/
uint8_t g_lin_diag_config_active = 0;
uint8_t linSlaveResponsFlag = 0;
/*****************Externs*******************/
extern enum AUTO_STATE_ENUM auto_state;
extern unsigned char groupCommanded;
extern INT16U ui16high_limit;
extern INT16U ui16low_limit;
extern INT16U ui16group_high_limit[4];
extern INT16U ui16group_low_limit[4];
extern bool ui1any_motor_has_responded_flag;
extern bool ui1all_at_home_flag;
extern bool ui1restart_arbitration_flag;
extern volatile INT16U ui16highest_motor_pos;
extern volatile INT8U ui8group_ISP[4];
extern INT16U ui16near_high_limit;
extern INT16U ui16near_low_limit;
extern INT16U ui16high_limit_motors;
extern INT16U ui16low_limit_motors; 
extern INT16U ui16group_high_limit_motors[4];
extern INT16U ui16group_low_limit_motors[4]; 
extern INT8U ui8temp_motor_group;
extern INT8U ui8group_list[8];
extern INT16U ui16BackOffDistanceUp;
extern INT16U ui16BackOffDistanceDown; 
extern INT8U ui8BackOffTime;
extern INT16U lowestVersion;
extern INT16U highestVersion;
extern bool linTiMEOUT;
extern uint8_t ActiveMotorPID;
extern uint8_t g_lin_active_pid;
extern INT8U ui8MotorResponses;
extern unsigned char actionCommanded;
extern DeskProfile deskProfile;
uint8_t SlaveResp[8];
uint8_t MasterCMD[8] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};
bool ui1complete_arbitration = 0U;
bool ConfigDone = false;
uint8_t DispData[8];
uint16_t ui16temp = 0U; /* Used as temporary storage for 16-bit valueas read from the motor EEPROM */
INT8U ui8general_timer_1ms_tick = 0U; /* 1ms tick timer */
INT8U ui8timer125usCnt = 0U; /* Used in main() to distribute CPU load */
INT8U ui8DelayTime = 0U; /* Used in the MASTER_NEGOTIOATION process */
INT8U ui8temp1 = 0U; /* Temporary byte variable to be used anywhere except in interrupt */
INT8U ui8temp2 = 0U; /* Temporary byte variable to be used anywhere except in interrupt */
INT8U ui8temp3 = 0U; /* Temporary byte variable to be used anywhere except in interrupt */
INT8U ui8old_lin_slave_state_g = idle; /* Used to detect lin_slave_state_g transitions during arbitration */
/* Used to count number of arbitration initialisations to get all slaves to synch in the arbitration */
INT8U ui8ArbitrationCounter = 0U; /* Used to count number of arbitration initialisations to get all slaves to synch in the arbitration */
INT8U ui8ArbMotorNumber = 0U; /* Used to count motor number during arbitration, holds the number of active motors after arbitration */
INT8U ui8ArbResponseCntMs = 0U; /* Used to detect response timeout during LIN arbitration */
INT8U arb_state = ARB_IDLE;
INT8U ui8SchedulePhase = 0U; /* Used to keep track of LIN communication schedule phase */
INT8U ui8LoopKey = 0; //Key loop counter
INT16U ui16ReceivedRandomKey;

INT8U ui8NumberOfSlaves = 0; //Number of slaves detected and addressed
INT8U ui8SlaveReported = 0; //Bitwise of which slave reported
bool ui1clear_limits_flag = 0U; /* clear eeprom user defined limits and memory positions */
INT8U ui8NumberOfMotorsRequired = 0; //Number of motors required for system to run
INT8U ui8MotorSpeed = 0, ui8ReciepeRevision = 0;
enum abs_state_enum ui8AbsByteState = ABS_IDLE; /* State to indicate byte reception in arbitration. */
enum LINComFailState_enum LINComFailState = DISABLED; /* Used to flag detected LIN communication failures in arbitration */

unsigned char oldSlaveDetected = 0; //Indicate that a slave was detected but could not be addressed, i.e. an pre 4.15 version slave
unsigned char selectedMenu = VISUAL_KYB; //assume newest menu system
#ifdef USE_SPI
//unsigned char selectedMenu = VISUAL_KYB; //assume newest menu system
#endif
static lin_cmd_packet_t NORMALSCHED[] = {
    //Command, Type, TX/RX Length, Timeout, Period, Data Address
    {LIN_ID_17, TRANSMIT, 3, 0, PERIODTIME, MotorSamplePos},
    {LIN_ID_8, RECEIVE, 3, TIMEOUT, PERIODTIME, MotorData},
    {LIN_ID_9, RECEIVE, 3, TIMEOUT, PERIODTIME, MotorData},
    {LIN_ID_10, RECEIVE, 3, TIMEOUT, PERIODTIME, MotorData},
    {LIN_ID_11, RECEIVE, 3, TIMEOUT, PERIODTIME, MotorData},
    {LIN_ID_12, RECEIVE, 3, TIMEOUT, PERIODTIME, MotorData},
    {LIN_ID_13, RECEIVE, 3, TIMEOUT, PERIODTIME, MotorData},
    {LIN_ID_14, RECEIVE, 3, TIMEOUT, PERIODTIME, MotorData},
    {LIN_ID_15, RECEIVE, 3, TIMEOUT, PERIODTIME, MotorData},
    {LIN_ID_1, RECEIVE, 3, TIMEOUT, PERIODTIME, PeriphialData},
    {LIN_ID_2, RECEIVE, 3, TIMEOUT, PERIODTIME, PeriphialData},
    {LIN_ID_3, RECEIVE, 3, TIMEOUT, PERIODTIME, PeriphialData},
    {LIN_ID_4, RECEIVE, 3, TIMEOUT, PERIODTIME, PeriphialData},
    {LIN_ID_5, RECEIVE, 3, TIMEOUT, PERIODTIME, PeriphialData},
    {LIN_ID_6, RECEIVE, 3, TIMEOUT, PERIODTIME, PeriphialData},
    {LIN_ID_7, RECEIVE, 3, TIMEOUT, PERIODTIME, PeriphialData},
    {LIN_ID_18, TRANSMIT, 3, 0, PERIODTIME, MotorRun},
    {LIN_ID_0, TRANSMIT, 8, 0, 40, DispData}
};
static lin_cmd_packet_t CONFIGSCHED[] = {
    {LIN_ID_60, TRANSMIT, 8, 0, 40, MasterCMD},
    {LIN_ID_61, RECEIVE, 8, 52, 60, SlaveResp}
};

uint8_t TABLE_SIZE  = (sizeof(NORMALSCHED)/sizeof(lin_cmd_packet_t));
//Command, Type, TX/RX Length, Timeout, Period, Data Address


uint8_t TABLE_SIZE2 = (sizeof(CONFIGSCHED)/sizeof(lin_cmd_packet_t));




LIN_SLAVE_STATE_t lin_slave_state_g = idle;
unsigned int scheduleLookUp[] = {LIN_ID_8, LIN_ID_9, LIN_ID_10, LIN_ID_11, LIN_ID_12, LIN_ID_13, LIN_ID_14, LIN_ID_15, LIN_ID_1, LIN_ID_2, LIN_ID_3, LIN_ID_4, LIN_ID_5, LIN_ID_6, LIN_ID_7};

void LIN_Master_Initialize(uint8_t val) {
    switch (val) {
        case 1:
            LIN_init(TABLE_SIZE, NORMALSCHED, processLIN);
            break;

        case 2:
            LIN_init(TABLE_SIZE2, CONFIGSCHED, processLIN);
            break;
    }
}
void UpdateSendReqData(void);

void init_variables(void) {
    // main_state = LIN_ARBITRATION;
    arb_state = ARB_IDLE;
    ui8ArbMotorNumber = 0U;
    ui8ArbResponseCntMs = 0U;
    for (ui8temp_index = 0; ui8temp_index < 7; ui8temp_index++)
        slaveKeyboards[ui8temp_index].type = 0xff;
}

void init_controller(void) {
    WDT_WatchdogTimerClear();
    //SYSTEM_Initialize();
    BUS_PULLUP_OFF;
    main_state = MASTER_INIT_DELAY;
    WDT_WatchdogTimerClear();

    INT8U delayTimeMatrix[] = {MASTER_DELAY_0, MASTER_DELAY_1, MASTER_DELAY_2, MASTER_DELAY_3, MASTER_DELAY_4, MASTER_DELAY_5, MASTER_DELAY_6, MASTER_DELAY_7, MASTER_DELAY_8, MASTER_DELAY_9, MASTER_DELAY_10, MASTER_DELAY_11, MASTER_DELAY_12, MASTER_DELAY_13, MASTER_DELAY_14, MASTER_DELAY_15};
    ui8DelayTime = delayTimeMatrix[read_eeprom(MASTER_DELAY_NUMBER_ADDRESS)];

    if (ui8keyboard_type == SENSOR_KYB)
        ui8DelayTime = MASTER_DELAY_SLAVE;

    /* Get extra master delay offset from eeprom */
    ui8temp2 = read_eeprom(MASTER_DELAY_OFFSET_ADDRESS);

    /* Start up delay during which the master negotiation is performed */
    /* This takes typically 2.55 (255*10ms) seconds to complete */
    ui8timer125usCnt = 255U; /* Seed 125us tick counter to compensate for post-evaluation incrementation in the loop */
    ui8temp3 = 0U;

    while (ui8temp1 < 255U) /* Stay in this loop until all master arbitration is done */ {
        if (RX_IN_IS_HI) {
            /* Someone is driving the LIN bus */
            if (ui8temp3 < 70U) {
                ui8temp3++;
            }
        }/* if ( RX_IN_IS_HI ) */
        else /* if ( RX_IN_IS_LO ) */ {
            if (ui8temp3 < 70U) {
                ui8temp3 = 0U;
            }
        }
        if (timertick) /* 125 us tick */ {
            timertick = 0U;
            /*  10 ms period counter */
            if (ui8timer125usCnt >= 79U) /* 80*125us => 10ms */ {
                ui8timer125usCnt = 0U;

                if (ui8temp1 == ui8DelayTime) /* Important to be true first time if ui8DelayTime == 0U */ {
                    /* Now it's time to sample the LIN bus to see if we are allowed to be master */
                    /* Poll LIN bus 50 times ( takes ~100us @ 16 MHz)*/
                    /* If bus is high > ~10% of the time, start up as slave, someone else is driving the LIN bus high */
                    /* If bus is high < ~10% of the time, start up as master and drive the LIN bus high */
                    /* This will tolerate some transients during the polling */
                    ui8temp2 = 0U;
                    if (ui8temp3 < 70U) /* noone held the bus high more than about 300 �s during the delaytime */ {
                        for (ui8temp3 = 0U; ui8temp3 < 50U; ui8temp3++) {
                            if ((RX_IN_IS_HI) && (ui8temp2 < 50)) {
                                /* Someone else is driving the LIN bus */
                                ui8temp2++;
                            } /* if ( RX_IN_IS_HI ) */
                        } /* for (ui8temp3=0;ui8temp3<50;ui8temp3++) */
                        if (ui8temp2 < 5U) {
                            main_state = MASTER_STARTUP;
                            BUS_PULLUP_ON;
                        } else {
                            /* Someone held the bus high more than 10% of the polls */
                            main_state = SLAVE_STARTUP;
                            BUS_PULLUP_OFF;
                        }
                    } else {
                        /* Someone held the bus high more than 10% of the polls */
                        main_state = SLAVE_STARTUP;
                        BUS_PULLUP_OFF;
                    }
                    ui8temp2 = 0U; /* Important to clear this variable not to cause a new offset delay */
                }
                if (ui8temp2 > 0U) {
                    /* MASTER_DELAY_OFFSET has not yet elapsed */
                    ui8temp2--;
                } else {
                    ui8temp1++; /* Increment 10ms counter */
                }
            } else {
                ui8timer125usCnt++;
            }
            WDT_WatchdogTimerClear();
        }
    }

    init_variables();
    if (main_state == MASTER_STARTUP) {
        ui8temp1 = read_eeprom(MASTER_DELAY_NUMBER_ADDRESS);
        if ((ui8temp1 & 0x01) == 1U) {
            /* Odd delay number but started as master, change to even delay number */

            WDT_WatchdogTimerClear();
            write_eeprom(MASTER_DELAY_NUMBER_ADDRESS, ui8temp1 - 1U);
            WDT_WatchdogTimerClear();
        }

        /* Initialize LIN Master for arbitration */
        main_state = LIN_ARBITRATION;
        ui8ArbitrationCounter = 0U;
        //fixprobsl_sch_set(ConfigSchedule, 0); // start schedule
    }

    if (main_state == SLAVE_STARTUP) {
        ui8temp1 = read_eeprom(MASTER_DELAY_NUMBER_ADDRESS);
        if ((ui8temp1 & 0x01) == 0U) {
            /* Even delay number but started as slave, change to odd delay number */
            WDT_WatchdogTimerClear();
            write_eeprom(MASTER_DELAY_NUMBER_ADDRESS, ui8temp1 + 1U);
            WDT_WatchdogTimerClear();
        }
        /* Initialize LIN Slave */
        main_state = NORMAL_SLAVE;
        /*hand over execution to slave application */

        /* Here the slave stack should be initialised */
    }

    //

    //  Flashpage i flashen �r eeprom och sedan en kopia av eepromet i ram
}

void master_function(void) {

    switch (ui8timer125usCnt) {

        case 0U:
        case 2U:
        case 4U:
        case 6U:

            // if ((TST_LINComFailState(ENABLED)) && (linTiMEOUT == true))// && (ui8old_lin_slave_state_g == rx_data) && (lin_slave_state_g != rx_data))// && (lin_slave_state_g != rx_checksum))
            // {
            //     linTiMEOUT = false;
            //     SET_LINComFailState(FAILED); /* Set failure flag, will be cleared by lin_arbitration() */
            // }
            //moved to linmaster

            ui8old_lin_slave_state_g = lin_slave_state_g;
            switch (ui8AbsByteState) {
                case ABS_IDLE:
                {
                    /* Just wait, lin_arbitration() will change state to ABS_WAIT_FOR_PID_7D when necessary */
                }
                    break;
                case ABS_WAIT_FOR_PID_7D:
                {
                    if (g_lin_active_pid == 0x7DU) {
                        ui8AbsByteState = ABS_WAIT_FOR_INTERRUPT;
                    }
                }
                    break;
                case ABS_WAIT_FOR_INTERRUPT:
                {
                    if (ui8SchedulePhase == MASTER_REQUEST_FRAME) {
                        ui8AbsByteState = ABS_IDLE;
                    }
                }
                    break;
                case ABS_BYTE_RECEIVED:
                {
                    /* Just wait, lin_arbitration() will change state to ABS_IDLE when necessary */
                }
                    break;
                default:
                    break;
            }
            ui8timer125usCnt++;
            break;
        case 1:

            if (main_state == LIN_ARBITRATION) {
                lin_arbitration();
            }/* if (main_state == LIN_ARBITRATION) */
            else if (main_state == CONFIG) {
                ReadWriteConfig();
            } else { /* Redo complete arbitration */
                if (ui1complete_arbitration) {
                    ConfigDone = false;
                    ui1complete_arbitration = 0U;
                    main_state = LIN_ARBITRATION;
                    ui8ArbitrationCounter = 0U;
                    //LIN_init(TABLE_SIZE2, CONFIGSCHED,processLIN);
                    LIN_Master_Initialize(2);
                    //is this correcy init?
                    arb_state = ARB_IDLE;
                    ui8ArbMotorNumber = 0U;
                    ui8ArbResponseCntMs = 0U;
                    MasterCMD[0] = 0x40U;
                }
                if (ui1restart_arbitration_flag) {
                    ConfigDone = false;
                    ui1restart_arbitration_flag = 0U;
                    main_state = LIN_ARBITRATION;
                    ui8ArbitrationCounter = 0U;
                    //LIN_init(TABLE_SIZE2, CONFIGSCHED,processLIN);
                    LIN_Master_Initialize(2);
                    //is this correcy init?

                    arb_state = ARB_RESTART;
                    ui8ArbResponseCntMs = 0U;
                    MasterCMD[0] = 0x40U; /*No slave selected*/
                }
            }
            ui8general_timer_1ms_tick++;

            //TODO:ROLP
            if (ui8NumberOfMotorsRequired == 0 || ui8ArbMotorNumber == ui8NumberOfMotorsRequired) {
                if (error_type == WRONG_NUMBER_OF_MOTORS)
                    error_type = NO_ERROR;
                if (menustate < 13 || menustate > 249)
                    get_user_input();
            } else if (main_state == NORMAL)
                error_type = WRONG_NUMBER_OF_MOTORS;

            if (control_action == DONT_MOVE && auto_state == AUTO_IDLE) { //if no button pressed, use latest SPI command
                if (error_priority == SOFT_STOP && main_state == NORMAL) { //Error from motors - reset errors on stop or released button
                    l_bool_wr_MOTOR_RUN_RESET_ALL_ERRORS(1U);
                    actionCommanded = ACTION_STOP;
                    error_priority = NO_ACTION;
                }
#ifdef USE_SPI
                if ((error_type != NO_ERROR && error_type != OUT_OF_SYNC) || error_priority != NO_ACTION)
                    actionCommanded = NO_ACTION;

                switch (actionCommanded) {
                    case ACTION_HOME:
                    { //initiate homing
                        ui8group.byte = groupCommanded;
                        main_state = HOME;
                        ui1clear_limits_flag = 1U;
                        ui8related_address = USER_HIGH_LIMIT_G1H_ADD; /* Update the related address - important for clearing user defined limits */
                        actionCommanded = ACTION_HOMING;
                        control_action = DOWN_ONLY;
                    }
                        break;
                    case ACTION_HOMING:
                    { //g ongoing
                        if (ui1all_at_home_flag) {
                            actionCommanded = ACTION_STOP;
                            control_action = DONT_MOVE;
                        } else
                            control_action = DOWN_ONLY;
                        ui8group.byte = groupCommanded;
                    }
                        break;
                    case ACTION_GOTO_MEM:
                        if (menustate < 13)
                            spiActionGotoMem();
                        break;
                }
#endif
            }

            ui8timer125usCnt++;
            break;
        case 3:

            ui8timer125usCnt++;
            break;

        case 5U:

            if ((ui1any_motor_has_responded_flag) && (ui16highest_motor_pos >= 0xFE00U)) {
                if (main_state != CONFIG) {
                    main_state = HOME;
                }
            }

            ui8timer125usCnt++;
            break;

        case 7U:

            if (ui8general_timer_1ms_tick == 50U) {
                if (main_state != NORMAL_SLAVE) {

                    /* ~18 us @ 16Mhz */
                    run_action_select();
                }
            }

            if (ui1clear_limits_flag) {
                if (ui8related_address <= USER_LOW_LIMIT_G4L_ADD) {
                    /* clear only if related address is used */
                    if (read_eeprom_16(ui8related_address) != 0U) {
                        write_eeprom(ui8related_address, 0U);
                    }
                    ui8related_address++;
                } else {
                    ui1clear_limits_flag = 0U;
                    reset_active_limits();
                }
            }

            if (selectedMenu == VISUAL_KYB)
                lcd_menu2();
            else
                lcd_menu();

            ui8timer125usCnt = 0U;
            break;

        default:
            ui8timer125usCnt = 0U;
            break;

    } //end switch ui8timer125usCnt

    WDT_WatchdogTimerClear();
}

void UpdateSendReqData(void) {
    uint8_t i;
    if (flags.MOTOR_RUN_REQ_UPDATE == 1) {
        for (i = 0; i < 3; i++) {
            MotorRun[i] = MotorRun_Req[i];
        }
        flags.MOTOR_RUN_REQ_UPDATE = 0;
    } else if (flags.MOTOR_SAMPLE_REQ_UPDATE == 1) {
        for (i = 0; i < 3; i++) {
            MotorSamplePos[i] = MotorSamplePos_Req[i];
        }
        flags.MOTOR_SAMPLE_REQ_UPDATE = 0;
    } else if (flags.DISPLAY_UPDATE == 1) {

        for (i = 0; i < 8; i++) {
            DispData[i] = DispData_Req[i];
        }
        flags.DISPLAY_UPDATE = 0;
    }
}

void processLIN(void) {
    uint8_t tempRxData[8];
    uint8_t cmd;
    uint8_t i;
    cmd = LIN_getPacket(tempRxData);
    switch (cmd) {
        case 1U:
        case 2U:
        case 3U:
        case 4U:
        case 5U:
        case 6U:
        case 7U:
            for (i = 0; i < 3; i++) {
                PeriphialData[i] = tempRxData[i];
            }
            uint8_t slaveIndex = (cmd & 0x07) - 1;
            slaveKeyboards[slaveIndex].type = l_u8_rd_KEYBOARD_KEY_SET();
            slaveKeyboards[slaveIndex].keys.byte = l_u8_rd_KEYBOARD_BASIC_KEYS();
            slaveKeyboards[slaveIndex].groupmask = l_u8_rd_KEYBOARD_ENHANCED_KEYS();
            ui8SlaveReported |= 1 << slaveIndex;

            if (slaveIndex == 0) { //First of the slaves - check if is an unaddressed slave
                if (!oldSlaveDetected && slaveKeyboards[0].type != 0xFF && ui8NumberOfSlaves < 7) { //This slave has not been tried to arbitrate before - rearbitrate
                    ui1complete_arbitration = 1U;
                    slaveKeyboards[0].type = 0xFF;
                    oldSlaveDetected = 1;
                    ui8SlaveReported = 0;
                }
            }
            lcd_keyboard();
            break;
        case 8U:
        case 9U:
        case 10U:
        case 11U:
        case 12U:
        case 13U:
        case 14U:
        case 15U:
            for (i = 0; i < 3; i++) {
                MotorData[i] = tempRxData[i];
            }
            ui8MotorResponses++;
            ActiveMotorPID = cmd;
            get_lin_motor_data();
            break;
        case 16:
            //unarbitratedMotorReplyFlag = 1;
            ui1restart_arbitration_flag = 1U;
            break;

        case 61:
            for (i = 0; i < 8; i++) {
                SlaveResp[i] = tempRxData[i];
            }
            //TODO: Find another soulution to clear the MasterCMD bytes
            //memset(MasterCMD, 0xFF, 8);
            linSlaveResponsFlag = 1;
            break;

        default:
            break;
    }
}

static uint32_t cntr = 0;

void lin_arbitration(void) {
    INT8U response = 0, read_state;
    static unsigned char oldNumberOfSlaves = 0;
    /* State machine arb_state handles the motor arbitration procedure */
    switch (arb_state) {
        case ARB_IDLE:
            /* Check if in LIN_ARBITRATION and no config transmission is pending */
            if ((main_state == LIN_ARBITRATION) && (!g_lin_diag_config_active)) {
                /* Clear reception flag if pending, it shouldn't be */
                linSlaveResponsFlag = 0U;
                arb_state = ARB_START;
                ui8ArbitrationCounter++;
            }
            break;

        case ARB_START:
            /* Set arbitration KEY to select ALL motors */
            ui8_wr_MASTER_REQ_KEY(ARB_GenericKey);
            /* Reset motor number counter */
            ui8ArbMotorNumber = 0U;
            ui8NumberOfSlaves = 0;
            /* Reset high and low limits to extreme values */
            ui16high_limit = 0xFFFFU;
            ui16high_limit_motors = 0xFFFFU;
            for (ui8temp_motor_group = 0U; ui8temp_motor_group < 4U; ui8temp_motor_group++) {
                ui16group_high_limit_motors[ui8temp_motor_group] = 0xFFFFU;
                ui16group_high_limit[ui8temp_motor_group] = 0xFFFFU;
                ui16group_low_limit_motors[ui8temp_motor_group] = 0U;
                ui16group_low_limit[ui8temp_motor_group] = 0U;
                ui8group_ISP[ui8temp_motor_group] = 0;
            }
            ui16low_limit = 0U;
            ui16low_limit_motors = 0U;
            arb_state = ARB_RESET;
            break;

        case ARB_RESET:
            /* Set arbitration CMD */
            ui8_wr_MASTER_REQ_CMD(ARB_Reset_Arbitration);
            /* Initiate MASTER_REQUEST frame transmission */
            g_lin_diag_config_active = 1U;

            /* Execute reset arbitration command several times */
            if (ui8ArbitrationCounter >= 4U) {
                arb_state = ARB_GEN_KEY;
            } else {
                arb_state = ARB_IDLE;
            }
            break;

        case ARB_RESTART:
            /* Restart arbitration. Done after detecting generic motors in normal schedule */
            /* Set arbitration KEY */
            ui8_wr_MASTER_REQ_KEY(ARB_UnarbitratedLinKey);
            /* Motor number will be kept from last run */
            arb_state = ARB_GEN_KEY;
            break;

        case ARB_GEN_KEY:
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                /* Set arbitration CMD */
                ui8_wr_MASTER_REQ_CMD(ARB_Generate_Random_Key);
                ui8_wr_MASTER_REQ_PAR1(ARB_RandomKeyMaxValue);
                /* Initiate MASTER_REQUEST frame transmission */
                g_lin_diag_config_active = 1U;

                arb_state = ARB_LIN_KEY;
            }
            break;

        case ARB_LIN_KEY:
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                /* Set arbitration KEY */
                ui8_wr_MASTER_REQ_KEY(ARB_UnarbitratedLinKey);
                arb_state = ARB_READ_SER;
            }
            break;

        case ARB_READ_SER:
            /* Set arbitration CMD */
            ui8_wr_MASTER_REQ_CMD(ARB_Read_Serial_Number);
            /* Initiate MASTER_REQUEST frame transmission */
            g_lin_diag_config_active = 1U;
            /* Clear reception flag if pending, it shouldn't be */
            linSlaveResponsFlag = 0U;

            arb_state = ARB_WAIT_SER;
            break;

        case ARB_WAIT_SER:
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                if (ui8SchedulePhase == MASTER_RESPONSE_FRAME) {
                    /* Now it's start of response frame */
                    ui8AbsByteState = ABS_WAIT_FOR_PID_7D;
                    /* Clear timeout timer */
                    ui8ArbResponseCntMs = 0U;
                    arb_state = ARB_ANY_REPLY;
                    /* Enable LIN state failures */
                    SET_LINComFailState(ENABLED);
                } /* if ( ui8SchedulePhase == MASTER_RESPONSE_FRAME ) */
            } /* if ( !g_lin_diag_config_active ) */
            break;

        case ARB_ANY_REPLY:
            if (TST_LINComFailState(FAILED)) {
                /* LIN communication failure detected */

                SET_LINComFailState(DISABLED);
                /* Will give timeout when checking the answer, and then go to ARB_CHANGE_KEY */
                arb_state = ARB_CHECK_REPLY;
            } else if (ui8ArbResponseCntMs >= ARB_RespTimeoutMs) {
                arb_state = ARB_NO_REPLY;
                SET_LINComFailState(DISABLED);
            } else if (ui8AbsByteState == ABS_BYTE_RECEIVED) {
                /* Add a tick to the timeout counter to keep synch */
                ui8ArbResponseCntMs++;
                /* Response frame received, but not successfully, most likely caused by bus collision */
                /* due to multiple slave responding to the frame */
                arb_state = ARB_CHECK_REPLY;
            } else /* No response, waiting for timeout */ {
                /* Increment reply timeout counter */
                ui8ArbResponseCntMs++;
            }
            break;

        case ARB_CHECK_REPLY:
            /* Clear config byte reception flag */
            ui8AbsByteState = ABS_IDLE;

            if (linSlaveResponsFlag) /* Correct response received, only one slave responded */ {
                linSlaveResponsFlag = 0U;
                SET_LINComFailState(DISABLED);
                //arb_state = ARB_READ_HIGH_LIMIT_H;
                arb_state = ARB_READ_GROUP_NO;
            } else if (ui8ArbResponseCntMs >= ARB_RespTimeoutMs) {
                /* Timeout after receiving a corrupt reply */
                arb_state = ARB_CHANGE_KEY;
                SET_LINComFailState(DISABLED);
            } else {
                /* Increment reply timeout counter */
                ui8ArbResponseCntMs++;
            }
            break;

            /*********************************** READ group numbers from motors ***************************************/

        case ARB_READ_GROUP_NO:
        {
            static uint8_t cntr = 0;
            read_state = readWriteByteFromSlave(1, MOTOR_GROUP_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                switch (response) {
                        /* translate to group mask format */
                    case 2:
                        ui8group_list[ui8ArbMotorNumber] = 2U;
                        ui8temp_motor_group = 1U;
                        break;

                    case 3:
                        ui8group_list[ui8ArbMotorNumber] = 4U;
                        ui8temp_motor_group = 2U;
                        break;

                    case 4:
                        ui8group_list[ui8ArbMotorNumber] = 8U;
                        ui8temp_motor_group = 3U;
                        break;

                    default:
                        ui8group_list[ui8ArbMotorNumber] = 1U;
                        ui8temp_motor_group = 0U;
                }
                /*here */
                arb_state = ARB_READ_HIGH_LIMIT_H;
                processData.LegSlaveAddress[cntr++] = MasterCMD[0];
            }
        }
            break;

            /*****************************************************************************************************************************************/
            /* READ BACK MOTOR LIMITS */
        case ARB_READ_HIGH_LIMIT_H:
        {
            read_state = readWriteByteFromSlave(1, HIGH_LIMIT_H_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                /* Set high limit high byte */
                ui16temp = (INT16U) response << 8;
                arb_state = ARB_READ_HIGH_LIMIT_L;
            }
        }
            break;

        case ARB_READ_HIGH_LIMIT_L:
        {
            read_state = readWriteByteFromSlave(1, HIGH_LIMIT_L_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                ui16temp += response; /* Add high limit low byte */
                if (ui16temp < ui16high_limit_motors) {
                    /* Only reduce the highest limit if the motor holds a lower value, never increase */
                    ui16high_limit_motors = ui16temp;
                    ui16near_high_limit = ui16temp - NEAR_LIMIT_STOP_DISTANCE; // changed by khalid to upper limit value - 7.2mm
                }
                if (ui16temp < ui16group_high_limit_motors[ui8temp_motor_group]) {
                    ui16group_high_limit_motors[ui8temp_motor_group] = ui16temp;
                }
                arb_state = ARB_READ_LOW_LIMIT_H;
            }
        }
            break;

        case ARB_READ_LOW_LIMIT_H:
        {
            read_state = readWriteByteFromSlave(1, LOW_LIMIT_H_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                /* Set low limit high byte */
                ui16temp = (INT16U) response << 8;
                arb_state = ARB_READ_LOW_LIMIT_L;
            }
        }
            break;

        case ARB_READ_LOW_LIMIT_L:
        {
            read_state = readWriteByteFromSlave(1, LOW_LIMIT_L_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                ui16temp += response; /* Add low limit low byte */
                if (ui16temp > ui16low_limit_motors) {
                    /* Only increase the lowest limit if the motor holds a higher value, never decrease */
                    ui16low_limit_motors = ui16temp;
                    ui16near_low_limit = ui16temp + END_STOP_DISTANCE;
                }
                if (ui16temp > ui16group_low_limit_motors[ui8temp_motor_group]) {
                    ui16group_low_limit_motors[ui8temp_motor_group] = ui16temp;
                }
                arb_state = ARB_READ_UNITS;
            }
        }
            break;

        case ARB_READ_UNITS:
        {
            read_state = readWriteByteFromSlave(1, UNITS_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                if (deskProfile.units == 0xff) {
                    if (response == 0xff)
                        deskProfile.units = UNITS; //Default to Inches
                    else
                        deskProfile.units = response;
                }
                arb_state = ARB_READ_RECIEPE_REVISION;
            }
        }
            break;

        case ARB_READ_RECIEPE_REVISION:
        {
            read_state = readWriteByteFromSlave(1, RECIEPT_REVISION_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                ui8ReciepeRevision = response;
                arb_state = ARB_READ_SPEED_OF_MOTORS;
            }
        }
            break;

        case ARB_READ_SPEED_OF_MOTORS:
        {
            read_state = readWriteByteFromSlave(1, MAX_PWM_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                ui8MotorSpeed = response;
                arb_state = ARB_READ_NUMBER_OF_MOTORS;
            }
        }
            break;

        case ARB_READ_NUMBER_OF_MOTORS:
        {
            read_state = readWriteByteFromSlave(1, NUMBER_OF_MOTORS_REQUIRED_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                if (response > 8)
                    ui8NumberOfMotorsRequired = 0; //Default to no restriction
                else
                    ui8NumberOfMotorsRequired = response;

                //Detect old IKEA legs
                if (ui8ReciepeRevision == 170 && ui8MotorSpeed == 32)
                    ui8NumberOfMotorsRequired = 2;
                arb_state = ARB_READ_AC_OF_MOTORS;
            }
        }
            break;

        case ARB_READ_AC_OF_MOTORS:
        {
            read_state = readWriteByteFromSlave(1, ISP_PRIORITY_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                if (response > 0)
                    response--; //remap to error_priorities
                ui8group_ISP[ui8temp_motor_group] = response;

                arb_state = ARB_READ_MAJOR_VERSION;
            }
        }
            break;

        case ARB_READ_MAJOR_VERSION:
        {
            read_state = readWriteByteFromSlave(1, VERSION_MAJOR_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                ui16temp = ((INT16U) response << 8);
                arb_state = ARB_READ_MINOR_VERSION;
            }
        }
            break;

        case ARB_READ_MINOR_VERSION:
        {
            read_state = readWriteByteFromSlave(1, VERSION_MINOR_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                ui16temp |= response;
                if (ui16temp == 0xffff)
                    ui16temp = 0x0307; //Pre v3.8
                if (ui16temp < lowestVersion)
                    lowestVersion = ui16temp;
                if (ui16temp > highestVersion)
                    highestVersion = ui16temp;
                arb_state = ARB_READ_BACKOFF_UP_H;
            }
        }
            break;

        case ARB_READ_BACKOFF_UP_H:
        {
            read_state = readWriteByteFromSlave(1, BACK_OFF_DISTANCE_UP_H_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                /* Set high limit high byte */
                ui16temp = (INT16U) response << 8;
                arb_state = ARB_READ_BACKOFF_UP_L;
            }
        }
            break;

        case ARB_READ_BACKOFF_UP_L:
        {
            read_state = readWriteByteFromSlave(1, BACK_OFF_DISTANCE_UP_L_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                ui16temp += response; /* Add high limit low byte */
                if (ui16temp != 0xFFFF)
                    ui16BackOffDistanceUp = ui16temp;
                arb_state = ARB_READ_BACKOFF_DOWN_H;
            }
        }
            break;

        case ARB_READ_BACKOFF_DOWN_H:
        {
            read_state = readWriteByteFromSlave(1, BACK_OFF_DISTANCE_DOWN_H_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                /* Set high limit high byte */
                ui16temp = (INT16U) response << 8;
                arb_state = ARB_READ_BACKOFF_DOWN_L;
            }
        }
            break;

        case ARB_READ_BACKOFF_DOWN_L:
        {
            read_state = readWriteByteFromSlave(1, BACK_OFF_DISTANCE_DOWN_L_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                ui16temp += response; /* Add high limit low byte */
                if (ui16temp != 0xFFFF)
                    ui16BackOffDistanceDown = ui16temp;
                arb_state = ARB_READ_BACKOFFTIME;
            }
        }
            break;

        case ARB_READ_BACKOFFTIME:
        {
            read_state = readWriteByteFromSlave(1, BACK_OFF_TIME_ADDRESS, &response);
            if (read_state == READ_BYTE_RECIEVED) {
                if (ui16BackOffDistanceDown != DEFAULT_BACKOFF_DISTANCE_DOWN || ui16BackOffDistanceUp != DEFAULT_BACKOFF_DISTANCE_UP)
                    ui8BackOffTime = response; //Use recipie time only if distance is set
                arb_state = ARB_SET_MOTOR;
            }
        }
            break;

            /* END _ READ BACK MOTOR LIMITS */
            /*****************************************************************************************************************************************/
            /* Set motor number */

        case ARB_SET_MOTOR:
            /* Wait for next available request frame  */
            if (g_lin_diag_config_active == 0U) /* No MASTER_REQUEST frame pending */ {
                /* Keep current KEY */
                /* Set arbitration CMD */
                ui8_wr_MASTER_REQ_CMD(ARB_Set_Motor_Number);
                ui8_wr_MASTER_REQ_PAR1(ui8ArbMotorNumber);
                ui8_wr_MASTER_REQ_PAR2(0x00U);

                /* Move on in the procedure */
                arb_state = ARB_WAIT_MOTOR;

                /* Initiate MASTER_REQUEST frame transmission */
                g_lin_diag_config_active = 1U;
            } else {
                /* Do nothing, just wait */
            }
            break;

        case ARB_WAIT_MOTOR:
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                if (ui8SchedulePhase == MASTER_RESPONSE_FRAME) {
                    /* Now it's start of response frame */
                    /* Clear timeout timer */
                    ui8ArbResponseCntMs = 0U;
                    /* Clear eventual old communication failure flag */
                    SET_LINComFailState(ENABLED);
                    arb_state = ARB_MOTOR_REPLY;
                } /* if ( ui8SchedulePhase == MASTER_RESPONSE_FRAME ) */
            } /* if ( !g_lin_diag_config_active ) */
            break;

        case ARB_MOTOR_REPLY:
            if (TST_LINComFailState(FAILED)) {
                /* LIN communication failure detected */
                SET_LINComFailState(DISABLED);
                arb_state = ARB_IDLE;
                /* AWR Maybe implement a retry-loop-counter? */
            } else if (ui8ArbResponseCntMs >= ARB_RespTimeoutMs) {
                /* No reply before timeout, this has to restart the whole arbitration */
                arb_state = ARB_IDLE;
                SET_LINComFailState(DISABLED);
            } else if (ui8AbsByteState == ABS_BYTE_RECEIVED) {
                /* Clear config byte reception flag */
                ui8AbsByteState = ABS_IDLE;
                /* Response frame received, but not successfully, most likely caused by bus transients */
                /* Continue arbitration with next key if writing failed, will give motor a new try later */
                arb_state = ARB_CHANGE_KEY;
            } else if (linSlaveResponsFlag) /* Correct response received */ {
                /* Check if motor number received matches the request */
                if (ui8ArbMotorNumber == ui8_rd_SLAVE_RESP2()) {
                    arb_state = ARB_INC_MOTOR;
                } else {
                    /* Continue arbitration with next key if writing failed, will give motor a new try later */
                    arb_state = ARB_CHANGE_KEY;
                }
                /* Clear receive flag */
                linSlaveResponsFlag = 0U;
                SET_LINComFailState(DISABLED);
            } else /* No response, waiting for timeout */ {
                /* Increment reply timeout counter */
                ui8ArbResponseCntMs++;
            }
            break;

        case ARB_INC_MOTOR:
            if (ui8ArbMotorNumber <= ARB_MotorNumberMax) {
                /* Increment motor number */
                ui8ArbMotorNumber++;
                arb_state = ARB_CHANGE_KEY;
            } else {
                arb_state = ARB_SLAVE;
            }
            break;

        case ARB_CHANGE_KEY:
            if (ui8_rd_MASTER_REQ_KEY() == ARB_UnarbitratedLinKey) {
                /* Last response answered by only one motor, or a collision occured on the GenericKey */
                /* Set up key for random numbers, starting at 0 */
                ui8_wr_MASTER_REQ_KEY(ARB_ZeroKey);
                arb_state = ARB_READ_SER;
            }/* if ( ui8_rd_MASTER_REQ_KEY() == Arb_UnabitratedLinKey ) ) */
            else {
                /* Increment random key for next loop */
                ui8_wr_MASTER_REQ_KEY(ui8_rd_MASTER_REQ_KEY() + 1U);

                /* Check that random key limit is not reached */
                if (ui8_rd_MASTER_REQ_KEY() <= ARB_RandomKeyMaxValue) {
                    /* Make a new try with a random key */
                    arb_state = ARB_READ_SER;
                } else {
                    /* Last random key reached, start over with new random numbers */
                    ui8_wr_MASTER_REQ_KEY(ARB_UnarbitratedLinKey);
                    /* Last random key reached, end arbitration after one loop */
                    arb_state = ARB_GEN_KEY;
                }
            } /* else -> if ( ui8_rd_MASTER_REQ_KEY() == Arb_UnabitratedLinKey ) ) */
            break;

        case ARB_NO_REPLY:
            if (ui8_rd_MASTER_REQ_KEY() == ARB_UnarbitratedLinKey) {
                /* No reply received, end if it was on unabitratedkey */
                arb_state = ARB_SLAVE;
            } else {
                arb_state = ARB_CHANGE_KEY;
            }
            break;

            /* End set motor number */
            /*****************************************************************************************************************************************/
            /*****************************************************************************************************************************************/
            /* Assign slave numbers */

        case ARB_SLAVE:
            //Request random keys
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                ui8_wr_MASTER_REQ_KEY(ARB_Unaddressed_Slave_Key);
                ui8_wr_MASTER_REQ_CMD(ARB_Generate_Random_Key);
                ui8_wr_MASTER_REQ_PAR1(0x0FU); //Maximum random key value
                ui8_wr_MASTER_REQ_PAR2(0x00U);

                linSlaveResponsFlag = 0U; // Clear reception flag if pending, it shouldn't be
                g_lin_diag_config_active = 1U; // Initiate MASTER_REQUEST frame transmission

                ui8LoopKey = 0; //Initiate loop
                ui16ReceivedRandomKey = 0;

                arb_state = ARB_SEND_KEY;
            }
            break;

            /* Loop through 16 slaves*/

        case ARB_SEND_KEY:
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                /* Set arbitration KEY to select ALL motors */
                ui8_wr_MASTER_REQ_KEY(ARB_Unaddressed_Slave_Key);
                ui8_wr_MASTER_REQ_CMD(ARB_Send_Random_Key);
                ui8_wr_MASTER_REQ_PAR1(ui8LoopKey);
                ui8_wr_MASTER_REQ_PAR2(0x00);
                /* Clear reception flag if pending, it shouldn't be */
                linSlaveResponsFlag = 0U;
                /* Initiate MASTER_REQUEST frame transmission */
                g_lin_diag_config_active = 1U;

                arb_state = ARB_WAIT_SEND_KEY_REPLY;
            }
            break;

        case ARB_WAIT_SEND_KEY_REPLY:
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                if (ui8SchedulePhase == MASTER_RESPONSE_FRAME) { // recieved start of frame
                    ui8ArbResponseCntMs = 0U; /* Clear timeout timer */
                    ui8AbsByteState = ABS_WAIT_FOR_PID_7D;
                    arb_state = ARB_ANY_SEND_KEY_REPLY;
                    SET_LINComFailState(ENABLED); /* Enable LIN state failures */
                }
            }
            break;

        case ARB_ANY_SEND_KEY_REPLY:
            if (TST_LINComFailState(FAILED)) { /* LIN communication failure detected */
                SET_LINComFailState(DISABLED);
                arb_state = ARB_CHECK_SEND_KEY_REPLY;
            } else if (ui8ArbResponseCntMs >= ARB_RespTimeoutMs) { //Timeout - no reply
                arb_state = ARB_SLAVE_END;
                SET_LINComFailState(DISABLED);
            } else if (ui8AbsByteState == ABS_BYTE_RECEIVED) {
                /* Add a tick to the timeout counter to keep synch */
                ui8ArbResponseCntMs++;
                /* Response frame received, but not successfully, most likely caused by bus collision */
                /* due to multiple slave responding to the frame */
                arb_state = ARB_CHECK_SEND_KEY_REPLY;
            } else /* No response, waiting for timeout */ {
                /* Increment reply timeout counter */
                ui8ArbResponseCntMs++;
            }
            break;

        case ARB_CHECK_SEND_KEY_REPLY:
            /* Clear config byte reception flag */
            ui8AbsByteState = ABS_IDLE;

            if (linSlaveResponsFlag) { /* Correct response received, only one slave responded */
                if (SlaveResp[0] == 0xFE) { //Got key reply
                    ui16ReceivedRandomKey = (INT16U) (SlaveResp[1] << 8) | SlaveResp[2];
                    arb_state = ARB_SET_SLAVE_NUMBER;
                } else { //non valid reply
                    arb_state = ARB_SLAVE_END;
                }
                linSlaveResponsFlag = 0U;
                SET_LINComFailState(DISABLED);
            } else if (ui8ArbResponseCntMs >= ARB_RespTimeoutMs) { /* Timeout after receiving a corrupt reply */
                arb_state = ARB_SLAVE_END;
                SET_LINComFailState(DISABLED);
            } else {
                /* Increment reply timeout counter */
                ui8ArbResponseCntMs++;
            }
            break;

            /* Single slave identified, set slave number*/

        case ARB_SET_SLAVE_NUMBER:
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                /* Set arbitration KEY to select ALL motors */
                ui8_wr_MASTER_REQ_KEY(ARB_Unaddressed_Slave_Key);
                ui8_wr_MASTER_REQ_CMD(ARB_Set_Slave_Number);
                ui8_wr_MASTER_REQ_PAR1(ui8NumberOfSlaves + 1);
                ui8_wr_MASTER_REQ_PAR2(HIGH_BYTE(ui16ReceivedRandomKey));
                ui8_wr_MASTER_REQ_PAR3(LOW_BYTE(ui16ReceivedRandomKey));
                /* Clear reception flag if pending, it shouldn't be */
                linSlaveResponsFlag = 0U;
                /* Initiate MASTER_REQUEST frame transmission */
                g_lin_diag_config_active = 1U;

                arb_state = ARB_WAIT_SET_NUMBER_REPLY;
            }
            break;

        case ARB_WAIT_SET_NUMBER_REPLY:
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                if (ui8SchedulePhase == MASTER_RESPONSE_FRAME) { // recieved start of frame
                    ui8ArbResponseCntMs = 0U; /* Clear timeout timer */
                    ui8AbsByteState = ABS_WAIT_FOR_PID_7D;
                    arb_state = ARB_ANY_SET_NUMBER_REPLY;
                    SET_LINComFailState(ENABLED); /* Enable LIN state failures */
                }
            }
            break;

        case ARB_ANY_SET_NUMBER_REPLY:
            if (TST_LINComFailState(FAILED)) { /* LIN communication failure detected */
                SET_LINComFailState(DISABLED);
                arb_state = ARB_CHECK_SET_NUMBER_REPLY;
            } else if (ui8ArbResponseCntMs >= ARB_RespTimeoutMs) { //Timeout - no reply
                arb_state = ARB_SLAVE_END;
                SET_LINComFailState(DISABLED);
            } else if (ui8AbsByteState == ABS_BYTE_RECEIVED) {
                /* Add a tick to the timeout counter to keep synch */
                ui8ArbResponseCntMs++;
                /* Response frame received, but not successfully, most likely caused by bus collision */
                /* due to multiple slave responding to the frame */
                arb_state = ARB_CHECK_SET_NUMBER_REPLY;
            } else /* No response, waiting for timeout */ {
                /* Increment reply timeout counter */
                ui8ArbResponseCntMs++;
            }
            break;

        case ARB_CHECK_SET_NUMBER_REPLY:
            /* Clear config byte reception flag */
            ui8AbsByteState = ABS_IDLE;

            if (linSlaveResponsFlag) { /* Correct response received, only one slave responded */
                linSlaveResponsFlag = 0U;
                SET_LINComFailState(DISABLED);
                ui8NumberOfSlaves++;
                arb_state = ARB_SLAVE_END;
            } else if (ui8ArbResponseCntMs >= ARB_RespTimeoutMs) { /* Timeout after receiving a corrupt reply */
                arb_state = ARB_SLAVE_END;
                SET_LINComFailState(DISABLED);
            } else {
                /* Increment reply timeout counter */
                ui8ArbResponseCntMs++;
            }
            break;

        case ARB_SLAVE_END:
            if (ui8NumberOfSlaves > 6) {
                arb_state = ARB_END; //At least 7 slaves identified
            } else if (ui8LoopKey < 0x0F) {
                ui8LoopKey++;
                arb_state = ARB_SEND_KEY;
            } else
                arb_state = ARB_CHECK_FOR_UNADDRESSED;
            break;

            /* Calling for any unaddressed slaves */

        case ARB_CHECK_FOR_UNADDRESSED:
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                /* Set arbitration KEY to select ALL motors */
                ui8_wr_MASTER_REQ_KEY(ARB_Unaddressed_Slave_Key);
                ui8_wr_MASTER_REQ_CMD(ARB_Send_Acknowledge);
                ui8_wr_MASTER_REQ_PAR1(0x00);
                ui8_wr_MASTER_REQ_PAR2(0x00);
                /* Clear reception flag if pending, it shouldn't be */
                linSlaveResponsFlag = 0U;
                /* Initiate MASTER_REQUEST frame transmission */
                g_lin_diag_config_active = 1U;

                arb_state = ARB_WAIT_SEND_ACK_REPLY;
            }
            break;

        case ARB_WAIT_SEND_ACK_REPLY:
            if (!g_lin_diag_config_active) /* LIN stack has serviced the transmission */ {
                if (ui8SchedulePhase == MASTER_RESPONSE_FRAME) { // recieved start of frame
                    ui8ArbResponseCntMs = 0U; /* Clear timeout timer */
                    ui8AbsByteState = ABS_WAIT_FOR_PID_7D;
                    arb_state = ARB_ANY_SEND_ACK_REPLY;
                    SET_LINComFailState(ENABLED); /* Enable LIN state failures */
                }
            }
            break;

        case ARB_ANY_SEND_ACK_REPLY:
            if (TST_LINComFailState(FAILED)) { /* LIN communication failure detected */
                SET_LINComFailState(DISABLED);
                arb_state = ARB_CHECK_SEND_KEY_REPLY;
            } else if (ui8ArbResponseCntMs >= ARB_RespTimeoutMs) { //Timeout - no reply
                arb_state = ARB_END; //No unaddressed slaves detected
                SET_LINComFailState(DISABLED);
            } else if (ui8AbsByteState == ABS_BYTE_RECEIVED) {
                /* Add a tick to the timeout counter to keep synch */
                ui8ArbResponseCntMs++;
                /* Response frame received, but not successfully, most likely caused by bus collision */
                /* due to multiple slave responding to the frame */
                arb_state = ARB_CHECK_SEND_ACK_REPLY;
            } else /* No response, waiting for timeout */ {
                /* Increment reply timeout counter */
                ui8ArbResponseCntMs++;
            }
            break;

        case ARB_CHECK_SEND_ACK_REPLY:
            /* Clear config byte reception flag */
            ui8AbsByteState = ABS_IDLE;

            if (linSlaveResponsFlag) { /* Correct response received, only one slave responded */
                arb_state = ARB_SLAVE; //Found unaddresed slave
                linSlaveResponsFlag = 0U;
                SET_LINComFailState(DISABLED);
            } else if (ui8ArbResponseCntMs >= ARB_RespTimeoutMs) { /* Timeout after receiving a corrupt reply */
                arb_state = ARB_SLAVE; //Found unaddressed slaves
                SET_LINComFailState(DISABLED);
            } else {
                /* Increment reply timeout counter */
                ui8ArbResponseCntMs++;
            }
            break;

            /* End of Set slave addresses */
            /*****************************************************************************************************************************************/

        case ARB_END:
            g_lin_diag_config_active = 0U;
            linSlaveResponsFlag = 0U;

            /* Reinitialize predefined schedule table for separate ID's */
            /* Only reinitialize the number of motors arbitrated */
            /* Will make eventual unarbitrated motors to cause continuous collisions on the MOTOR_POS_RESPONSE frame/frames with kept default ID 16 */
            /* All unarbitrated slaves shall send all 0's in the MOTOR_POS_RESPONSE frame, will be detected in the master as framing errors */
            /*  First reset all MOTOR POS RESPONSE schedule slot ID's */

            for (ui8temp1 = 1; ui8temp1 <= 8; ui8temp1++) {
                if (ui8ArbMotorNumber < ui8temp1)
                    NORMALSCHED[ui8temp1].cmd = LIN_ID_16; //inactive

                else
                    NORMALSCHED[ui8temp1].cmd = scheduleLookUp[ui8temp1 - 1];
            }

            /* Start normal master schedule */
            if (oldNumberOfSlaves != ui8NumberOfSlaves) {
                oldSlaveDetected = 0; //The unaddressed slaves where addressable and counter so we can detect new slaves if needed
                oldNumberOfSlaves = ui8NumberOfSlaves;
            }
            main_state = NORMAL;
            MasterCMD[0] = 0xFF;
            MasterCMD[1] = 0xFF;
            MasterCMD[2] = 0xFF;
            MasterCMD[3] = 0xFF;
            MasterCMD[4] = 0xFF;
            MasterCMD[5] = 0xFF;
            MasterCMD[6] = 0xFF;
            MasterCMD[7] = 0xFF;
            //l_sch_set(CompleteSchedule, 0);
            LIN_Master_Initialize(1);
            /* Go back to IDLE state */
            arb_state = ARB_IDLE;
            ConfigDone = true;
            reset_active_limits();
            break;

        default:
            break;
    } /* switch ( arb_state ) */
} /* void lin_arbitration(void) */

INT8U readWriteByteFromSlave(INT8U read, INT8U adr, INT8U *dat) //read = 1, write - read=0
{
    static INT8U arb_read_state = READ_REQUEST_BYTE;
    switch (arb_read_state) {
        case READ_REQUEST_BYTE:
            /* Wait for next available request frame  */
            if (g_lin_diag_config_active == 0U) { /* No MASTER_REQUEST frame pending */
                if (!read)
                    ui8_wr_MASTER_REQ_KEY(ARB_GenericKey);
                ui8_wr_MASTER_REQ_CMD(read ? ARB_Read_EEPROM_Byte : ARB_Write_EEPROM_Byte); /* Set arbitration CMD */
                ui8_wr_MASTER_REQ_PAR1(adr);
                ui8_wr_MASTER_REQ_PAR2(*dat);
                MasterCMD[4] = 0xFF;
                MasterCMD[5] = 0xFF;
                MasterCMD[6] = 0xFF;
                MasterCMD[7] = 0xFF;
                g_lin_diag_config_active = 1U; /* Initiate MASTER_REQUEST frame transmission */
                linSlaveResponsFlag = 0U; /* Clear reception flag if pending, it shouldn't be */
                arb_read_state = READ_WAIT_REPLY;
            }
            break;

        case READ_WAIT_REPLY:
            if (!g_lin_diag_config_active) { /* LIN stack has serviced the transmission */
                if (ui8SchedulePhase == MASTER_RESPONSE_FRAME) {
                    /* Clear timeout timer */
                    ui8ArbResponseCntMs = 0U;
                    arb_read_state = READ_CHECK_REPLY;
                    /* Enable LIN state failures */
                    SET_LINComFailState(ENABLED);
                }
            } /* if ( !g_lin_diag_config_active ) */
            break;

        case READ_CHECK_REPLY:
            if (TST_LINComFailState(FAILED)) { /* LIN communication failure detected */
                SET_LINComFailState(DISABLED);
                /* Will give timeout when checking the answer, and then go to READ_CORRUPT */
                arb_read_state = READ_CORRUPT;
                arb_state = ARB_CHANGE_KEY;
            } else if (ui8ArbResponseCntMs >= ARB_RespTimeoutMs) {
                arb_read_state = READ_NO_REPLY;
                SET_LINComFailState(DISABLED);
                arb_state = ARB_IDLE;
            } else if (ui8AbsByteState == ABS_BYTE_RECEIVED) {
                ui8ArbResponseCntMs++; /* Add a tick to the timeout counter to keep synch */
                /* Response frame received, but not successfully, most likely caused by bus collision */
                /* due to multiple slave responding to the frame */
                arb_read_state = READ_CORRUPT;
                arb_state = ARB_CHANGE_KEY;
            } else if (linSlaveResponsFlag) { /* Correct response received */
                if (ui8_rd_SLAVE_RESP1() == adr) {
                    *dat = ui8_rd_SLAVE_RESP2();
                    arb_read_state = READ_BYTE_RECIEVED;
                } else { /* Continue arbitration with next key if reading failed (wrong address returned), will give motor a new try later */
                    arb_read_state = READ_CORRUPT;
                    arb_state = ARB_CHANGE_KEY;
                }
                /* Clear receive flag */
                linSlaveResponsFlag = 0U;
                SET_LINComFailState(DISABLED);
            } else /* No response, waiting for timeout */ {
                /* Increment reply timeout counter */
                ui8ArbResponseCntMs++;
            }
            break;
        default:
            arb_read_state = READ_REQUEST_BYTE; //restart - next request will start new fetch.
            break;
    }

    return arb_read_state;
}

//volatile t_l_flags_DISPLAY_REQUEST l_flags_DISPLAY_REQUEST;

volatile t_l_flags_KEYBOARD_RESPONSE l_flags_KEYBOARD_RESPONSE;

volatile t_l_flags_MOTOR_POS_RESPONSE l_flags_MOTOR_POS_RESPONSE;

volatile t_l_flags_MOTOR_SAMPLE_REQUEST l_flags_MOTOR_SAMPLE_REQUEST;

volatile t_l_flags_MOTOR_RUN_REQUEST l_flags_MOTOR_RUN_REQUEST;

//*****************************************************************************
//	 NOTATION:
//     void l_get_byte_array(unsigned char*,unsigned char,unsigned char*);
//
//	DESCRIPTION: LIN-API Function to access byte array signals.
//
//                                                       Author: B.Kroepfl
//*****************************************************************************

void l_get_byte_array(unsigned char *start, unsigned char count, unsigned char *destination) {
    unsigned char temp;
    for (temp = 0; temp < count; temp++) {
        destination[temp] = start[temp];
    }
}
//*****************************************************************************
//	 NOTATION:
//     void l_set_byte_array(unsigned char*,unsigned char,const unsigned char*);
//
//	DESCRIPTION: LIN-API Function to access byte array signals.
//
//               l_bytes_wr_DISPLAY_DATA(0,4,lcd_digit); //Send to any slaves...                                        Author: B.Kroepfl
//*****************************************************************************

void l_set_byte_array(unsigned char *start, unsigned char count, const unsigned char *source) {
    unsigned char temp;
    for (temp = 0; temp < count; temp++) {
        start[temp] = source[temp];
    }
}
