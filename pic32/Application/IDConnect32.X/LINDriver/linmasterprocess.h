/* 
 * File:   linMAsterProcess.h
 * Author: Ramsen Nissan
 *
 * Created on den 13 Feb 2018
 */

#ifndef LINMASTERPROCESS_H
#define	LINMASTERPROCESS_H

#include "linmaster.h"
#include "motor.h"
#include "genLinConfig.h"
#include "hal.h"

uint8_t DispData_Req[8];
uint8_t PeriphialData[3];
uint8_t MotorData[3];
uint8_t MotorPID[3];
uint8_t MotorSamplePos[3];
uint8_t MotorSamplePos_Req[3];
uint8_t MotorRun[3];
uint8_t MotorRun_Req[3];

#define LIN_ID_0 0x80 //0x80 //MASTER NODE ALL PERIPHERALS DISPLAY DATA
#define LIN_ID_1 0xC1//0xC1 //PERIPHERAL 1
#define LIN_ID_2 0x42 //0x42 //PERIPHERAL 2
#define LIN_ID_3 0x03  //0x03 //PERIPHERAL 3
#define LIN_ID_4 0xC4 //0xC4 //PERIPHERAL 4
#define LIN_ID_5 0x85 //0x85 //PERIPHERAL 5
#define LIN_ID_6 0x06 //0x06 //PERIPHERAL 6
#define LIN_ID_7 0x47 //0x47 //PERIPHERAL 7
#define LIN_ID_8 0x08 //0x08 // MOTOR 0 Byte 2 MSB, Byte 1 LSB Following bytes
#define LIN_ID_9 0x49 //0x49
#define LIN_ID_10 0xCA //0xCA
#define LIN_ID_11 0x8B //0x8B
#define LIN_ID_12 0x4C //0x4C
#define LIN_ID_13 0x0D //0x0D
#define LIN_ID_14 0x8E //0x8E
#define LIN_ID_15 0xCF //0xCF
#define LIN_ID_16 0x50 //0x50
#define LIN_ID_17 0x11 //0x11 //MASTER NODE ALL MOTORS SAMPLE POSITION
#define LIN_ID_18 0x92 //0x92 //MASTER NODE ALL MOTORS MOTOR RUN
#define LIN_ID_60 0x3C //0x3C 60//Master Request
#define LIN_ID_61 0x7D // 61  //Slave response
#define PERIODTIME 20U
#define TIMEOUT 12U
#define TIMEOUTTIME 5 

#define HIGH_BYTE(x)    ((x >> 8) & 0xFF)
#define LOW_BYTE(x)     (x & 0xFF)

#define READ_REQUEST_BYTE   0
#define READ_WAIT_REPLY     1
#define READ_CHECK_REPLY    3
#define READ_NO_REPLY       4
#define READ_CORRUPT        5
#define READ_BYTE_RECIEVED  6
#define READ_READY_TO_SERVE 7



/************************************************ definitions  ****************************************************/
#define		ARB_IDLE			1U
#define		ARB_START			2U
#define		ARB_RESET			3U
#define		ARB_GEN_KEY			4U
#define		ARB_LIN_KEY			5U
#define		ARB_READ_SER			6U
#define		ARB_WAIT_SER			7U
#define		ARB_ANY_REPLY			8U
#define		ARB_CHECK_REPLY			9U
#define		ARB_CHANGE_KEY			10U

#define		ARB_READ_GROUP_NO		11U
#define		ARB_WAIT_GROUP_NO		12U
#define		ARB_GROUP_NO_REPLY		13U

/* States for reading data from the motors */
#define		ARB_READ_HIGH_LIMIT_H		15U
#define		ARB_READ_HIGH_LIMIT_L		16U
#define		ARB_READ_LOW_LIMIT_H		17U
#define		ARB_READ_LOW_LIMIT_L		18U
#define         ARB_READ_UNITS                  19U
#define         ARB_READ_NUMBER_OF_MOTORS       20U
#define         ARB_READ_MAJOR_VERSION          21U
#define         ARB_READ_MINOR_VERSION          22U
#define         ARB_READ_AC_OF_MOTORS           23U
#define         ARB_READ_RECIEPE_REVISION       24U
#define         ARB_READ_SPEED_OF_MOTORS        25U
#define         ARB_READ_BACKOFF_UP_H           26U
#define         ARB_READ_BACKOFF_UP_L           27U
#define         ARB_READ_BACKOFF_DOWN_H         28U
#define         ARB_READ_BACKOFF_DOWN_L         29U
#define         ARB_READ_BACKOFFTIME            30U

/* States for writing the motor number */
#define		ARB_SET_MOTOR			32U
#define		ARB_WAIT_MOTOR			33U
#define		ARB_MOTOR_REPLY			34U
#define		ARB_INC_MOTOR			35U
#define		ARB_LAST_MOTOR			36U
#define		ARB_NO_REPLY			37U
#define		ARB_FAILURE_END			38U
#define		ARB_RESTART			39U

/* States for adddressing slaves */
#define         ARB_SLAVE                       40U
#define         ARB_SEND_KEY                    41U
#define         ARB_WAIT_SEND_KEY_REPLY         42U
#define         ARB_ANY_SEND_KEY_REPLY          43U
#define         ARB_CHECK_SEND_KEY_REPLY        44U
#define         ARB_SET_SLAVE_NUMBER            45U
#define         ARB_WAIT_SET_NUMBER_REPLY       46U
#define         ARB_ANY_SET_NUMBER_REPLY        47U
#define         ARB_CHECK_SET_NUMBER_REPLY      48U
#define         ARB_SLAVE_END                   49U
#define         ARB_CHECK_FOR_UNADDRESSED       50U
#define         ARB_WAIT_SEND_ACK_REPLY         51U
#define         ARB_ANY_SEND_ACK_REPLY          52U
#define         ARB_CHECK_SEND_ACK_REPLY        53U

#define		ARB_END				99U


/* LIN Arbitration Definitions */
#define		ARB_Reserved			0x00U
#define		ARB_Generate_Random_Key		0x01U
#define		ARB_Read_Serial_Number		0x02U
#define		ARB_Read_Type_Number		0x03U
#define		ARB_Set_Motor_Number		0x04U
#define		ARB_Write_EEPROM_Byte		0x05U
#define		ARB_Read_EEPROM_Byte		0x06U
#define		ARB_Reset_Arbitration		0x07U
#define		ARB_Set_Slave_Number		0x08U
#define		ARB_Send_Random_Key     	0x09U
#define		ARB_Send_Acknowledge     	0x0AU

#define		ARB_GenericKey			0xFFU
#define		ARB_UnarbitratedLinKey		0xD0U
#define		ARB_ZeroKey			0x00U
#define		ARB_RandomKeyMaxValue		0x07U				/* OBS, also used as mask for random key in motor, valid values are 0x01, 0x03, 0x07, 0x0F, 0x1F, 0x3F */
#define		ARB_MotorNumberMax		7U					/* OBS, zero count, allows ARB_MotorNumberMax + 1 motors */
#define         ARB_Unaddressed_Slave_Key       0x40U

#define		ARB_RespTimeoutMs		10U					/* Timeout in ms */
#define		ARB_RetryLoopsMax		5U					/* How many times a no reply is allowed */
#define		ARB_MismatchNumberOf100ms	3U					/* 3 x 100 ms => 300 msec delay until missing or added motors since last arbitration cause a new arbitration */

/* ConfigSchedule frame number in schedule, compared against ui8SchedulePhase returned by the l_sch_tick() */

#define		MASTER_REQUEST_FRAME		0
#define		MASTER_RESPONSE_FRAME		1


/* Frame index for the MOTOR_POS_RESPONSE frame in the LIN_FRAME_CTRL_INIT definition of genlinconfig.h */
#define		MOTOR_POS_RESPONSE_FRAME_INDEX		2U


/* Frame index for the KEYBOARD_RESPONSE frame in the LIN_FRAME_CTRL_INIT definition of genlinconfig.h */
#define		KEYBOARD_RESPONSE_FRAME_INDEX		5U
#define		KEYBOARD_REQUEST_FRAME_INDEX		1U

/* Number of us for timeout, in step of 250 us => 5 ms. Keep even divisable by 250. */
#define 	MOTOR_RESPONSE_TIMEOUT_VALUE		4000 / 250

/************ MOTOR EEPROM address declarations ******* */
#define MOTOR_POS_RESPONSE_ID_ADDRESS           0
#define MOTOR_SAMPLE_REQUEST_ID_ADDRESS         1
#define MOTOR_RUN_REQUEST_ID_ADDRESS            2
#define VERSION_MAJOR_ADDRESS                   3
#define VERSION_MINOR_ADDRESS                   4
#define RECIEPT_NUMBER_HIGH_ADDRESS             5
#define RECIEPT_NUMBER_LOW_ADDRESS              6
#define RECIEPT_REVISION_ADDRESS                7

#define MOTOR_NUMBER_ADDRESS			8
#define MOTOR_GROUP_ADDRESS			9
#define LOW_LIMIT_H_ADDRESS			10
#define LOW_LIMIT_L_ADDRESS			11
#define HIGH_LIMIT_H_ADDRESS			12
#define HIGH_LIMIT_L_ADDRESS			13
#define MEMORY_POSITION_H_ADDRESS		14
#define MEMORY_POSITION_L_ADDRESS		15

#define MEMORY_HALL_COMBO_ADDRESS		16
#define MAX_PWM_ADDRESS				17
#define SERIAL_NUMBER_H_ADDRESS			18
#define SERIAL_NUMBER_M_ADDRESS			19
#define SERIAL_NUMBER_L_ADDRESS			20
#define TYPE_NUMBER_H_ADDRESS			21
#define TYPE_NUMBER_M_ADDRESS			22
#define TYPE_NUMBER_L_ADDRESS			23

#define ISP_PRIORITY_ADDRESS			24
#define ISP_CURRENT_H_ADDRESS			25
#define ISP_CURRENT_L_ADDRESS			26
#define ISP_DELAY_TIME_ADDRESS			27
#define START_PWM_ADDRESS			28
#define HOMING_PWM_ADDRESS			29
#define MAX_CURRENT_ADDRESS			30
#define MAX_HOMING_CURRENT_ADDRESS		31

#define TEMP_LIMIT_H_ADDRESS			32
#define TEMP_LIMIT_L_ADDRESS			33
#define TEMP_HYST_ADDRESS			34
#define TEMP_TIME_CONST_ADDRESS			35
#define ISP_FAST_TIME_ADDRESS			36
#define ISP_SLOW_TIME_ADDRESS			37
#define ISP_PARTIAL_COLLISION_ADDRESS           38
#define SPARE_ADDRESS_39                        39

#define RAMP_UP_FACTOR_ADDRESS                  40
#define RAMP_DOWN_FACTOR_ADDRESS                41
#define NUMBER_OF_MOTORS_REQUIRED_ADDRESS       42
#define UNITS_ADDRESS                           43
#define BACK_OFF_TIME_ADDRESS                   44
#define MOTOR_SPEED_UP_D05_ADDRESS              45
#define MOTOR_SPEED_DOWN_D05_ADDRESS            46
#define ERROR_STORAGE_ADDRESS                   47

#define LOW_VOLTAGE_THRESHOLD_H_ADDRESS         48
#define LOW_VOLTAGE_THRESHOLD_L_ADDRESS         49
#define HIGH_VOLTAGE_THRESHOLD_H_ADDRESS        50
#define HIGH_VOLTAGE_THRESHOLD_L_ADDRESS        51
#define VOLTAGE_LINEAR_LIMIT_H_ADDRESS          52
#define VOLTAGE_LINEAR_LIMIT_L_ADDRESS          53
#define STOPPING_STORAGE_H_ADDRESS              54
#define STOPPING_STORAGE_L_ADDRESS              55

#define RAMPUP_TIME_H_ADDRESS                   56
#define RAMPUP_TIME_L_ADDRESS                   57

#define VOLTAGE_DROP_LIMIT_ADDRESS              58
#define KICK_START_FACTOR_ADDRESS               59
#define ARMATURE_CURRENT_FACTOR_ADDRESS         60

#define HALL_CHECK_ADDRESS                      61
#define DYNAMIC_START_ADDRESS                   62
#define MIN_VOLTAGE_PWM_FACTOR_ADDRESS          63
#define BALANCE_FACTOR_ADDRESS                  64

#define ISP_OUT_CURRENT_UP_H_ADDRESS            65
#define ISP_OUT_CURRENT_UP_L_ADDRESS            66
#define ISP_OUT_CURRENT_DOWN_H_ADDRESS          67
#define ISP_OUT_CURRENT_DOWN_L_ADDRESS          68
#define ISP_MINPOS_EXTRA_H_ADDRESS              69
#define ISP_MINPOS_EXTRA_L_ADDRESS              70
#define ISP_MAXPOS_EXTRA_H_ADDRESS              71
#define ISP_MAXPOS_EXTRA_L_ADDRESS              72
#define ISP_CURRENT_UP_H_ADDRESS                73
#define ISP_CURRENT_UP_L_ADDRESS                74
#define ISP_CURRENT_DOWN_H_ADDRESS              75
#define ISP_CURRENT_DOWN_L_ADDRESS              76
#define PWM_FILTER_ADDRESS                      77
#define MOTOR_TYPE_ADDRESS                      78
#define OFFSET_H_ADDRESS                        79
#define OFFSET_L_ADDRESS                        80
#define ISP_BACKOFF_SPEED_ADDRESS               81
#define SPARE_ADDRESS_82                        82
#define VOLTAGE_LINEAR_LIMIT_D05_H_ADDRESS      83
#define VOLTAGE_LINEAR_LIMIT_D05_L_ADDRESS      84
#define VOLTAGE_DROP_LIMIT_D05_ADDRESS          85
#define VOLTAGE_DIVIDOR_LEVEL_H_ADDRESS         86
#define VOLTAGE_DIVIDOR_LEVEL_L_ADDRESS         87
#define MOTOR_SPEED_UP_ADDRESS                  88
#define MOTOR_SPEED_DOWN_ADDRESS                89

#define BACK_OFF_DISTANCE_UP_H_ADDRESS          90
#define BACK_OFF_DISTANCE_UP_L_ADDRESS          91
#define BACK_OFF_DISTANCE_DOWN_H_ADDRESS        92
#define BACK_OFF_DISTANCE_DOWN_L_ADDRESS        93

#define EEPROMWRITE_COUNTER_H_ADDRESS          128
#define EEPROMWRITE_COUNTER_L_ADDRESS          129
#define START_COUNTER_H_ADDRESS                130
#define START_COUNTER_L_ADDRESS                131
#define TRIPPIT_H_ADDRESS                      132
#define TRIPPIT_L_ADDRESS                      133
#define TRIPPIT_SECTION_H_ADDRESS              134
#define TRIPPIT_SECTION_L_ADDRESS              135
#define OVERHEAT_COUNTER_ADDRESS               136
#define OVERLOAD_COUNTER_ADDRESS               137
#define HALL_ERROR_COUNTER_ADDRESS             138
#define MOTOR_ISP_COUNTER_H_ADDRESS            139
#define MOTOR_ISP_COUNTER_L_ADDRESS            140

/****** Master arbitration parameters *************************************************/

#define 	MASTER_DELAY_0			0U
#define 	MASTER_DELAY_1			10U
#define 	MASTER_DELAY_2			21U
#define 	MASTER_DELAY_3			32U
#define 	MASTER_DELAY_4			44U
#define 	MASTER_DELAY_5			56U
#define 	MASTER_DELAY_6			69U
#define 	MASTER_DELAY_7			82U
#define 	MASTER_DELAY_8			96U
#define 	MASTER_DELAY_9			110U
#define 	MASTER_DELAY_10			125U
#define 	MASTER_DELAY_11			140U
#define 	MASTER_DELAY_12			156U
#define 	MASTER_DELAY_13			173U
#define 	MASTER_DELAY_14			190U
#define 	MASTER_DELAY_15			208U
#define 	MASTER_DELAY_SLAVE      226U    //Set for controllers that should be forced as slaves
/************************************************** Macros ********************************************************/

///* Macros for accessing  MASTER_REQUEST parameters */
#define 	ui8_wr_MASTER_REQ_KEY(x)				MasterCMD[0] = (x)
#define 	ui8_rd_MASTER_REQ_KEY()					(MasterCMD[0])
#define 	ui8_wr_MASTER_REQ_CMD(x)				MasterCMD[1] = (x)
#define 	ui8_wr_MASTER_REQ_PAR1(x)				MasterCMD[2] = (x)
#define 	ui8_wr_MASTER_REQ_PAR2(x)				MasterCMD[3] = (x)
#define 	ui8_wr_MASTER_REQ_PAR3(x)				MasterCMD[4] = (x)
//
//
///* Macros for accessing SLAVE_RESPONSE parameters */
#define 	ui8_rd_SLAVE_RESP1()					(SlaveResp[0])
#define 	ui8_rd_SLAVE_RESP2()					(SlaveResp[1])
#define 	ui8_rd_SLAVE_RESP3()					(SlaveResp[2])
#define 	ui8_rd_SLAVE_RESP4()					(SlaveResp[3])
#define 	ui8_rd_SLAVE_RESP5()					(SlaveResp[4])
#define 	ui8_rd_SLAVE_RESP6()					(SlaveResp[5])
#define 	ui8_rd_SLAVE_RESP7()					(SlaveResp[6])
#define 	ui8_rd_SLAVE_RESP8()					(SlaveResp[7])

extern INT8U ui8old_lin_slave_state_g;
extern enum LINComFailState_enum LINComFailState;


/********************************************* enum declarations **************************************************/

enum abs_state_enum {
    ABS_IDLE,
    ABS_WAIT_FOR_RESPONSE,
    ABS_WAIT_FOR_PID_7D,
    ABS_WAIT_FOR_INTERRUPT,
    ABS_BYTE_RECEIVED

};

enum LINComFailState_enum {
    DISABLED,
    ENABLED,
    FAILED
};

typedef enum _lin_slave_state {
    idle = 0,
    break_received,
    sync_received,
    rx_data,
    rx_checksum,
    tx_data,
    tx_checksum,
} LIN_SLAVE_STATE_t;


#define SET_LINComFailState(x)						do { ui8old_lin_slave_state_g=idle;		\
																		LINComFailState=(x); }while(0);

#define TST_LINComFailState(x)						(LINComFailState==(x))
extern LIN_SLAVE_STATE_t lin_slave_state_g;
/*************************************************** flags ********************************************************/

/********************** Slave data **************************/
keyboardType slaveKeyboards[7];

/********************************************* function prototypes ************************************************/



void lin_arbitration(void);
INT8U readWriteByteFromSlave(INT8U read, INT8U adr, INT8U *dat);
void linTasks(void);
void master_function(void);
void l_set_byte_array(unsigned char* start, unsigned char count, const unsigned char* source);
void l_get_byte_array(unsigned char* start, unsigned char count, unsigned char* destination);
void LIN_Master_Initialize(uint8_t val);
void processLIN(void);
#endif

