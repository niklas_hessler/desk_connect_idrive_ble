/* ************************************************************************** */
/** Descriptive File Name

  @Company
 ROL Ergo AB

  @File Name
 BLE.c

  @Summary
 Communication with BGM300 BLE module through UART2

  
/* ************************************************************************** */
#include "BLE.h"
#include "uart2.h"
#include "string.h"
#include "accessory_process.h"
#include "accessory_commands.h"
#include "indicator.h"
#include "flashSettings.h"
#include "spi_commands.h"

void BLE_Task(void)
{
    Uart2RxTask();
    Uart2TxTask();
    
    /* Recieved data is transmitted */
    if (Uart2GetRxCount() > 4)
    {
        // Check if complete frame been recieved
        uint8_t buf[20], i, out[100];
        if (Uart2PeekRx(0) != SYNC_BYTE_1)
        {
            Uart2ReadRxData(buf, 1); // Throw away
        }
        else
        {
            if (Uart2PeekRx(1) != SYNC_BYTE_2)
            {
                Uart2ReadRxData(buf, 2); // Throw away
            }
            
            else
            {
                uint8_t len = Uart2PeekRx(2) + 3;
                if ((len < (2 + 3)) || (len > (20 + 3)))
                {
                    // Invalid length - skip package
                    Uart2ReadRxData(buf, 2);
                }
                else if (len <= Uart2GetRxCount()) 
                {
                    // Complete frame
                    Uart2ReadRxData(buf, len);
                   
                    if (validateDataFrame(&buf[2]))
                    { 
                        // New frame - add to queue
                        if (!addDataFrame(&incomingDataFrames, &buf[2]))
                        {
                            AddError(WARNING_BUFFER_FULL);
                        }
                    }
                }
            }
        }
    }
    
    // Send any outgoing frames
    if (Uart2GetTxCount() == 0)
    {
        BYTE *packet = getNextFrame(&outgoingDataFrames);
        if (packet != NULL)
        { 
            //Send outgoing frames
            uint8_t ToSendDataBuffer[25];
            ToSendDataBuffer[0] = SYNC_BYTE_1;
            ToSendDataBuffer[1] = SYNC_BYTE_2;
            memcpy(&ToSendDataBuffer[2], packet, packet[0]);
            ToSendDataBuffer[packet[0] + 2] = calculateChecksum(packet);             

            Uart2WriteTxData(ToSendDataBuffer, ToSendDataBuffer[2] + 3); 

            if (processData.connection_state != CONNECTION_OPEN)
                removeNextFrame(&outgoingDataFrames);   // USB not listening...
        }
        else if (processData.bleFlags.value)
        {
            if (processData.bleFlags.setDeviceName)
            {
                // Send devicename command to BLE module
                uint8_t ToSendDataBuffer[25];
                ToSendDataBuffer[0] = BLE_SYNCBYTE_1;     // 0x69 0x96 0x06 0x01 N A M N CS
                ToSendDataBuffer[1] = BLE_SYNCBYTE_2;
                ToSendDataBuffer[2] = strlen(deskProfile.deviceName) + 2;
                ToSendDataBuffer[3] = BLE_COMMAND_SET_DEVICENAME;
                memcpy(&ToSendDataBuffer[4], deskProfile.deviceName, strlen(deskProfile.deviceName));
                ToSendDataBuffer[ToSendDataBuffer[2] + 2] = calculateChecksum(&ToSendDataBuffer[2]);
                Uart2WriteTxData(ToSendDataBuffer, ToSendDataBuffer[2] + 3); 
                processData.bleFlags.setDeviceName=0;
            }
        }
    }
}