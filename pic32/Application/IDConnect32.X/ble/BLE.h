/* ************************************************************************** */
/** Descriptive File Name

  @Company
 ROL Ergo AB

  @File Name
 BLE.h

  @Summary
 Communication with BGM300 BLE module through UART2

  
/* ************************************************************************** */

#ifndef _BLE_H    /* Guard against multiple inclusion */
#define _BLE_H

//PIC-BLE communication protocol
// All commands originally from Accessory command passes through BLE transperantly, i.e. starting with Syncbytes 0x5a 0xa5
// All commands between PIC and BLE module starting with BLE SyncBytes
/* Frame definition:
 * Byte 1: Sync byte 1
 * Byte 2: Sync byte 2
 * Byte 3: Length for dataframe (including checksum)
 * Dataframe:
 * Byte 4: Command
 * Byte 5..n-1: Data
 * Byte n: Checksum (sum of lengthbyte and dataframe)
 * */

#define BLE_SYNCBYTE_1 0x69
#define BLE_SYNCBYTE_2 0x96

typedef enum _BLE_COMMANDS {
    //Actions
    BLE_COMMAND_SET_DEVICENAME = 0x01, //Data: String - device name [Max length 16 bytes]
    BLE_COMMAND_DISCONNECT = 0x02, //Data: No data
    BLE_COMMAND_RESET = 0xFF    //Data: No data
} BLE_COMMANDS;

void BLE_Task(void);    //Poll repetedly to drive BLE communication

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
